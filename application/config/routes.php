<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'common';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

/* CMS Route */
#Home
$route['login']                             = 'common/login';
$route['sa/dashboard']                      = 'sa/dashboard/page';
$route['r/dashboard']                       = 'r/dashboard/page';
$route['r/history']                         = 'r/dashboard/history';
$route['e/dashboard']                       = 'e/dashboard/page';
$route['e/request']                         = 'e/dashboard/request';
#User
$route['sa/building']                       = 'sa/building/page';
#Category Asset
$route['sa/category-asset']                 = 'sa/category/asset_page';
#Category Function
$route['sa/category-function']              = 'sa/category/function_page';
#Category Maintenance
$route['sa/category-maintenance']           = 'sa/category/maintenance_page';
#Category Office Stationary
$route['sa/category-office-stationary']     = 'sa/category/office_stationary_page';
#Category Meeting Consumption
$route['sa/category-meeting-consumption']   = 'sa/category/meeting_consumption_page';
#Category Additional Request
$route['sa/category-additional-request']    = 'sa/category/additional_request_page';
#Category Office Household
$route['sa/category-office-household']      = 'sa/category/office_household_page';
#Category Fuel and ECard
$route['sa/category-fuel-and-ecard']        = 'sa/category/fuel_and_ecard_page';
#Category Workstation and Furniture
$route['sa/category-workstation-and-furniture'] = 'sa/category/workstation_and_furniture_page';
#Category Consumable
$route['sa/category-consumable']            = 'sa/category/consumable_page';
#Category courier and Mailing
$route['sa/category-courier-and-mailing']   = 'sa/category/courier_and_mailing_page';
#Category Event Support
$route['sa/category-event-support']         = 'sa/category/event_support_page';
#Config
$route['sa/config']                         = 'sa/config/page';
#Driver
$route['sa/driver']                         = 'sa/driver/page';
#Meeting Room
$route['sa/meeting-room']                   = 'sa/meeting_room/page';
#User
$route['sa/user']                           = 'sa/user/page';

/* API Route */
#Building
$route['api/building']['GET']                  = 'api/building/get_building';
$route['api/building/total']['GET']            = 'api/building/get_building_total';
$route['api/building/id/(:any)']['GET']        = 'api/building/get_building_by_id/$1';
$route['api/building']['POST']                 = 'api/building/create_building';
$route['api/building']['PUT']                  = 'api/building/update_building';
$route['api/building/delete/(:any)']['DELETE'] = 'api/building/delete_building/$1';
#Common
$route['api/test']              = 'api/common/test';
$route['api/test-email/(:any)'] = 'api/common/test_email/$1';
$route['api/test-pdf/(:any)']   = 'api/common/generate_requisition_list/$1';
#Category
$route['api/category']['GET']                  = 'api/category/get_category';
$route['api/category/id/(:any)']['GET']        = 'api/category/get_category_by_id/$1';
$route['api/category']['POST']                 = 'api/category/create_category';
$route['api/category']['PUT']                  = 'api/category/update_category';
$route['api/category/delete/(:any)']['DELETE'] = 'api/category/delete_category/$1';
#Config
$route['api/config']['GET']         = 'api/config/get_config';
$route['api/config']['POST']        = 'api/config/save_config';
$route['api/config/upload']['POST'] = 'api/config/upload_config_image';
#Driver
$route['api/driver']['GET']                  = 'api/driver/get_driver';
$route['api/driver/total']['GET']            = 'api/driver/get_driver_total';
$route['api/driver/id/(:any)']['GET']        = 'api/driver/get_driver_by_id/$1';
$route['api/driver']['POST']                 = 'api/driver/create_driver';
$route['api/driver']['PUT']                  = 'api/driver/update_driver';
$route['api/driver/free/(:any)']['PUT']      = 'api/driver/free_driver/$1';
$route['api/driver/book/(:any)']['PUT']      = 'api/driver/book_driver/$1';
$route['api/driver/delete/(:any)']['DELETE'] = 'api/driver/delete_driver/$1';
#Meeting Room
$route['api/meeting-room']['GET']                  = 'api/meeting_room/get_meeting_room';
$route['api/meeting-room/total']['GET']            = 'api/meeting_room/get_meeting_room_total';
$route['api/meeting-room/id/(:any)']['GET']        = 'api/meeting_room/get_meeting_room_by_id/$1';
$route['api/meeting-room']['POST']                 = 'api/meeting_room/create_meeting_room';
$route['api/meeting-room']['PUT']                  = 'api/meeting_room/update_meeting_room';
$route['api/meeting-room/free/(:any)']['PUT']      = 'api/meeting_room/free_meeting_room/$1';
$route['api/meeting-room/book/(:any)']['PUT']      = 'api/meeting_room/book_meeting_room/$1';
$route['api/meeting-room/delete/(:any)']['DELETE'] = 'api/meeting_room/delete_meeting_room/$1';
#User
$route['api/user']['GET']                   = 'api/user/get_user';
$route['api/user/id/(:any)']['GET']         = 'api/user/get_user_by_id/$1';
$route['api/user']['POST']                  = 'api/user/create_user';
$route['api/user']['PUT']                   = 'api/user/update_user';
$route['api/user/active/(:any)']['PUT']     = 'api/user/active_user/$1';
$route['api/user/inactive/(:any)']['PUT']   = 'api/user/inactive_user/$1';
$route['api/user/delete/(:any)']['PUT']     = 'api/user/delete_user/$1';
$route['api/user/password']['PUT']          = 'api/user/change_password_user';
$route['api/user/login']['POST']            = 'api/user/login';
$route['api/user/refresh']['POST']          = 'api/user/login_user';
$route['api/user/profile']['GET']           = 'api/user/get_user_profile';
#Request
$route['api/request']['GET']                    = 'api/request/get_request';
$route['api/request/id/(:any)']['GET']          = 'api/request/get_request_by_id/$1';
$route['api/request/total']['GET']              = 'api/request/get_request_total';
$route['api/request/reject/(:any)']['PUT']      = 'api/request/reject_request/$1';
$route['api/request/confirm/(:any)']['PUT']     = 'api/request/confirm_request/$1';
$route['api/request/complete/(:any)']['PUT']    = 'api/request/complete_request/$1';
$route['api/request/accomplished/(:any)']['PUT']= 'api/request/accomplished_request/$1';
$route['api/request/upload']['POST']            = 'api/request/upload_request_file';
#Request Asset
$route['api/request/asset']['POST']                 = 'api/request_asset/create_request_data';
#Request Maintenance
$route['api/request/maintenance']['POST']           = 'api/request_maintenance/create_request_data';
#Request Meeting
$route['api/request/meeting-room']['POST']          = 'api/request_meeting_room/create_request_data';
$route['api/request/meeting-room/id/(:any)']['GET'] = 'api/request_meeting_room/get_request_data_by_id/$1';
#Request Meeting Consumption
$route['api/request/meeting-consumption']['POST']   = 'api/request_meeting_consumption/create_request_data';
#Request Office Stationery
$route['api/request/office-stationery']['POST']     = 'api/request_office_stationery/create_request_data';
#Request Operation Vehicle
$route['api/request/operation-vehicle']['POST']     = 'api/request_operation_vehicle/create_request_data';
#Request Other
$route['api/request/other']['POST']                 = 'api/request_other/create_request_data';
#Request Office Household
$route['api/request/office-household']['POST']      = 'api/request_office_household/create_request_data';
#Request Fuel and ECard
$route['api/request/fuel-and-ecard']['POST']        = 'api/request_fuel_and_ecard/create_request_data';
#Request Workstation and Furniture
$route['api/request/workstation-and-furniture']['POST'] = 'api/request_workstation_and_furniture/create_request_data';
#Request Consumable
$route['api/request/consumable']['POST'] = 'api/request_consumable/create_request_data';
#Request Courier and Mailing
$route['api/request/courier-and-mailing']['POST']   = 'api/request_courier_and_mailing/create_request_data';
#Request Event Support
$route['api/request/event-support']['POST']         = 'api/request_event_support/create_request_data';