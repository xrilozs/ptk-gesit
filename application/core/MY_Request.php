<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Request extends CI_Controller {
    public function __construct(){
        parent::__construct();
        $this->load->helper('url_helper');
    }

    function get_request_detail($id){
        // $header         = $this->input->request_headers();
        // $verify_resp    = verify_user_token($header);
        // $requestor      = $verify_resp['data']['user'];

        $request = $this->request_model->get_request_by_id($id);
        if(is_null($request)){
            $resp_obj = new Response_api();
            logging('debug', 'request is not found');
            $resp_obj->set_response(400, "failed", "request is not found");
            set_output($resp_obj->get_response());
            return null;
        }
        return $request;
    }

    function create_request($data, $request_category){
        $header         = $this->input->request_headers();
        $verify_resp    = verify_user_token($header);
        $requestor      = $verify_resp['data']['user'];

        $request_id     = get_uniq_id();
        $req = array(
            "id"                    => $request_id,
            "request_category"      => $request_category,
            "requestor_id"          => $requestor->id,
            "category_function_id"  => $data['category_function_id'],
            "request_number"        => get_request_number(),
            "notes"                 => array_key_exists("notes", $data) ? $data['notes'] : null
        );

        $req_history = array(
            "id"            => get_uniq_id(),
            "request_id"    => $request_id,
            "executor_id"   => $requestor->role == 'EXECUTOR' ? $requestor->id : null,
            "status"        => "REQUESTED"
        );

        $flag           = $this->request_model->create_request($req);
        $flag_history   = $this->request_history_model->create_request_history($req_history);

        return $flag && $flag_history ? $request_id : null;
    }

    function update_request_status($request, $status){
        $header         = $this->input->request_headers();
        $verify_resp    = verify_user_token($header);
        $requestor      = $verify_resp['data']['user'];

        $this->db->trans_begin();

        #update request status
        $flag_request = $this->request_model->update_request_status($request->id, $status);
        $flag_history = $this->request_history_model->create_request_history(array(
            "id"            => get_uniq_id(),
            "request_id"    => $request->id,
            "executor_id"   => $requestor->role == 'EXECUTOR' ? $requestor->id : null,
            "status"        => $status
        ));

        $flag = 0;
        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
            
            $err_msg = $this->db->error();
                
            logging('error', 'Update request status failed', array("error"=>$err_msg));
            $resp->set_response(500, "failed", "Update request status failed", array("error"=>$err_msg));
            set_output($resp->get_response());
        }else{
            if($flag_request && $flag_history){
                $flag = 1;
                $this->db->trans_commit();
            }else{
                $this->db->trans_rollback();

                $err_msg = $this->db->error();
                
                logging('error', 'Update request status failed', array("error"=>$err_msg));
                $resp->set_response(500, "failed", "Update request status failed", array("error"=>$err_msg));
                set_output($resp->get_response());
            }
        }

        return $flag;
    }

}
