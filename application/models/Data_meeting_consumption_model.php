<?php
  class Data_meeting_consumption_model extends CI_Model{
    public $id;
    public $request_id;
    public $data_meeting_id;
    public $category_meeting_consumption_id;
    public $qty;

    function get_data_meeting_consumption($search=null, $request_id=null, $data_meeting_id=null, $order=null, $limit=null){
        $this->db->select("c.*, m.meeting_title, cm.name as category_meeting_consumption");
        if($search){
          $where_search = "CONCAT_WS(',', m.meeting_title) LIKE '%".$search."%'";
          $this->db->where($where_search);
        }
        if($request_id){
          $this->db->where("c.request_id", $request_id);
        }
        if($data_meeting_id){
          $this->db->where("c.data_meeting_id", $data_meeting_id);
        }
        if($order){
          $this->db->order_by("c.{$order['field']}", $order['order']); 
        }
        if($limit){
          $this->db->limit($limit['size'], $limit['start']);
        }
        $this->db->from("data_meeting_consumption c");
        $this->db->join("data_meeting m", "m.id = c.data_meeting_id", "LEFT");
        $this->db->join("category_meeting_consumption cm", "cm.id = c.category_meeting_consumption_id", "LEFT");
        $query = $this->db->get();
        return $query->result();
    }
  
    function get_data_meeting_consumption_by_id($id, $is_assoc=false){
        $this->db->select("c.*, m.meeting_title, cm.name as category_meeting_consumption");
        $this->db->join("data_meeting m", "m.id = c.data_meeting_id", "LEFT");
        $this->db->join("category_meeting_consumption cm", "cm.id = c.category_meeting_consumption_id", "LEFT");
        $this->db->where("c.id", $id);
        $query = $this->db->get('data_meeting_consumption c');
        if($is_assoc){
          return $query->num_rows() ? $query->row_array() : null;
        }else{
          return $query->num_rows() ? $query->row() : null;
        }
    }
  
    function create_data_meeting_consumption($data){
        $this->id                               = $data['id'];
        $this->request_id                       = $data['request_id'];
        $this->data_meeting_id                  = $data['data_meeting_id'];
        $this->category_meeting_consumption_id  = $data['category_meeting_consumption_id'];
        $this->qty                              = $data['qty'];
        $this->created_at                       = date('Y-m-d H:i:s');
  
        $this->db->insert('data_meeting_consumption', $this);
        return $this->db->affected_rows();
    }
  }
?>
