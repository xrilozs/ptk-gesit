<?php
  class Data_additional_request_model extends CI_Model{
    public $id;
    public $data_meeting_id;
    public $category_additional_request_id;
    public $qty;

    function get_data_additional_request($search=null, $data_meeting_id=null, $category_additional_request_id=null, $order=null, $limit=null){
        $this->db->select("a.*, m.meeting_title, c.name as category_additional_request");
        if($search){
          $where_search = "CONCAT_WS(',', m.meeting_title, c.name, a.qty) LIKE '%".$search."%'";
          $this->db->where($where_search);
        }
        if($data_meeting_id){
          $this->db->where("a.data_meeting_id", $data_meeting_id);
        }
        if($category_additional_request_id){
          $this->db->where("a.category_additional_request_id", $category_additional_request_id);
        }
        if($order){
          $this->db->order_by("a.{$order['field']}", $order['order']); 
        }
        if($limit){
          $this->db->limit($limit['size'], $limit['start']);
        }
        $this->db->from("data_additional_request a");
        $this->db->join("data_meeting m", "m.id = a.data_meeting_id", "LEFT");
        $this->db->join("category_additional_request c", "c.id = a.category_additional_request_id", "LEFT");
        $query = $this->db->get();
        return $query->result();
    }
  
    function get_data_additional_request_by_id($id, $is_assoc=false){
        $this->db->select("a.*, m.meeting_title, c.name as category_additional_request");
        $this->db->join("data_meeting m", "m.id = a.data_meeting_id", "LEFT");
        $this->db->join("category_additional_request c", "c.id = a.category_additional_request_id", "LEFT");
        $this->db->where("a.id", $id);
        $query = $this->db->get('data_additional_request a');
        if($is_assoc){
          return $query->num_rows() ? $query->row_array() : null;
        }else{
          return $query->num_rows() ? $query->row() : null;
        }
    }
  
    function create_data_additional_request($data){
        $this->id                               = $data['id'];
        $this->data_meeting_id                  = $data['data_meeting_id'];
        $this->category_additional_request_id   = $data['category_additional_request_id'];
        $this->qty                              = $data['qty'];
        $this->created_at                       = date('Y-m-d H:i:s');
  
        $this->db->insert('data_additional_request', $this);
        return $this->db->affected_rows();
    }
  }
?>
