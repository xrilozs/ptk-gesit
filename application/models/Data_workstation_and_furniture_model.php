<?php
  class Data_workstation_and_furniture_model extends CI_Model{
    public $id;
    public $request_id;
    public $category_workstation_and_furniture_id;
    public $qty;

    function get_data_workstation_and_furniture($search=null, $request_id=null, $category_workstation_and_furniture_id=null, $order=null, $limit=null){
        $this->db->select("s.*, c.name as category_workstation_and_furniture");
        if($search){
          $where_search = "CONCAT_WS(',', c.name, s.qty) LIKE '%".$search."%'";
          $this->db->where($where_search);
        }
        if($request_id){
          $this->db->where("s.request_id", $request_id);
        }
        if($category_workstation_and_furniture_id){
          $this->db->where("s.category_workstation_and_furniture_id", $category_workstation_and_furniture_id);
        }
        if($order){
          $this->db->order_by("s.{$order['field']}", $order['order']); 
        }
        if($limit){
          $this->db->limit($limit['size'], $limit['start']);
        }
        $this->db->from("data_workstation_and_furniture s");
        $this->db->join("category_workstation_and_furniture c", "c.id = s.category_workstation_and_furniture_id", "LEFT");
        $query = $this->db->get();
        return $query->result();
    }
  
    function get_data_workstation_and_furniture_by_id($id, $is_assoc=false){
        $this->db->select("s.*, c.name as category_workstation_and_furniture");
        $this->db->join("category_workstation_and_furniture c", "c.id = s.category_workstation_and_furniture_id", "LEFT");
        $this->db->where("s.id", $id);
        $query = $this->db->get('data_workstation_and_furniture s');
        if($is_assoc){
          return $query->num_rows() ? $query->row_array() : null;
        }else{
          return $query->num_rows() ? $query->row() : null;
        }
    }
  
    function create_data_workstation_and_furniture($data){
        $this->id                             = $data['id'];
        $this->request_id                     = $data['request_id'];
        $this->category_workstation_and_furniture_id  = $data['category_workstation_and_furniture_id'];
        $this->qty                            = $data['qty'];
        $this->created_at                     = date('Y-m-d H:i:s');
  
        $this->db->insert('data_workstation_and_furniture', $this);
        return $this->db->affected_rows();
    }
  }
?>
