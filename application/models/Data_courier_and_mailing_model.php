<?php
  class Data_courier_and_mailing_model extends CI_Model{
    public $id;
    public $request_id;
    public $delivery_date;
    public $destination;
    public $receiver_name;
    public $receiver_phone;

    function get_data_courier_and_mailing($search=null, $request_id=null, $order=null, $limit=null){
        if($search){
          $where_search = "CONCAT_WS(',', c.name, s.qty) LIKE '%".$search."%'";
          $this->db->where($where_search);
        }
        if($request_id){
          $this->db->where("s.request_id", $request_id);
        }
        if($order){
          $this->db->order_by("s.{$order['field']}", $order['order']); 
        }
        if($limit){
          $this->db->limit($limit['size'], $limit['start']);
        }
        $this->db->from("data_courier_and_mailing s");
        $query = $this->db->get();
        return $query->result();
    }
  
    function get_data_courier_and_mailing_by_id($id, $is_assoc=false){
        $this->db->where("s.id", $id);
        $query = $this->db->get('data_courier_and_mailing s');
        if($is_assoc){
          return $query->num_rows() ? $query->row_array() : null;
        }else{
          return $query->num_rows() ? $query->row() : null;
        }
    }
  
    function create_data_courier_and_mailing($data){
        $this->id             = $data['id'];
        $this->request_id     = $data['request_id'];
        $this->delivery_date  = $data['delivery_date'];
        $this->destination    = $data['destination'];
        $this->receiver_name  = $data['receiver_name'];
        $this->receiver_phone = $data['receiver_phone'];
        $this->created_at     = date('Y-m-d H:i:s');
  
        $this->db->insert('data_courier_and_mailing', $this);
        return $this->db->affected_rows();
    }
  }
?>
