<?php
  class Data_operational_vehicle_model extends CI_Model{
    public $id;
    public $request_id;
    public $total_passenger;
    public $request_date;
    public $request_time;
    public $dest_1_mob;
    public $dest_2_mob;
    public $driver_id;
    public $acceptance_non_preferred_driver;

    function get_data_operational_vehicle($search=null, $request_id=null, $request_date=null, $order=null, $limit=null){
        $this->db->select("v.*, d.name as driver_name");
        if($search){
          $where_search = "CONCAT_WS(',', v.total_passenger, v.request_date, v.request_time, v.dest_1_mob, v.dest_2_mob, d.name) LIKE '%".$search."%'";
          $this->db->where($where_search);
        }
        if($request_id){
            $this->db->where("v.request_id", $request_id);
        }
        if($request_date){
            $this->db->where("v.request_date", $request_date);
        }
        if($order){
          $this->db->order_by("v.{$order['field']}", $order['order']); 
        }
        if($limit){
          $this->db->limit($limit['size'], $limit['start']);
        }
        $this->db->from("data_operational_vehicle v");
        $this->db->join("driver d", "d.id = v.driver_id", "LEFT");
        $query = $this->db->get();
        return $query->result();
    }
  
    function get_data_operational_vehicle_by_id($id, $is_assoc=false){
        $this->db->select("v.*, d.name as driver_name");
        $this->db->join("driver d", "d.id = v.driver_id", "LEFT");
        $this->db->where("v.id", $id);
        $query = $this->db->get('data_operational_vehicle v');
        if($is_assoc){
          return $query->num_rows() ? $query->row_array() : null;
        }else{
          return $query->num_rows() ? $query->row() : null;
        }
    }
  
    function create_data_operational_vehicle($data){
        $this->id                               = $data['id'];
        $this->request_id                       = $data['request_id'];
        $this->total_passenger                  = $data['total_passenger'];
        $this->request_date                     = $data['request_date'];
        $this->request_time                     = $data['request_time'];
        $this->dest_1_mob                       = $data['dest_1_mob'];
        $this->dest_2_mob                       = $data['dest_2_mob'];
        $this->driver_id                        = array_key_exists("driver_id", $data) ? $data['driver_id'] : null;
        $this->acceptance_non_preferred_driver  = array_key_exists("acceptance_non_preferred_driver", $data) ? $data['acceptance_non_preferred_driver'] : null;
        $this->created_at                       = date('Y-m-d H:i:s');
  
        $this->db->insert('data_operational_vehicle', $this);
        return $this->db->affected_rows();
    }
  }
?>
