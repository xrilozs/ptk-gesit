<?php
  class Data_maintenance_model extends CI_Model{
    public $id;
    public $request_id;
    public $category_maintenance_id;
    public $before_url;
    public $after_url;

    function get_data_maintenance($search=null, $request_id=null, $category_maintenance_id=null, $order=null, $limit=null){
        $this->db->select("m.*, c.name as category_maintenance");
        if($search){
          $where_search = "CONCAT_WS(',', c.name, m.qty) LIKE '%".$search."%'";
          $this->db->where($where_search);
        }
        if($request_id){
          $this->db->where("m.request_id", $request_id);
        }
        if($category_maintenance_id){
          $this->db->where("m.category_maintenance_id", $category_maintenance_id);
        }
        if($order){
          $this->db->order_by("m.{$order['field']}", $order['order']); 
        }
        if($limit){
          $this->db->limit($limit['size'], $limit['start']);
        }
        $this->db->from("data_maintenance m");
        $this->db->join("category_maintenance c", "c.id = m.category_maintenance_id", "LEFT");
        $query = $this->db->get();
        return $query->result();
    }
  
    function get_data_maintenance_by_id($id, $is_assoc=false){
        $this->db->select("m.*, c.name as category_maintenance");
        $this->db->join("category_maintenance c", "c.id = m.category_maintenance_id", "LEFT");
        $this->db->where("m.id", $id);
        $query = $this->db->get('data_maintenance m');
        if($is_assoc){
          return $query->num_rows() ? $query->row_array() : null;
        }else{
          return $query->num_rows() ? $query->row() : null;
        }
    }
  
    function create_data_maintenance($data){
        $this->id                       = $data['id'];
        $this->request_id               = $data['request_id'];
        $this->category_maintenance_id  = $data['category_maintenance_id'];
        $this->before_url               = $data['before_url'];
        $this->created_at               = date('Y-m-d H:i:m');
  
        $this->db->insert('data_maintenance', $this);
        return $this->db->affected_rows();
    }

    function update_data_maintenance($data, $id){
      $this->db->update('data_maintenance', $data, array('id' => $id));
      return $this->db->affected_rows();
    }
  }
?>
