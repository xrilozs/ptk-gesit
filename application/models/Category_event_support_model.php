<?php
  class Category_event_support_model extends CI_Model{
    public $id;
    public $name;

    function get_category_event_support($search=null, $order=null, $limit=null){
      if($search){
        $this->db->like("name", $search);
      }
      if($order){
        $this->db->order_by($order['field'], $order['order']); 
      }
      if($limit){
        $this->db->limit($limit['size'], $limit['start']);
      }
      $query = $this->db->get('category_event_support');
      return $query->result();
    }

    function count_category_event_support($search=null){
      if($search){
        $this->db->like("name", $search);
      }
      $this->db->from('category_event_support');
      return $this->db->count_all_results();
    }

    function get_category_event_support_by_id($id, $is_assoc=false){
      $this->db->where("id", $id);
      $query = $this->db->get('category_event_support');
      if($is_assoc){
        return $query->num_rows() ? $query->row_array() : null;
      }else{
        return $query->num_rows() ? $query->row() : null;
      }
    }

    function create_category_event_support($data){
      $this->id         = $data['id'];
      $this->name       = $data['name'];
      $this->created_at = date('Y-m-d H:i:s');

      $this->db->insert('category_event_support', $this);
      return $this->db->affected_rows();
    }

    function update_category_event_support($data, $id){
      $this->db->update('category_event_support', $data, array('id' => $id));
      return $this->db->affected_rows();
    }

    function delete_category_event_support($id){
      $this->db->where('id', $id);
      $this->db->delete('category_event_support');
      return $this->db->affected_rows();
    }
  }
?>
