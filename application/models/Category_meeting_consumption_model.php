<?php
  class Category_meeting_consumption_model extends CI_Model{
    public $id;
    public $name;

    function get_category_meeting_consumption($search=null, $order=null, $limit=null){
      if($search){
        $this->db->like("name", $search);
      }
      if($order){
        $this->db->order_by($order['field'], $order['order']); 
      }
      if($limit){
        $this->db->limit($limit['size'], $limit['start']);
      }
      $query = $this->db->get('category_meeting_consumption');
      return $query->result();
    }

    function count_category_meeting_consumption($search=null){
      if($search){
        $this->db->like("name", $search);
      }
      $this->db->from('category_meeting_consumption');
      return $this->db->count_all_results();
    }

    function get_category_meeting_consumption_by_id($id, $is_assoc=false){
      $this->db->where("id", $id);
      $query = $this->db->get('category_meeting_consumption');
      if($is_assoc){
        return $query->num_rows() ? $query->row_array() : null;
      }else{
        return $query->num_rows() ? $query->row() : null;
      }
    }

    function create_category_meeting_consumption($data){
      $this->id         = $data['id'];
      $this->name       = $data['name'];
      $this->created_at = date('Y-m-d H:i:s');

      $this->db->insert('category_meeting_consumption', $this);
      return $this->db->affected_rows();
    }

    function update_category_meeting_consumption($data, $id){
      $this->db->update('category_meeting_consumption', $data, array('id' => $id));
      return $this->db->affected_rows();
    }

    function delete_category_meeting_consumption($id){
      $this->db->where('id', $id);
      $this->db->delete('category_meeting_consumption');
      return $this->db->affected_rows();
    }
  }
?>
