<?php
  class Data_other_model extends CI_Model{
    public $id;
    public $request_id;

    function get_data_other($search=null, $request_id=null, $order=null, $limit=null){
        $this->db->select("o.*, r.notes");
        if($search){
          $where_search = "CONCAT_WS(',', r.notes) LIKE '%".$search."%'";
          $this->db->where($where_search);
        }
        if($request_id){
          $this->db->where("o.request_id", $request_id);
        }
        if($order){
          $this->db->order_by("o.{$order['field']}", $order['order']); 
        }
        if($limit){
          $this->db->limit($limit['size'], $limit['start']);
        }
        $this->db->from("data_other o");
        $this->db->join("requests r", "r.id = o.request_id", "LEFT");
        $query = $this->db->get();
        return $query->result();
    }
  
    function get_data_other_by_id($id, $is_assoc=false){
        $this->db->select("o.*, r.notes");
        $this->db->join("requests r", "r.id = o.request_id", "LEFT");
        $this->db->where("o.id", $id);
        $query = $this->db->get('data_other o');
        if($is_assoc){
          return $query->num_rows() ? $query->row_array() : null;
        }else{
          return $query->num_rows() ? $query->row() : null;
        }
    }
  
    function create_data_other($data){
        $this->id                   = $data['id'];
        $this->request_id           = $data['request_id'];
        $this->created_at           = date('Y-o-d H:i:o');
  
        $this->db->insert('data_other', $this);
        return $this->db->affected_rows();
    }
  }
?>
