<?php
  class Meeting_room_model extends CI_Model{
    public $id;
    public $building_id;
    public $floor_number;
    public $description;
    public $name;
    public $capacity;
    public $status;

    function get_meeting_room($search=null, $building_id=null, $status=null, $order=null, $limit=null){
      $this->db->select("m.*, b.name as building_name");
      if($search){
        $where_search = "CONCAT_WS(',', b.name, m.floor_number, m.description, m.name, m.capacity, m.status) LIKE '%".$search."%'";
        $this->db->where($where_search);
      }
      if($building_id){
        $this->db->where("m.building_id", $building_id);
      }
      if($status){
        $this->db->where("m.status", $status);
      }
      if($order){
        $this->db->order_by("m.{$order['field']}", $order['order']); 
      }
      if($limit){
        $this->db->limit($limit['size'], $limit['start']);
      }
      $this->db->from("meeting_room m");
      $this->db->join("building b", "b.id = m.building_id", "LEFT");
      $query = $this->db->get();
      return $query->result();
    }

    function count_meeting_room($search=null, $building_id=null, $status=null){
      if($search){
        $where_search = "CONCAT_WS(',', b.name, m.floor_number, m.description, m.name, m.capacity, m.status) LIKE '%".$search."%'";
        $this->db->where($where_search);
      }
      if($building_id){
        $this->db->where("m.building_id", $building_id);
      }
      if($status){
        $this->db->where("m.status", $status);
      }
      $this->db->from("meeting_room m");
      $this->db->join("building b", "b.id = m.building_id", "LEFT");
      return $this->db->count_all_results();
    }

    function get_meeting_room_by_id($id, $is_assoc=false){
      $this->db->where("id", $id);
      $query = $this->db->get('meeting_room');
      if($is_assoc){
        return $query->num_rows() ? $query->row_array() : null;
      }else{
        return $query->num_rows() ? $query->row() : null;
      }
    }

    function create_meeting_room($data){
      $this->id           = $data['id'];
      $this->building_id  = $data['building_id'];
      $this->capacity     = array_key_exists("capacity", $data) ? $data['capacity'] : null;
      $this->floor_number = $data['floor_number'];
      $this->name         = $data['name'];
      $this->description  = $data['description'];
      $this->status       = "AVAILABLE";
      $this->created_at   = date('Y-m-d H:i:s');

      $this->db->insert('meeting_room', $this);
      return $this->db->affected_rows();
    }

    function update_meeting_room($data, $id){
      $this->db->update('meeting_room', $data, array('id' => $id));
      return $this->db->affected_rows();
    }

    function delete_meeting_room($id){
      $this->db->where('id', $id);
      $this->db->delete('meeting_room');
      return $this->db->affected_rows();
    }

    function free_meeting_room($id){
      $data = array(
        'status' => "AVAILABLE"
      );
      $this->db->update('meeting_room', $data, array('id' => $id));
      return $this->db->affected_rows();
    }

    function book_meeting_room($id){
      $data = array(
        'status' => "BOOKED"
      );
      $this->db->update('meeting_room', $data, array('id' => $id));
      return $this->db->affected_rows();
    }
  }
?>
