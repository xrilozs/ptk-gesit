<?php
  class Building_model extends CI_Model{
    public $id;
    public $name;

    function get_building($search=null, $order=null, $limit=null){
      if($search){
        $this->db->like("name", $search);
      }
      if($order){
        $this->db->order_by($order['field'], $order['order']); 
      }
      if($limit){
        $this->db->limit($limit['size'], $limit['start']);
      }
      $query = $this->db->get('building');
      return $query->result();
    }

    function count_building($search=null){
      if($search){
        $this->db->like("name", $search);
      }
      $this->db->from('building');
      return $this->db->count_all_results();
    }

    function get_building_by_id($id, $is_assoc=false){
      $this->db->where("id", $id);
      $query = $this->db->get('building');
      if($is_assoc){
        return $query->num_rows() ? $query->row_array() : null;
      }else{
        return $query->num_rows() ? $query->row() : null;
      }
    }

    function create_building($data){
      $this->id         = $data['id'];
      $this->name       = $data['name'];
      $this->created_at = date('Y-m-d H:i:s');

      $this->db->insert('building', $this);
      return $this->db->affected_rows();
    }

    function update_building($data, $id){
      $this->db->update('building', $data, array('id' => $id));
      return $this->db->affected_rows();
    }

    function delete_building($id){
      $this->db->where('id', $id);
      $this->db->delete('building');
      return $this->db->affected_rows();
    }
  }
?>
