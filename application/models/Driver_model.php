<?php
  class Driver_model extends CI_Model{
    public $id;
    public $name;
    public $status;

    function get_driver($search=null, $status=null, $order=null, $limit=null){
      if($search){
        $this->db->like("name", $search);
      }
      if($status){
        $this->db->where("status", $status);
      }
      if($order){
        $this->db->order_by($order['field'], $order['order']); 
      }
      if($limit){
        $this->db->limit($limit['size'], $limit['start']);
      }
      $query = $this->db->get('driver');
      return $query->result();
    }

    function count_driver($search=null, $status=null){
      if($search){
        $this->db->like("name", $search);
      }
      if($status){
        $this->db->where("status", $status);
      }
      $this->db->from('driver');
      return $this->db->count_all_results();
    }

    function get_driver_by_id($id, $is_assoc=false){
      $this->db->where("id", $id);
      $query = $this->db->get('driver');
      if($is_assoc){
        return $query->num_rows() ? $query->row_array() : null;
      }else{
        return $query->num_rows() ? $query->row() : null;
      }
    }

    function create_driver($data){
      $this->id         = $data['id'];
      $this->name       = $data['name'];
      $this->status     = "AVAILABLE";
      $this->created_at = date('Y-m-d H:i:s');

      $this->db->insert('driver', $this);
      return $this->db->affected_rows();
    }

    function update_driver($data, $id){
      $this->db->update('driver', $data, array('id' => $id));
      return $this->db->affected_rows();
    }

    function delete_driver($id){
      $this->db->where('id', $id);
      $this->db->delete('driver');
      return $this->db->affected_rows();
    }

    function free_driver($id){
      $data = array(
        'status' => "AVAILABLE"
      );
      $this->db->update('driver', $data, array('id' => $id));
      return $this->db->affected_rows();
    }

    function book_driver($id){
      $data = array(
        'status' => "BOOKED"
      );
      $this->db->update('driver', $data, array('id' => $id));
      return $this->db->affected_rows();
    }
  }
?>
