<?php
  class Data_fuel_and_ecard_model extends CI_Model{
    public $id;
    public $request_id;
    public $category_fuel_and_ecard_id;

    function get_data_fuel_and_ecard($search=null, $request_id=null, $category_fuel_and_ecard_id=null, $order=null, $limit=null){
        $this->db->select("s.*, c.name as category_fuel_and_ecard");
        if($search){
          $where_search = "CONCAT_WS(',', c.name) LIKE '%".$search."%'";
          $this->db->where($where_search);
        }
        if($request_id){
          $this->db->where("s.request_id", $request_id);
        }
        if($category_fuel_and_ecard_id){
          $this->db->where("s.category_fuel_and_ecard_id", $category_fuel_and_ecard_id);
        }
        if($order){
          $this->db->order_by("s.{$order['field']}", $order['order']); 
        }
        if($limit){
          $this->db->limit($limit['size'], $limit['start']);
        }
        $this->db->from("data_fuel_and_ecard s");
        $this->db->join("category_fuel_and_ecard c", "c.id = s.category_fuel_and_ecard_id", "LEFT");
        $query = $this->db->get();
        return $query->result();
    }
  
    function get_data_fuel_and_ecard_by_id($id, $is_assoc=false){
        $this->db->select("s.*, c.name as category_fuel_and_ecard");
        $this->db->join("category_fuel_and_ecard c", "c.id = s.category_fuel_and_ecard_id", "LEFT");
        $this->db->where("s.id", $id);
        $query = $this->db->get('data_fuel_and_ecard s');
        if($is_assoc){
          return $query->num_rows() ? $query->row_array() : null;
        }else{
          return $query->num_rows() ? $query->row() : null;
        }
    }
  
    function create_data_fuel_and_ecard($data){
        $this->id                             = $data['id'];
        $this->request_id                     = $data['request_id'];
        $this->category_fuel_and_ecard_id     = $data['category_fuel_and_ecard_id'];
        $this->created_at                     = date('Y-m-d H:i:s');
  
        $this->db->insert('data_fuel_and_ecard', $this);
        return $this->db->affected_rows();
    }
  }
?>
