<?php
  class User_model extends CI_Model {
    public $id;
    public $fullname;
    public $username;
    public $email;
    public $password;
    public $role;
    public $status;

    function get_user($search=null, $role=null, $status=null, $order=null, $limit=null){
      if($search){
        $where_search = "CONCAT_WS(',', username, email, fullname, role, status) LIKE '%".$search."%'";
        $this->db->where($where_search);
      }
      if($role){
        $this->db->where("role", $role);
      }
      $this->db->where("status !=", "DELETED");
      if($status){
        $this->db->where("status", $status);
      }
      $this->db->from('user');
      if($order){
        $this->db->order_by($order['field'], $order['order']); 
      }
      if($limit){
        $this->db->limit($limit['size'], $limit['start']);
      }
      $query = $this->db->get();
      return $query->result();
    }

    function count_user($search=null, $role=null, $status=null){
      $this->db->from('user');
      if($search){
        $where_search = "CONCAT_WS(',', username, email, fullname, role, status) LIKE '%".$search."%'";
        $this->db->where($where_search);
      }
      if($role){
        $this->db->where("role", $role);
      }
      $this->db->where("status !=", "DELETED");
      if($status){
        $this->db->where("status", $status);
      }
      return $this->db->count_all_results();
    }

    function get_user_by_username($username, $is_assoc=false){
      $this->db->where("username", $username);
      $this->db->where("status !=", "DELETED");
      $query = $this->db->get('user');
      if($is_assoc){
        return $query->num_rows() ? $query->row_array() : null;
      }else{
        return $query->num_rows() ? $query->row() : null;
      }
    }

    function get_user_by_id($id, $is_assoc=false){
      $this->db->where("id", $id);
      $this->db->where("status !=", "DELETED");
      $this->db->from('user');
      $query = $this->db->get();
      if($is_assoc){
        return $query->num_rows() ? $query->row_array() : null;
      }else{
        return $query->num_rows() ? $query->row() : null;
      }
    }

    function create_user($data){
      $this->id         = $data['id'];
      $this->username   = $data['username'];
      $this->email      = $data['email'];
      $this->password   = $data['password'];
      $this->fullname   = $data['fullname'];
      $this->role       = $data['role'];
      $this->status     = $data['status'];
      $this->created_at = date("Y-m-d H:i:s");
      $this->db->insert('user', $this);
      return $this->db->affected_rows() > 0;
    }

    function update_user($data, $id){
      $this->db->update('user', $data, array("id" => $id));
      return $this->db->affected_rows();
    }

    function inactive_user($id){
      $data = array(
        'status' => "INACTIVE"
      );
      $this->db->update('user', $data, array('id' => $id));
      return $this->db->affected_rows();
    }

    function active_user($id){
      $data = array(
        'status' => "ACTIVE"
      );
      $this->db->update('user', $data, array('id' => $id));
      return $this->db->affected_rows();
    }

    function delete_user($id){
      $data = array(
        'status' => "DELETED"
      );
      $this->db->update('user', $data, array('id' => $id));
      return $this->db->affected_rows();
    }
    
    function change_password_user($password, $id){
      $data = array(
        'password' => $password
      );
      $this->db->update('user', $data, array('id' => $id));
      return $this->db->affected_rows();
    }
  }
?>
