<?php
  class Request_model extends CI_Model{
    public $id;
    public $request_category;
    public $requestor_id;
    public $category_function_id;
    public $status;
    public $request_number;
    public $notes;
    public $requisition_url;
    public $reject_reason;

    function get_request($search=null, $request_category=null, $request_date=null, $requestor_id=null, $category_function_id=null, $status=null, $order=null, $limit=null){
      if($request_category == "MEETING ROOM"){
        $this->db->select("r.*, u.fullname as requestor_name, c.name as category_function_name, m.meeting_title, m.id as meeting_id");
        $this->db->join("data_meeting m", "m.request_id = r.id", "LEFT");
      }else{
        $this->db->select("r.*, u.fullname as requestor_name, c.name as category_function_name");
      }

      if($search){
        $where_search = "CONCAT_WS(',', r.status, r.request_number, r.notes, r.request_category, r.reject_reason, u.fullname, c.name) LIKE '%".$search."%'";
        $this->db->where($where_search);
      }
      if($request_category){
        $this->db->where("r.request_category", $request_category);
      }
      if($request_date){
        $this->db->where("DATE(r.created_at)", $request_date);
      }
      if($requestor_id){
        $this->db->where("r.requestor_id", $requestor_id);
      }
      if($category_function_id){
        $this->db->where("r.category_function_id", $category_function_id);
      }
      if($status){
        $this->db->where("r.status", $status);
      }
      if($order){
        $this->db->order_by("r.{$order['field']}", $order['order']); 
      }
      if($limit){
        $this->db->limit($limit['size'], $limit['start']);
      }
      $this->db->from("requests r");
      $this->db->join("user u", "u.id = r.requestor_id", "LEFT");
      $this->db->join("category_function c", "c.id = r.category_function_id", "LEFT");
      $query = $this->db->get();
      return $query->result();
    }

    function count_request($search=null, $request_category=null, $request_date=null, $requestor_id=null, $category_function_id=null, $status=null){
      if($search){
        $where_search = "CONCAT_WS(',', r.status, r.request_number, r.notes, r.request_category, r.reject_reason, u.fullname, c.name) LIKE '%".$search."%'";
        $this->db->where($where_search);
      }
      if($request_category){
        $this->db->where("r.request_category", $request_category);
      }
      if($request_date){
        $this->db->where("DATE(r.created_at)", $request_date);
      }
      if($requestor_id){
        $this->db->where("r.requestor_id", $requestor_id);
      }
      if($category_function_id){
        $this->db->where("r.category_function_id", $category_function_id);
      }
      if($status){
        $this->db->where("r.status", $status);
      }
      $this->db->join("user u", "u.id = r.requestor_id", "LEFT");
      $this->db->join("category_function c", "c.id = r.category_function_id", "LEFT");
      $this->db->from('requests r');
      return $this->db->count_all_results();
    }

    function get_request_by_id($id, $is_assoc=false){
      $this->db->select("r.*, u.fullname as requestor_name, c.name as category_function_name");
      $this->db->where("r.id", $id);
      $this->db->join("category_function c", "c.id = r.category_function_id", "LEFT");
      $this->db->join("user u", "u.id = r.requestor_id", "LEFT");
      $query = $this->db->get('requests r');
      if($is_assoc){
        return $query->num_rows() ? $query->row_array() : null;
      }else{
        return $query->num_rows() ? $query->row() : null;
      }
    }

    function create_request($data){
      $this->id                   = $data['id'];
      $this->request_category     = $data['request_category'];
      $this->requestor_id         = $data['requestor_id'];
      $this->category_function_id = $data['category_function_id'];
      $this->status               = "REQUESTED";
      $this->request_number       = $data['request_number'];
      $this->notes                = array_key_exists("notes", $data) ? $data['notes'] : null;
      $this->created_at           = date('Y-m-d H:i:s');

      $this->db->insert('requests', $this);
      return $this->db->affected_rows();
    }

    function update_request($data, $id){
      $this->db->update('requests', $data, array('id' => $id));
      return $this->db->affected_rows();
    }

    function update_request_status($id, $status){
      $data = array(
        "status" => $status
      );
      $this->db->update('requests', $data, array('id' => $id));
      return $this->db->affected_rows();
    }

    function delete_request($id){
      $this->db->where('id', $id);
      $this->db->delete('requests');
      return $this->db->affected_rows();
    }
  }
?>
