<?php
  class Data_event_support_model extends CI_Model{
    public $id;
    public $request_id;
    public $event_name;
    public $event_date;
    public $start_time;
    public $end_time;
    public $is_outside_office;
    public $location;
    public $use_eo;

    function get_data_event_support($search=null, $request_id=null, $order=null, $limit=null){
        if($search){
          $where_search = "CONCAT_WS(',', request_id, event_name, event_date, start_time, end_time, location) LIKE '%".$search."%'";
          $this->db->where($where_search);
        }
        if($request_id){
          $this->db->where("request_id", $request_id);
        }
        if($order){
          $this->db->order_by($order['field'], $order['order']); 
        }
        if($limit){
          $this->db->limit($limit['size'], $limit['start']);
        }
        $this->db->from("data_event_support");
        $query = $this->db->get();
        return $query->result();
    }
  
    function get_data_event_support_by_id($id, $is_assoc=false){
        $this->db->where("id", $id);
        $query = $this->db->get('data_event_support');
        if($is_assoc){
          return $query->num_rows() ? $query->row_array() : null;
        }else{
          return $query->num_rows() ? $query->row() : null;
        }
    }
  
    function create_data_event_support($data){
        $this->id                 = $data['id'];
        $this->request_id         = $data['request_id'];
        $this->event_name         = $data['event_name'];;
        $this->event_date         = $data['event_date'];;
        $this->start_time         = $data['start_time'];;
        $this->end_time           = $data['end_time'];;
        $this->is_outside_office  = $data['is_outside_office'];;
        $this->location           = $data['location'];;
        $this->use_eo             = $data['use_eo'];;
        $this->created_at         = date('Y-m-d H:i:s');
  
        $this->db->insert('data_event_support', $this);
        return $this->db->affected_rows();
    }
  }
?>
