<?php
  class Data_event_support_items_model extends CI_Model{
    public $id;
    public $data_event_support_id;
    public $category_event_support_id;
    public $qty;

    function get_data_event_support_items($search=null, $data_event_support_id=null, $category_event_support_id=null, $order=null, $limit=null){
        $this->db->select("s.*, c.name as category_event_support");
        if($search){
          $where_search = "CONCAT_WS(',', c.name, s.qty) LIKE '%".$search."%'";
          $this->db->where($where_search);
        }
        if($data_event_support_id){
          $this->db->where("s.data_event_support_id", $data_event_support_id);
        }
        if($category_event_support_id){
          $this->db->where("s.category_event_support_id", $category_event_support_id);
        }
        if($order){
          $this->db->order_by("s.{$order['field']}", $order['order']); 
        }
        if($limit){
          $this->db->limit($limit['size'], $limit['start']);
        }
        $this->db->from("data_event_support_items s");
        $this->db->join("category_event_support c", "c.id = s.category_event_support_id", "LEFT");
        $query = $this->db->get();
        return $query->result();
    }
  
    function get_data_event_support_items_by_id($id, $is_assoc=false){
        $this->db->select("s.*, c.name as category_event_support");
        $this->db->join("category_event_support c", "c.id = s.category_event_support_id", "LEFT");
        $this->db->where("s.id", $id);
        $query = $this->db->get('data_event_support_items s');
        if($is_assoc){
          return $query->num_rows() ? $query->row_array() : null;
        }else{
          return $query->num_rows() ? $query->row() : null;
        }
    }
  
    function create_data_event_support_items($data){
        $this->id                             = $data['id'];
        $this->data_event_support_id          = $data['data_event_support_id'];
        $this->category_event_support_id      = $data['category_event_support_id'];
        $this->qty                            = $data['qty'];
        $this->created_at                     = date('Y-m-d H:i:s');
  
        $this->db->insert('data_event_support_items', $this);
        return $this->db->affected_rows();
    }
  }
?>
