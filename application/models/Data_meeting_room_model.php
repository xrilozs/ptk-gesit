<?php
  class Data_meeting_room_model extends CI_Model{
    public $id;
    public $request_id;
    public $building_id;
    public $meeting_room_id;
    public $requestor_name;
    public $requestor_email;
    public $meeting_type;
    public $meeting_title;
    public $total_participant;
    public $meeting_date;
    public $start_time;
    public $end_time;
    public $meeting_invitation_url;
    public $attachment;

    function get_data_meeting_room($search=null, $request_id=null, $building_id=null, $meeting_room_id=null, $meeting_date=null, $order=null, $limit=null){
        $this->db->select("m.*, b.name as building, r.name as meeting_room");
        if($search){
          $where_search = "CONCAT_WS(',', r.name, m.requestor_name, m.requestor_email, m.meeting_type, m.meeting_title, m.total_participant, m.meeting_date, m.start_time, m.end_time) LIKE '%".$search."%'";
          $this->db->where($where_search);
        }
        if($request_id){
          $this->db->where("m.request_id", $request_id);
        }
        if($building_id){
          $this->db->where("m.building_id", $building_id);
        }
        if($meeting_room_id){
          $this->db->where("m.meeting_room_id", $meeting_room_id);
        }
        if($meeting_date){
            $this->db->where("m.meeting_date", $meeting_date);
        }
        if($order){
          $this->db->order_by("m.{$order['field']}", $order['order']); 
        }
        if($limit){
          $this->db->limit($limit['size'], $limit['start']);
        }
        $this->db->from("data_meeting m");
        $this->db->join("meeting_room r", "r.id = m.meeting_room_id", "LEFT");
        $this->db->join("building b", "b.id = m.building_id", "LEFT");
        $query = $this->db->get();
        return $query->result();
    }
  
    function get_data_meeting_room_by_id($id, $is_assoc=false){
        $this->db->select("m.*, b.name as building, r.name as meeting_room, req.status as meeting_status");
        $this->db->join("meeting_room r", "r.id = m.meeting_room_id", "LEFT");
        $this->db->join("building b", "b.id = m.building_id", "LEFT");
        $this->db->join("requests req", "req.id = m.request_id", "LEFT");
        $this->db->where("m.id", $id);
        $query = $this->db->get('data_meeting m');
        if($is_assoc){
          return $query->num_rows() ? $query->row_array() : null;
        }else{
          return $query->num_rows() ? $query->row() : null;
        }
    }
  
    function create_data_meeting_room($data){
        $this->id                   = $data['id'];
        $this->request_id           = $data['request_id'];
        $this->meeting_room_id      = $data['meeting_room_id'];
        $this->building_id          = $data['building_id'];
        $this->requestor_name       = $data['requestor_name'];
        $this->requestor_email      = $data['requestor_email'];
        $this->meeting_type         = $data['meeting_type'];
        $this->meeting_title        = $data['meeting_title'];
        $this->total_participant    = $data['total_participant'];
        $this->meeting_date         = $data['meeting_date'];
        $this->start_time           = $data['start_time'];
        $this->end_time             = $data['end_time'];
        $this->meeting_invitation_url= $data['meeting_invitation_url'];
        $this->attachment           = array_key_exists("attachment", $data) ? $data['attachment'] : null;
        $this->created_at           = date('Y-m-d H:i:s');
  
        $this->db->insert('data_meeting', $this);
        return $this->db->affected_rows();
    }

    function update_data_meeting_room($data, $id){
      $this->db->update('data_meeting', $data, array('id' => $id));
      return $this->db->affected_rows();
    }
  }
?>
