<?php
  class Category_office_stationery_model extends CI_Model{
    public $id;
    public $name;

    function get_category_office_stationery($search=null, $order=null, $limit=null){
      if($search){
        $this->db->like("name", $search);
      }
      if($order){
        $this->db->order_by($order['field'], $order['order']); 
      }
      if($limit){
        $this->db->limit($limit['size'], $limit['start']);
      }
      $query = $this->db->get('category_office_stationery');
      return $query->result();
    }

    function count_category_office_stationery($search=null){
      if($search){
        $this->db->like("name", $search);
      }
      $this->db->from('category_office_stationery');
      return $this->db->count_all_results();
    }

    function get_category_office_stationery_by_id($id, $is_assoc=false){
      $this->db->where("id", $id);
      $query = $this->db->get('category_office_stationery');
      if($is_assoc){
        return $query->num_rows() ? $query->row_array() : null;
      }else{
        return $query->num_rows() ? $query->row() : null;
      }
    }

    function create_category_office_stationery($data){
      $this->id         = $data['id'];
      $this->name       = $data['name'];
      $this->created_at = date('Y-m-d H:i:s');

      $this->db->insert('category_office_stationery', $this);
      return $this->db->affected_rows();
    }

    function update_category_office_stationery($data, $id){
      $this->db->update('category_office_stationery', $data, array('id' => $id));
      return $this->db->affected_rows();
    }

    function delete_category_office_stationery($id){
      $this->db->where('id', $id);
      $this->db->delete('category_office_stationery');
      return $this->db->affected_rows();
    }
  }
?>
