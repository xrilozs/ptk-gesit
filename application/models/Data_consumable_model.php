<?php
  class Data_consumable_model extends CI_Model{
    public $id;
    public $request_id;
    public $category_consumable_id;
    public $qty;

    function get_data_consumable($search=null, $request_id=null, $category_consumable_id=null, $order=null, $limit=null){
        $this->db->select("s.*, c.name as category_consumable");
        if($search){
          $where_search = "CONCAT_WS(',', c.name, s.qty) LIKE '%".$search."%'";
          $this->db->where($where_search);
        }
        if($request_id){
          $this->db->where("s.request_id", $request_id);
        }
        if($category_consumable_id){
          $this->db->where("s.category_consumable_id", $category_consumable_id);
        }
        if($order){
          $this->db->order_by("s.{$order['field']}", $order['order']); 
        }
        if($limit){
          $this->db->limit($limit['size'], $limit['start']);
        }
        $this->db->from("data_consumable s");
        $this->db->join("category_consumable c", "c.id = s.category_consumable_id", "LEFT");
        $query = $this->db->get();
        return $query->result();
    }
  
    function get_data_consumable_by_id($id, $is_assoc=false){
        $this->db->select("s.*, c.name as category_consumable");
        $this->db->join("category_consumable c", "c.id = s.category_consumable_id", "LEFT");
        $this->db->where("s.id", $id);
        $query = $this->db->get('data_consumable s');
        if($is_assoc){
          return $query->num_rows() ? $query->row_array() : null;
        }else{
          return $query->num_rows() ? $query->row() : null;
        }
    }
  
    function create_data_consumable($data){
        $this->id                             = $data['id'];
        $this->request_id                     = $data['request_id'];
        $this->category_consumable_id  = $data['category_consumable_id'];
        $this->qty                            = $data['qty'];
        $this->created_at                     = date('Y-m-d H:i:s');
  
        $this->db->insert('data_consumable', $this);
        return $this->db->affected_rows();
    }
  }
?>
