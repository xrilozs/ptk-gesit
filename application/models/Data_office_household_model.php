<?php
  class Data_office_household_model extends CI_Model{
    public $id;
    public $request_id;
    public $category_office_household_id;
    public $qty;

    function get_data_office_household($search=null, $request_id=null, $category_office_household_id=null, $order=null, $limit=null){
        $this->db->select("s.*, c.name as category_office_household");
        if($search){
          $where_search = "CONCAT_WS(',', c.name, s.qty) LIKE '%".$search."%'";
          $this->db->where($where_search);
        }
        if($request_id){
          $this->db->where("s.request_id", $request_id);
        }
        if($category_office_household_id){
          $this->db->where("s.category_office_household_id", $category_office_household_id);
        }
        if($order){
          $this->db->order_by("s.{$order['field']}", $order['order']); 
        }
        if($limit){
          $this->db->limit($limit['size'], $limit['start']);
        }
        $this->db->from("data_office_household s");
        $this->db->join("category_office_household c", "c.id = s.category_office_household_id", "LEFT");
        $query = $this->db->get();
        return $query->result();
    }
  
    function get_data_office_household_by_id($id, $is_assoc=false){
        $this->db->select("s.*, c.name as category_office_household");
        $this->db->join("category_office_household c", "c.id = s.category_office_household_id", "LEFT");
        $this->db->where("s.id", $id);
        $query = $this->db->get('data_office_household s');
        if($is_assoc){
          return $query->num_rows() ? $query->row_array() : null;
        }else{
          return $query->num_rows() ? $query->row() : null;
        }
    }
  
    function create_data_office_household($data){
        $this->id                             = $data['id'];
        $this->request_id                     = $data['request_id'];
        $this->category_office_household_id  = $data['category_office_household_id'];
        $this->qty                            = $data['qty'];
        $this->created_at                     = date('Y-m-d H:i:s');
  
        $this->db->insert('data_office_household', $this);
        return $this->db->affected_rows();
    }
  }
?>
