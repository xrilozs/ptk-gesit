<?php
  class Data_asset_model extends CI_Model{
    public $id;
    public $request_id;
    public $category_asset_id;

    function get_data_asset($search=null, $request_id=null, $category_asset_id=null, $order=null, $limit=null){
        $this->db->select("a.*, c.name as category_asset");
        if($search){
          $where_search = "CONCAT_WS(',', c.name) LIKE '%".$search."%'";
          $this->db->where($where_search);
        }
        if($request_id){
          $this->db->where("a.request_id", $request_id);
        }
        if($category_asset_id){
          $this->db->where("a.category_asset_id", $category_asset_id);
        }
        if($order){
          $this->db->order_by("a.{$order['field']}", $order['order']); 
        }
        if($limit){
          $this->db->limit($limit['size'], $limit['start']);
        }
        $this->db->from("data_asset a");
        $this->db->join("category_asset c", "c.id = a.category_asset_id", "LEFT");
        $query = $this->db->get();
        return $query->result();
    }
  
    function get_data_asset_by_id($id, $is_assoc=false){
        $this->db->select("a.*, c.name as category_asset");
        $this->db->join("category_asset c", "c.id = a.category_asset_id", "LEFT");
        $this->db->where("a.id", $id);
        $query = $this->db->get('data_asset a');
        if($is_assoc){
          return $query->num_rows() ? $query->row_array() : null;
        }else{
          return $query->num_rows() ? $query->row() : null;
        }
    }
  
    function create_data_asset($data){
        $this->id                   = $data['id'];
        $this->request_id           = $data['request_id'];
        $this->category_asset_id    = $data['category_asset_id'];
        $this->created_at           = date('Y-m-d H:i:a');
  
        $this->db->insert('data_asset', $this);
        return $this->db->affected_rows();
    }
  }
?>
