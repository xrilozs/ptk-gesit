<?php
  class Request_history_model extends CI_Model{
    public $id;
    public $request_id;
    public $executor_id;
    public $status;

    function get_request_history_by_executor_id($executor_id, $order=null, $limit=null){
      $this->db->select("h.*, u.fullname as executor_name, r.request_number");
      $this->db->where("h.executor_id", $executor_id);
      if($order){
        $this->db->order_by("h.{$order['field']}", $order['order']); 
      }
      if($limit){
        $this->db->limit($limit['size'], $limit['start']);
      }
      $this->db->from("request_history h");
      $this->db->join("user u", "u.id = h.executor_id", "LEFT");
      $this->db->join("requests r", "r.id = h.request_id", "LEFT");
      $query = $this->db->get();
      return $query->result();
    }

    function get_request_history_by_request_id($request_id){
      $this->db->select("h.*, u.fullname as executor_name, r.request_number");
      $this->db->where("h.request_id", $request_id);
      $this->db->from("request_history h");
      $this->db->join("user u", "u.id = h.executor_id", "LEFT");
      $this->db->join("requests r", "r.id = h.request_id", "LEFT");
      $query = $this->db->get();
      return $query->result();
    }

    function get_request_history_by_id($id, $is_assoc=false){
      $this->db->where("h.*, u.fullname as executor_name");
      $this->db->where("h.id", $id);
      $this->db->join("user u", "u.id = h.executor_id", "LEFT");
      $query = $this->db->get('request_history h');
      if($is_assoc){
        return $query->num_rows() ? $query->row_array() : null;
      }else{
        return $query->num_rows() ? $query->row() : null;
      }
    }

    function create_request_history($data){
      $this->id           = $data['id'];
      $this->request_id   = $data['request_id'];
      $this->executor_id  = array_key_exists("executor_id", $data) ? $data['executor_id'] : null;
      $this->status       = $data['status'];
      $this->created_at   = date('Y-m-d H:i:s');

      $this->db->insert('request_history', $this);
      return $this->db->affected_rows();
    }

    function update_request_history($data, $id){
      $this->db->update('request_history', $data, array('id' => $id));
      return $this->db->affected_rows();
    }

    function delete_request_history($id){
      $this->db->where('id', $id);
      $this->db->delete('request_history');
      return $this->db->affected_rows();
    }
  }
?>
