<?php
  class Category_fuel_and_ecard_model extends CI_Model{
    public $id;
    public $name;

    function get_category_fuel_and_ecard($search=null, $order=null, $limit=null){
      if($search){
        $this->db->like("name", $search);
      }
      if($order){
        $this->db->order_by($order['field'], $order['order']); 
      }
      if($limit){
        $this->db->limit($limit['size'], $limit['start']);
      }
      $query = $this->db->get('category_fuel_and_ecard');
      return $query->result();
    }

    function count_category_fuel_and_ecard($search=null){
      if($search){
        $this->db->like("name", $search);
      }
      $this->db->from('category_fuel_and_ecard');
      return $this->db->count_all_results();
    }

    function get_category_fuel_and_ecard_by_id($id, $is_assoc=false){
      $this->db->where("id", $id);
      $query = $this->db->get('category_fuel_and_ecard');
      if($is_assoc){
        return $query->num_rows() ? $query->row_array() : null;
      }else{
        return $query->num_rows() ? $query->row() : null;
      }
    }

    function create_category_fuel_and_ecard($data){
      $this->id         = $data['id'];
      $this->name       = $data['name'];
      $this->created_at = date('Y-m-d H:i:s');

      $this->db->insert('category_fuel_and_ecard', $this);
      return $this->db->affected_rows();
    }

    function update_category_fuel_and_ecard($data, $id){
      $this->db->update('category_fuel_and_ecard', $data, array('id' => $id));
      return $this->db->affected_rows();
    }

    function delete_category_fuel_and_ecard($id){
      $this->db->where('id', $id);
      $this->db->delete('category_fuel_and_ecard');
      return $this->db->affected_rows();
    }
  }
?>
