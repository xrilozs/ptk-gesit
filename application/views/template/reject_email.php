<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <style>
    	body{
    	    font-family: Helvetica, sans-serif;
    	    height: 100%;
            margin: 0px;
        }
        img.center {
            display: block;
            margin: 0 auto;
        }
        #container {
            width:100%; 
            height:100vh;
    	    background-color: #025fa4;
        }
        table, th, td {
            border: 2px solid #025fa4;
            border-collapse: collapse;
        }
        .blank-side {
            width:15%
        }
        .body {
            width:70%;
            background-color:white;
            /* margin: 20px;*/
            padding:10px;
            /* border-radius: 10px; */
        }
        #detail{
            text-align: center;
        }
        #header {
            text-align: center;
            /* margin-top: 20px;
            margin-bottom: 20px; */
        }
        #logo {
            width: 100px;
        }
        #google-play{
            width: 100px;
        }
        #content {
            /* text-align: center; */
            font-size: 20px;
        }
        #footer {
            /* padding-top: 20px; */
            text-align: center;
            font-family: "Open Sans", sans-serif;
            font-size: 12px;
        }
        #footer-title {
            font-weight: bold;
            color: #ec000e;
            font-size: 14px;
        }
    </style>
    </head>
    <body>
    <table id="container">
      <tr>
        <td class="blank-side"></td>
        <td>
            <table width="100%">
      		    <tr><td></td></tr>
                <tr>
                    <td class="body">
                        <div>
                            <div id="header">
                                <h1>Request Ditolak</h1>
                            </div>
			            </div>
                    </td>
                </tr>
      		    <tr>
      		        <td class="body">
      		    	    <div>
                            <div id="content">
                                <p align="center">Request Anda ditolak, berikut detailnya:</p>
                                <p>
                                    <b>Nomer Request: </b><?=$request->request_number;?><br>
                                    <b>Kategori: </b><?=$request->request_category;?><br>
                                </p>
                            </div>
			            </div>

                        <div id="table" style="margin-bottom:20px;">
                            <table style="width:100%" id="table-list-item">
                                <tr>
                                    <?php foreach ($header['title'] as $table_header): ?>
                                    <th class="column-list-item"><?=$table_header;?></th>
                                    <?php endforeach; ?>
                                </tr>
                                <?php foreach ($request->data as $data): ?>
                                <tr>
                                    <?php foreach ($header['key'] as $key): ?>
                                        <?php if($key == 'category_function'): ?>
                                            <td  class="column-list-item"><?=$request->category_function_name;?></td>
                                        <?php else: ?>
                                            <td  class="column-list-item"><?=$data->{$key};?></td>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                </tr>
                                <?php endforeach; ?>
                            </table>
                            <p>Catatan: <?=$request->notes;?></p>
                        </div>
                        <p style="font-weight:bold;">Alasan ditolak: <span style="color:red"><?=$request->reject_reason;?></span></p>
                    </td>
                </tr>
                <tr>
                    <td class="body">
                        <div>
                            <div id="footer">
                                &copy; Copyright <span id="footer-title"><?=COMPANY_NAME;?></span>. All Rights Reserved
                            </div>
			            </div>
                    </td>
                </tr>
      		    <tr><td></td></tr>
		    </table>
        </td>
        <td class="blank-side"></td>
      </tr>
    </table>
    </body>
</html>