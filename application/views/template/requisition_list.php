<html>
<head>
    <style>
		.table-list-item {
			font-family: arial, sans-serif;
			border-collapse: collapse;
			width: 100%;
		}
		.column-list-item{
			border: 1px solid #dddddd;
			text-align: left;
			padding: 5px;
      		ont-size:11px;
		}
	</style>
</head>
<body>
	<div id="header" style="margin-bottom:20px;">
		<table style="width:100%; padding-left:20px; margin-bottom:50px;">
			<tr>
				<td width="25%"></td>
				<td width="50%">
					<p>
						<span style="font-size:26px; text-decoration:underline;">REQUISITION LIST</span><br>
						<span style="font-size:20px;"><?=$request->request_number;?></span>
					</p>
				</td>
				<td width="25%"></td>
			</tr>
		</table>
		<table style="width:100%; padding-left:20px; margin-bottom:25px;">
			<tr>
				<td>
					<p style="padding-top:10px;">
						Kepada: Manager Asset Management
					</p>
					<p>
						Dari: <?=$request->requestor_name;?>
					</p>
				</td>
			</tr>
		</table>
	</div>
	<div id="table" style="margin-bottom:20px;">
		<table style="width:100%" class="table-list-item">
			<tr>
				<?php foreach ($header['title'] as $table_header): ?>
				<th class="column-list-item"><?=$table_header;?></th>
				<?php endforeach; ?>
			</tr>
			<?php foreach ($request->data as $data): ?>
			<tr>
				<?php foreach ($header['key'] as $key): ?>
					<?php if($key == 'category_function'): ?>
						<td  class="column-list-item"><?=$request->category_function_name;?></td>
					<?php else: ?>
						<td  class="column-list-item"><?=$data->{$key};?></td>
					<?php endif; ?>
				<?php endforeach; ?>
			</tr>
			<?php endforeach; ?>
		</table>
	</div>
	<?php if(isset($request->items)): ?>
		<div id="table" style="margin-bottom:20px;">
			<table style="width:100%" class="table-list-item">
				<tr>
					<?php foreach ($header['itemTitle'] as $table_header): ?>
					<th class="column-list-item"><?=$table_header;?></th>
					<?php endforeach; ?>
				</tr>
				<?php foreach ($request->items as $data): ?>
				<tr>
					<?php foreach ($header['itemKey'] as $key): ?>
						<td  class="column-list-item"><?=$data->{$key};?></td>
					<?php endforeach; ?>
				</tr>
				<?php endforeach; ?>
			</table>
		</div>
	<?php endif; ?>
	<div>
		<p>Catatan: <?=$request->notes;?></p>
	</div>
	<div id="footer" style="margin-top: 50px;">
		<table style="width:100%">
			<tr>
				<td>
					<p align="center" style="font-weight:bold; font-size:10px;">
						<?php
							$date = new DateTime($request->created_at);
							setlocale(LC_TIME, 'id_ID.UTF-8');

							$r_formatted_date 	= strftime('%e %B %Y', $date->getTimestamp());
							$r_time 			= $date->format('H:i');
						?>
						Telah disetujui oleh<br><?=$request->requestor_name;?><br>pada tanggal <?=$r_formatted_date;?> pukul <?=$r_time;?> WIB
					</p>
					<!-- <br><br>
					<p>
						(........................)
					</p> -->
				</td>
				<td>
						<?php
							$history_approve = null;
							foreach ($request->history as $history) {
								if($history->status == 'ON PROGRESS'){
									$history_approve = $history;
									break;
								}
							}

							$date = new DateTime($history_approve->created_at);
							setlocale(LC_TIME, 'id_ID.UTF-8');

							$e_formatted_date 	= strftime('%e %B %Y', $date->getTimestamp());
							$e_time 			= $date->format('H:i');
						?>
					<p style="font-weight:bold; font-size:10px; text-align:center !important;">
						Telah disetujui oleh<br>Manager Asset Management<br>pada tanggal <?=$e_formatted_date;?> pukul <?=$e_time;?> WIB
					</p>
					<!-- <br><br> -->
					<!-- <p>
						(........................)
					</p> -->
				</td>
			</tr>
		</table>
	</div>
</body>
</html>