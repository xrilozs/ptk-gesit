<div class="row mb-5">
    <div class="col-12 d-flex justify-content-center">
        <div class="btn-group" role="group" aria-label="Basic example">
            <a href="<?=base_url("e/dashboard");?>" class="btn btn-light">Dashboard</a>
            <a href="<?=base_url("e/request");?>" class="btn btn-primary">Daftar Request</a>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-4 col-md-4 col-6">
        <div class="info-box bg-dark">
            <span class="info-box-icon"><i class="far fa-envelope-open"></i></span>

            <div class="info-box-content">
                <span class="info-box-number">Total</span>
                <span class="info-box-text" id="request-total">410</span>
            </div>
        </div>
    </div>
    <div class="col-lg-4 col-md-4 col-6">
        <div class="info-box bg-secondary">
            <span class="info-box-icon"><i class="far fa-envelope"></i></span>

            <div class="info-box-content">
                <span class="info-box-number">Requested</span>
                <span class="info-box-text" id="request-total-requested">410</span>
            </div>
        </div>
    </div>
    <div class="col-lg-4 col-md-4 col-6">
        <div class="info-box bg-danger">
            <span class="info-box-icon"><i class="fa fa-times"></i></span>

            <div class="info-box-content">
                <span class="info-box-number">Rejected</span>
                <span class="info-box-text" id="request-total-rejected">410</span>
            </div>
        </div>
    </div>
    <div class="col-lg-4 col-md-4 col-6">
        <div class="info-box bg-warning">
            <span class="info-box-icon"><i class="far fa-clock"></i></span>

            <div class="info-box-content">
                <span class="info-box-number">Progress</span>
                <span class="info-box-text" id="request-total-onprogress">410</span>
            </div>
        </div>
    </div>
    <div class="col-lg-4 col-md-4 col-6">
        <div class="info-box bg-success">
            <span class="info-box-icon"><i class="fas fa-check"></i></span>

            <div class="info-box-content">
                <span class="info-box-number">Completed</span>
                <span class="info-box-text" id="request-total-completed">410</span>
            </div>
        </div>
    </div>
    <div class="col-lg-4 col-md-4 col-6">
        <div class="info-box bg-info">
            <span class="info-box-icon"><i class="fas fa-check-double"></i></span>

            <div class="info-box-content">
                <span class="info-box-number">Accomplished</span>
                <span class="info-box-text"  id="request-total-requestaccomplished">410</span>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-6">
        <div class="form-group">
          <label>Status Request:</label>
          <select id="request-status-option" class="form-control">
            <option value="">Semua</option>
            <option value="REQUESTED">Requested</option>
            <option value="REJECTED">Rejected</option>
            <option value="ON PROGRESS">On Progress</option>
            <option value="COMPLETED">Completed</option>
            <option value="REQUEST ACCOMPLISHED">Accomplished</option>
          </select>
        </div>
    </div>
    <div class="col-6">  
        <div class="form-group">
          <label>Tanggal Request:</label>
          <input type="text" name="request_date" class="form-control datepicker" id="request-date" placeholder="Tanggal Request.." required>
        </div>
    </div>
    <div class="col-12 table-responsive">
        <table class="table table-bordered" id="request-datatable">
            <thead>
                <tr>
                    <th>Kategori</th>
                    <th>Requestor</th>
                    <th>Nomor Request</th>
                    <th>Status</th>
                    <th>Alasan Reject</th>
                    <th>Tanggal</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>