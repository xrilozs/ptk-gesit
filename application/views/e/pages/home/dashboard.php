<div class="row mb-5">
    <div class="col-12 d-flex justify-content-center">
        <div class="btn-group" role="group" aria-label="Basic example">
            <a href="<?=base_url("e/dashboard");?>" class="btn btn-primary">Dashboard</a>
            <a href="<?=base_url("e/request");?>" class="btn btn-light">Daftar Request</a>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-4 col-6 mb-4">
        <div class="info-box bg-light">
             <div class="row">
                <div class="col-lg-6 col-md-6 col-12">
                    <img src="<?=base_url("assets/img/logo_operational_vehicle.png");?>" class="info-box-icon"  style="width:150px;">
                </div>
                <div class="col-lg-6 col-md-6 col-12 d-flex align-items-center">
                    <div class="info-box-content">
                        <span class="info-box-number">Request Kendaraan Operasional</span>
                        <span class="info-box-text" id="request-total-operational-vehicle">410</span>
                    </div>
                </div>
             </div>
        </div>
    </div>
    <div class="col-lg-4 col-6 mb-4">
        <div class="info-box bg-light">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-12">
                    <img src="<?=base_url("assets/img/logo_meeting_room.png");?>" class="info-box-icon" style="width:150px;">
                </div>
                <div class="col-lg-6 col-md-6 col-12 d-flex align-items-center">
                    <div class="info-box-content">
                        <span class="info-box-number">Request Ruang Rapat</span>
                        <span class="info-box-text" id="request-total-meeting-room">410</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-4 col-6 mb-4">
        <div class="info-box bg-light">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-12">
                    <img src="<?=base_url("assets/img/logo_meeting_consumption.png");?>" class="info-box-icon" style="width:150px;">
                </div>
                <div class="col-lg-6 col-md-6 col-12 d-flex align-items-center">
                    <div class="info-box-content">
                        <span class="info-box-number">Request Konsumsi Rapat</span>
                        <span class="info-box-text" id="request-total-meeting-consumption">410</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-4 col-6 mb-4">
        <div class="info-box bg-light">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-12">
                    <img src="<?=base_url("assets/img/logo_office_stationery.png");?>" class="info-box-icon" style="width:150px;">
                </div>
                <div class="col-lg-6 col-md-6 col-12 d-flex align-items-center">
                    <div class="info-box-content">
                        <span class="info-box-number">Request Alat Tulis Kantor (ATK)</span>
                        <span class="info-box-text" id="request-total-office-stationery">410</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-4 col-6 mb-4">
        <div class="info-box bg-light">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-12">
                    <img src="<?=base_url("assets/img/logo_maintenance.png");?>" class="info-box-icon" style="width:150px;">
                </div>
                <div class="col-lg-6 col-md-6 col-12 d-flex align-items-center">
                    <div class="info-box-content">
                        <span class="info-box-number">Request Facility Maintenance</span>
                        <span class="info-box-text" id="request-total-maintenance">410</span>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-4 col-6 mb-4">
        <div class="info-box bg-light">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-12">
                    <img src="<?=base_url("assets/img/logo_office_household.png");?>" class="info-box-icon" style="width:150px;">
                </div>
                <div class="col-lg-6 col-md-6 col-12 d-flex align-items-center">
                    <div class="info-box-content">
                        <span class="info-box-number">Request Rumah Tangga Kantor</span>
                        <span class="info-box-text" id="request-total-office-household">410</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-4 col-6 mb-4">
        <div class="info-box bg-light">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-12">
                    <img src="<?=base_url("assets/img/logo_fuel_and_ecard.png");?>" class="info-box-icon" style="width:150px;">
                </div>
                <div class="col-lg-6 col-md-6 col-12 d-flex align-items-center">
                    <div class="info-box-content">
                        <span class="info-box-number">Request Bahan Bakar & Kartu Elektronik</span>
                        <span class="info-box-text" id="request-total-fuel-and-ecard">410</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-4 col-6 mb-4">
        <div class="info-box bg-light">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-12">
                    <img src="<?=base_url("assets/img/logo_workstation_and_furniture.png");?>" class="info-box-icon" style="width:150px;">
                </div>
                <div class="col-lg-6 col-md-6 col-12 d-flex align-items-center">
                    <div class="info-box-content">
                        <span class="info-box-number">Request Workstation & Furniture</span>
                        <span class="info-box-text" id="request-total-workstation-and-furniture">410</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-4 col-6 mb-4">
        <div class="info-box bg-light">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-12">
                    <img src="<?=base_url("assets/img/logo_consumable.png");?>" class="info-box-icon" style="width:150px;">
                </div>
                <div class="col-lg-6 col-md-6 col-12 d-flex align-items-center">
                    <div class="info-box-content">
                        <span class="info-box-number">Request Consumables</span>
                        <span class="info-box-text" id="request-total-consumable">410</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-4 col-6 mb-4">
        <div class="info-box bg-light">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-12">
                    <img src="<?=base_url("assets/img/logo_courier_and_mailing.png");?>" class="info-box-icon" style="width:150px;">
                </div>
                <div class="col-lg-6 col-md-6 col-12 d-flex align-items-center">
                    <div class="info-box-content">
                        <span class="info-box-number">Request Pengiriman Dokumen</span>
                        <span class="info-box-text" id="request-total-courier-and-mailing">410</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-4 col-6 mb-4">
        <div class="info-box bg-light">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-12">
                    <img src="<?=base_url("assets/img/logo_event_support.png");?>" class="info-box-icon" style="width:150px;">
                </div>
                <div class="col-lg-6 col-md-6 col-12 d-flex align-items-center">
                    <div class="info-box-content">
                        <span class="info-box-number">Request Event Support</span>
                        <span class="info-box-text" id="request-total-event-support">410</span>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-4 col-6 mb-4">
        <div class="info-box bg-light">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-12">
                    <img src="<?=base_url("assets/img/logo_data_asset.png");?>" class="info-box-icon" style="width:150px;">
                </div>
                <div class="col-lg-6 col-md-6 col-12 d-flex align-items-center">
                    <div class="info-box-content">
                        <span class="info-box-number">Request Data Asset Management</span>
                        <span class="info-box-text" id="request-total-asset">410</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-4 col-6 mb-4">
        <div class="info-box bg-light">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-12">
                    <img src="<?=base_url("assets/img/logo_other.png");?>" class="info-box-icon" style="width:150px;">
                </div>
                <div class="col-lg-6 col-md-6 col-12 d-flex align-items-center">
                    <div class="info-box-content">
                        <span class="info-box-number">Request Others</span>
                        <span class="info-box-text" id="request-total-other">410</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>