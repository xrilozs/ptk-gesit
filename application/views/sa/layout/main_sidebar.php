  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-light-primary elevation-1">
    <!-- Brand Logo -->
    <a href="<?=base_url('dashboard');?>" class="brand-link">
      <img src="https://placehold.co/600x400?text=Logo" alt="Logo" class="brand-image" style="opacity: .8">
      <span class="font-weight-bold" style="font-size:12px">PTK Gesit</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <li class="nav-item" id="sidebar-dashboard-menu">
            <a href="<?=base_url('sa/dashboard');?>" class="nav-link <?=$title == 'Dashboard' ? 'active' : '';?>">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
              </p>
            </a>
          </li>
          <li class="nav-item <?=in_array($title, ['Kategori Aset', 'Kategori Fungsi', 'Kategori Pemeliharaan', 'Kategori ATK', 'Kategori Konsumsi Meeting', 'Kategori Permintaan Tambahan']) ? 'menu-is-opening menu-open' : '';?>">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-tags"></i>
              <p>
                Kategori
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?=base_url('sa/category-asset');?>" class="nav-link <?=$title == 'Kategori Aset' ? 'active' : '';?>">
                  <p>Data Aset Manajemen</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?=base_url('sa/category-function');?>" class="nav-link <?=$title == 'Kategori Fungsi' ? 'active' : '';?>">
                  <p>Fungsi</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?=base_url('sa/category-maintenance');?>" class="nav-link <?=$title == 'Kategori Pemeliharaan' ? 'active' : '';?>">
                  <p>Pemeliharaan</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?=base_url('sa/category-office-stationary');?>" class="nav-link <?=$title == 'Kategori ATK' ? 'active' : '';?>">
                  <p>ATK</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?=base_url('sa/category-meeting-consumption');?>" class="nav-link <?=$title == 'Kategori Konsumsi Meeting' ? 'active' : '';?>">
                  <p>Konsumsi Meeting</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?=base_url('sa/category-additional-request');?>" class="nav-link <?=$title == 'Kategori Permintaan Tambahan' ? 'active' : '';?>">
                  <p>Permintaan Tambahan</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?=base_url('sa/category-consumable');?>" class="nav-link <?=$title == 'Kategori Barang Habis Pakai' ? 'active' : '';?>">
                  <p>Barang Habis Pakai</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?=base_url('sa/category-event-support');?>" class="nav-link <?=$title == 'Kategori Event Support' ? 'active' : '';?>">
                  <p>Event Support</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?=base_url('sa/category-courier-and-mailing');?>" class="nav-link <?=$title == 'Kategori Kurir dan Pesan' ? 'active' : '';?>">
                  <p>Kurir dan Pesan</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?=base_url('sa/category-fuel-and-ecard');?>" class="nav-link <?=$title == 'Kategori Bahan Bakar dan Kartu Elektronik' ? 'active' : '';?>">
                  <p>Bahan Bakar dan Kartu Elektronik</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?=base_url('sa/category-office-household');?>" class="nav-link <?=$title == 'Kategori Rumah Tangga Kantor' ? 'active' : '';?>">
                  <p>Rumah Tangga Kantor</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?=base_url('sa/category-workstation-and-furniture');?>" class="nav-link <?=$title == 'Kategori Tempat Kerja dan Perabotan' ? 'active' : '';?>">
                  <p>Tempat Kerja dan Perabotan</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item <?=in_array($title, ['Gedung', 'Ruang Meeting']) ? 'menu-is-opening menu-open' : '';?>">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-building"></i>
              <p>
                Meeting
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?=base_url('sa/building');?>" class="nav-link <?=$title == 'Gedung' ? 'active' : '';?>">
                  <p>Gedung</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?=base_url('sa/meeting-room');?>" class="nav-link <?=$title == 'Ruang Meeting' ? 'active' : '';?>">
                  <p>Ruang Meeting</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item">
            <a href="<?=base_url('sa/driver');?>" class="nav-link <?=$title == 'Pengemudi' ? 'active' : '';?>">
              <i class="nav-icon fas fa-car-alt"></i>
              <p>
                Pengemudi
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?=base_url('sa/user');?>" class="nav-link <?=$title == 'User' ? 'active' : '';?>">
              <i class="nav-icon fas fa-users"></i>
              <p>
                User
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?=base_url('sa/config');?>" class="nav-link <?=$title == 'Pengaturan' ? 'active' : '';?>">
              <i class="nav-icon fas fa-cog"></i>
              <p>
                Pengaturan
              </p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>
