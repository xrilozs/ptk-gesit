<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>PTK Gesit | <?=$title;?></title>
  <link rel="icon" id="favicon" href="https://placehold.co/60x40"/>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?=base_url('assets/third-party/adminlte/plugins/fontawesome-free/css/all.min.css');?>">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Tempusdominus Bootstrap 4 -->
  <link rel="stylesheet" href="<?=base_url('assets/third-party/adminlte/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css');?>">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?=base_url('assets/third-party/adminlte/plugins/icheck-bootstrap/icheck-bootstrap.min.css');?>">
  <!-- JQVMap -->
  <link rel="stylesheet" href="<?=base_url('assets/third-party/adminlte/plugins/jqvmap/jqvmap.min.css');?>">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?=base_url('assets/third-party/adminlte/dist/css/adminlte.min.css');?>">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="<?=base_url('assets/third-party/adminlte/plugins/overlayScrollbars/css/OverlayScrollbars.min.css');?>">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="<?=base_url('assets/third-party/adminlte/plugins/daterangepicker/daterangepicker.css');?>">
  <!-- Date picker -->
  <link rel="stylesheet" href="<?=base_url('assets/third-party/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css');?>">
  <!-- summernote -->
  <link rel="stylesheet" href="<?=base_url('assets/third-party/adminlte/plugins/summernote/summernote-bs4.min.css');?>">
  <!-- DataTables -->
  <link rel="stylesheet" href="<?=base_url('assets/third-party/adminlte/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css');?>">
  <link rel="stylesheet" href="<?=base_url('assets/third-party/adminlte/plugins/datatables-responsive/css/responsive.bootstrap4.min.css');?>">
  <link rel="stylesheet" href="<?=base_url('assets/third-party/adminlte/plugins/datatables-buttons/css/buttons.bootstrap4.min.css');?>">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?=base_url('assets/third-party/adminlte/dist/css/adminlte.min.css');?>">
   <!-- Select2 -->
  <link rel="stylesheet" href="<?=base_url('assets/third-party/adminlte/plugins/select2/css/select2.min.css');?>">
  <link rel="stylesheet" href="<?=base_url('assets/third-party/adminlte/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css');?>">
  <!-- SweetAlert2 -->
  <link rel="stylesheet" href="<?=base_url('assets/third-party/adminlte/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css');?>">
  <!-- CodeMirror -->
  <link rel="stylesheet" href="<?=base_url('assets/third-party/adminlte/plugins/codemirror/codemirror.css');?>">
  <link rel="stylesheet" href="<?=base_url('assets/third-party/adminlte/plugins/codemirror/theme/monokai.css');?>">
  <!-- Dropify -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/css/dropify.min.css" integrity="sha512-EZSUkJWTjzDlspOoPSpUFR0o0Xy7jdzW//6qhUkoZ9c4StFkVsp9fbbd0O06p9ELS3H486m4wmrCELjza4JEog==" crossorigin="anonymous" referrerpolicy="no-referrer" />
  <!-- Tagify -->
  <link href="https://cdn.jsdelivr.net/npm/@yaireo/tagify/dist/tagify.css" rel="stylesheet" type="text/css" />

  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
  <!-- Custom CSS -->
  <link rel="stylesheet" href="<?=base_url('assets/css/common.css?v=').time();?>">
</head>
<body class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed">
<div class="wrapper">

  <!-- Preloader -->
  
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-primary">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" style="color:white;" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
    </ul>
    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      <!-- Profile Dropdown Menu -->
      <li class="nav-item dropdown">
        <a class="nav-link" style="color:white;" data-toggle="dropdown" href="#" id="header-user-name">
          User
        </a>
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
          <a href="#" id="change-password-toggle" class="dropdown-item" data-toggle="modal" data-target="#change-password-modal">
            Ganti Password
          </a>
          <a href="#" id="logout-button" class="dropdown-item">
            Keluar
          </a>
        </div>
      </li>
    </ul>
  </nav>
  <!-- /.navbar -->
