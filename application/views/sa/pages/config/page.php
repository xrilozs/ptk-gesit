<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Pengaturan</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="<?=base_url('dashboard');?>">Dashboard</a></li>
            <li class="breadcrumb-item active">Pengaturan</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-body">
              <form id="config-form">
                <div class="row">
                  <div class="col-12">
                    <div class="form-group">
                      <label>Apps Logo (*.jpg, *.png):</label>
                      <input 
                        type="file" 
                        id="config-apps-logo"
                        data-allowed-file-extensions="jpeg jpg png" 
                      >
                    </div>
                  </div>
                  <div class="col-12">
                    <div class="form-group">
                      <label>Icon (*.ico):</label>
                      <input 
                        type="file" 
                        id="config-apps-icon"
                        data-allowed-file-extensions="ico" 
                      >
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-12">
                    <button type="submit" class="btn btn-primary btn-block" id="config-button">Submit</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>

