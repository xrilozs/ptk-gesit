<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0">Dashboard</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item active">Dashboard</li>
          </ol>
        </div>
      </div>
    </div>
  </div>

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-6 col-6">
          <!-- small box -->
          <div class="small-box bg-primary">
            <div class="inner">
              <h3 id="dashboard-request-total">0</h3>
              <p>Request</p>
            </div>
            <div class="icon">
            <i class="fas fa-file-alt"></i>
            </div>
            <span class="small-box-footer">&nbsp;</span>
          </div>
        </div>
        <div class="col-lg-6 col-6">
          <!-- small box -->
          <div class="small-box bg-primary">
            <div class="inner">
              <h3 id="dashboard-driver-total">0</h3>
              <p>Pengemudi</p>
            </div>
            <div class="icon">
            <i class="fas fa-car"></i>
            </div>
            <a href="<?=base_url('sa/driver');?>" class="small-box-footer">Detail <i class="fas fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <div class="col-lg-6 col-6">
          <!-- small box -->
          <div class="small-box bg-primary">
            <div class="inner">
              <h3 id="dashboard-building-total">0</h3>
              <p>Bangunan</p>
            </div>
            <div class="icon">
            <i class="fas fa-building"></i>
            </div>
            <a href="<?=base_url('sa/building');?>" class="small-box-footer">Detail <i class="fas fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <div class="col-lg-6 col-6">
          <!-- small box -->
          <div class="small-box bg-primary">
            <div class="inner">
              <h3 id="dashboard-room-total">0</h3>
              <p>Ruangan</p>
            </div>
            <div class="icon">
            <i class="fas fa-building"></i>
            </div>
            <a href="<?=base_url('sa/meeting-room');?>" class="small-box-footer">Detail <i class="fas fa-arrow-circle-right"></i></a>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>