<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Gedung</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="<?=base_url('dashboard');?>">Dashboard</a></li>
            <li class="breadcrumb-item active">Gedung</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-body">
              <div class="row d-flex justify-content-end mb-3">
                <button type="button" class="btn btn-primary" id="building-create-toggle" data-toggle="modal" data-target="#building-create-modal">
                  <i class="fas fa-plus"></i> Buat
                </button>
              </div>
              <div class="table-responsive">
                <table class="table table-bordered table-hover" id="building-datatable">
                  <thead>
                    <tr>
                      <th>Nama</th>
                      <th>Tanggal Dibuat</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>

<!-- Modal -->
<div class="modal fade" id="building-create-modal" data-backdrop="static" data-keyboard="false" tabindex="-1" style="overflow-y: auto !important;">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Buat Gedung</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="building-create-form">
        <div class="form-group">
          <label for="building-name-create-field">Nama:</label>
          <input type="text" name="name" class="form-control" id="building-name-create-field" placeholder="Nama.." autocomplete="off" required>
        </div>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
        <button type="submit" class="btn btn-primary" id="building-create-button">Submit</button>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="building-update-modal"  data-backdrop="static" data-keyboard="false" tabindex="-1" style="overflow-y: auto !important;">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="overlay" id="building-update-overlay">
          <i class="fas fa-2x fa-sync fa-spin"></i>
      </div>
      <div class="modal-header">
        <h4 class="modal-title">Update Gedung</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="building-update-form">
        <div class="form-group">
          <label for="building-name-update-field">Nama:</label>
          <input type="text" name="name" class="form-control" id="building-name-update-field" placeholder="Nama pengemudi.." autocomplete="off" required>
        </div>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
        <button type="submit" class="btn btn-primary" id="building-update-button">Submit</button>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="building-delete-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Hapus Gedung</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Apakah Anda yakin ingin menghapus gedung tersebut?
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
        <button type="button" class="btn btn-danger" id="building-delete-button">Ya</button>
      </div>
    </div>
  </div>
</div>

