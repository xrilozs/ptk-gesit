<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Ruang Meeting</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="<?=base_url('dashboard');?>">Dashboard</a></li>
            <li class="breadcrumb-item active">Ruang Meeting</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-body">
              <div class="row d-flex justify-content-end mb-3">
                <button type="button" class="btn btn-primary" id="meeting-room-create-toggle" data-toggle="modal" data-target="#meeting-room-create-modal">
                  <i class="fas fa-plus"></i> Buat
                </button>
              </div>
              <div class="table-responsive">
                <table class="table table-bordered table-hover" id="meeting-room-datatable">
                  <thead>
                    <tr>
                      <th>Gedung</th>
                      <th>Nama</th>
                      <th>Deskripsi</th>
                      <th>Nomor Lantai</th>
                      <th>Kapasitas</th>
                      <th>Status</th>
                      <th>Tanggal Dibuat</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>

<!-- Modal -->
<div class="modal fade" id="meeting-room-create-modal" data-backdrop="static" data-keyboard="false" tabindex="-1" style="overflow-y: auto !important;">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Buat Ruang Meeting</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="meeting-room-create-form">
        <div class="form-group">
          <label for="meeting-room-building-create-field">Gedung:</label>
          <select name="building" class="form-control building-option" id="meeting-room-building-create-field" required></select>
        </div>
        <div class="form-group">
          <label for="meeting-room-name-create-field">Nama:</label>
          <input type="text" name="name" class="form-control" id="meeting-room-name-create-field" placeholder="Nama.." autocomplete="off" required>
        </div>
        <div class="form-group">
          <label for="meeting-room-description-create-field">Deskripsi:</label>
          <textarea name="description" class="form-control" id="meeting-room-description-create-field" placeholder="Deskripsi.." required></textarea>
        </div>
        <div class="form-group">
          <label for="meeting-room-floor-number-create-field">Nomor Lantai:</label>
          <input type="number" name="floor_number" class="form-control" id="meeting-room-floor-number-create-field" placeholder="Nomor Lantai.." required>
        </div>
        <div class="form-group">
          <label for="meeting-room-capacity-create-field">Kapasitas:</label>
          <input type="number" name="capacity" class="form-control" id="meeting-room-capacity-create-field" placeholder="Kapasitas..">
        </div>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
        <button type="submit" class="btn btn-primary" id="meeting-room-create-button">Submit</button>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="meeting-room-update-modal"  data-backdrop="static" data-keyboard="false" tabindex="-1" style="overflow-y: auto !important;">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="overlay" id="meeting-room-update-overlay">
          <i class="fas fa-2x fa-sync fa-spin"></i>
      </div>
      <div class="modal-header">
        <h4 class="modal-title">Update Ruang Meeting</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="meeting-room-update-form">
        <div class="form-group">
          <label for="meeting-room-building-update-field">Gedung:</label>
          <select name="building" class="form-control building-option" id="meeting-room-building-update-field" required></select>
        </div>
        <div class="form-group">
          <label for="meeting-room-name-update-field">Nama:</label>
          <input type="text" name="name" class="form-control" id="meeting-room-name-update-field" placeholder="Nama.." autocomplete="off" required>
        </div>
        <div class="form-group">
          <label for="meeting-room-description-update-field">Deskripsi:</label>
          <textarea name="description" class="form-control" id="meeting-room-description-update-field" placeholder="Deskripsi.." required></textarea>
        </div>
        <div class="form-group">
          <label for="meeting-room-floor-number-update-field">Nomor Lantai:</label>
          <input type="number" name="floor_number" class="form-control" id="meeting-room-floor-number-update-field" placeholder="Nomor Lantai.." required>
        </div>
        <div class="form-group">
          <label for="meeting-room-capacity-update-field">Kapasitas:</label>
          <input type="number" name="capacity" class="form-control" id="meeting-room-capacity-update-field" placeholder="Kapasitas..">
        </div>
        <div class="form-group">
          <label for="meeting-room-status-update-field">Status:</label>
          <span id="meeting-room-status-update-field"></span>
        </div>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
        <button type="submit" class="btn btn-primary" id="meeting-room-update-button">Submit</button>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="meeting-room-delete-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Hapus Ruang Meeting</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Apakah Anda yakin ingin menghapus ruang meeting tersebut?
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
        <button type="button" class="btn btn-danger" id="meeting-room-delete-button">Ya</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="meeting-room-book-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Set Ruang Meeting dipesan</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Apakah Anda yakin ingin set ruang meeting menjadi dipesan?
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
        <button type="button" class="btn btn-danger" id="meeting-room-book-button">Ya</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="meeting-room-free-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Set Ruang Meeting Tersedia</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Apakah Anda yakin ingin set ruang meeting menjadi tersedia?
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
        <button type="button" class="btn btn-danger" id="meeting-room-free-button">Ya</button>
      </div>
    </div>
  </div>
</div>
