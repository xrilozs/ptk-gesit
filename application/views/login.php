<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>PTK Gesit | <?=$title;?></title>
  <link rel="icon" href="https://placehold.co/60x40"/>
  
  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=Roboto+Condensed&display=swap" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css2?family=Mochiy+Pop+P+One&family=Roboto+Condensed&display=swap" rel="stylesheet">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?=base_url('assets/third-party/adminlte/plugins/fontawesome-free/css/all.min.css');?>">
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="<?=base_url('assets/third-party/adminlte/plugins/icheck-bootstrap/icheck-bootstrap.min.css');?>">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?=base_url('assets/third-party/adminlte/dist/css/adminlte.min.css');?>">
  <!-- SweetAlert2 -->
  <link rel="stylesheet" href="<?=base_url('assets/third-party/adminlte/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css');?>">
  <!-- Custom CSS -->
  <link rel="stylesheet" href="<?=base_url('assets/css/login.css?v=').time();?>">
</head>
<body class="hold-transition login-page">
<div class="login-box">  
  <div class="card shadow mb-5">
    <div class="card-body login-card-body" id="login-card">
      <div class="login-logo">
        <img src="https://placehold.co/600x400?text=Logo" class="img-fluid brand-image" style="max-width:200px;"><br>
      </div>
      <h1 id="login-title">PTK Gesit</h1>
      <form id="login-form">
        <div class="input-group mb-3">
          <input type="text" id="login-username-field" class="form-control" name="username" placeholder="Username" required autofocus>
          <div class="input-group-append" style="background-color:white;">
            <div class="input-group-text">
              <span class="fas fa-envelope"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-3">
           <input type="password" id="login-password-field" class="form-control" name="password" placeholder="Password" required>
          <div class="input-group-append" style="background-color:white;">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
        <div class="social-auth-links text-center">
          <button type="submit" id="login-button" class="btn btn-block btn-primary">
            Masuk
          </button>
        </div>
      </form>
    </div>
  </div>
</div>

<!-- jQuery -->
<script src="<?=base_url('assets/third-party/adminlte/plugins/jquery/jquery.min.js');?>"></script>
<!-- Bootstrap 4 -->
<script src="<?=base_url('assets/third-party/adminlte/plugins/bootstrap/js/bootstrap.bundle.min.js');?>"></script>
<!-- AdminLTE App -->
<script src="<?=base_url('assets/third-party/adminlte/dist/js/adminlte.min.js');?>"></script>
<!-- SweetAlert2 -->
<script src="<?=base_url('assets/third-party/adminlte/plugins/sweetalert2/sweetalert2.min.js');?>"></script>
<!-- Custom JS -->
<script>
    const WEB_URL = "<?=base_url()?>"
    const API_URL = "<?=base_url('api')?>"
</script>
<?php
  if(isset($js_file)){
    foreach ($js_file as $file) {
        echo "<script src='".$file."'></script>";
    }
  }
?>
</body>
</body>
</html>
