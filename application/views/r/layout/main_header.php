<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>PTK Gesit | <?=$title;?></title>
  <link rel="icon" id="favicon" href="https://placehold.co/60x40"/>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?=base_url('assets/third-party/adminlte/plugins/fontawesome-free/css/all.min.css');?>">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?=base_url('assets/third-party/adminlte/dist/css/adminlte.min.css');?>">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="<?=base_url('assets/third-party/adminlte/plugins/overlayScrollbars/css/OverlayScrollbars.min.css');?>">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="<?=base_url('assets/third-party/adminlte/plugins/daterangepicker/daterangepicker.css');?>">
  <!-- Date picker -->
  <link rel="stylesheet" href="<?=base_url('assets/third-party/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css');?>">
  <!-- summernote -->
  <link rel="stylesheet" href="<?=base_url('assets/third-party/adminlte/plugins/summernote/summernote-bs4.min.css');?>">
  <!-- DataTables -->
  <link rel="stylesheet" href="<?=base_url('assets/third-party/adminlte/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css');?>">
  <link rel="stylesheet" href="<?=base_url('assets/third-party/adminlte/plugins/datatables-responsive/css/responsive.bootstrap4.min.css');?>">
  <link rel="stylesheet" href="<?=base_url('assets/third-party/adminlte/plugins/datatables-buttons/css/buttons.bootstrap4.min.css');?>">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?=base_url('assets/third-party/adminlte/dist/css/adminlte.min.css');?>">
   <!-- Select2 -->
  <link rel="stylesheet" href="<?=base_url('assets/third-party/adminlte/plugins/select2/css/select2.min.css');?>">
  <link rel="stylesheet" href="<?=base_url('assets/third-party/adminlte/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css');?>">
  <!-- SweetAlert2 -->
  <link rel="stylesheet" href="<?=base_url('assets/third-party/adminlte/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css');?>">
  <!-- Dropify -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/css/dropify.min.css" integrity="sha512-EZSUkJWTjzDlspOoPSpUFR0o0Xy7jdzW//6qhUkoZ9c4StFkVsp9fbbd0O06p9ELS3H486m4wmrCELjza4JEog==" crossorigin="anonymous" referrerpolicy="no-referrer" />
  <!-- Tagify -->
  <link href="https://cdn.jsdelivr.net/npm/@yaireo/tagify/dist/tagify.css" rel="stylesheet" type="text/css" />

  <!-- <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css"> -->
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/timepicker@1.14.1/jquery.timepicker.min.css">

  <!-- Custom CSS -->
  <link rel="stylesheet" href="<?=base_url('assets/css/common.css?v=').time();?>">
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-12 mt-1">
                <nav class="nav justify-content-end">
                    <div class="dropdown">
                        <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false">
                            <span id="header-user-name">User</span>&nbsp;&nbsp;<i class="fas fa-user"></i>
                        </button>
                        <div class="dropdown-menu">
                            <a class="dropdown-item" href="#" data-toggle="modal" data-target="#change-password-modal">
                                <i class="fas fa-key"></i>&nbsp;&nbsp;Ganti Kata Sandi
                            </a>
                            <a class="dropdown-item" href="#" id="logout-button">
                                <i class="fas fa-sign-out-alt"></i>&nbsp;&nbsp;Keluar
                            </a>
                        </div>
                    </div>
                </nav>
            </div>
        </div>
        <div class="row my-3">
            <div class="col-12 d-flex justify-content-center">
                <img src="https://placehold.co/300x200" class="brand-image" style="max-height:300px;">
            </div>
        </div>
