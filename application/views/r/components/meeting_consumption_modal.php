<div class="modal fade" id="meeting-consumption-create-modal" data-backdrop="static" data-keyboard="false" tabindex="-1" style="overflow-y: auto !important;">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Buat Request - Konsumsi Rapat</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="meeting-consumption-create-form">
        <div class="form-group">
          <label for="meeting-consumption-function-create-field">Fungsi:</label>
          <select name="function" class="form-control function-option" id="meeting-consumption-function-create-field" required>
            <option>--Pilih Fungsi--</option>
          </select>
        </div>
        <div class="form-group">
          <label for="meeting-consumption-meeting-create-field">Agenda Rapat:</label>
          <select name="function" class="form-control meeting-option" id="meeting-consumption-meeting-create-field" required>
            <option disabled>--Pilih Meeting--</option>
          </select>
        </div>
        <div class="row bg-light">
          <div class="col-12 pt-2 px-2">
            <label>Konsumsi Rapat</label>
          </div>
          <div class="col-12 pb-2 px-2">
            <div class="row" id="meeting-consumption-create-section">
            </div>
            <div class="row mt-2">
              <div class="col-12 text-right">
                <button type="button" class="btn btn-dark" id="meeting-consumption-add-button">
                  <i class="fas fa-plus"></i>&nbsp;Tambah
                </button>
              </div>
            </div>
          </div>
        </div>
        <div class="form-group">
          <label>Keterangan</label>
          <textarea name="notes" id="meeting-consumption-notes-create-field" class="form-control" placeholder="Keterangan.." required></textarea>
        </div>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-primary" id="meeting-consumption-create-button">Submit</button>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="meeting-consumption-update-modal" data-backdrop="static" data-keyboard="false" tabindex="-1" style="overflow-y: auto !important;">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Ubah Request - Konsumsi</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="meeting-consumption-update-form">
        <div class="form-group">
          <label for="meeting-consumption-meeting-update-field">Meeting:</label>
          <select name="function" class="form-control meeting-option" id="meeting-consumption-meeting-update-field" required>
            <option disabled>--Pilih Meeting--</option>
          </select>
        </div>
        <div class="row bg-light">
          <div class="col-12 pt-2 px-2">
            <label>Konsumsi Meeting</label>
          </div>
          <div class="col-12 pb-2 px-2">
            <div class="row" id="meeting-consumption-update-section">
            </div>
            <div class="row mt-2">
              <div class="col-12 text-right">
                <button type="button" class="btn btn-dark" id="meeting-consumption-add2-button">
                  <i class="fas fa-plus"></i>&nbsp;Tambah
                </button>
              </div>
            </div>
          </div>
        </div>
        <div class="form-group">
          <label>Keterangan</label>
          <textarea name="notes" id="meeting-consumption-notes-update-field" class="form-control" placeholder="Keterangan.." required></textarea>
        </div>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-primary" id="meeting-consumption-update-button">Submit</button>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="meeting-consumption-detail-modal" data-backdrop="static" data-keyboard="false" tabindex="-1" style="overflow-y: auto !important;">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Detail Request - Konsumsi Rapat</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="meeting-consumption-detail-form">
        <div class="form-group">
          <label for="meeting-consumption-function-detail-field">Fungsi:</label>
          <select name="function" class="form-control function-option" id="meeting-consumption-function-detail-field" readonly>
            <option>--Pilih Fungsi--</option>
          </select>
        </div>
        <div class="form-group">
          <label for="meeting-consumption-meeting-detail-field">Agenda Rapat:</label>
          <input type="text" name="meeting" class="form-control" id="meeting-consumption-meeting-detail-field" readonly>
        </div>
        <div class="row bg-light">
          <div class="col-12 pt-2 px-2">
            <label>Konsumsi Rapat</label>
          </div>
          <div class="col-12 pb-2 px-2">
            <div class="row" id="meeting-consumption-detail-section">
            </div>
            <!-- <div class="row mt-2">
              <div class="col-12 text-right">
                <button type="button" class="btn btn-dark" id="meeting-consumption-detail-button">
                  <i class="fas fa-plus"></i>&nbsp;Tambah
                </button>
              </div>
            </div> -->
          </div>
        </div>
        <div class="form-group">
          <label>Keterangan</label>
          <textarea name="notes" id="meeting-consumption-notes-detail-field" class="form-control" placeholder="Keterangan.." required readonly></textarea>
        </div>
        <div class="form-group">
          <label>Status</label>
          <div id="meeting-consumption-status-detail-field"></div>
        </div>
      </div>
      <div class="modal-footer justify-content-end request-detail-footer-modal">
        <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button> -->
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="meeting-consumption-approve-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Mengerjakan Request - Konsumsi Rapat</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Apakah Anda yakin ingin mengerjakan request konsumsi tersebut?
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
        <button type="button" class="btn btn-primary" id="meeting-consumption-approve-button">Ya</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="meeting-consumption-complete-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Menyelesaikan Request - Konsumsi Rapat</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Apakah Anda yakin ingin menyelesaikan request konsumsi tersebut?
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
        <button type="button" class="btn btn-primary" id="meeting-consumption-complete-button">Ya</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="meeting-consumption-accomplish-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Tutup Request - Konsumsi Rapat</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Apakah Anda yakin ingin Tutup Request konsumsi tersebut?
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
        <button type="button" class="btn btn-primary" id="meeting-consumption-accomplish-button">Ya</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="meeting-consumption-reject-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Reject Request - Konsumsi Rapat</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form class="form" id="meeting-consumption-reject-form">
        Apakah Anda yakin ingin reject request konsumsi tersebut?
        <div class="form-group">
          <label>Alasan Reject</label>
          <textarea name="reject_reason" class="form-control" placeholder="Alasan reject.." required></textarea>
        </div>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
        <button type="submit" class="btn btn-danger" id="meeting-consumption-reject-button">Ya</button>
        </form>
      </div>
    </div>
  </div>
</div>