<div class="modal fade" id="meeting-create-modal" data-backdrop="static" data-keyboard="false" tabindex="-1" style="overflow-y: auto !important;">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Buat Request - Ruang Rapat</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="meeting-create-form">
        <div class="form-group">
          <label for="meeting-function-create-field">Fungsi:</label>
          <select name="function" class="form-control function-option" id="meeting-function-create-field" required>
            <option disabled>--Pilih Fungsi--</option>
          </select>
        </div>
        <div class="form-group">
          <label for="meeting-type-create-field">Jenis Rapat:</label>
          <select name="meeting_type" class="form-control type-option" id="meeting-type-create-field" required>
            <option disabled>--Pilih Jenis Rapat--</option>
            <option value="INTERNAL">Internal</option>
            <option value="EXTERNAL">Eksternal</option>
            <option value="INTERNAL WITH VP">Internal dengan VP</option>
            <option value="INTERNAL WITH DIRECTOR">Internal dengan Direktur</option>
          </select>
        </div>
        <div class="form-group">
          <label for="meeting-title-create-field">Judul Agenda Rapat:</label>
          <input type="text" name="meeting_title" class="form-control" id="meeting-title-create-field" placeholder="Judul Agenda Rapat.." required>
        </div>
        <div class="form-group">
          <label for="meeting-building-create-field">Gedung Rapat:</label>
          <select name="building" class="form-control building-option" id="meeting-building-create-field" required>
            <option disabled>--Pilih Gedung--</option>
          </select>
        </div>
        <div class="form-group">
          <label for="meeting-room-create-field">Ruang Rapat:</label>
          <select name="meeting_room" class="form-control meeting-room-option" id="meeting-room-create-field" required disabled>
            <option disabled>--Pilih Ruangan--</option>
          </select>
        </div>
        <div class="form-group" id="meeting-room-capacity-create" style="display:none;">
          <label for="meeting-room-capacity-create-field">Kapasitas Ruangan:</label>
          <input type="number" name="room_capacity" class="form-control" id="meeting-room-capacity-create-field" placeholder="Kapasitas Ruangan.." readonly>
        </div>
        <div class="form-group">
          <label for="meeting-total-participant-create-field">Jumlah Partisipan:</label>
          <input type="number" name="total_participant" class="form-control" id="meeting-total-participant-create-field" placeholder="Jumlah partisipan.." max="100" min="1" required>
        </div>
        <div class="form-group">
          <label for="meeting-date-create-field">Tanggal Rapat:</label>
          <input type="text" name="meeting_date" class="form-control datepicker" id="meeting-date-create-field" placeholder="Tanggal Rapat.." required>
        </div>
        <div class="row">
          <div class="col">
            <div class="form-group">
              <label for="meeting-start-time-create-field">Waktu Mulai:</label>
              <input type="text" name="start_time" class="form-control timepicker" id="meeting-start-time-create-field" placeholder="Pilih Waktu.." required>
            </div>
          </div>
          <div class="col">
            <div class="form-group">
              <label for="meeting-end-time-create-field">Waktu Selesai:</label>
              <input type="text" name="end_time" class="form-control timepicker" id="meeting-end-time-create-field" placeholder="Pilih Waktu.." required>
            </div>
          </div>
        </div>
        <div class="form-group">
          <label for="meeting-invitation-create-field">Mohon Lampirkan Undangan Rapat:</label>
          <input type="file" name="meeting_invitation_url" id="meeting-invitation-create-field" class="dropify">
        </div>
        <div class="form-group">
          <label>Keterangan</label>
          <textarea name="notes" id="meeting-notes-create-field" class="form-control" placeholder="Keterangan.." required></textarea>
        </div>
        <div class="row bg-light">
          <div class="col-12 pt-2 px-2">
            <label>Permintaan Tambahan (Opsional)</label>
          </div>
          <div class="col-12 pb-2 px-2">
            <div class="row" id="additional-request-create-section">
            </div>
            <div class="row mt-2">
              <div class="col-12 text-right">
                <button type="button" class="btn btn-dark" id="additional-request-add-button">
                  <i class="fas fa-plus"></i>&nbsp;Tambah
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-primary" id="meeting-create-button">Submit</button>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="meeting-update-modal" data-backdrop="static" data-keyboard="false" tabindex="-1" style="overflow-y: auto !important;">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Ubah Request - Ruang Rapat</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="meeting-update-form">
        <!-- <div class="form-group">
          <label for="meeting-requestor-name-update-field">Nama:</label>
          <input type="text" name="requestor_name" class="form-control" id="meeting-requestor-name-update-field" placeholder="Nama.." required readonly>
        </div>
        <div class="form-group">
          <label for="meeting-requestor-email-update-field">Email:</label>
          <input type="text" name="requestor_email" class="form-control" id="meeting-requestor-email-update-field" placeholder="Email.." required readonly>
        </div> -->
        <div class="form-group">
          <label for="meeting-function-update-field">Fungsi:</label>
          <select name="function" class="form-control function-option" id="meeting-function-update-field" required>
            <option disabled>--Pilih Fungsi--</option>
          </select>
        </div>
        <div class="form-group">
          <label for="meeting-type-update-field">Jenis Rapat:</label>
          <select name="meeting_type" class="form-control type-option" id="meeting-type-update-field" required>
            <option disabled>--Pilih Jenis Rapat--</option>
            <option value="INTERNAL">Internal</option>
            <option value="EXTERNAL">Eksternal</option>
            <option value="INTERNAL WITH DIRECTOR">Internal dengan Direktur</option>
          </select>
        </div>
        <div class="form-group">
          <label for="meeting-title-update-field">Judul Agenda Rapat:</label>
          <input type="text" name="meeting_title" class="form-control" id="meeting-title-update-field" placeholder="Judul Agenda Rapat.." required>
        </div>        
        <div class="form-group">
          <label for="meeting-building-update-field">Gedung Rapat:</label>
          <select name="building" class="form-control building-option" id="meeting-building-update-field" required>
            <option disabled>--Pilih Gedung--</option>
          </select>
        </div>
        <div class="form-group">
          <label for="meeting-room-update-field">Ruangan:</label>
          <select name="meeting_room" class="form-control meeting-room-option" id="meeting-room-update-field" required>
            <option disabled>--Pilih Ruangan--</option>
          </select>
        </div>
        <div class="form-group">
          <label for="meeting-total-participant-update-field">Jumlah Partisipan:</label>
          <input type="number" name="total_participant" class="form-control" id="meeting-total-participant-update-field" placeholder="Jumlah partisipan.." required>
        </div>
        <div class="form-group">
          <label for="meeting-date-update-field">Tanggal Rapat:</label>
          <input type="text" name="meeting_date" class="form-control datepicker" id="meeting-date-update-field" placeholder="Tanggal Rapat.." required>
        </div>
        <div class="row">
          <div class="col">
            <div class="form-group">
              <label for="meeting-start-time-update-field">Waktu Mulai:</label>
              <input type="text" name="start_time" class="form-control timepicker" id="meeting-start-time-update-field" placeholder="Pilih Waktu.." required>
            </div>
          </div>
          <div class="col">
            <div class="form-group">
              <label for="meeting-end-time-update-field">Waktu Selesai:</label>
              <input type="text" name="end_time" class="form-control timepicker" id="meeting-end-time-update-field" placeholder="Pilih Waktu.." required>
            </div>
          </div>
        </div>
        <div class="form-group">
          <label for="meeting-attachment-update-field">Lampiran (Opsional):</label>
          <input type="file" name="attachment" id="meeting-attachment-update-field" class="dropify">
        </div>
        <div class="form-group">
          <label>Keterangan</label>
          <textarea name="notes" id="meeting-notes-update-field" class="form-control" placeholder="Keterangan.." required></textarea>
        </div>
        <div class="row bg-light">
          <div class="col-12 pt-2 px-2">
            <label>Permintaan Tambahan (Opsional)</label>
          </div>
          <div class="col-12 pb-2 px-2">
            <div class="row" id="additional-request-update-section">
            </div>
            <div class="row mt-2">
              <div class="col-12 text-right">
                <button type="button" class="btn btn-dark" id="additional-request-add2-button">
                  <i class="fas fa-plus"></i>&nbsp;Tambah
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-primary" id="meeting-update-button">Submit</button>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="meeting-room-detail-modal" data-backdrop="static" data-keyboard="false" tabindex="-1" style="overflow-y: auto !important;">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Detail Request - Ruang Rapat</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="meeting-detail-form">
        <div class="form-group">
          <label for="meeting-function-detail-field">Fungsi:</label>
          <select name="function" class="form-control function-option" id="meeting-function-detail-field" readonly>
            <option disabled>--Pilih Fungsi--</option>
          </select>
        </div>
        <div class="form-group">
          <label for="meeting-type-detail-field">Jenis Rapat:</label>
          <select name="meeting_type" class="form-control type-option" id="meeting-type-detail-field" readonly>
            <option disabled>--Pilih Jenis Rapat--</option>
            <option value="INTERNAL">Internal</option>
            <option value="EXTERNAL">Eksternal</option>
            <option value="INTERNAL WITH DIRECTOR">Internal dengan Direktur</option>
            <option value="INTERNAL WITH VP">Internal dengan VP</option>
          </select>
        </div>
        <div class="form-group">
          <label for="meeting-title-detail-field">Judul Agenda Rapat:</label>
          <input type="text" name="meeting_title" class="form-control" id="meeting-title-detail-field" placeholder="Judul Agenda Rapat.." readonly>
        </div>
        <div class="form-group">
          <label for="meeting-building-detail-field">Gedung Rapat:</label>
          <input type="text" name="building" class="form-control" id="meeting-building-detail-field" placeholder="Gedung.." readonly>
        </div>
        <div class="form-group">
          <label for="meeting-room-detail-field">Ruang Rapat:</label>
          <input type="text" name="meeting_room" class="form-control" id="meeting-room-detail-field" placeholder="Ruang Rapat.." readonly>
        </div>
        <div class="form-group">
          <label for="meeting-total-participant-detail-field">Jumlah Partisipan:</label>
          <input type="number" name="total_participant" class="form-control" id="meeting-total-participant-detail-field" placeholder="Jumlah partisipan.." readonly>
        </div>
        <div class="form-group">
          <label for="meeting-date-detail-field">Tanggal Rapat:</label>
          <input type="text" name="meeting_date" class="form-control datepicker" id="meeting-date-detail-field" placeholder="Tanggal Rapat.." readonly>
        </div>
        <div class="row">
          <div class="col">
            <div class="form-group">
              <label for="meeting-start-time-detail-field">Waktu Mulai:</label>
              <input type="text" name="start_time" class="form-control" id="meeting-start-time-detail-field" readonly>
            </div>
          </div>
          <div class="col">
            <div class="form-group">
              <label for="meeting-end-time-detail-field">Waktu Selesai:</label>
              <input type="text" name="end_time" class="form-control" id="meeting-end-time-detail-field" readonly>
            </div>
          </div>
        </div>
        <div class="form-group">
          <label for="meeting-attachment-detail-field">Undangan Rapat:</label>
          <div id="meeting-invitation-detail"></div>
        </div>
        <div class="form-group">
          <label for="meeting-attachment-detail-field">Lampiran:</label>
          <div id="meeting-attachment-detail"></div>
        </div>
        <div class="form-group">
          <label>Status</label>
          <div id="meeting-status-detail-field"></div>
        </div>
        <div class="form-group">
          <label>Keterangan</label>
          <textarea name="notes" id="meeting-notes-detail-field" class="form-control" placeholder="Keterangan.." required readonly></textarea>
        </div>
        <div class="row bg-light">
          <div class="col-12 pt-2 px-2">
            <label>Permintaan Tambahan (Opsional)</label>
          </div>
          <div class="col-12 pb-2 px-2">
            <div class="row" id="additional-request-detail-section">
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer justify-content-end request-detail-footer-modal">
        <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button> -->
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="meeting-approve-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Mengerjakan Request - Ruang Rapat</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Apakah Anda yakin ingin mengerjakan request ruang rapat tersebut?
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
        <button type="button" class="btn btn-primary" id="meeting-approve-button">Ya</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="meeting-complete-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Menyelesaikan Request - Ruang Rapat</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Apakah Anda yakin ingin menyelesaikan request ruang rapat tersebut?
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
        <button type="button" class="btn btn-primary" id="meeting-complete-button">Ya</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="meeting-accomplish-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Tutup Request - Ruang Rapat</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="meeting-accomplish-form" class="form">
        Apakah Anda yakin ingin Tutup Request ruang rapat tersebut?
        <div class="form-group">
          <label for="meeting-attachment-accomplish-field">Lampiran:</label>
          <input type="file" name="attachment" id="meeting-attachment-accomplish-field" required>
        </div>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
        <button type="submit" class="btn btn-primary" id="meeting-accomplish-button">Ya</button>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="meeting-reject-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Reject Request - Ruang Rapat</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form class="form" id="meeting-reject-form">
        Apakah Anda yakin ingin reject request ruang rapat tersebut?
        <div class="form-group">
          <label>Alasan Reject</label>
          <textarea name="reject_reason" class="form-control" placeholder="Alasan reject.." required></textarea>
        </div>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
        <button type="submit" class="btn btn-danger" id="meeting-reject-button">Ya</button>
        </form>
      </div>
    </div>
  </div>
</div>