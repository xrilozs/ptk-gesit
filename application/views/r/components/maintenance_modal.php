<div class="modal fade" id="maintenance-create-modal" data-backdrop="static" data-keyboard="false" tabindex="-1" style="overflow-y: auto !important;">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Buat Request - Facility Maintenance</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="maintenance-create-form">
        <div class="form-group">
          <label for="maintenance-function-create-field">Fungsi:</label>
          <select name="function" class="form-control function-option" id="maintenance-function-create-field" required>
            <option>--Pilih Fungsi--</option>
          </select>
        </div>
        <div class="form-group">
          <label for="maintenance-category-create-field">Kategori:</label>
          <select name="function" class="form-control category-option" id="maintenance-category-create-field" required>
            <option disabled>--Pilih Kategori--</option>
          </select>
        </div>
        <div class="form-group">
          <label for="maintenance-attachment-create-field">Mohon dilampirkan foto kerusakan:</label>
          <input type="file" name="attachment" id="maintenance-attachment-create-field" data-allowed-file-extensions="jpg png jpeg bmp webp" required>
        </div>
        <div class="form-group">
          <label>Keterangan</label>
          <textarea name="notes" id="maintenance-notes-create-field" class="form-control" placeholder="Keterangan.." required required></textarea>
        </div>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-primary" id="maintenance-create-button">Submit</button>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="maintenance-update-modal" data-backdrop="static" data-keyboard="false" tabindex="-1" style="overflow-y: auto !important;">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Ubah Request - Facility Maintenance</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="maintenance-update-form">
        <div class="form-group">
          <label for="maintenance-category-update-field">Kategori:</label>
          <select name="function" class="form-control category-option" id="maintenance-category-update-field" required>
            <option disabled>--Pilih Kategori--</option>
          </select>
        </div>
        <div class="form-group">
          <label for="maintenance-attachment-update-field">Lampiran:</label>
          <input type="file" name="attachment" id="maintenance-attachment-update-field" class="dropify" required>
        </div>
        <div class="form-group">
          <label>Keterangan</label>
          <textarea name="notes" id="maintenance-notes-update-field" class="form-control" placeholder="Keterangan.." required></textarea>
        </div>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-primary" id="maintenance-update-button">Submit</button>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="maintenance-detail-modal" data-backdrop="static" data-keyboard="false" tabindex="-1" style="overflow-y: auto !important;">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Buat Request - Facility Maintenance</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="maintenance-detail-form">
        <div class="form-group">
          <label for="maintenance-function-detail-field">Fungsi:</label>
          <select name="function" class="form-control function-option" id="maintenance-function-detail-field" readonly>
            <option>--Pilih Fungsi--</option>
          </select>
        </div>
        <div class="form-group">
          <label for="maintenance-category-detail-field">Kategori:</label>
          <select name="function" class="form-control category-option" id="maintenance-category-detail-field" readonly>
            <option disabled>--Pilih Kategori--</option>
          </select>
        </div>
        <div class="form-group">
          <label for="maintenance-before-detail-field">Lampiran Sebelum:</label>
          <div id="maintenance-before-detail"></div>
          <!-- <input type="file" name="before_url" id="maintenance-before-detail-field" disabled> -->
        </div>
        <div class="form-group">
          <label for="maintenance-after-detail-field">Lampiran Setelah:</label>
          <div id="maintenance-after-detail"></div>
          <!-- <input type="file" name="after_url" id="maintenance-after-detail-field" disabled> -->
        </div>
        <div class="form-group">
          <label>Keterangan</label>
          <textarea name="notes" id="maintenance-notes-detail-field" class="form-control" placeholder="Keterangan.." required readonly></textarea>
        </div>
        <div class="form-group">
          <label>Status</label>
          <div id="maintenance-status-detail-field"></div>
        </div>
      </div>
      <div class="modal-footer justify-content-end request-detail-footer-modal">
        <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button> -->
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="maintenance-approve-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Mengerjakan Request - Facility Maintenance</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Apakah Anda yakin ingin mengerjakan request Facility Maintenance tersebut?
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
        <button type="button" class="btn btn-primary" id="maintenance-approve-button">Ya</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="maintenance-complete-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Menyelesaikan Request - Facility Maintenance</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="maintenance-complete-form" class="form">
        Apakah Anda yakin ingin menyelesaikan request Facility Maintenance tersebut?
        <div class="form-group">
          <label for="maintenance-attachment-complete-field">Lampiran:</label>
          <input type="file" name="attachment" id="maintenance-attachment-complete-field" required>
        </div>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
        <button type="submit" class="btn btn-primary" id="maintenance-complete-button">Ya</button>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="maintenance-accomplish-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Tutup Request - Facility Maintenance</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Apakah Anda yakin ingin Tutup Request Facility Maintenance tersebut?
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
        <button type="button" class="btn btn-primary" id="maintenance-accomplish-button">Ya</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="maintenance-reject-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Reject Request - Facility Maintenance</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form class="form" id="maintenance-reject-form">
        Apakah Anda yakin ingin reject request Facility Maintenance tersebut?
        <div class="form-group">
          <label>Alasan Reject</label>
          <textarea name="reject_reason" class="form-control" placeholder="Alasan reject.." required></textarea>
        </div>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
        <button type="submit" class="btn btn-danger" id="maintenance-reject-button">Ya</button>
        </form>
      </div>
    </div>
  </div>
</div>