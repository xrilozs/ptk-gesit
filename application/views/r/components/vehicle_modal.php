<div class="modal fade" id="vehicle-create-modal" data-backdrop="static" data-keyboard="false" tabindex="-1" style="overflow-y: auto !important;">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Buat Request - Kendaraan Operasional</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="vehicle-create-form">
        <div class="form-group">
          <label for="vehicle-function-create-field">Fungsi:</label>
          <select name="function" class="form-control function-option" id="vehicle-function-create-field" required>
            <option disabled>--Pilih Fungsi--</option>
          </select>
        </div>
        <div class="form-group">
          <label for="vehicle-total-passenger-create-field">Jumlah Penumpang:</label>
          <input type="number" name="total_passenger" class="form-control" id="vehicle-total-passenger-create-field" placeholder="Jumlah penumpang.." required>
        </div>
        <div class="row">
          <div class="col">
            <div class="form-group">
              <label for="vehicle-request-date-create-field">Tanggal Kebutuhan:</label>
              <input type="text" name="request_date" class="form-control datepicker" id="vehicle-request-date-create-field" placeholder="Tanggal Kebutuhan.." required>
            </div>
          </div>
          <div class="col">
            <div class="form-group">
              <label for="vehicle-request-time-create-field">Jam Kebutuhan:</label>
              <input type="text" name="request_time" class="form-control timepicker" id="vehicle-request-time-create-field" placeholder="Pilih Waktu" required>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col">
            <div class="form-group">
              <label for="vehicle-dest-1-create-field">Tujuan 1 Mobilisasi:</label>
              <input type="text" name="dest_1_mob" class="form-control" id="vehicle-dest-1-create-field" placeholder="Tujuan 1 Mobilisasi.." required>
            </div>
          </div>
          <div class="col">
            <div class="form-group">
              <label for="vehicle-dest-2-create-field">Tujuan 2 Mobilisasi:</label>
              <input type="text" name="dest_2_mob" class="form-control" id="vehicle-dest-2-create-field" placeholder="Tujuan 2 Mobilisasi.." required>
            </div>
          </div>
        </div>
        <div class="form-group">
          <label for="vehicle-driver-create-field">Pengemudi:</label>
          <select name="driver" class="form-control driver-option" id="vehicle-driver-create-field" required>
            <option disabled>--Pilih Pengemudi--</option>
          </select>
        </div>
        <div class="form-group">
          <label for="vehicle-driver-create-field">Apakah berkenan jika pengemudi tidak sesuai?</label>
          <div class="custom-control custom-radio">
            <input type="radio" id="customRadio1-create" name="acceptance_non_preferred_driver" class="custom-control-input" value="1" required>
            <label class="custom-control-label" for="customRadio1-create">Iya</label>
          </div>
          <div class="custom-control custom-radio">
            <input type="radio" id="customRadio2-create" name="acceptance_non_preferred_driver" class="custom-control-input" value="0" required>
            <label class="custom-control-label" for="customRadio2-create">Tidak</label>
          </div>
        </div>
        <div class="form-group">
          <label>Keterangan</label>
          <textarea name="notes" id="vehicle-notes-create-field" class="form-control" placeholder="Keterangan.." required></textarea>
        </div>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-primary" id="vehicle-create-button">Submit</button>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="vehicle-update-modal" data-backdrop="static" data-keyboard="false" tabindex="-1" style="overflow-y: auto !important;">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="overlay" id="vehicle-update-overlay">
          <i class="fas fa-2x fa-sync fa-spin"></i>
      </div>
      <div class="modal-header">
        <h4 class="modal-title">Ubah Request - Kendaraan Operasional</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="vehicle-update-form">
        <div class="form-group">
          <label for="vehicle-school-update-field">Nama Sekolah:</label>
          <input type="text" name="school" class="form-control" id="vehicle-school-update-field" placeholder="Nama Sekolah.." required>
        </div>
        <div class="form-group">
          <label for="vehicle-level-update-field">Tingkat:</label>
          <select name="level" class="form-control"  id="vehicle-level-update-field" required>
            <option disabled>--Pilih Tingkat--</option>
            <option value="SD">SD</option>
            <option value="SMP">SMP</option>
            <option value="SMA">SMA</option>
            <option value="D1">D1</option>
            <option value="D3">D3</option>
            <option value="D4">D4</option>
            <option value="S1">S1</option>
            <option value="S2">S2</option>
            <option value="S3">S3</option>
          </select>
        </div>
        <div class="form-group">
          <label for="vehicle-major-update-field">Jurusan:</label>
          <input type="text" name="major" class="form-control" id="vehicle-major-update-field" placeholder="Jurusan.." required>
        </div>
        <div class="form-group">
          <label for="vehicle-start-year-update-field">Tahun Masuk:</label>
          <input type="number" name="start_year" class="form-control" id="vehicle-start-year-update-field" placeholder="Tahun Masuk.." required>
        </div>
        <div class="form-group">
          <label for="vehicle-end-year-update-field">Tahun Lulus:</label>
          <input type="text" name="end_year" class="form-control" id="vehicle-end-year-update-field" placeholder="Tahun Lulus.." required>
        </div>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-primary" id="vehicle-update-button">Submit</button>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="operational-vehicle-detail-modal" data-backdrop="static" data-keyboard="false" tabindex="-1" style="overflow-y: auto !important;">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Detail Request - Kendaraan Operasional</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="vehicle-detail-form">
        <div class="form-group">
          <label for="vehicle-function-detail-field">Fungsi:</label>
          <select name="function" class="form-control function-option" id="vehicle-function-detail-field" readonly>
            <option disabled>--Pilih Fungsi--</option>
          </select>
        </div>
        <div class="form-group">
          <label for="vehicle-total-passenger-detail-field">Jumlah Penumpang:</label>
          <input type="number" name="total_passenger" class="form-control" id="vehicle-total-passenger-detail-field" placeholder="Jumlah penumpang.." readonly>
        </div>
        <div class="row">
          <div class="col">
            <div class="form-group">
              <label for="vehicle-request-date-detail-field">Tanggal Kebutuhan:</label>
              <input type="text" name="request_date" class="form-control datepicker" id="vehicle-request-date-detail-field" placeholder="Tanggal Kebutuhan.." readonly>
            </div>
          </div>
          <div class="col">
            <div class="form-group">
              <label for="vehicle-request-time-detail-field">Jam Kebutuhan:</label>
              <input type="text" name="request_time" class="form-control" id="vehicle-request-time-detail-field" readonly>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col">
            <div class="form-group">
              <label for="vehicle-dest-1-detail-field">Tujuan 1 Mobilisasi:</label>
              <input type="text" name="dest_1_mob" class="form-control" id="vehicle-dest-1-detail-field" placeholder="Tujuan 1 Mobilisasi.." readonly>
            </div>
          </div>
          <div class="col">
            <div class="form-group">
              <label for="vehicle-dest-2-detail-field">Tujuan 2 Mobilisasi:</label>
              <input type="text" name="dest_2_mob" class="form-control" id="vehicle-dest-2-detail-field" placeholder="Tujuan 2 Mobilisasi.." readonly>
            </div>
          </div>
        </div>
        <div class="form-group">
          <label for="vehicle-driver-detail-field">Pengemudi (Opsional):</label>
          <input type="text" name="driver" class="form-control" id="vehicle-driver-detail-field" placeholder="Pengemudi.." readonly>
        </div>
        <div class="form-group">
          <label>Keterangan</label>
          <textarea name="notes" id="vehicle-notes-detail-field" class="form-control" placeholder="Keterangan.." required readonly></textarea>
        </div>
        <div class="form-group">
          <label for="vehicle-driver-create-field">Apakah berkenan jika pengemudi tidak sesuai?</label>
          <div class="custom-control custom-radio">
            <input type="radio" id="customRadio1-detail" name="acceptance_non_preferred_driver" class="custom-control-input" value="1" disabled>
            <label class="custom-control-label" for="customRadio1-detail">Iya</label>
          </div>
          <div class="custom-control custom-radio">
            <input type="radio" id="customRadio2" name="acceptance_non_preferred_driver" class="custom-control-input" value="0" disabled>
            <label class="custom-control-label" for="customRadio2">Tidak</label>
          </div>
        </div>
        <div class="form-group">
          <label>Status</label>
          <div id="vehicle-status-detail-field"></div>
        </div>
      </div>
      <div class="modal-footer justify-content-end request-detail-footer-modal">
        <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button> -->
        <!-- <button type="submit" class="btn btn-primary" id="vehicle-detail-button">Submit</button> -->
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="vehicle-approve-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Mengerjakan Request - Kendaraan Operasional</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Apakah Anda yakin ingin mengerjakan request kendaraan operasional tersebut?
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
        <button type="button" class="btn btn-primary" id="vehicle-approve-button">Ya</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="vehicle-complete-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Menyelesaikan Request - Kendaraan Operasional</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Apakah Anda yakin ingin menyelesaikan request kendaraan operasional tersebut?
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
        <button type="button" class="btn btn-primary" id="vehicle-complete-button">Ya</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="vehicle-accomplish-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Tutup Request - Kendaraan Operasional</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Apakah Anda yakin ingin Tutup Request kendaraan operasional tersebut?
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
        <button type="button" class="btn btn-primary" id="vehicle-accomplish-button">Ya</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="vehicle-reject-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Reject Request - Kendaraan Operasional</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form class="form" id="vehicle-reject-form">
        Apakah Anda yakin ingin reject request kendaraan operasional tersebut?
        <div class="form-group">
          <label>Alasan Reject</label>
          <textarea name="reject_reason" class="form-control" placeholder="Alasan reject.." required></textarea>
        </div>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
        <button type="submit" class="btn btn-danger" id="vehicle-reject-button">Ya</button>
        </form>
      </div>
    </div>
  </div>
</div>