<div class="modal fade" id="office-stationery-create-modal" data-backdrop="static" data-keyboard="false" tabindex="-1" style="overflow-y: auto !important;">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Buat Request - Alat Tulis Kantor</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="office-stationery-create-form">
        <div class="form-group">
          <label for="office-stationery-function-create-field">Fungsi:</label>
          <select name="function" class="form-control function-option" id="office-stationery-function-create-field" required>
            <option>--Pilih Fungsi--</option>
          </select>
        </div>
        <div class="row bg-light">
          <div class="col-12 pt-2 px-2">
            <label>Alat Tulis Kantor</label>
          </div>
          <div class="col-12 pb-2 px-2">
            <div class="row" id="office-stationery-create-section">
            </div>
            <div class="row mt-2">
              <div class="col-12 text-right">
                <button type="button" class="btn btn-dark" id="office-stationery-add-button">
                  <i class="fas fa-plus"></i>&nbsp;Tambah
                </button>
              </div>
            </div>
          </div>
        </div>
        <div class="form-group">
          <label>Keterangan</label>
          <textarea name="notes" id="office-stationery-notes-create-field" class="form-control" placeholder="Keterangan.." required></textarea>
        </div>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-primary" id="office-stationery-create-button">Submit</button>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="office-stationery-update-modal" data-backdrop="static" data-keyboard="false" tabindex="-1" style="overflow-y: auto !important;">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Ubah Request - Alat Tulis Kantor</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="office-stationery-update-form">
        <div class="row bg-light">
          <div class="col-12 pt-2 px-2">
            <label>Alat Tulis Kantor</label>
          </div>
          <div class="col-12 pb-2 px-2">
            <div class="row" id="office-stationery-update-section">
            </div>
            <div class="row mt-2">
              <div class="col-12 text-right">
                <button type="button" class="btn btn-dark" id="office-stationery-add2-button">
                  <i class="fas fa-plus"></i>&nbsp;Tambah
                </button>
              </div>
            </div>
          </div>
        </div>
        <div class="form-group">
          <label>Keterangan</label>
          <textarea name="notes" id="office-stationery-notes-update-field" class="form-control" placeholder="Keterangan.." required></textarea>
        </div>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-primary" id="office-stationery-update-button">Submit</button>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="office-stationery-detail-modal" data-backdrop="static" data-keyboard="false" tabindex="-1" style="overflow-y: auto !important;">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Detail Request - Alat Tulis Kantor</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="office-stationery-detail-form">
        <div class="form-group">
          <label for="office-stationery-function-detail-field">Fungsi:</label>
          <select name="function" class="form-control function-option" id="office-stationery-function-detail-field" readonly>
            <option disabled>--Pilih Fungsi--</option>
          </select>
        </div>
        <div class="row bg-light">
          <div class="col-12 pt-2 px-2">
            <label>Alat Tulis Kantor</label>
          </div>
          <div class="col-12 pb-2 px-2">
            <div class="row" id="office-stationery-detail-section">
            </div>
          </div>
        </div>
        <div class="form-group">
          <label>Keterangan</label>
          <textarea name="notes" id="office-stationery-notes-detail-field" class="form-control" placeholder="Keterangan.." readonly></textarea>
        </div>
        <div class="form-group">
          <label>Status</label>
          <div id="office-stationery-status-detail-field"></div>
        </div>
      </div>
      <div class="modal-footer justify-content-end request-detail-footer-modal">
        <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button> -->
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="office-stationery-approve-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Mengerjakan Request - Alat Tulis Kantor</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Apakah Anda yakin ingin mengerjakan request alat tulis kantor tersebut?
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
        <button type="button" class="btn btn-primary" id="office-stationery-approve-button">Ya</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="office-stationery-complete-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Menyelesaikan Request - Alat Tulis Kantor</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Apakah Anda yakin ingin menyelesaikan request alat tulis kantor tersebut?
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
        <button type="button" class="btn btn-primary" id="office-stationery-complete-button">Ya</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="office-stationery-accomplish-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Tutup Request - Alat Tulis Kantor</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Apakah Anda yakin ingin Tutup Request alat tulis kantor tersebut?
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
        <button type="button" class="btn btn-primary" id="office-stationery-accomplish-button">Ya</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="office-stationery-reject-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Reject Request - Alat Tulis Kantor</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form class="form" id="office-stationery-reject-form">
        Apakah Anda yakin ingin reject request alat tulis kantor tersebut?
        <div class="form-group">
          <label>Alasan Reject</label>
          <textarea name="reject_reason" class="form-control" placeholder="Alasan reject.." required></textarea>
        </div>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
        <button type="submit" class="btn btn-primary" id="office-stationery-reject-button">Ya</button>
        </form>
      </div>
    </div>
  </div>
</div>