<div class="modal fade" id="office-household-create-modal" data-backdrop="static" data-keyboard="false" tabindex="-1" style="overflow-y: auto !important;">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Buat Request - Rumah Tangga Kantor</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="office-household-create-form">
        <div class="form-group">
          <label for="office-household-function-create-field">Fungsi:</label>
          <select name="function" class="form-control function-option" id="office-household-function-create-field" required>
            <option>--Pilih Fungsi--</option>
          </select>
        </div>
        <div class="row bg-light">
          <div class="col-12 pt-2 px-2">
            <label>Rumah Tangga Kantor</label>
          </div>
          <div class="col-12 pb-2 px-2">
            <div class="row" id="office-household-create-section">
            </div>
            <div class="row mt-2">
              <div class="col-12 text-right">
                <button type="button" class="btn btn-dark" id="office-household-add-button">
                  <i class="fas fa-plus"></i>&nbsp;Tambah
                </button>
              </div>
            </div>
          </div>
        </div>
        <div class="form-group">
          <label>Keterangan</label>
          <textarea name="notes" id="office-household-notes-create-field" class="form-control" placeholder="Keterangan.." required></textarea>
        </div>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-primary" id="office-household-create-button">Submit</button>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="office-household-update-modal" data-backdrop="static" data-keyboard="false" tabindex="-1" style="overflow-y: auto !important;">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Ubah Request - Rumah Tangga Kantor</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="office-household-update-form">
        <div class="row bg-light">
          <div class="col-12 pt-2 px-2">
            <label>Rumah Tangga Kantor</label>
          </div>
          <div class="col-12 pb-2 px-2">
            <div class="row" id="office-household-update-section">
            </div>
            <div class="row mt-2">
              <div class="col-12 text-right">
                <button type="button" class="btn btn-dark" id="office-household-add2-button">
                  <i class="fas fa-plus"></i>&nbsp;Tambah
                </button>
              </div>
            </div>
          </div>
        </div>
        <div class="form-group">
          <label>Keterangan</label>
          <textarea name="notes" id="office-household-notes-update-field" class="form-control" placeholder="Keterangan.." required></textarea>
        </div>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-primary" id="office-household-update-button">Submit</button>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="office-household-detail-modal" data-backdrop="static" data-keyboard="false" tabindex="-1" style="overflow-y: auto !important;">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Detail Request - Rumah Tangga Kantor</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="office-household-detail-form">
        <div class="form-group">
          <label for="office-household-function-detail-field">Fungsi:</label>
          <select name="function" class="form-control function-option" id="office-household-function-detail-field" readonly>
            <option disabled>--Pilih Fungsi--</option>
          </select>
        </div>
        <div class="row bg-light">
          <div class="col-12 pt-2 px-2">
            <label>Rumah Tangga Kantor</label>
          </div>
          <div class="col-12 pb-2 px-2">
            <div class="row" id="office-household-detail-section">
            </div>
          </div>
        </div>
        <div class="form-group">
          <label>Keterangan</label>
          <textarea name="notes" id="office-household-notes-detail-field" class="form-control" placeholder="Keterangan.." readonly></textarea>
        </div>
        <div class="form-group">
          <label>Status</label>
          <div id="office-household-status-detail-field"></div>
        </div>
      </div>
      <div class="modal-footer justify-content-end request-detail-footer-modal">
        <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button> -->
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="office-household-approve-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Mengerjakan Request - Rumah Tangga Kantor</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Apakah Anda yakin ingin mengerjakan request Rumah Tangga Kantor tersebut?
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
        <button type="button" class="btn btn-primary" id="office-household-approve-button">Ya</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="office-household-complete-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Menyelesaikan Request - Rumah Tangga Kantor</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Apakah Anda yakin ingin menyelesaikan request Rumah Tangga Kantor tersebut?
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
        <button type="button" class="btn btn-primary" id="office-household-complete-button">Ya</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="office-household-accomplish-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Tutup Request - Rumah Tangga Kantor</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Apakah Anda yakin ingin Tutup Request Rumah Tangga Kantor tersebut?
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
        <button type="button" class="btn btn-primary" id="office-household-accomplish-button">Ya</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="office-household-reject-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Reject Request - Rumah Tangga Kantor</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form class="form" id="office-household-reject-form">
        Apakah Anda yakin ingin reject request Rumah Tangga Kantor tersebut?
        <div class="form-group">
          <label>Alasan Reject</label>
          <textarea name="reject_reason" class="form-control" placeholder="Alasan reject.." required></textarea>
        </div>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
        <button type="submit" class="btn btn-danger" id="office-household-reject-button">Ya</button>
        </form>
      </div>
    </div>
  </div>
</div>