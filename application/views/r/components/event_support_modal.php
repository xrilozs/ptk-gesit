<div class="modal fade" id="event-support-create-modal" data-backdrop="static" data-keyboard="false" tabindex="-1" style="overflow-y: auto !important;">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Buat Request - Event Support</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="event-support-create-form">
        <div class="form-group">
          <label for="event-support-function-create-field">Fungsi:</label>
          <select name="function" class="form-control function-option" id="event-support-function-create-field" required>
            <option>--Pilih Fungsi--</option>
          </select>
        </div>
        <div class="form-group">
          <label>Nama Agenda Event:</label>
          <input type="text" name="event_name" class="form-control" required>
        </div>
        <div class="form-group">
          <label>Tanggal Agenda Event:</label>
          <input type="text" name="event_date" class="form-control datepicker" required>
        </div>
        <div class="row">
          <div class="col-6">
            <div class="form-group">
              <label>Waktu Mulai:</label>
              <input type="text" name="start_time" class="form-control timepicker" required>
            </div>
          </div>
          <div class="col-6">
            <div class="form-group">
              <label>Waktu Selesai:</label>
              <input type="text" name="end_time" class="form-control timepicker" required>
            </div>
          </div>
        </div>
        <div class="form-group">
          <label>Apakah lokasi event di luar kantor?</label>
          <div class="custom-control custom-radio">
            <input type="radio" id="outside-radio1-create" name="is_outside_office" class="custom-control-input" value="1" required>
            <label class="custom-control-label" for="outside-radio1-create">Iya</label>
          </div>
          <div class="custom-control custom-radio">
            <input type="radio" id="outside-radio2-create" name="is_outside_office" class="custom-control-input" value="0" required>
            <label class="custom-control-label" for="outside-radio2-create">Tidak</label>
          </div>
        </div>
        <div class="form-group" id="location-text-create" style="display:none;">
          <label>Lokasi:</label>
          <input type="text" name="location" class="form-control" required>
        </div>
        <div class="form-group" id="location-option-create" style="display:none;">
          <label>Lokasi:</label>
          <select class="form-control" id="location-create" required>
            <option value="Aula lantai 7">Aula lantai 7</option>
            <option value="Griya Sehat">Griya Sehat</option>
            <option value="Transkopi">Transkopi</option>
            <option value="Lapangan Parkir">Lapangan Parkir</option>
          </select>
        </div>
        <div class="form-group">
          <label>Apakah menggunakan konsultan event organizer?</label>
          <div class="custom-control custom-radio">
            <input type="radio" id="customRadio3-create" name="use_eo" class="custom-control-input" value="1" required>
            <label class="custom-control-label" for="customRadio3-create">Iya</label>
          </div>
          <div class="custom-control custom-radio">
            <input type="radio" id="customRadio4-create" name="use_eo" class="custom-control-input" value="0" required>
            <label class="custom-control-label" for="customRadio4-create">Tidak</label>
          </div>
        </div>
        <div class="row bg-light">
          <div class="col-12 pt-2 px-2">
            <label>Event Support</label>
          </div>
          <div class="col-12 pb-2 px-2">
            <div class="row" id="event-support-create-section">
            </div>
            <div class="row mt-2">
              <div class="col-12 text-right">
                <button type="button" class="btn btn-dark" id="event-support-add-button">
                  <i class="fas fa-plus"></i>&nbsp;Tambah
                </button>
              </div>
            </div>
          </div>
        </div>
        <div class="form-group">
          <label>Keterangan</label>
          <textarea name="notes" id="event-support-notes-create-field" class="form-control" placeholder="Keterangan.." required></textarea>
        </div>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-primary" id="event-support-create-button">Submit</button>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="event-support-update-modal" data-backdrop="static" data-keyboard="false" tabindex="-1" style="overflow-y: auto !important;">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Ubah Request - Event Support</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="event-support-update-form">
        <div class="row bg-light">
          <div class="col-12 pt-2 px-2">
            <label>Event Support</label>
          </div>
          <div class="col-12 pb-2 px-2">
            <div class="row" id="event-support-update-section">
            </div>
            <div class="row mt-2">
              <div class="col-12 text-right">
                <button type="button" class="btn btn-dark" id="event-support-add2-button">
                  <i class="fas fa-plus"></i>&nbsp;Tambah
                </button>
              </div>
            </div>
          </div>
        </div>
        <div class="form-group">
          <label>Keterangan</label>
          <textarea name="notes" id="event-support-notes-update-field" class="form-control" placeholder="Keterangan.." required></textarea>
        </div>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-primary" id="event-support-update-button">Submit</button>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="event-support-detail-modal" data-backdrop="static" data-keyboard="false" tabindex="-1" style="overflow-y: auto !important;">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Detail Request - Event Support</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="event-support-detail-form">
        <div class="form-group">
          <label for="event-support-function-detail-field">Fungsi:</label>
          <select name="function" class="form-control function-option" id="event-support-function-detail-field" readonly>
            <option>--Pilih Fungsi--</option>
          </select>
        </div>
        <div class="form-group">
          <label>Nama Agenda Event:</label>
          <input type="text" name="event_name" class="form-control" readonly>
        </div>
        <div class="form-group">
          <label>Tanggal Agenda Event:</label>
          <input type="text" name="event_date" class="form-control datepicker" readonly>
        </div>
        <div class="row">
          <div class="col-6">
            <div class="form-group">
              <label>Waktu Mulai:</label>
              <input type="text" name="start_time" class="form-control timepicker" readonly>
            </div>
          </div>
          <div class="col-6">
            <div class="form-group">
              <label>Waktu Selesai:</label>
              <input type="text" name="end_time" class="form-control timepicker" readonly>
            </div>
          </div>
        </div>
        <div class="form-group">
          <label>Apakah lokasi event di luar kantor?</label>
          <div class="custom-control custom-radio">
            <input type="radio" id="outside-radio1-detail" name="is_outside_office" class="custom-control-input" value="1" disabled>
            <label class="custom-control-label" for="outside-radio1-detail">Iya</label>
          </div>
          <div class="custom-control custom-radio">
            <input type="radio" id="outside-radio2-detail" name="is_outside_office" class="custom-control-input" value="0" disabled>
            <label class="custom-control-label" for="outside-radio2-detail">Tidak</label>
          </div>
        </div>
        <div class="form-group" id="location-text-detail">
          <label>Lokasi:</label>
          <input type="text" name="location" class="form-control" readonly>
        </div>
        <div class="form-group" id="location-option-detail">
          <label>Lokasi:</label>
          <select class="form-control" id="location-detail" readonly>
            <option value="Aula lantai 7">Aula lantai 7</option>
            <option value="Griya Sehat">Griya Sehat</option>
            <option value="Transkopi">Transkopi</option>
            <option value="Lapangan Parkir">Lapangan Parkir</option>
          </select>
        </div>
        <div class="form-group">
          <label>Apakah menggunakan konsultan event organizer?</label>
          <div class="custom-control custom-radio">
            <input type="radio" id="customRadio3-detail" name="use_eo" class="custom-control-input" value="1" disabled>
            <label class="custom-control-label" for="customRadio3-detail">Iya</label>
          </div>
          <div class="custom-control custom-radio">
            <input type="radio" id="customRadio4-detail" name="use_eo" class="custom-control-input" value="0" disabled>
            <label class="custom-control-label" for="customRadio4-detail">Tidak</label>
          </div>
        </div>
        <div class="row bg-light">
          <div class="col-12 pt-2 px-2">
            <label>Event Support</label>
          </div>
          <div class="col-12 pb-2 px-2">
            <div class="row" id="event-support-detail-section">
            </div>
          </div>
        </div>
        <div class="form-group">
          <label>Keterangan</label>
          <textarea name="notes" id="event-support-notes-detail-field" class="form-control" placeholder="Keterangan.." required readonly></textarea>
        </div>
        <div class="form-group">
          <label>Status</label>
          <div id="event-support-status-detail-field"></div>
        </div>
      </div>
      <div class="modal-footer justify-content-end request-detail-footer-modal">
        <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button> -->
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="event-support-approve-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Mengerjakan Request - Event Support</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Apakah Anda yakin ingin mengerjakan request Event Support tersebut?
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
        <button type="button" class="btn btn-primary" id="event-support-approve-button">Ya</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="event-support-complete-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Menyelesaikan Request - Event Support</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Apakah Anda yakin ingin menyelesaikan request Event Support tersebut?
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
        <button type="button" class="btn btn-primary" id="event-support-complete-button">Ya</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="event-support-accomplish-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Tutup Request - Event Support</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Apakah Anda yakin ingin Tutup Request Event Support tersebut?
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
        <button type="button" class="btn btn-primary" id="event-support-accomplish-button">Ya</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="event-support-reject-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Reject Request - Event Support</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form class="form" id="event-support-reject-form">
        Apakah Anda yakin ingin reject request Event Support tersebut?
        <div class="form-group">
          <label>Alasan Reject</label>
          <textarea name="reject_reason" class="form-control" placeholder="Alasan reject.." required></textarea>
        </div>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
        <button type="submit" class="btn btn-danger" id="event-support-reject-button">Ya</button>
        </form>
      </div>
    </div>
  </div>
</div>