<div class="modal fade" id="courier-and-mailing-create-modal" data-backdrop="static" data-keyboard="false" tabindex="-1" style="overflow-y: auto !important;">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Buat Request - Pengiriman Dokumen</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="courier-and-mailing-create-form">
        <div class="form-group">
          <label for="courier-and-mailing-function-create-field">Fungsi:</label>
          <select name="function" class="form-control function-option" id="courier-and-mailing-function-create-field" required>
            <option>--Pilih Fungsi--</option>
          </select>
        </div>
        <div class="form-group">
          <label>Tanggal Pengiriman</label>
          <input type="text" class="form-control datepicker" name="delivery_date" placeholder="Tanggal pengiriman.." required>
        </div>
        <div class="form-group">
          <label>Tujuan Pengiriman</label>
          <input type="text" class="form-control" name="destination" placeholder="Tujuan pengiriman.." required>
        </div>
        <div class="form-group">
          <label>Nama Penerima</label>
          <input type="text" class="form-control" name="receiver_name" placeholder='Nama penerima..' required>
        </div>
        <div class="form-group">
          <label>Nomor Telepon Penerima</label>
          <div class="input-group flex-nowrap">
            <div class="input-group-prepend">
              <span class="input-group-text" id="addon-wrapping">+62</span>
            </div>
            <input type="text" class="form-control" name="receiver_phone" placeholder="87341231" required>
          </div>
        </div>
        <div class="form-group">
          <label>Keterangan</label>
          <textarea name="notes" id="courier-and-mailing-notes-create-field" class="form-control" placeholder="Keterangan.." required></textarea>
        </div>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-primary" id="courier-and-mailing-create-button">Submit</button>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="courier-and-mailing-update-modal" data-backdrop="static" data-keyboard="false" tabindex="-1" style="overflow-y: auto !important;">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Ubah Request - Pengiriman Dokumen</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="courier-and-mailing-update-form">
        <div class="form-group">
          <label for="courier-and-mailing-function-update-field">Fungsi:</label>
          <select name="function" class="form-control function-option" id="courier-and-mailing-function-update-field" required>
            <option>--Pilih Fungsi--</option>
          </select>
        </div>
        <div class="form-group">
          <label>Tanggal Pengiriman</label>
          <input type="text" class="form-control datepicker" name="delivery_date" required>
        </div>
        <div class="form-group">
          <label>Tujuan Pengiriman</label>
          <input type="text" class="form-control" name="destination" required>
        </div>
        <div class="form-group">
          <label>Nama Penerima</label>
          <input type="text" class="form-control" name="receiver_name" required>
        </div>
        <div class="form-group">
          <label>Nomor Telepon Penerima</label>
          <div class="input-group flex-nowrap">
            <div class="input-group-prepend">
              <span class="input-group-text" id="addon-wrapping">+62</span>
            </div>
            <input type="text" class="form-control" name="receiver_phone" placeholder="87341231" required>
          </div>
        </div>
        <div class="form-group">
          <label>Keterangan</label>
          <textarea name="notes" id="courier-and-mailing-notes-update-field" class="form-control" placeholder="Keterangan.." required></textarea>
        </div>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-primary" id="courier-and-mailing-update-button">Submit</button>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="courier-and-mailing-detail-modal" data-backdrop="static" data-keyboard="false" tabindex="-1" style="overflow-y: auto !important;">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Detail Request - Pengiriman Dokumen</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="courier-and-mailing-detail-form">
        <div class="form-group">
          <label for="courier-and-mailing-function-detail-field">Fungsi:</label>
          <select name="function" class="form-control function-option" id="courier-and-mailing-function-detail-field" readonly>
            <option>--Pilih Fungsi--</option>
          </select>
        </div>
        <div class="form-group">
          <label>Tanggal Pengiriman</label>
          <input type="text" class="form-control datepicker" name="delivery_date" readonly>
        </div>
        <div class="form-group">
          <label>Tujuan Pengiriman</label>
          <input type="text" class="form-control" name="destination" readonly>
        </div>
        <div class="form-group">
          <label>Nama Penerima</label>
          <input type="text" class="form-control" name="receiver_name" readonly>
        </div>
        <div class="form-group">
          <label>Nomor Telepon Penerima</label>
          <div class="input-group flex-nowrap">
            <div class="input-group-prepend">
              <span class="input-group-text" id="addon-wrapping">+62</span>
            </div>
            <input type="text" class="form-control" name="receiver_phone" placeholder="87341231" readonly>
          </div>
        </div>
        <div class="form-group">
          <label>Keterangan</label>
          <textarea name="notes" id="courier-and-mailing-notes-detail-field" class="form-control" placeholder="Keterangan.." readonly></textarea>
        </div>
        <div class="form-group">
          <label>Status</label>
          <div id="courier-and-mailing-status-detail-field"></div>
        </div>
      </div>
      <div class="modal-footer justify-content-end request-detail-footer-modal">
        <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button> -->
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="courier-and-mailing-approve-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Mengerjakan Request - Pengiriman Dokumen</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Apakah Anda yakin ingin mengerjakan request Pengiriman Dokumen tersebut?
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
        <button type="button" class="btn btn-primary" id="courier-and-mailing-approve-button">Ya</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="courier-and-mailing-complete-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Menyelesaikan Request - Pengiriman Dokumen</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Apakah Anda yakin ingin menyelesaikan request Pengiriman Dokumen tersebut?
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
        <button type="button" class="btn btn-primary" id="courier-and-mailing-complete-button">Ya</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="courier-and-mailing-accomplish-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Tutup Request - Pengiriman Dokumen</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Apakah Anda yakin ingin Tutup Request Pengiriman Dokumen tersebut?
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
        <button type="button" class="btn btn-primary" id="courier-and-mailing-accomplish-button">Ya</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="courier-and-mailing-reject-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Reject Request - Pengiriman Dokumen</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form class="form" id="courier-and-mailing-reject-form">
        Apakah Anda yakin ingin reject request Pengiriman Dokumen tersebut?
        <div class="form-group">
          <label>Alasan Reject</label>
          <textarea name="reject_reason" class="form-control" placeholder="Alasan reject.." required></textarea>
        </div>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
        <button type="submit" class="btn btn-danger" id="courier-and-mailing-reject-button">Ya</button>
        </form>
      </div>
    </div>
  </div>
</div>