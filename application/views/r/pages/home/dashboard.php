<div class="row mb-5">
    <div class="col-12 d-flex justify-content-center">
        <div class="btn-group" role="group" aria-label="Basic example">
            <a href="<?=base_url("r/dashboard");?>" class="btn btn-primary">Request baru</a>
            <a href="<?=base_url("r/history");?>" class="btn btn-light">Daftar Request</a>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-3 col-md-4 col-6 mb-4">
        <button class="btn btn-link" style="color:#025fa4;" data-toggle="modal" data-target="#vehicle-create-modal" id="vehicle-create-toggle">
            <img src="<?=base_url("assets/img/logo_text_operational_vehicle.png");?>" style="width:150px;"><br><br>
            <!-- <i class="fas fa-car fa-5x"></i><br><br> -->
            <!-- <span class="font-weight-bold">Kendaran Operasional</span> -->
        </button>
    </div>
    <div class="col-lg-3 col-md-4 col-6 mb-4">
        <button class="btn btn-link" style="color:#025fa4;" data-toggle="modal" data-target="#meeting-create-modal" id="meeting-create-toggle">
            <img src="<?=base_url("assets/img/logo_text_meeting_room.png");?>" style="width:150px;"><br><br>
            <!-- <i class="fas fa-building fa-5x"></i><br><br> -->
            <!-- <span class="font-weight-bold">Ruang Rapat</span> -->
        </button>
    </div>
    <div class="col-lg-3 col-md-4 col-6 mb-4">
        <button class="btn btn-link" style="color:#025fa4;" data-toggle="modal" data-target="#meeting-consumption-create-modal" id="meeting-consumption-create-toggle">
            <img src="<?=base_url("assets/img/logo_text_meeting_consumption.png");?>" style="width:150px;"><br><br>
            <!-- <i class="fas fa-utensils fa-5x"></i><br><br> -->
            <!-- <span class="font-weight-bold">Konsumsi Rapat</span> -->
        </button>
    </div>
    <div class="col-lg-3 col-md-4 col-6 mb-4">
        <button class="btn btn-link" style="color:#025fa4;" data-toggle="modal" data-target="#office-stationery-create-modal" id="office-stationery-create-toggle">
            <img src="<?=base_url("assets/img/logo_text_office_stationery.png");?>" style="width:150px;"><br><br>
            <!-- <i class="fas fa-folder-open fa-5x"></i><br><br> -->
            <!-- <span class="font-weight-bold">Alat Tulis Kantor</span> -->
        </button>
    </div>
    <div class="col-lg-3 col-md-4 col-6 mb-4">
        <button class="btn btn-link" style="color:#025fa4;" data-toggle="modal" data-target="#maintenance-create-modal" id="maintenance-create-toggle">
            <img src="<?=base_url("assets/img/logo_text_maintenance.png");?>" style="width:150px;"><br><br>
            <!-- <i class="fas fa-hammer fa-5x"></i><br><br> -->
            <!-- <span class="font-weight-bold">Facility Maintenance</span> -->
        </button>
    </div>
    <div class="col-lg-3 col-md-4 col-6 mb-4">
        <button class="btn btn-link" style="color:#025fa4;" data-toggle="modal" data-target="#office-household-create-modal" id="office-household-create-toggle">
            <img src="<?=base_url("assets/img/logo_text_office_household.png");?>" style="width:150px;"><br><br>
            <!-- <i class="fas fa-hammer fa-5x"></i><br><br> -->
            <!-- <span class="font-weight-bold">Office Household</span> -->
        </button>
    </div>
    <div class="col-lg-3 col-md-4 col-6 mb-4">
        <button class="btn btn-link" style="color:#025fa4;" data-toggle="modal" data-target="#fuel-and-ecard-create-modal" id="fuel-and-ecard-create-toggle">
            <img src="<?=base_url("assets/img/logo_text_fuel_and_ecard.png");?>" style="width:150px;"><br><br>
            <!-- <i class="fas fa-hammer fa-5x"></i><br><br> -->
            <!-- <span class="font-weight-bold">Facility Maintenance</span> -->
        </button>
    </div>
    <div class="col-lg-3 col-md-4 col-6 mb-4">
        <button class="btn btn-link" style="color:#025fa4;" data-toggle="modal" data-target="#workstation-and-furniture-create-modal" id="workstation-and-furniture-create-toggle">
            <img src="<?=base_url("assets/img/logo_text_workstation_and_furniture.png");?>" style="width:150px;"><br><br>
            <!-- <i class="fas fa-hammer fa-5x"></i><br><br> -->
            <!-- <span class="font-weight-bold">Facility Maintenance</span> -->
        </button>
    </div>
    <div class="col-lg-3 col-md-4 col-6 mb-4">
        <button class="btn btn-link" style="color:#025fa4;" data-toggle="modal" data-target="#consumable-create-modal" id="consumable-create-toggle">
            <img src="<?=base_url("assets/img/logo_text_consumable.png");?>" style="width:150px;"><br><br>
            <!-- <i class="fas fa-hammer fa-5x"></i><br><br> -->
            <!-- <span class="font-weight-bold">Facility Maintenance</span> -->
        </button>
    </div>
    <div class="col-lg-3 col-md-4 col-6 mb-4">
        <button class="btn btn-link" style="color:#025fa4;" data-toggle="modal" data-target="#courier-and-mailing-create-modal" id="courier-and-mailing-create-toggle">
            <img src="<?=base_url("assets/img/logo_text_courier_and_mailing.png");?>" style="width:150px;"><br><br>
            <!-- <i class="fas fa-hammer fa-5x"></i><br><br> -->
            <!-- <span class="font-weight-bold">Facility Maintenance</span> -->
        </button>
    </div>
    <div class="col-lg-3 col-md-4 col-6 mb-4">
        <button class="btn btn-link" style="color:#025fa4;" data-toggle="modal" data-target="#event-support-create-modal" id="event-support-create-toggle">
            <img src="<?=base_url("assets/img/logo_text_event_support.png");?>" style="width:150px;"><br><br>
            <!-- <i class="fas fa-hammer fa-5x"></i><br><br> -->
            <!-- <span class="font-weight-bold">Facility Maintenance</span> -->
        </button>
    </div>
    <div class="col-lg-3 col-md-4 col-6 mb-4">
        <button class="btn btn-link" style="color:#025fa4;" data-toggle="modal" data-target="#asset-create-modal" id="asset-create-toggle">
            <img src="<?=base_url("assets/img/logo_text_data_asset.png");?>" style="width:150px;"><br><br>
            <!-- <i class="fas fa-scroll fa-5x"></i><br><br> -->
            <!-- <span class="font-weight-bold">Data Aset</span> -->
        </button>
    </div>
    <div class="col-lg-3 col-md-4 col-6 mb-4">
        <button class="btn btn-link" style="color:#025fa4;" data-toggle="modal" data-target="#other-create-modal" id="other-create-toggle">
            <img src="<?=base_url("assets/img/logo_text_other.png");?>" style="width:150px;"><br><br>
            <!-- <i class="fas fa-feather fa-5x"></i><br><br> -->
            <!-- <span class="font-weight-bold">Lainnya</span> -->
        </button>
    </div>
</div>