<?php
    require 'vendor/autoload.php';
    // require_once APPPATH . '../vendor/autoload.php';

    function generate_requisition_list($request, $header){
        $CI   =& get_instance();
        $mpdf = new \Mpdf\Mpdf([
            'mode' => 'utf-8'
        ]);

        $data = array(
            "request"  => $request,
            "header"   => $header
        );
        
        $html           = $CI->load->view('template/requisition_list', $data, true);
        $mpdf->WriteHTML($html);
        $filename = str_replace(['/', '\\'], '_', $request->request_number);
        $file_location  = 'assets/requesition_list/'.$filename.'.pdf';
        $mpdf->Output($file_location, 'F');

        return $file_location;
    }
?>