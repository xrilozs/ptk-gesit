<?php
    function send_reject_email($email, $request, $header){
        $CI         =& get_instance();
        $data       = array(
            "request"      => $request,
            "header"    => $header
        );
        $html       = $CI->load->view('template/reject_email', $data, true);
        $mail       = new Send_mail();
        $emailResp  = $mail->send($email, 'Request Anda Direject', $html);
        return $emailResp;
    }
?>