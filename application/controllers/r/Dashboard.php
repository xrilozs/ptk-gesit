<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

    public function page()
	{
		$data = array(
			"js_file" 	=> array(
				base_url('assets/js/r/request_workstation_and_furniture.js?v=').time(),
				base_url('assets/js/r/request_courier_and_mailing.js?v=').time(),
				base_url('assets/js/r/request_fuel_and_ecard.js?v=').time(),
				base_url('assets/js/r/request_consumable.js?v=').time(),
				base_url('assets/js/r/request_event_support.js?v=').time(),
				base_url('assets/js/r/request_office_household.js?v=').time(),
				base_url('assets/js/r/request_other.js?v=').time(),
				base_url('assets/js/r/request_asset.js?v=').time(),
				base_url('assets/js/r/request_maintenance.js?v=').time(),
				base_url('assets/js/r/request_office_stationery.js?v=').time(),
				base_url('assets/js/r/request_meeting_consumption.js?v=').time(),
				base_url('assets/js/r/request_meeting.js?v=').time(),
				base_url('assets/js/r/request_vehicle.js?v=').time(),
				base_url('assets/js/r/dashboard.js?v=').time()
			),
			"title" => "Dashboard"
		);
		$this->load->view('r/layout/main_header', $data);
		$this->load->view('r/pages/home/dashboard');
		$this->load->view('r/layout/main_footer', $data);
		$this->load->view('r/components/vehicle_modal', $data);
		$this->load->view('r/components/meeting_modal', $data);
		$this->load->view('r/components/meeting_consumption_modal', $data);
		$this->load->view('r/components/office_stationery_modal', $data);
		$this->load->view('r/components/maintenance_modal', $data);
		$this->load->view('r/components/asset_modal', $data);
		$this->load->view('r/components/other_modal', $data);
		$this->load->view('r/components/consumable_modal', $data);
		$this->load->view('r/components/courier_and_mailing_modal', $data);
		$this->load->view('r/components/fuel_and_ecard_modal', $data);
		$this->load->view('r/components/event_support_modal', $data);
		$this->load->view('r/components/office_household_modal', $data);
		$this->load->view('r/components/workstation_and_furniture_modal', $data);
	}

	public function history()
	{
		$data = array(
			"js_file" 	=> array(
				base_url('assets/js/r/request_workstation_and_furniture.js?v=').time(),
				base_url('assets/js/r/request_courier_and_mailing.js?v=').time(),
				base_url('assets/js/r/request_fuel_and_ecard.js?v=').time(),
				base_url('assets/js/r/request_consumable.js?v=').time(),
				base_url('assets/js/r/request_event_support.js?v=').time(),
				base_url('assets/js/r/request_office_household.js?v=').time(),
				base_url('assets/js/r/request_other.js?v=').time(),
				base_url('assets/js/r/request_asset.js?v=').time(),
				base_url('assets/js/r/request_maintenance.js?v=').time(),
				base_url('assets/js/r/request_office_stationery.js?v=').time(),
				base_url('assets/js/r/request_meeting_consumption.js?v=').time(),
				base_url('assets/js/r/request_meeting.js?v=').time(),
				base_url('assets/js/r/request_vehicle.js?v=').time(),
				base_url('assets/js/r/history.js?v=').time()
			),
			"title" => "Daftar Request"
		);
		$this->load->view('r/layout/main_header', $data);
		$this->load->view('r/pages/home/history');
		$this->load->view('r/layout/main_footer', $data);
		$this->load->view('r/components/vehicle_modal', $data);
		$this->load->view('r/components/meeting_modal', $data);
		$this->load->view('r/components/meeting_consumption_modal', $data);
		$this->load->view('r/components/office_stationery_modal', $data);
		$this->load->view('r/components/maintenance_modal', $data);
		$this->load->view('r/components/asset_modal', $data);
		$this->load->view('r/components/other_modal', $data);
		$this->load->view('r/components/consumable_modal', $data);
		$this->load->view('r/components/courier_and_mailing_modal', $data);
		$this->load->view('r/components/fuel_and_ecard_modal', $data);
		$this->load->view('r/components/event_support_modal', $data);
		$this->load->view('r/components/office_household_modal', $data);
		$this->load->view('r/components/workstation_and_furniture_modal', $data);
	}
}
?>
