<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Building extends CI_Controller {

	public function page(){
		$data = array(
			"js_file" 	=> array(
				base_url('assets/js/sa/building.js?v=').time()
			),
			"title"		=> "Gedung"
		);

		$this->load->view('sa/layout/main_header', $data);
		$this->load->view('sa/layout/main_sidebar', $data);
		$this->load->view('sa/pages/building/page', $data);
		$this->load->view('sa/layout/main_footer', $data);
	}
}
