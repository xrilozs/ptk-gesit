<?php
    defined('BASEPATH') OR exit('No direct script access allowed');

    class Config extends CI_Controller {
        public function page(){
            $data = array(
                "js_file" => array(
                    base_url('assets/js/sa/config.js?v=').time()
                ),
                "title"		=> "Pengaturan"
            );
    
            $this->load->view('sa/layout/main_header', $data);
            $this->load->view('sa/layout/main_sidebar', $data);
            $this->load->view('sa/pages/config/page');
            $this->load->view('sa/layout/main_footer', $data);
        }

    }
?>