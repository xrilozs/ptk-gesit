<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Meeting_room extends CI_Controller {

	public function page(){
		$data = array(
			"js_file" 	=> array(
				base_url('assets/js/sa/meeting_room.js?v=').time()
			),
			"title"		=> "Ruang meeting"
		);

		$this->load->view('sa/layout/main_header', $data);
		$this->load->view('sa/layout/main_sidebar', $data);
		$this->load->view('sa/pages/meeting_room/page', $data);
		$this->load->view('sa/layout/main_footer', $data);
	}
}
