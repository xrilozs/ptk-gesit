<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends CI_Controller {

	public function asset_page(){
		$data = array(
			"js_file" 			=> array(
				base_url('assets/js/sa/category.js?v=').time()
			),
			"title"				=> "Kategori Aset",
			"type"				=> "asset",
			"category_type" 	=> "Aset"
		);

		$this->load->view('sa/layout/main_header', $data);
		$this->load->view('sa/layout/main_sidebar', $data);
		$this->load->view('sa/pages/category/page', $data);
		$this->load->view('sa/layout/main_footer', $data);
	}

	public function function_page(){
		$data = array(
			"js_file" 			=> array(
				base_url('assets/js/sa/category.js?v=').time()
			),
			"title"				=> "Kategori Fungsi",
			"type"				=> "function",
			"category_type" 	=> "Fungsi"
		);

		$this->load->view('sa/layout/main_header', $data);
		$this->load->view('sa/layout/main_sidebar', $data);
		$this->load->view('sa/pages/category/page', $data);
		$this->load->view('sa/layout/main_footer', $data);
	}

	public function maintenance_page(){
		$data = array(
			"js_file" 			=> array(
				base_url('assets/js/sa/category.js?v=').time()
			),
			"title"				=> "Kategori Pemeliharaan",
			"type"				=> "maintenance",
			"category_type" 	=> "Pemeliharaan"
		);

		$this->load->view('sa/layout/main_header', $data);
		$this->load->view('sa/layout/main_sidebar', $data);
		$this->load->view('sa/pages/category/page', $data);
		$this->load->view('sa/layout/main_footer', $data);
	}

	public function additional_request_page(){
		$data = array(
			"js_file" 			=> array(
				base_url('assets/js/sa/category.js?v=').time()
			),
			"title"				=> "Kategori Permintaan Tambahan",
			"type"				=> "additional_request",
			"category_type" 	=> "Permintaan Tambahan"
		);

		$this->load->view('sa/layout/main_header', $data);
		$this->load->view('sa/layout/main_sidebar', $data);
		$this->load->view('sa/pages/category/page', $data);
		$this->load->view('sa/layout/main_footer', $data);
	}

	public function meeting_consumption_page(){
		$data = array(
			"js_file" 			=> array(
				base_url('assets/js/sa/category.js?v=').time()
			),
			"title"				=> "Kategori Konsumsi Meeting",
			"type"				=> "meeting_consumption",
			"category_type" 	=> "Konsumsi Meeting"
		);

		$this->load->view('sa/layout/main_header', $data);
		$this->load->view('sa/layout/main_sidebar', $data);
		$this->load->view('sa/pages/category/page', $data);
		$this->load->view('sa/layout/main_footer', $data);
	}

	public function office_stationary_page(){
		$data = array(
			"js_file" 			=> array(
				base_url('assets/js/sa/category.js?v=').time()
			),
			"title"				=> "Kategori ATK",
			"type"				=> "office_stationery",
			"category_type" 	=> "ATK"
		);

		$this->load->view('sa/layout/main_header', $data);
		$this->load->view('sa/layout/main_sidebar', $data);
		$this->load->view('sa/pages/category/page', $data);
		$this->load->view('sa/layout/main_footer', $data);
	}

	public function office_household_page(){
		$data = array(
			"js_file" 			=> array(
				base_url('assets/js/sa/category.js?v=').time()
			),
			"title"				=> "Kategori Rumah Tangga Kantor",
			"type"				=> "office_household",
			"category_type" 	=> "Rumah Tangga Kantor"
		);

		$this->load->view('sa/layout/main_header', $data);
		$this->load->view('sa/layout/main_sidebar', $data);
		$this->load->view('sa/pages/category/page', $data);
		$this->load->view('sa/layout/main_footer', $data);
	}

	public function consumable_page(){
		$data = array(
			"js_file" 			=> array(
				base_url('assets/js/sa/category.js?v=').time()
			),
			"title"				=> "Kategori Barang Habis Pakai",
			"type"				=> "consumable",
			"category_type" 	=> "Barang Habis Pakai"
		);

		$this->load->view('sa/layout/main_header', $data);
		$this->load->view('sa/layout/main_sidebar', $data);
		$this->load->view('sa/pages/category/page', $data);
		$this->load->view('sa/layout/main_footer', $data);
	}

	public function event_support_page(){
		$data = array(
			"js_file" 			=> array(
				base_url('assets/js/sa/category.js?v=').time()
			),
			"title"				=> "Kategori Event Support",
			"type"				=> "event_support",
			"category_type" 	=> "Event Support"
		);

		$this->load->view('sa/layout/main_header', $data);
		$this->load->view('sa/layout/main_sidebar', $data);
		$this->load->view('sa/pages/category/page', $data);
		$this->load->view('sa/layout/main_footer', $data);
	}

	public function fuel_and_ecard_page(){
		$data = array(
			"js_file" 			=> array(
				base_url('assets/js/sa/category.js?v=').time()
			),
			"title"				=> "Kategori Bahan Bakar dan Kartu Elektronik",
			"type"				=> "fuel_and_ecard",
			"category_type" 	=> "Bahan Bakar dan Kartu Elektronik"
		);

		$this->load->view('sa/layout/main_header', $data);
		$this->load->view('sa/layout/main_sidebar', $data);
		$this->load->view('sa/pages/category/page', $data);
		$this->load->view('sa/layout/main_footer', $data);
	}

	public function courier_and_mailing_page(){
		$data = array(
			"js_file" 			=> array(
				base_url('assets/js/sa/category.js?v=').time()
			),
			"title"				=> "Kategori Kurir dan Pesan",
			"type"				=> "courier_and_mailing",
			"category_type" 	=> "Kurir dan Pesan"
		);

		$this->load->view('sa/layout/main_header', $data);
		$this->load->view('sa/layout/main_sidebar', $data);
		$this->load->view('sa/pages/category/page', $data);
		$this->load->view('sa/layout/main_footer', $data);
	}

	public function workstation_and_furniture_page(){
		$data = array(
			"js_file" 			=> array(
				base_url('assets/js/sa/category.js?v=').time()
			),
			"title"				=> "Kategori Tempat Kerja dan Perabotan",
			"type"				=> "workstation_and_furniture",
			"category_type" 	=> "Tempat Kerja dan Perabotan"
		);

		$this->load->view('sa/layout/main_header', $data);
		$this->load->view('sa/layout/main_sidebar', $data);
		$this->load->view('sa/pages/category/page', $data);
		$this->load->view('sa/layout/main_footer', $data);
	}
}
