<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

    public function page()
	{
		$data = array(
			"js_file" 	=> array(
				base_url('assets/js/sa/dashboard.js?v=').time()
			),
			"title" => "Dashboard"
		);
		$this->load->view('sa/layout/main_header', $data);
		$this->load->view('sa/layout/main_sidebar', $data);
		$this->load->view('sa/pages/home/dashboard');
		$this->load->view('sa/layout/main_footer', $data);
	}
}
?>
