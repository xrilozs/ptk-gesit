<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');    // cache for 1 day
}

// Access-Control headers are received during OPTIONS requests
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");         

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}

class Category extends CI_Controller {

    protected $category_enum = array(
        'asset'                     => "category_asset",
        'function'                  => "category_function",
        'maintenance'               => "category_maintenance",
        'meeting_consumption'       => "category_meeting_consumption",
        'office_stationery'         => "category_office_stationery",
        'additional_request'        => "category_additional_request",
        'consumable'                => "category_consumable",
        'courier_and_mailing'       => "category_courier_and_mailing",
        'event_support'             => "category_event_support",
        'fuel_and_ecard'            => "category_fuel_and_ecard",
        'office_household'          => "category_office_household",
        'workstation_and_furniture' => "category_workstation_and_furniture"
    );

    public function __construct(){
        parent::__construct();
        $this->load->helper('url_helper');
    }

    #path: /api/category [GET]
    function get_category(){
        #init req & resp
        $resp_obj   = new Response_api();
        $page_number= $this->input->get('page_number');
        $page_size  = $this->input->get('page_size');
        $search     = $this->input->get('search');
        $type       = $this->input->get('type');
        $draw       = $this->input->get('draw');
        $params     = array($type);

        #check token
        $header      = $this->input->request_headers();
        $verify_resp = verify_user_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/category [GET] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }

        #check request params
        if(!check_parameter($params)){
            logging('error', "/api/category [GET] - Missing parameter. please check API documentation", array('page_number'=>$page_number, 'page_size'=>$page_size, 'type'=>$type));
            $resp->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp->get_response());
            return;
        }

        if(!isset($this->category_enum[$type])){
            logging('error', "/api/category [GET] - Type is not valid");
            $resp_obj->set_response(400, "failed", "Type is not valid");
            set_output($resp_obj->get_response());
            return;
        }

        #get category asset
        $start              = $page_number * $page_size;
        $limit              = array('start'=>$start, 'size'=>$page_size);
        $order              = array('field'=>'created_at', 'order'=>'DESC');
        $model_name         = "{$this->category_enum[$type]}_model";
        $get_function_name  = "get_{$this->category_enum[$type]}";
        $count_function_name= "count_{$this->category_enum[$type]}";
        $category           = $this->$model_name->$get_function_name($search, $order, $limit);
        $total              = $this->$model_name->$count_function_name($search);

        #response
        if(empty($draw)){
            logging('debug', '/api/category [GET] - Get category is success');
            $resp_obj->set_response(200, "success", "Get category is success", $category);
            set_output($resp_obj->get_response());
            return;
        }else{
            logging('debug', '/api/category [GET] - Get category is success');
            $resp_obj->set_response_datatable(200, $category, $draw, $total, $total);
            set_output($resp_obj->get_response_datatable());
            return;
        }
    }

    #path: /api/category/id/$id [GET]
    function get_category_by_id($id){
        $resp_obj = new Response_api();
        $type     = $this->input->get('type');

        #check token
        $header      = $this->input->request_headers();
        $verify_resp = verify_user_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/category [GET] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }

        if(!isset($this->category_enum[$type])){
            logging('error', "/api/category/id/$id [GET] - Type is not valid");
            $resp_obj->set_response(400, "failed", "Type is not valid");
            set_output($resp_obj->get_response());
            return;
        }

        #get category detail
        $model_name         = "{$this->category_enum[$type]}_model";
        $get_function_name  = "get_{$this->category_enum[$type]}_by_id";
        $category           = $this->$model_name->$get_function_name($id);
        if(is_null($category)){
            logging('error', "/api/category/id/$id [GET] - category $type not found");
            $resp_obj->set_response(404, "failed", "category $type not found");
            set_output($resp_obj->get_response());
            return;
        }

        #response
        logging('debug', '/api/category/id/'.$id.' [GET] - Get category by id success', $category);
        $resp_obj->set_response(200, "success", "Get category by id success", $category);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /api/category [POST]
    function create_category(){
        #init req & res
        $resp_obj   = new Response_api();
        $request    = json_decode($this->input->raw_input_stream, true);
        $type       = $this->input->get('type');
        
        #check token
        $header = $this->input->request_headers();
        $resp = verify_user_token($header, "SUPERADMIN");
        if($resp['status'] == 'failed'){
            logging('error', '/api/category [POST] - '.$resp['message']);
            set_output($resp);
            return;
        }
        
        #check request params
        $keys = array('name');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/api/category [POST] - Missing parameter. please check API documentation', $request);
            $resp_obj->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp_obj->get_response());
            return;
        }

        if(!isset($this->category_enum[$type])){
            logging('error', "/api/category [POST] - Type is not valid");
            $resp_obj->set_response(400, "failed", "Type is not valid");
            set_output($resp_obj->get_response());
            return;
        }

        #create category
        $request['id']          = get_uniq_id();
        $model_name             = "{$this->category_enum[$type]}_model";
        $create_function_name   = "create_{$this->category_enum[$type]}";
        $flag                   = $this->$model_name->$create_function_name($request);
        
        #response
        if(!$flag){
            logging('error', '/api/category [POST] - Internal server error', $request);
            $resp_obj->set_response(500, "failed", "Internal server error");
            set_output($resp_obj->get_response());
            return;
        }
        logging('debug', '/api/category [POST] - Create category success', $request);
        $resp_obj->set_response(200, "success", "Create category success", $request);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /api/category [PUT]
    function update_category(){
        $resp_obj = new Response_api();
        $request  = json_decode($this->input->raw_input_stream, true);
        $type     = $this->input->get('type');

        #check token
        $header = $this->input->request_headers();
        $resp   = verify_user_token($header, "SUPERADMIN");
        if($resp['status'] == 'failed'){
            logging('error', '/api/category [PUT] - '.$resp['message']);
            set_output($resp);
            return;
        }
        
        #check request params
        $keys = array('id', 'name');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/api/category [PUT] - Missing parameter. please check API documentation', $request);
            $resp_obj->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp_obj->get_response());
            return;
        }

        #check category asset
        $id                 = $request['id'];
        $model_name         = "{$this->category_enum[$type]}_model";
        $get_function_name  = "get_{$this->category_enum[$type]}_by_id";
        $category           = $this->$model_name->$get_function_name($id);
        if(is_null($category)){
            logging('error', "/api/category [PUT] - category $type not found", $request);
            $resp_obj->set_response(404, "failed", "category $type not found");
            set_output($resp_obj->get_response());
            return;
        }

        #update category asset
        $update_function_name   = "update_{$this->category_enum[$type]}";
        $flag                   = $this->$model_name->$update_function_name($request, $id);
        
        #response
        if(!$flag){
            logging('debug', '/api/category [PUT] - Data does not change', $request);
            $resp_obj->set_response(200, "success", "Data does not change");
            set_output($resp_obj->get_response());
            return;
        }
        logging('debug', "/api/category [PUT] - Update category $type success", $request);
        $resp_obj->set_response(200, "success", "Update category $type success", $request);
        set_output($resp_obj->get_response());
        return;
    }
  
    #path: /api/category/delete/$id [DELETE]
    function delete_category($id){
        $resp_obj = new Response_api();
        $type     = $this->input->get('type');

        #check token
        $header = $this->input->request_headers();
        $resp   = verify_user_token($header, "SUPERADMIN");
        if($resp['status'] == 'failed'){
            logging('error', '/api/category/delete/'.$id.' [DELETE] - '.$resp['message']);
            set_output($resp);
            return;
        }

        #check category asset
        $model_name         = "{$this->category_enum[$type]}_model";
        $get_function_name  = "get_{$this->category_enum[$type]}_by_id";
        $category           = $this->$model_name->$get_function_name($id);
        if(is_null($category)){
            logging('error', "/api/category/delete/$id [DELETE] - category $type not found");
            $resp_obj->set_response(404, "failed", "category $type not found");
            set_output($resp_obj->get_response());
            return;
        }

        #delete category asset
        $delete_function_name   = "delete_{$this->category_enum[$type]}";
        $flag                   = $this->$model_name->$delete_function_name($id);

        #response
        if(!$flag){
            logging('error', '/api/category/delete/'.$id.' [DELETE] - Internal server error');
            $resp_obj->set_response(500, "failed", "Internal server error");
            set_output($resp_obj->get_response());
            return;
        }
        logging('debug', "/api/category/delete/$id [DELETE] - Delete category $type success");
        $resp_obj->set_response(200, "success", "Delete category $type success");
        set_output($resp_obj->get_response());
        return;
    }
  
}