<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');    // cache for 1 day
}

// Access-Control headers are received during OPTIONS requests
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");         

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}

require_once APPPATH . 'core/MY_Request.php';

class Request_event_support extends MY_Request {
    public function __construct(){
        parent::__construct();
        $this->load->helper('url_helper');
    }

    #path: /api/request/event-support [POST]
    function create_request_data(){
        #init req & res
        $resp_obj   = new Response_api();
        $request    = json_decode($this->input->raw_input_stream, true);
        
        #check token
        $header = $this->input->request_headers();
        $resp   = verify_user_token($header, "REQUESTOR");
        if($resp['status'] == 'failed'){
            logging('error', '/api/request/event-support [POST] - '.$resp['message']);
            set_output($resp);
            return;
        }
        $requestor = $resp['data']['user'];
        
        #check request params
        $keys = array('category_function_id', 'notes', "event_name", "event_date", "start_time", "end_time", "is_outside_office", "location", "use_eo", 'items');
        $param_res = check_parameter_by_keysV2($request, $keys);
        if(!$param_res['success']){
            logging('error', '/api/request/event-support [POST] - '.$param_res['message'], $request);
            $resp_obj->set_response(400, "failed", $param_res['message']);
            set_output($resp_obj->get_response());
            return;
        }

        #begin transaction
        $this->db->trans_begin();

        #create request 
        $request_id = $this->create_request($request, "EVENT SUPPORT");
        if(is_null($request_id)){
            $this->db->trans_rollback();
            $err_msg = $this->db->error();

            logging('debug', '/api/request/event-support [POST] - Create request event support failed', array("error"=>$err_msg));
            $resp_obj->set_response(400, "failed", "Create request event support failed", array("error"=>$err_msg));
            set_output($resp_obj->get_response());
            return;
        }

        $request['id']          = get_uniq_id();
        $request['request_id']  = $request_id;
        $flag = $this->data_event_support_model->create_data_event_support($request);
        if(!$flag){
            $this->db->trans_rollback();
            $err_msg = $this->db->error();

            logging('debug', '/api/request/event-support [POST] - Create request event support failed', array("error"=>$err_msg));
            $resp_obj->set_response(400, "failed", "Create request event support failed", array("error"=>$err_msg));
            set_output($resp_obj->get_response());
            return;
        }

        #create request data
        $count_success = 0;
        if(count($request['items']) > 0){
            foreach ($request['items'] as $item_request) {
                #check request params
                $keys = array('category_event_support_id', 'qty');
                if(!check_parameter_by_keys($item_request, $keys)){
                    break;
                }
                
                $item_request['id']                     = get_uniq_id();
                $item_request['data_event_support_id']  = $request['id'];
                $success = $this->data_event_support_items_model->create_data_event_support_items($item_request);
                if(!$success){
                    break;
                }
                $count_success++;
            }
        }
        if($count_success < count($request['items'])){
            $this->db->trans_rollback();
            $err_msg = $this->db->error();

            logging('error', 'Create request failed', array("error"=>$err_msg));
            $resp_obj->set_response(400, "failed", "Create request failed", array("error"=>$err_msg));
            set_output($resp_obj->get_response());
            return;
        }

        if($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
            $err_msg = $this->db->error();

            logging('error', 'Create request failed', array("error"=>$err_msg));
            $resp_obj->set_response(400, "failed", "Create request failed", array("error"=>$err_msg));
            set_output($resp_obj->get_response());
            return;
        }
        $this->db->trans_commit();

        logging('debug', '/api/request/event-support [POST] - Create request event support success', $request);
        $resp_obj->set_response(200, "success", "Create request event support success", $request);
        set_output($resp_obj->get_response());
        return;
    }
}
