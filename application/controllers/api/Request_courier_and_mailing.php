<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');    // cache for 1 day
}

// Access-Control headers are received during OPTIONS requests
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");         

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}

require_once APPPATH . 'core/MY_Request.php';

class Request_courier_and_mailing extends MY_Request {
    public function __construct(){
        parent::__construct();
        $this->load->helper('url_helper');
    }

    #path: /api/request/courier-and-mailing [POST]
    function create_request_data(){
        #init req & res
        $resp_obj   = new Response_api();
        $request    = json_decode($this->input->raw_input_stream, true);
        
        #check token
        $header = $this->input->request_headers();
        $resp   = verify_user_token($header, "REQUESTOR");
        if($resp['status'] == 'failed'){
            logging('error', '/api/request/courier-and-mailing [POST] - '.$resp['message']);
            set_output($resp);
            return;
        }
        $requestor = $resp['data']['user'];
        
        #check request params
        $keys = array('category_function_id', 'notes', 'delivery_date', 'destination', 'receiver_name', 'receiver_phone');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/api/request/courier-and-mailing [POST] - Missing parameter. please check API documentation', $request);
            $resp_obj->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp_obj->get_response());
            return;
        }

        #begin transaction
        $this->db->trans_begin();

        #create request 
        $request_id = $this->create_request($request, "COURIER AND MAILING");
        if(is_null($request_id)){
            $this->db->trans_rollback();
            $err_msg = $this->db->error();

            logging('debug', '/api/request/courier-and-mailing [POST] - Create request courier and mailing failed', array("error"=>$err_msg));
            $resp_obj->set_response(400, "failed", "Create request courier and mailing failed", array("error"=>$err_msg));
            set_output($resp_obj->get_response());
            return;
        }

        #create request data        
        $request['id']         = get_uniq_id();
        $request['request_id'] = $request_id;
        $flag = $this->data_courier_and_mailing_model->create_data_courier_and_mailing($request);
        if(!$flag){
            $this->db->trans_rollback();
            $err_msg = $this->db->error();

            logging('error', 'Create request failed', array("error"=>$err_msg));
            $resp->set_response(400, "failed", "Create request failed", array("error"=>$err_msg));
            set_output($resp->get_response());
            return;
        }

        if($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
        }else{
            $this->db->trans_commit();
        }

        logging('debug', '/api/request/courier-and-mailing [POST] - Create request courier and mailing success', $request);
        $resp_obj->set_response(200, "success", "Create request courier and mailing success", $request);
        set_output($resp_obj->get_response());
        return;
    }
}
