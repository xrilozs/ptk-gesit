<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');    // cache for 1 day
}

// Access-Control headers are received during OPTIONS requests
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");         

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}

require_once APPPATH . 'core/MY_Request.php';

class Request_meeting_room extends MY_Request {
    public function __construct(){
        parent::__construct();
        $this->load->helper('url_helper');
    }

    #path: /api/request/meeting-room [POST]
    function create_request_data(){
        #init req & res
        $resp_obj   = new Response_api();
        $request    = json_decode($this->input->raw_input_stream, true);
        
        #check token
        $header = $this->input->request_headers();
        $resp   = verify_user_token($header, "REQUESTOR");
        if($resp['status'] == 'failed'){
            logging('error', '/api/request/meeting-room [POST] - '.$resp['message']);
            set_output($resp);
            return;
        } 
        $requestor = $resp['data']['user'];
        
        #check request params
        $keys = array('category_function_id', 'meeting_room_id', 'meeting_type', 'meeting_title', 'total_participant', 'meeting_date', 'start_time', 'end_time', 'meeting_invitation_url', 'additional_request');
        $param_res = check_parameter_by_keysV2($request, $keys);
        if(!$param_res['success']){
            logging('error', '/api/request/meeting-room [POST] - '.$param_res['message'], $request);
            $resp_obj->set_response(400, "failed", $param_res['message']);
            set_output($resp_obj->get_response());
            return;
        }

        $category_function = $this->category_function_model->get_category_function_by_id($request['category_function_id']);
        if(is_null($category_function)){
            logging('error', '/api/request/meeting-room [POST] - Category function is not found', $request);
            $resp_obj->set_response(400, "failed", "Category function is not found");
            set_output($resp_obj->get_response());
            return;
        }

        $meeting_room = $this->meeting_room_model->get_meeting_room_by_id($request['meeting_room_id']);
        if(is_null($meeting_room)){
            logging('error', '/api/request/meeting-room [POST] - meeting room is not found', $request);
            $resp_obj->set_response(400, "failed", "meeting room is not found");
            set_output($resp_obj->get_response());
            return;
        }
        // if($meeting_room->status != "AVAILABLE"){
        //     logging('error', '/api/request/meeting-room [POST] - meeting room is not available', $request);
        //     $resp_obj->set_response(400, "failed", "meeting room is not available");
        //     set_output($resp_obj->get_response());
        //     return;
        // }

        #begin transaction
        $this->db->trans_begin();

        // $flag_meeting_room = $this->meeting_room_model->book_meeting_room($meeting_room->id);
        // if(!$flag_meeting_room){
        //     $this->db->trans_rollback();
        //     $err_msg = $this->db->error();

        //     logging('error', 'Book meeting room failed', array("error"=>$err_msg));
        //     $resp->set_response(400, "failed", "Book meeting room failed", array("error"=>$err_msg));
        //     set_output($resp->get_response());
        //     return;
        // }

        #create request 
        $request_id = $this->create_request($request, "MEETING ROOM");
        if(is_null($request_id)){
            $this->db->trans_rollback();
            $err_msg = $this->db->error();

            logging('debug', '/api/driver [POST] - Create request meeting room failed', array("error"=>$err_msg));
            $resp_obj->set_response(400, "failed", "Create request opertion vehicle failed", array("error"=>$err_msg));
            set_output($resp_obj->get_response());
            return;
        }

        #create request data
        $meeting_room_id            = get_uniq_id();
        $request['id']              = $meeting_room_id;
        $request['request_id']      = $request_id;
        $request['requestor_name']  = $requestor->fullname;
        $request['requestor_email'] = $requestor->email;
        $flag_data                  = $this->data_meeting_room_model->create_data_meeting_room($request);

        $count_success = 0;
        if(count($request['additional_request']) > 0){
            foreach ($request['additional_request'] as $additional_request) {
                #check request params
                $keys = array('category_additional_request_id', 'qty');
                if(!check_parameter_by_keys($additional_request, $keys)){
                    break;
                }
                
                $additional_request['id']           = get_uniq_id();
                $additional_request['data_meeting_id']   = $meeting_room_id;
                $success = $this->data_additional_request_model->create_data_additional_request($additional_request);
                if(!$success){
                    break;
                }
                $count_success++;
            }
        }
        if(!$flag_data || $count_success < count($request['additional_request'])){
            $this->db->trans_rollback();
            $err_msg = $this->db->error();

            logging('error', 'Create request failed', array("error"=>$err_msg));
            $resp_obj->set_response(400, "failed", "Create request failed", array("error"=>$err_msg));
            set_output($resp_obj->get_response());
            return;
        }

        if($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
            $err_msg = $this->db->error();

            logging('error', 'Create request failed', array("error"=>$err_msg));
            $resp_obj->set_response(400, "failed", "Create request failed", array("error"=>$err_msg));
            set_output($resp_obj->get_response());
            return;
        }
        $this->db->trans_commit();

        logging('debug', '/api/driver [POST] - Create request meeting room success', $request);
        $resp_obj->set_response(200, "success", "Create request opertion vehicle success", $request);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /api/request/meeting-room/id/$1 [GET]
    function get_request_data_by_id($id){
        #init req & res
        $resp_obj   = new Response_api();
        $request    = json_decode($this->input->raw_input_stream, true);
        
        #check token
        $header = $this->input->request_headers();
        $resp   = verify_user_token($header);
        if($resp['status'] == 'failed'){
            logging('error', '/api/request/meeting-room/id/$1 [GET] - '.$resp['message']);
            set_output($resp);
            return;
        } 
        $requestor = $resp['data']['user'];

        $request_data = $this->data_meeting_room_model->get_data_meeting_room_by_id($id);
        if(is_null($request_data)){
            logging('error', '/api/request/meeting-room/id/$1 [GET] - Request data not found');
            $resp_obj->set_response(400, "failed", "Request data not found");
            set_output($resp_obj->get_response());
            return;
        }

        logging('debug', '/api/request/meeting-room/id/$1 [GET] - Get request data success');
        $resp_obj->set_response(200, "success", "Get request data success", $request_data);
        set_output($resp_obj->get_response());
        return;
    }
}
