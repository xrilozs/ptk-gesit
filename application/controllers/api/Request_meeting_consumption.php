<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');    // cache for 1 day
}

// Access-Control headers are received during OPTIONS requests
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");         

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}

require_once APPPATH . 'core/MY_Request.php';

class Request_meeting_consumption extends MY_Request {
    public function __construct(){
        parent::__construct();
        $this->load->helper('url_helper');
    }

    #path: /api/request/meeting-consumption [POST]
    function create_request_data(){
        #init req & res
        $resp_obj   = new Response_api();
        $request    = json_decode($this->input->raw_input_stream, true);
        
        #check token
        $header = $this->input->request_headers();
        $resp   = verify_user_token($header, "REQUESTOR");
        if($resp['status'] == 'failed'){
            logging('error', '/api/request/meeting-consumption [POST] - '.$resp['message']);
            set_output($resp);
            return;
        }
        $requestor = $resp['data']['user'];
        
        #check request params
        $keys = array('category_function_id', 'data_meeting_id', 'notes', 'items');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/api/request/meeting-consumption [POST] - Missing parameter. please check API documentation', $request);
            $resp_obj->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp_obj->get_response());
            return;
        }

        $data_meeting = $this->data_meeting_room_model->get_data_meeting_room_by_id($request['data_meeting_id']);
        if(is_null($data_meeting)){
            logging('error', '/api/request/meeting-consumption [POST] - Data meeting is not found', $request);
            $resp_obj->set_response(400, "failed", "Data meeting is not found");
            set_output($resp_obj->get_response());
            return;
        }
        if($data_meeting->meeting_status != "ON PROGRESS"){
            logging('error', '/api/request/meeting-consumption [POST] - Data meeting is not on progress', $request);
            $resp_obj->set_response(400, "failed", "Data meeting is not on progress");
            set_output($resp_obj->get_response());
            return;
        }
        if($data_meeting->meeting_type == "INTERNAL"){
            logging('error', '/api/request/meeting-consumption [POST] - Cannot request consumption on internal meeting', $request);
            $resp_obj->set_response(400, "failed", "Cannot request consumption on internal meeting");
            set_output($resp_obj->get_response());
            return;
        }

        #begin transaction
        $this->db->trans_begin();

        #create request 
        $request_id = $this->create_request($request, "MEETING CONSUMPTION");
        if(is_null($request_id)){
            $this->db->trans_rollback();
            $err_msg = $this->db->error();

            logging('debug', '/api/request/meeting-consumption [POST] - Create request meeting consumption failed', array("error"=>$err_msg));
            $resp_obj->set_response(400, "failed", "Create request opertion vehicle failed", array("error"=>$err_msg));
            set_output($resp_obj->get_response());
            return;
        }

        #create request data
        $count_success = 0;
        if(count($request['items']) > 0){
            foreach ($request['items'] as $item_request) {
                #check request params
                $keys = array('category_meeting_consumption_id', 'qty');
                if(!check_parameter_by_keys($item_request, $keys)){
                    break;
                }
                
                $item_request['id']                 = get_uniq_id();
                $item_request['request_id']         = $request_id;
                $item_request['data_meeting_id']    = $request['data_meeting_id'];
                $success = $this->data_meeting_consumption_model->create_data_meeting_consumption($item_request);
                if(!$success){
                    break;
                }
                $count_success++;
            }
        }
        if($count_success < count($request['items'])){
            $this->db->trans_rollback();
            $err_msg = $this->db->error();

            logging('error', 'Create request failed', array("error"=>$err_msg));
            $resp_obj->set_response(400, "failed", "Create request failed", array("error"=>$err_msg));
            set_output($resp_obj->get_response());
            return;
        }

        if($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
            $err_msg = $this->db->error();

            logging('error', 'Create request failed', array("error"=>$err_msg));
            $resp_obj->set_response(400, "failed", "Create request failed", array("error"=>$err_msg));
            set_output($resp_obj->get_response());
            return;
        }
        $this->db->trans_commit();

        logging('debug', '/api/request/meeting-consumption [POST] - Create request meeting consumption success', $request);
        $resp_obj->set_response(200, "success", "Create request meeting consumption success", $request);
        set_output($resp_obj->get_response());
        return;
    }
}
