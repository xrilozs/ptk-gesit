<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');    // cache for 1 day
}

// Access-Control headers are received during OPTIONS requests
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");         

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}

require_once APPPATH . 'core/MY_Request.php';

class Request_operation_vehicle extends MY_Request {
    public function __construct(){
        parent::__construct();
        $this->load->helper('url_helper');
    }

    #path: /api/request/operation-vehicle [POST]
    function create_request_data(){
        #init req & res
        $resp_obj   = new Response_api();
        $request    = json_decode($this->input->raw_input_stream, true);
        
        #check token
        $header = $this->input->request_headers();
        $resp   = verify_user_token($header, "REQUESTOR");
        if($resp['status'] == 'failed'){
            logging('error', '/api/request/operation-vehicle [POST] - '.$resp['message']);
            set_output($resp);
            return;
        } 
        
        #check request params
        $keys = array('category_function_id', 'total_passenger', 'request_date', 'request_time', 'dest_1_mob', 'dest_2_mob');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/api/request/operation-vehicle [POST] - Missing parameter. please check API documentation', $request);
            $resp_obj->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp_obj->get_response());
            return;
        }

        $category_function = $this->category_function_model->get_category_function_by_id($request['category_function_id']);
        if(is_null($category_function)){
            logging('error', '/api/request/operation-vehicle [POST] - Category function is not found', $request);
            $resp_obj->set_response(400, "failed", "Category function is not found");
            set_output($resp_obj->get_response());
            return;
        }

        #begin transaction
        $this->db->trans_begin();

        if(isset($request['driver_id'])){
            $driver = $this->driver_model->get_driver_by_id($request['driver_id']);
            if(is_null($driver)){
                $this->db->trans_rollback();
                logging('error', '/api/request/operation-vehicle [POST] - driver is not found', $request);
                $resp_obj->set_response(400, "failed", "driver is not found");
                set_output($resp_obj->get_response());
                return;
            }
            if($driver->status != "AVAILABLE"){
                $this->db->trans_rollback();
                logging('error', '/api/request/operation-vehicle [POST] - driver is not available', $request);
                $resp_obj->set_response(400, "failed", "driver is not available");
                set_output($resp_obj->get_response());
                return;
            }
        }

        #create request 
        $request_id = $this->create_request($request, "OPERATIONAL VEHICLE");
        if(is_null($request_id)){
            $this->db->trans_rollback();
            $err_msg = $this->db->error();

            logging('debug', '/api/driver [POST] - Create request operation vehicle failed', array("error"=>$err_msg));
            $resp_obj->set_response(400, "failed", "Create request opertion vehicle failed", array("error"=>$err_msg));
            set_output($resp_obj->get_response());
            return;
        }

        #create request data
        $request['id']          = get_uniq_id();
        $request['request_id']  = $request_id;
        $flag_data              = $this->data_operational_vehicle_model->create_data_operational_vehicle($request);
        if(!$flag_data){
            $this->db->trans_rollback();
            $err_msg = $this->db->error();

            logging('error', 'Create request failed', array("error"=>$err_msg));
            $resp_obj->set_response(400, "failed", "Create request failed", array("error"=>$err_msg));
            set_output($resp_obj->get_response());
            return;
        }

        if($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
        }else{
            $this->db->trans_commit();
        }       

        logging('debug', '/api/driver [POST] - Create request operation vehicle success', $request);
        $resp_obj->set_response(200, "success", "Create request opertion vehicle success", $request);
        set_output($resp_obj->get_response());
        return;
    }
}
