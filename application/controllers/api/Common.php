<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');
}

if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");         

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}

require_once APPPATH . 'core/MY_Request.php';

class Common extends MY_Request {
  protected $data_models = array(
    "OTHER"                     => "data_other",
    "OPERATIONAL VEHICLE"       => "data_operational_vehicle",
    "OFFICE STATIONERY"         => "data_office_stationery",
    "MEETING ROOM"              => "data_meeting_room",
    "MEETING CONSUMPTION"       => "data_meeting_consumption",
    "MAINTENANCE"               => "data_maintenance",
    "ASSET"                     => "data_asset",
    "OFFICE HOUSEHOLD"          => "data_office_household",
    "WORKSTATION AND FURNITURE" => "data_workstation_and_furniture",
    "FUEL AND ECARD"            => "data_fuel_and_ecard",
    "COURIER AND MAILING"       => "data_courier_and_mailing",
    "CONSUMABLE"                => "data_consumable",
    "EVENT SUPPORT"             => "data_event_support"
  );

  protected $data_headers = array(
    "OTHER"                     => array(
                                        "key"   => array("category_function"),
                                        "title" => array("Fungsi")
                                    ),
    "OPERATIONAL VEHICLE"       =>  array(
                                        "key"   => array("category_function", "total_passenger", "request_date", "request_time", "dest_1_mob", "dest_2_mob", "driver_name"),
                                        "title" => array("Fungsi", "Jumlah Penumpang", "Tanggal", "Waktu", "Tujuan #1", "Tujuan #2", "Pengemudi")
                                    ),
    "OFFICE STATIONERY"         =>  array(
                                        "key"   => array("category_function", "category_office_stationery", "qty"),
                                        "title" => array("Fungsi", "Item", "Jumlah")
                                    ),
    "MEETING ROOM"              =>  array(
                                        "key"   => array("category_function", "meeting_room", "meeting_type", "meeting_title", "total_participant", "meeting_date", "start_time", "end_time"),
                                        "title" => array("Fungsi", "Ruangan", "Tipe", "Judul", "Total Peserta", "Tanggal", "Waktu Mulai", "Waktu Akhir")
                                    ),
    "MEETING CONSUMPTION"       =>  array(
                                        "key"   => array("category_function", "category_meeting_consumption", "qty"),
                                        "title" => array("Fungsi", "Item", "Jumlah")
                                    ),
    "MAINTENANCE"               =>  array(
                                        "key"   => array("category_function", "category_maintenance"),
                                        "title" => array("Fungsi", "Item")
                                    ),
    "ASSET"                     =>  array(
                                        "key"   => array("category_function", "category_asset"),
                                        "title" => array("Fungsi", "Item")
                                    ),
    "OFFICE HOUSEHOLD"          =>  array(
                                        "key"   => array("category_function", "category_office_household", "qty"),
                                        "title" => array("Fungsi", "Item", "Jumlah")
                                    ),
    "WORKSTATION AND FURNITURE" =>  array(
                                        "key"   => array("category_function", "category_workstation_and_furniture", "qty"),
                                        "title" => array("Fungsi", "Item", "Jumlah")
                                    ),
    "FUEL AND ECARD"            =>  array(
                                        "key"   => array("category_function", "category_fuel_and_ecard"),
                                        "title" => array("Fungsi", "Item")
                                    ),
    "COURIER AND MAILING"       =>  array(
                                        "key"   => array("category_function", "delivery_date", "destination", "receiver_name", "receiver_phone"),
                                        "title" => array("Fungsi", "Tanggal Pengiriman", "Tujuan Pengiriman", "Nama Penerima", "Telepon Penerima")
                                    ),
    "CONSUMABLE"                =>  array(
                                        "key"   => array("category_function", "category_consumable", "qty"),
                                        "title" => array("Fungsi", "Item", "Jumlah")
                                    ),
    "EVENT SUPPORT"             =>  array(
                                        "key"   => array("category_function", "event_name", "event_date", "start_time", "end_time", "location"),
                                        "itemKey"   => array("category_event_support", "qty"),
                                        "title" => array("Fungsi", "Nama Agenda", "Tanggal Agenda", "Waktu Mulai", "Waktu Selesai", "Lokasi"),
                                        "itemTitle" => array("Item", "Jumlah")
                                    )
  );

  public function __construct($config = 'rest'){
    parent::__construct($config);
  }
  
  function index(){
    $respObj = new Response_api();
    $respObj->set_response(200, "success", "Not Implemented.");
    $resp = $respObj->get_response();
    set_output($resp);
  }
  
  function test(){
    $respObj = new Response_api();
    $respObj->set_response(200, "success", "APIs is working.");
    $resp = $respObj->get_response();
    set_output($resp);
  }
  
  function test_email($id){
    $respObj = new Response_api();

    $request = $this->get_request_detail($id);
    if(is_null($request)){
      $respObj->set_response(400, "failed", "generate requisition failed");
      $resp = $respObj->get_response();
      set_output($resp);
      return;
    }
    
    $emailResp = "";
    $requestor = $this->user_model->get_user_by_id($request->requestor_id);
    if($requestor && $requestor->email){
      #generate requisition list
      $header             = $this->data_headers[$request->request_category];
      $model_name         = "{$this->data_models[$request->request_category]}_model";
      $function_name      = "get_{$this->data_models[$request->request_category]}";
      $request->data      = $this->$model_name->$function_name(null, $request->id);
      $emailResp          = send_reject_email($requestor->email, $request, $header);
    }

    $respObj->set_response(200, "success", "Send email...", $emailResp);
    $resp = $respObj->get_response();
    set_output($resp);
  }

  function generate_password($plain){
    $respObj = new Response_api();
    $hash = password_hash($plain, PASSWORD_DEFAULT);

    $respObj = new Response_api();
    $respObj->set_response(200, "success", "generate password success", $hash);
    $resp = $respObj->get_response();
    set_output($resp);
  }
  
  function generate_requisition_list($id){
    $respObj = new Response_api();

    $request = $this->get_request_detail($id);
    if(is_null($request)){
      $respObj->set_response(400, "failed", "generate requisition failed");
      $resp = $respObj->get_response();
      set_output($resp);
      return;
    }
    
    $header             = $this->data_headers[$request->request_category];
    $model_name         = "{$this->data_models[$request->request_category]}_model";
    $function_name      = "get_{$this->data_models[$request->request_category]}";
    $request->data      = $this->$model_name->$function_name(null, $id);
    $request->history   = $this->request_history_model->get_request_history_by_request_id($id);
    if($request->request_category == "EVENT SUPPORT"){
      $data_id        = $request->data[0]->id;
      $model_item     = "{$this->data_models[$request->request_category]}_items_model";
      $function_item  = $function_name."_items";
      $request->items = $this->$model_item->$function_item(null, $data_id);
    }

    $requisition_url = generate_requisition_list($request, $header);

    $respObj->set_response(200, "success", "generate requisition success", base_url($requisition_url));
    $resp = $respObj->get_response();
    set_output($resp);
  }
}

?>