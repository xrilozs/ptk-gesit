<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');    // cache for 1 day
}

// Access-Control headers are received during OPTIONS requests
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");         

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}

class Building extends CI_Controller {
    public function __construct(){
        parent::__construct();
        $this->load->helper('url_helper');
    }

    #path: /api/building/total [GET]
    function get_building_total(){
        #init req & resp
        $resp_obj   = new Response_api();

        #check token
        $header      = $this->input->request_headers();
        $verify_resp = verify_user_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/building/total [GET] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }

        #get building total
        $total = $this->building_model->count_building();

        #response
        logging('debug', '/api/building/total [GET] - Get building total is success');
        $resp_obj->set_response(200, "success", "Get building total is success", $total);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /api/building [GET]
    function get_building(){
        #init req & resp
        $resp_obj   = new Response_api();
        $page_number= $this->input->get('page_number');
        $page_size  = $this->input->get('page_size');
        $search     = $this->input->get('search');
        $draw       = $this->input->get('draw');
        $params     = array($page_number, $page_size);

        #check token
        $header      = $this->input->request_headers();
        $verify_resp = verify_user_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/building [GET] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }

        #check request params
        // if(!check_parameter($params)){
        //     logging('error', "/api/building [GET] - Missing parameter. please check API documentation", array('page_number'=>$page_number, 'page_size'=>$page_size));
        //     $resp_obj->set_response(400, "failed", "Missing parameter. please check API documentation");
        //     set_output($resp_obj->get_response());
        //     return;
        // }

        #get building 
        $start      = $page_number * $page_size;
        $limit      = null;
        if($start && $page_size){
            $limit  = array('start'=>$start, 'size'=>$page_size);
        }
        $order      = array('field'=>'created_at', 'order'=>'DESC');
        $building   = $this->building_model->get_building($search, $order, $limit);
        $total      = $this->building_model->count_building($search);

        #response
        if(empty($draw)){
            logging('debug', '/api/building [GET] - Get building is success');
            $resp_obj->set_response(200, "success", "Get building is success", $building);
            set_output($resp_obj->get_response());
            return;
        }else{
            logging('debug', '/api/building [GET] - Get building is success');
            $resp_obj->set_response_datatable(200, $building, $draw, $total, $total);
            set_output($resp_obj->get_response_datatable());
            return;
        }
    }

    #path: /api/building/id/$id [GET]
    function get_building_by_id($id){
        $resp_obj = new Response_api();
        $type     = $this->input->get('type');

        #check token
        $header      = $this->input->request_headers();
        $verify_resp = verify_user_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/building [GET] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }

        #get building detail
        $building = $this->building_model->get_building_by_id($id);
        if(is_null($building)){
            logging('error', "/api/building/id/$id [GET] - building not found");
            $resp_obj->set_response(404, "failed", "building not found");
            set_output($resp_obj->get_response());
            return;
        }

        #response
        logging('debug', '/api/building/id/'.$id.' [GET] - Get building by id success', $building);
        $resp_obj->set_response(200, "success", "Get building by id success", $building);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /api/building [POST]
    function create_building(){
        #init req & res
        $resp_obj   = new Response_api();
        $request    = json_decode($this->input->raw_input_stream, true);
        $type       = $this->input->get('type');
        
        #check token
        $header = $this->input->request_headers();
        $resp = verify_user_token($header, "SUPERADMIN");
        if($resp['status'] == 'failed'){
            logging('error', '/api/building [POST] - '.$resp['message']);
            set_output($resp);
            return;
        }
        
        #check request params
        $keys = array('name');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/api/building [POST] - Missing parameter. please check API documentation', $request);
            $resp_obj->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp_obj->get_response());
            return;
        }

        #init variable
        $request['id']  = get_uniq_id();
        $flag           = $this->building_model->create_building($request);
        
        #response
        if(!$flag){
            logging('error', '/api/building [POST] - Internal server error', $request);
            $resp_obj->set_response(500, "failed", "Internal server error");
            set_output($resp_obj->get_response());
            return;
        }
        logging('debug', '/api/building [POST] - Create building  success', $request);
        $resp_obj->set_response(200, "success", "Create building  success", $request);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /api/building [PUT]
    function update_building(){
        $resp_obj = new Response_api();
        $request  = json_decode($this->input->raw_input_stream, true);

        #check token
        $header = $this->input->request_headers();
        $resp   = verify_user_token($header, "SUPERADMIN");
        if($resp['status'] == 'failed'){
            logging('error', '/api/building [PUT] - '.$resp['message']);
            set_output($resp);
            return;
        }
        
        #check request params
        $keys = array('id', 'name');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/api/building [PUT] - Missing parameter. please check API documentation', $request);
            $resp_obj->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp_obj->get_response());
            return;
        }

        #check building
        $building = $this->building_model->get_building_by_id($request['id']);
        if(is_null($building)){
            logging('error', "/api/building [PUT] - building not found", $request);
            $resp_obj->set_response(404, "failed", "building not found");
            set_output($resp_obj->get_response());
            return;
        }

        #update building
        $flag = $this->building_model->update_building($request, $request['id']);
        
        #response
        if(!$flag){
            logging('debug', '/api/building [PUT] - Data does not change', $request);
            $resp_obj->set_response(200, "success", "Data does not change");
            set_output($resp_obj->get_response());
            return;
        }
        logging('debug', "/api/building [PUT] - Update building success", $request);
        $resp_obj->set_response(200, "success", "Update building success", $request);
        set_output($resp_obj->get_response());
        return;
    }
  
    #path: /api/building/delete/$id [DELETE]
    function delete_building($id){
        $resp_obj = new Response_api();
        $type     = $this->input->get('type');

        #check token
        $header = $this->input->request_headers();
        $resp   = verify_user_token($header, "SUPERADMIN");
        if($resp['status'] == 'failed'){
            logging('error', '/api/building/delete/'.$id.' [DELETE] - '.$resp['message']);
            set_output($resp);
            return;
        }

        #check building
        $building = $this->building_model->get_building_by_id($id);
        if(is_null($building)){
            logging('error', "/api/building/delete/$id [DELETE] - building not found");
            $resp_obj->set_response(404, "failed", "building not found");
            set_output($resp_obj->get_response());
            return;
        }

        #delete building
        $flag = $this->building_model->delete_building($id);

        #response
        if(!$flag){
            logging('error', '/api/building/delete/'.$id.' [DELETE] - Internal server error');
            $resp_obj->set_response(500, "failed", "Internal server error");
            set_output($resp_obj->get_response());
            return;
        }
        logging('debug', "/api/building/delete/$id [DELETE] - Delete building success");
        $resp_obj->set_response(200, "success", "Delete building success");
        set_output($resp_obj->get_response());
        return;
    }
}