<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');    // cache for 1 day
}

// Access-Control headers are received during OPTIONS requests
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");         

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}

class Meeting_room extends CI_Controller {
    public function __construct(){
        parent::__construct();
        $this->load->helper('url_helper');
    }

    #path: /api/meeting-room/total [GET]
    function get_meeting_room_total(){
        #init req & resp
        $resp_obj   = new Response_api();

        #check token
        $header      = $this->input->request_headers();
        $verify_resp = verify_user_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/meeting-room/total [GET] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }

        #get meeting room total
        $total = $this->meeting_room_model->count_meeting_room();

        #response
        logging('debug', '/api/meeting-room/total [GET] - Get meeting room total is success');
        $resp_obj->set_response(200, "success", "Get meeting room total is success", $total);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /api/meeting-room [GET]
    function get_meeting_room(){
        #init req & resp
        $resp_obj   = new Response_api();
        $page_number= $this->input->get('page_number');
        $page_size  = $this->input->get('page_size');
        $search     = $this->input->get('search');
        $status     = $this->input->get('status');
        $building_id= $this->input->get('building_id');
        $draw       = $this->input->get('draw');
        $params     = array($page_number, $page_size);

        #check token
        $header      = $this->input->request_headers();
        $verify_resp = verify_user_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/meeting-room [GET] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }

        #check request params
        // if(!check_parameter($params)){
        //     logging('error', "/api/meeting-room [GET] - Missing parameter. please check API documentation", array('page_number'=>$page_number, 'page_size'=>$page_size));
        //     $resp->set_response(400, "failed", "Missing parameter. please check API documentation");
        //     set_output($resp->get_response());
        //     return;
        // }

        #get meeting room 
        $start          = $page_number * $page_size;
        $limit          = null;
        if($start && $page_size){
            $limit      = array('start'=>$start, 'size'=>$page_size);
        }
        $order          = array('field'=>'created_at', 'order'=>'DESC');
        $meeting_room   = $this->meeting_room_model->get_meeting_room($search, $building_id, $status, $order, $limit);
        $total          = $this->meeting_room_model->count_meeting_room($search, $building_id, $status);

        #response
        if(empty($draw)){
            logging('debug', '/api/meeting-room [GET] - Get meeting room is success');
            $resp_obj->set_response(200, "success", "Get meeting room is success", $meeting_room);
            set_output($resp_obj->get_response());
            return;
        }else{
            logging('debug', '/api/meeting-room [GET] - Get meeting room is success');
            $resp_obj->set_response_datatable(200, $meeting_room, $draw, $total, $total);
            set_output($resp_obj->get_response_datatable());
            return;
        }
    }

    #path: /api/meeting-room/id/$id [GET]
    function get_meeting_room_by_id($id){
        $resp_obj = new Response_api();

        #check token
        $header      = $this->input->request_headers();
        $verify_resp = verify_user_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/meeting-room [GET] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }

        #get meeting room detail
        $meeting_room = $this->meeting_room_model->get_meeting_room_by_id($id);
        if(is_null($meeting_room)){
            logging('error', "/api/meeting-room/id/$id [GET] - meeting room not found");
            $resp_obj->set_response(404, "failed", "meeting room not found");
            set_output($resp_obj->get_response());
            return;
        }

        #response
        logging('debug', '/api/meeting-room/id/'.$id.' [GET] - Get meeting room by id success', $meeting_room);
        $resp_obj->set_response(200, "success", "Get meeting room by id success", $meeting_room);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /api/meeting-room [POST]
    function create_meeting_room(){
        #init req & res
        $resp_obj   = new Response_api();
        $request    = json_decode($this->input->raw_input_stream, true);
        $type       = $this->input->get('type');
        
        #check token
        $header = $this->input->request_headers();
        $resp = verify_user_token($header, "SUPERADMIN");
        if($resp['status'] == 'failed'){
            logging('error', '/api/meeting-room [POST] - '.$resp['message']);
            set_output($resp);
            return;
        }
        
        #check request params
        $keys = array('building_id', 'floor_number', 'name', 'description');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/api/meeting-room [POST] - Missing parameter. please check API documentation', $request);
            $resp_obj->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp_obj->get_response());
            return;
        }

        #check building
        $building = $this->building_model->get_building_by_id($request['building_id']);
        if(is_null($building)){
            logging('error', '/api/meeting-room [POST] - Building is not found', $request);
            $resp_obj->set_response(400, "failed", "Building is not found");
            set_output($resp_obj->get_response());
            return;
        }

        #init variable
        $request['id']  = get_uniq_id();
        $flag           = $this->meeting_room_model->create_meeting_room($request);
        
        #response
        if(!$flag){
            logging('error', '/api/meeting-room [POST] - Internal server error', $request);
            $resp_obj->set_response(500, "failed", "Internal server error");
            set_output($resp_obj->get_response());
            return;
        }
        logging('debug', '/api/meeting-room [POST] - Create meeting room  success', $request);
        $resp_obj->set_response(200, "success", "Create meeting room  success", $request);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /api/meeting-room [PUT]
    function update_meeting_room(){
        $resp_obj = new Response_api();
        $request  = json_decode($this->input->raw_input_stream, true);

        #check token
        $header = $this->input->request_headers();
        $resp   = verify_user_token($header, "SUPERADMIN");
        if($resp['status'] == 'failed'){
            logging('error', '/api/meeting-room [PUT] - '.$resp['message']);
            set_output($resp);
            return;
        }
        
        #check request params
        $keys = array('id', 'building_id', 'floor_number', 'name', 'description');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/api/meeting-room [PUT] - Missing parameter. please check API documentation', $request);
            $resp_obj->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp_obj->get_response());
            return;
        }

        #check driver
        $meeting_room = $this->meeting_room_model->get_meeting_room_by_id($request['id']);
        if(is_null($meeting_room)){
            logging('error', "/api/meeting-room [PUT] - meeting room not found", $request);
            $resp_obj->set_response(404, "failed", "meeting room not found");
            set_output($resp_obj->get_response());
            return;
        }

        #check building
        $building = $this->building_model->get_building_by_id($request['building_id']);
        if(is_null($building)){
            logging('error', '/api/meeting-room [POST] - Building is not found', $request);
            $resp_obj->set_response(400, "failed", "Building is not found");
            set_output($resp_obj->get_response());
            return;
        }

        #update driver
        $flag = $this->meeting_room_model->update_meeting_room($request, $request['id']);
        
        #response
        if(!$flag){
            logging('debug', '/api/meeting-room [PUT] - Data does not change', $request);
            $resp_obj->set_response(200, "success", "Data does not change");
            set_output($resp_obj->get_response());
            return;
        }
        logging('debug', "/api/meeting-room [PUT] - Update meeting room success", $request);
        $resp_obj->set_response(200, "success", "Update meeting room success", $request);
        set_output($resp_obj->get_response());
        return;
    }
  
    #path: /api/meeting-room/delete/$id [DELETE]
    function delete_meeting_room($id){
        $resp_obj = new Response_api();

        #check token
        $header = $this->input->request_headers();
        $resp   = verify_user_token($header, "SUPERADMIN");
        if($resp['status'] == 'failed'){
            logging('error', '/api/meeting-room/delete/'.$id.' [DELETE] - '.$resp['message']);
            set_output($resp);
            return;
        }

        #check driver
        $meeting_room = $this->meeting_room_model->get_meeting_room_by_id($id);
        if(is_null($meeting_room)){
            logging('error', "/api/meeting-room/delete/$id [DELETE] - meeting room not found");
            $resp_obj->set_response(404, "failed", "meeting room not found");
            set_output($resp_obj->get_response());
            return;
        }

        #delete driver
        $flag = $this->meeting_room_model->delete_meeting_room($id);

        #response
        if(!$flag){
            logging('error', '/api/meeting-room/delete/'.$id.' [DELETE] - Internal server error');
            $resp_obj->set_response(500, "failed", "Internal server error");
            set_output($resp_obj->get_response());
            return;
        }
        logging('debug', "/api/meeting-room/delete/$id [DELETE] - Delete meeting room success");
        $resp_obj->set_response(200, "success", "Delete meeting room success");
        set_output($resp_obj->get_response());
        return;
    }
 
    #path: /api/meeting-room/book/$id [PUT]
    function book_meeting_room($id){
        $resp = new Response_api();

        #check token
        $header         = $this->input->request_headers();
        $verify_resp    = verify_user_token($header, "SUPERADMIN");
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/meeting-room/book/'.$id.' [PUT] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }

        #check driver
        $meeting_room = $this->meeting_room_model->get_meeting_room_by_id($id);
        if(is_null($meeting_room)){
            logging('error', '/api/meeting-room/book/'.$id.' [PUT] - meeting room not found');
            $resp->set_response(404, "failed", "meeting room not found");
            set_output($resp->get_response());
            return;
        }

        #book driver
        $flag = $this->meeting_room_model->book_meeting_room($id);
        
        #response
        if(!$flag){
            logging('error', '/api/meeting-room/book/'.$id.' [PUT] - Internal server error');
            $resp->set_response(500, "failed", "Internal server error");
            set_output($resp->get_response());
            return;
        }
        logging('debug', '/api/meeting-room/book/'.$id.' [PUT] - book meeting room success');
        $resp->set_response(200, "success", "book meeting room success");
        set_output($resp->get_response());
        return;
    }

    #path: /api/meeting-room/free/$id [PUT]
    function free_meeting_room($id){
        $resp = new Response_api();

        #check token
        $header         = $this->input->request_headers();
        $verify_resp    = verify_user_token($header, "SUPERADMIN");
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/meeting-room/free/'.$id.' [PUT] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }

        #check driver
        $meeting_room = $this->meeting_room_model->get_meeting_room_by_id($id);
        if(is_null($meeting_room)){
            logging('error', '/api/meeting-room/free/'.$id.' [PUT] - meeting room not found');
            $resp->set_response(404, "failed", "meeting room not found");
            set_output($resp->get_response());
            return;
        }

        #free driver
        $flag = $this->meeting_room_model->free_meeting_room($id);
        
        #response
        if(empty($flag)){
            logging('error', '/api/meeting-room/free/'.$id.' [PUT] - Internal server error');
            $resp->set_response(500, "failed", "Internal server error");
            set_output($resp->get_response());
            return;
        }
        logging('debug', '/api/meeting-room/free/'.$id.' [PUT] - free meeting room success');
        $resp->set_response(200, "success", "free meeting room success");
        set_output($resp->get_response());
        return;
    }
}