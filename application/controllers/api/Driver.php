<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');    // cache for 1 day
}

// Access-Control headers are received during OPTIONS requests
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");         

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}

class Driver extends CI_Controller {
    public function __construct(){
        parent::__construct();
        $this->load->helper('url_helper');
    }

    #path: /api/driver/total [GET]
    function get_driver_total(){
        #init req & resp
        $resp_obj   = new Response_api();

        #check token
        $header      = $this->input->request_headers();
        $verify_resp = verify_user_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/driver/total [GET] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }

        #get driver total
        $total = $this->driver_model->count_driver();

        #response
        logging('debug', '/api/driver/total [GET] - Get driver total is success');
        $resp_obj->set_response(200, "success", "Get driver total is success", $total);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /api/driver [GET]
    function get_driver(){
        #init req & resp
        $resp_obj   = new Response_api();
        $page_number= $this->input->get('page_number');
        $page_size  = $this->input->get('page_size');
        $search     = $this->input->get('search');
        $status     = $this->input->get('status');
        $draw       = $this->input->get('draw');
        // $params     = array($page_number, $page_size);

        #check token
        $header      = $this->input->request_headers();
        $verify_resp = verify_user_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/driver [GET] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }

        #check request params
        // if(!check_parameter($params)){
        //     logging('error', "/api/driver [GET] - Missing parameter. please check API documentation", array('page_number'=>$page_number, 'page_size'=>$page_size));
        //     $resp_obj->set_response(400, "failed", "Missing parameter. please check API documentation");
        //     set_output($resp->get_response());
        //     return;
        // }

        #get driver asset
        $start  = $page_number * $page_size;
        $limit  = array('start'=>$start, 'size'=>$page_size);
        $order  = array('field'=>'created_at', 'order'=>'DESC');
        $driver = $this->driver_model->get_driver($search, $status, $order, $limit);
        $total  = $this->driver_model->count_driver($search, $status);

        #response
        if(empty($draw)){
            logging('debug', '/api/driver [GET] - Get driver is success');
            $resp_obj->set_response(200, "success", "Get driver is success", $driver);
            set_output($resp_obj->get_response());
            return;
        }else{
            logging('debug', '/api/driver [GET] - Get driver is success');
            $resp_obj->set_response_datatable(200, $driver, $draw, $total, $total);
            set_output($resp_obj->get_response_datatable());
            return;
        }
    }

    #path: /api/driver/id/$id [GET]
    function get_driver_by_id($id){
        $resp_obj = new Response_api();

        #check token
        $header      = $this->input->request_headers();
        $verify_resp = verify_user_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/driver [GET] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }

        #get driver detail
        $driver = $this->driver_model->get_driver_by_id($id);
        if(is_null($driver)){
            logging('error', "/api/driver/id/$id [GET] - driver not found");
            $resp_obj->set_response(404, "failed", "driver not found");
            set_output($resp_obj->get_response());
            return;
        }

        #response
        logging('debug', '/api/driver/id/'.$id.' [GET] - Get driver by id success', $driver);
        $resp_obj->set_response(200, "success", "Get driver by id success", $driver);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /api/driver [POST]
    function create_driver(){
        #init req & res
        $resp_obj   = new Response_api();
        $request    = json_decode($this->input->raw_input_stream, true);
        
        #check token
        $header = $this->input->request_headers();
        $resp = verify_user_token($header, "SUPERADMIN");
        if($resp['status'] == 'failed'){
            logging('error', '/api/driver [POST] - '.$resp['message']);
            set_output($resp);
            return;
        }
        
        #check request params
        $keys = array('name');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/api/driver [POST] - Missing parameter. please check API documentation', $request);
            $resp_obj->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp_obj->get_response());
            return;
        }

        #init variable
        $request['id']  = get_uniq_id();
        $flag           = $this->driver_model->create_driver($request);
        
        #response
        if(!$flag){
            logging('error', '/api/driver [POST] - Internal server error', $request);
            $resp_obj->set_response(500, "failed", "Internal server error");
            set_output($resp_obj->get_response());
            return;
        }
        logging('debug', '/api/driver [POST] - Create driver asset success', $request);
        $resp_obj->set_response(200, "success", "Create driver asset success", $request);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /api/driver [PUT]
    function update_driver(){
        $resp_obj = new Response_api();
        $request  = json_decode($this->input->raw_input_stream, true);

        #check token
        $header = $this->input->request_headers();
        $resp   = verify_user_token($header, "SUPERADMIN");
        if($resp['status'] == 'failed'){
            logging('error', '/api/driver [PUT] - '.$resp['message']);
            set_output($resp);
            return;
        }
        
        #check request params
        $keys = array('id', 'name');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/api/driver [PUT] - Missing parameter. please check API documentation', $request);
            $resp_obj->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp_obj->get_response());
            return;
        }

        #check driver
        $driver = $this->driver_model->get_driver_by_id($request['id']);
        if(is_null($driver)){
            logging('error', "/api/driver [PUT] - driver not found", $request);
            $resp_obj->set_response(404, "failed", "driver not found");
            set_output($resp_obj->get_response());
            return;
        }

        #update driver
        $flag = $this->driver_model->update_driver($request, $request['id']);
        
        #response
        if(!$flag){
            logging('debug', '/api/driver [PUT] - Data does not change', $request);
            $resp_obj->set_response(200, "success", "Data does not change");
            set_output($resp_obj->get_response());
            return;
        }
        logging('debug', "/api/driver [PUT] - Update driver success", $request);
        $resp_obj->set_response(200, "success", "Update driver success", $request);
        set_output($resp_obj->get_response());
        return;
    }
  
    #path: /api/driver/delete/$id [DELETE]
    function delete_driver($id){
        $resp_obj = new Response_api();

        #check token
        $header = $this->input->request_headers();
        $resp   = verify_user_token($header, "SUPERADMIN");
        if($resp['status'] == 'failed'){
            logging('error', '/api/driver/delete/'.$id.' [DELETE] - '.$resp['message']);
            set_output($resp);
            return;
        }

        #check driver
        $driver = $this->driver_model->get_driver_by_id($id);
        if(is_null($driver)){
            logging('error', "/api/driver/delete/$id [DELETE] - driver not found");
            $resp_obj->set_response(404, "failed", "driver not found");
            set_output($resp_obj->get_response());
            return;
        }

        #delete driver
        $flag = $this->driver_model->delete_driver($id);

        #response
        if(!$flag){
            logging('error', '/api/driver/delete/'.$id.' [DELETE] - Internal server error');
            $resp_obj->set_response(500, "failed", "Internal server error");
            set_output($resp_obj->get_response());
            return;
        }
        logging('debug', "/api/driver/delete/$id [DELETE] - Delete driver success");
        $resp_obj->set_response(200, "success", "Delete driver success");
        set_output($resp_obj->get_response());
        return;
    }
 
    #path: /api/driver/book/$id [PUT]
    function book_driver($id){
        $resp = new Response_api();

        #check token
        $header         = $this->input->request_headers();
        $verify_resp    = verify_user_token($header, "SUPERADMIN");
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/driver/book/'.$id.' [PUT] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }

        #check driver
        $driver = $this->driver_model->get_driver_by_id($id);
        if(is_null($driver)){
            logging('error', '/api/driver/book/'.$id.' [PUT] - driver not found');
            $resp->set_response(404, "failed", "driver not found");
            set_output($resp->get_response());
            return;
        }

        #book driver
        $flag = $this->driver_model->book_driver($id);
        
        #response
        if(!$flag){
            logging('error', '/api/driver/book/'.$id.' [PUT] - Internal server error');
            $resp->set_response(500, "failed", "Internal server error");
            set_output($resp->get_response());
            return;
        }
        logging('debug', '/api/driver/book/'.$id.' [PUT] - book driver success');
        $resp->set_response(200, "success", "book driver success");
        set_output($resp->get_response());
        return;
    }

    #path: /api/driver/free/$id [PUT]
    function free_driver($id){
        $resp = new Response_api();

        #check token
        $header         = $this->input->request_headers();
        $verify_resp    = verify_user_token($header, "SUPERADMIN");
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/driver/free/'.$id.' [PUT] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }

        #check driver
        $driver = $this->driver_model->get_driver_by_id($id);
        if(is_null($driver)){
            logging('error', '/api/driver/free/'.$id.' [PUT] - driver not found');
            $resp->set_response(404, "failed", "driver not found");
            set_output($resp->get_response());
            return;
        }

        #free driver
        $flag = $this->driver_model->free_driver($id);
        
        #response
        if(empty($flag)){
            logging('error', '/api/driver/free/'.$id.' [PUT] - Internal server error');
            $resp->set_response(500, "failed", "Internal server error");
            set_output($resp->get_response());
            return;
        }
        logging('debug', '/api/driver/free/'.$id.' [PUT] - free driver success');
        $resp->set_response(200, "success", "free driver success");
        set_output($resp->get_response());
        return;
    }
}