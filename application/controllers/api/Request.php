<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');    // cache for 1 day
}

// Access-Control headers are received during OPTIONS requests
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");         

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}

require_once APPPATH . 'core/MY_Request.php';

class Request extends MY_Request {

    protected $data_models = array(
        "OTHER"                     => "data_other",
        "OPERATIONAL VEHICLE"       => "data_operational_vehicle",
        "OFFICE STATIONERY"         => "data_office_stationery",
        "MEETING ROOM"              => "data_meeting_room",
        "MEETING CONSUMPTION"       => "data_meeting_consumption",
        "MAINTENANCE"               => "data_maintenance",
        "ASSET"                     => "data_asset",
        "OFFICE HOUSEHOLD"          => "data_office_household",
        "WORKSTATION AND FURNITURE" => "data_workstation_and_furniture",
        "FUEL AND ECARD"            => "data_fuel_and_ecard",
        "COURIER AND MAILING"       => "data_courier_and_mailing",
        "CONSUMABLE"                => "data_consumable",
        "EVENT SUPPORT"             => "data_event_support"
    );

    protected $data_headers = array(
        "OTHER"                     => array(
                                            "key"   => array("category_function"),
                                            "title" => array("Fungsi")
                                        ),
        "OPERATIONAL VEHICLE"       =>  array(
                                            "key"   => array("category_function", "total_passenger", "request_date", "request_time", "dest_1_mob", "dest_2_mob", "driver_name"),
                                            "title" => array("Fungsi", "Jumlah Penumpang", "Tanggal", "Waktu", "Tujuan #1", "Tujuan #2", "Pengemudi")
                                        ),
        "OFFICE STATIONERY"         =>  array(
                                            "key"   => array("category_function", "category_office_stationery", "qty"),
                                            "title" => array("Fungsi", "Item", "Jumlah")
                                        ),
        "MEETING ROOM"              =>  array(
                                            "key"   => array("category_function", "meeting_room", "meeting_type", "meeting_title", "total_participant", "meeting_date", "start_time", "end_time"),
                                            "title" => array("Fungsi", "Ruangan", "Tipe", "Judul", "Total Peserta", "Tanggal", "Waktu Mulai", "Waktu Akhir")
                                        ),
        "MEETING CONSUMPTION"       =>  array(
                                            "key"   => array("category_function", "category_meeting_consumption", "qty"),
                                            "title" => array("Fungsi", "Item", "Jumlah")
                                        ),
        "MAINTENANCE"               =>  array(
                                            "key"   => array("category_function", "category_maintenance"),
                                            "title" => array("Fungsi", "Item")
                                        ),
        "ASSET"                     =>  array(
                                            "key"   => array("category_function", "category_asset"),
                                            "title" => array("Fungsi", "Item")
                                        ),
        "OFFICE HOUSEHOLD"          =>  array(
                                            "key"   => array("category_function", "category_office_household", "qty"),
                                            "title" => array("Fungsi", "Item", "Jumlah")
                                        ),
        "WORKSTATION AND FURNITURE" =>  array(
                                            "key"   => array("category_function", "category_workstation_and_furniture", "qty"),
                                            "title" => array("Fungsi", "Item", "Jumlah")
                                        ),
        "FUEL AND ECARD"            =>  array(
                                            "key"   => array("category_function", "category_fuel_and_ecard"),
                                            "title" => array("Fungsi", "Item")
                                        ),
        "COURIER AND MAILING"       =>  array(
                                        "key"   => array("category_function", "delivery_date", "destination", "receiver_name", "receiver_phone"),
                                        "title" => array("Fungsi", "Tanggal Pengiriman", "Tujuan Pengiriman", "Nama Penerima", "Telepon Penerima")
                                        ),
        "CONSUMABLE"                =>  array(
                                            "key"   => array("category_function", "category_consumable", "qty"),
                                            "title" => array("Fungsi", "Item", "Jumlah")
                                        ),
        "EVENT SUPPORT"             =>  array(
                                            "key"   => array("category_function", "event_name", "event_date", "start_time", "end_time", "location"),
                                            "itemKey"   => array("category_event_support", "qty"),
                                            "title" => array("Fungsi", "Nama Agenda", "Tanggal Agenda", "Waktu Mulai", "Waktu Selesai", "Lokasi"),
                                            "itemTitle" => array("Item", "Jumlah")
                                        )
    );
    
    public function __construct(){
        parent::__construct();
        $this->load->helper('url_helper');
    }

    #path: /api/request/total [GET]
    function get_request_total(){
        #init req & resp
        $resp_obj           = new Response_api();
        $status             = $this->input->get("status");
        $request_category   = $this->input->get("request_category");
        $requestor_id       = null;

        #check token
        $header      = $this->input->request_headers();
        $verify_resp = verify_user_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/request/total [GET] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }
        $user = $verify_resp['data']['user'];
        if($user->role == 'REQUESTOR'){
            $requestor_id = $user->id;
        }

        #get request total
        $total = $this->request_model->count_request(null, $request_category, null, $requestor_id, null, $status);
        

        #response
        logging('debug', '/api/request/total [GET] - Get request total is success');
        $resp_obj->set_response(200, "success", "Get request total is success", $total);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /api/request [GET]
    function get_request(){
        #init req & resp
        $resp_obj           = new Response_api();
        $page_number        = $this->input->get('page_number');
        $page_size          = $this->input->get('page_size');
        $search             = $this->input->get('search');
        $status             = $this->input->get('status');
        $request_category   = $this->input->get("request_category");
        $request_date       = $this->input->get("request_date");
        $requestor_id       = null;
        $draw               = $this->input->get('draw');
        // $params             = array($page_number, $page_size);

        #check token
        $header      = $this->input->request_headers();
        $verify_resp = verify_user_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/request [GET] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }
        $user = $verify_resp['data']['user'];
        if($user->role == 'REQUESTOR'){
            $requestor_id = $user->id;
        }

        #check request params
        // if(!check_parameter($params)){
        //     logging('error', "/api/request [GET] - Missing parameter. please check API documentation", array('page_number'=>$page_number, 'page_size'=>$page_size));
        //     $resp_obj->set_response(400, "failed", "Missing parameter. please check API documentation");
        //     set_output($resp_obj->get_response());
        //     return;
        // }

        #get request asset
        $limit = null;
        if($page_number && $page_size){
            $start      = $page_number * $page_size;
            $limit      = array('start'=>$start, 'size'=>$page_size);
        }
        $order      = array('field'=>'created_at', 'order'=>'DESC');
        $request    = $this->request_model->get_request($search, $request_category, $request_date, $requestor_id, null, $status, $order, $limit);
        $total      = $this->request_model->count_request($search, $request_category, $request_date, $requestor_id, null, $status);

        #response
        if(empty($draw)){
            logging('debug', '/api/request [GET] - Get request is success');
            $resp_obj->set_response(200, "success", "Get request is success", $request);
            set_output($resp_obj->get_response());
            return;
        }else{
            logging('debug', '/api/request [GET] - Get request is success');
            $resp_obj->set_response_datatable(200, $request, $draw, $total, $total);
            set_output($resp_obj->get_response_datatable());
            return;
        }
    }

    #path: /api/request/id/$id [GET]
    function get_request_by_id($id){
        $resp_obj       = new Response_api();
        $requestor_id   = null;

        #check token
        $header      = $this->input->request_headers();
        $verify_resp = verify_user_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/request [GET] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }
        $user = $verify_resp['data']['user'];

        #get request detail
        $request = $this->get_request_detail($id);
        if(is_null($request)){
            return;
        }

        if($user->role == 'REQUESTOR' && $user->id != $request->requestor_id){
            logging('error', "/api/request/id/$id [GET] - request not found");
            $resp_obj->set_response(404, "failed", "request not found");
            set_output($resp_obj->get_response());
            return;
        }

        #get request data
        $model_name     = "{$this->data_models[$request->request_category]}_model";
        $function_name  = "get_{$this->data_models[$request->request_category]}";
        $request_data   = $this->$model_name->$function_name(null, $request->id);

        if($request->request_category == "MEETING ROOM"){
            $additional_request                     = $this->data_additional_request_model->get_data_additional_request(null, $request_data[0]->id);
            $request_data[0]->additional_request    = $additional_request;
        }else if($request->request_category == "EVENT SUPPORT"){
            $event_support_items    = $this->data_event_support_items_model->get_data_event_support_items(null, $request_data[0]->id);
            $request_data[0]->items = $event_support_items;
        }

        $request->data  = $request_data;

        #response
        logging('debug', '/api/request/id/'.$id.' [GET] - Get request by id success', $request);
        $resp_obj->set_response(200, "success", "Get request by id success", $request);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /api/request/reject/$id [PUT]
    function reject_request($id){
        $resp   = new Response_api();
        $req    = json_decode($this->input->raw_input_stream, true);

        #check token
        $header         = $this->input->request_headers();
        $verify_resp    = verify_user_token($header, "EXECUTOR");
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/request/reject/'.$id.' [PUT] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }

        #check request params
        $keys = array('reject_reason');
        if(!check_parameter_by_keys($req, $keys)){
            logging('error', '/api/request/reject/$id [PUT] - Missing parameter. please check API documentation', $req);
            $resp_obj->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp_obj->get_response());
            return;
        }

        #check request
        $request = $this->get_request_detail($id);
        if(is_null($request)){
            return;
        }

        #update request status
        $request_update = array(
            'status'        => "REJECTED",
            'reject_reason' => $req['reject_reason']
        );
        $flag = $this->request_model->update_request($request_update, $request->id);
        if(!$flag){
            return;
        }

        #send email
        $requestor = $this->user_model->get_user_by_id($request->requestor_id);
        if($requestor && $requestor->email){
            #generate requisition list
            $header             = $this->data_headers[$request->request_category];
            $model_name         = "{$this->data_models[$request->request_category]}_model";
            $function_name      = "get_{$this->data_models[$request->request_category]}";
            $request->data      = $this->$model_name->$function_name(null, $request->id);
            $request->reject_reason = $req['reject_reason'];

            send_reject_email($requestor->email, $request, $header);
        }

        #response
        logging('debug', '/api/request/reject/'.$id.' [PUT] - reject request success');
        $resp->set_response(200, "success", "reject request success");
        set_output($resp->get_response());
        return;
    }
 
    #path: /api/request/confirm/$id [PUT]
    function confirm_request($id){
        $resp = new Response_api();

        #check token
        $header         = $this->input->request_headers();
        $verify_resp    = verify_user_token($header, "EXECUTOR");
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/request/confirm/'.$id.' [PUT] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }

        #check request
        $request = $this->get_request_detail($id);
        if(is_null($request)){
            return;
        }

        if($request->request_category == "OPERATIONAL VEHICLE"){
            $data_request = $this->data_operational_vehicle_model->get_data_operational_vehicle(null, $id);
            if(count($data_request) <= 0){
                logging('error', 'Request data is empty');
                $resp->set_response(400, "failed", "Request data is empty");
                set_output($resp->get_response());
                return;
            }
            
            $flag_driver = $this->driver_model->book_driver($data_request[0]->driver_id);
            if(!$flag_driver){
                logging('error', 'Book driver failed');
                $resp->set_response(400, "failed", "Book driver failed");
                set_output($resp->get_response());
                return;
            }
        }

        #update request status
        $flag = $this->update_request_status($request, "ON PROGRESS");
        if(!$flag){
            return;
        }

        #response
        logging('debug', '/api/request/confirm/'.$id.' [PUT] - confirm request success');
        $resp->set_response(200, "success", "confirm request success");
        set_output($resp->get_response());
        return;
    }

    #path: /api/request/complete/$id [PUT]
    function complete_request($id){
        $resp   = new Response_api();
        $req    = json_decode($this->input->raw_input_stream, true);

        #check token
        $header         = $this->input->request_headers();
        $verify_resp    = verify_user_token($header, "EXECUTOR");
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/request/complete/'.$id.' [PUT] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }

        #check request
        $request = $this->get_request_detail($id);
        if(is_null($request)){
            return;
        }

        if($request->request_category == 'MAINTENANCE'){
            #check request params
            $keys = array('data_maintenance_id', 'after_url');
            if(!check_parameter_by_keys($req, $keys)){
                logging('error', '/api/request/complete/$id [PUT] - Missing parameter. please check API documentation', $req);
                $resp->set_response(400, "failed", "Missing parameter. please check API documentation");
                set_output($resp->get_response());
                return;
            }

            $data_maintenance = $this->data_maintenance_model->get_data_maintenance_by_id($req['data_maintenance_id'], true);
            if(is_null($data_maintenance)){
                logging('error', '/api/request/complete/$id [PUT] - Request data is not found', $req);
                $resp->set_response(400, "failed", "Request data is not found");
                set_output($resp->get_response());
                return;
            }

            $req_maintenance = array('after_url' => $req['after_url']);
            $flag = $this->data_maintenance_model->update_data_maintenance($req_maintenance, $req['data_maintenance_id']);
            if(!$flag){
                logging('error', '/api/request/complete/$id [PUT] - Update request data failed', $req);
                $resp->set_response(400, "failed", "Update request data failed");
                set_output($resp->get_response());
                return;
            }
        }

        #update request status
        $flag = $this->update_request_status($request, "COMPLETED");
        if(!$flag){
            return;
        }
        
        #response
        logging('debug', '/api/request/complete/'.$id.' [PUT] - complete request success');
        $resp->set_response(200, "success", "complete request success");
        set_output($resp->get_response());
        return;
    }

    #path: /api/request/accomplished/$id [PUT]
    function accomplished_request($id){
        $resp   = new Response_api();
        $req    = json_decode($this->input->raw_input_stream, true);

        #check token
        $header         = $this->input->request_headers();
        $verify_resp    = verify_user_token($header, "REQUESTOR");
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/request/accomplished/'.$id.' [PUT] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }

        #check request
        $request = $this->get_request_detail($id);
        if(is_null($request)){
            return;
        }

        if($request->request_category == 'MEETING ROOM'){
            #check request params
            $keys = array('data_meeting_id', 'attachment');
            if(!check_parameter_by_keys($req, $keys)){
                logging('error', '/api/request/accomplished/$id [PUT] - Missing parameter. please check API documentation', $request);
                $resp->set_response(400, "failed", "Missing parameter. please check API documentation");
                set_output($resp->get_response());
                return;
            }

            $data_meeting = $this->data_meeting_room_model->get_data_meeting_room_by_id($req['data_meeting_id'], true);
            if(is_null($data_meeting)){
                logging('error', '/api/request/accomplished/$id [PUT] - Request data is not found', $req);
                $resp->set_response(400, "failed", "Request data is not found");
                set_output($resp->get_response());
                return;
            }

            $req_meeting = array('attachment' => $req['attachment']);
            $flag = $this->data_meeting_room_model->update_data_meeting_room($req_meeting, $req['data_meeting_id']);
            if(!$flag){
                logging('error', '/api/request/accomplished/$id [PUT] - Update request data failed', $req);
                $resp->set_response(400, "failed", "Update request data failed");
                set_output($resp->get_response());
                return;
            }
        }else if($request->request_category == "OPERATIONAL VEHICLE"){
            $data_request = $this->data_operational_vehicle_model->get_data_operational_vehicle(null, $id);
            if(count($data_request) <= 0){
                logging('error', 'Request data is empty');
                $resp->set_response(400, "failed", "Request data is empty");
                set_output($resp->get_response());
                return;
            }
            
            $flag_driver = $this->driver_model->free_driver($data_request[0]->driver_id);
            if(!$flag_driver){
                logging('error', 'Free driver failed');
                $resp->set_response(400, "failed", "Free driver failed");
                set_output($resp->get_response());
                return;
            }
        }

        #update request status
        $flag = $this->update_request_status($request, "REQUEST ACCOMPLISHED");
        if(!$flag){
            return;
        }

        #generate requisition list
        $header             = $this->data_headers[$request->request_category];
        $model_name         = "{$this->data_models[$request->request_category]}_model";
        $function_name      = "get_{$this->data_models[$request->request_category]}";
        $request->data      = $this->$model_name->$function_name(null, $request->id);
        $request->history   = $this->request_history_model->get_request_history_by_request_id($request->id);
        if($request->request_category == "EVENT SUPPORT"){
            $data_id        = $request->data[0]->id;
            $model_item     = "{$this->data_models[$request->request_category]}_items_model";
            $function_item  = $function_name."_items";
            $request->items = $this->$model_item->$function_item(null, $data_id);
        }
        
        $requisition_url = generate_requisition_list($request, $header);
        $this->request_model->update_request(array("requisition_url" => $requisition_url), $request->id);
        
        #response
        logging('debug', '/api/request/accomplished/'.$id.' [PUT] - accomplished request success');
        $resp->set_response(200, "success", "accomplished request success");
        set_output($resp->get_response());
        return;
    }

    #path: /api/request/upload [POST]
    function upload_request_file(){
        #init variable
        $resp_obj = new Response_api();

        #check token
        $header = $this->input->request_headers();
        $resp   = verify_user_token($header);
        if($resp['status'] == 'failed'){
            logging('error', '/api/request/upload [POST] - '.$resp['message']);
            set_output($resp);
            return;
        }

        #check requested param
        $destination = 'assets/request/';
        if (empty($_FILES['file']['name'])) {
            logging('error', '/api/request/upload [POST] - Missing parameter. please check API documentation');
            $resp_obj->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp_obj->get_response());
            return;   
        }

        #upload image
        $file = $_FILES['file'];
        $resp = _upload_file($file, $destination);

        #response
        if($resp['status'] == 'failed'){
            logging('error', '/api/request/upload [POST] - '.$resp['message']);
            $resp_obj->set_response(400, "failed", $resp['message']);
            set_output($resp_obj->get_response());
            return; 
        }
        $data                   = $resp['data'];
        $data['full_file_url']  = base_url($data['file_url']);
        
        logging('debug', '/api/request/upload [POST] - Upload file success', $data);
        $resp_obj->set_response(200, "success", "Upload file success", $data);
        set_output($resp_obj->get_response());
        return; 
    }
}
