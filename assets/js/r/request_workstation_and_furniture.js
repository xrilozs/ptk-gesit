let WORKSTATION_AND_FURNITURE = [] 
let CATEGORY_WORKSTATION_AND_FURNITURE = []

$(document).ready(function(){
    getCategoryWorkstationAndFurniture()
    $('#workstation-and-furniture-create-toggle').click(function(){
        $(`#workstation-and-furniture-create-form`)[0].reset();
        WORKSTATION_AND_FURNITURE = []
        renderRequestWorkstationAndFurnitureOption()
    })

    $('#workstation-and-furniture-add-button').click(function(){
        WORKSTATION_AND_FURNITURE.push({category_workstation_and_furniture_id: null, qty: null})
        console.log("TEST")
        renderRequestWorkstationAndFurnitureOption()
    })

    $("body").delegate("#workstation-and-furniture-reject-form", "submit", function(e) {
        startLoadingButton('#workstation-and-furniture-reject-button')
        let $form = $(this)
        let data = {
            reject_reason: $form.find( "textarea[name='reject_reason']" ).val(),
        }

        $.ajax({
            async: true,
            url: `${REQUEST_API_URL}/reject/${REQUEST_ID}`,
            type: 'PUT',
            data: JSON.stringify(data),
            beforeSend: function (xhr) {
                xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
            },
            error: function(res) {
                const response = JSON.parse(res.responseText)
                let isRetry = retryRequest(response)
                if(isRetry) $.ajax(this)
                else endLoadingButton('#workstation-and-furniture-reject-button', 'Iya')
            },
            success: function(res) {
                endLoadingButton('#workstation-and-furniture-reject-button', 'Iya')
                showSuccess(res.message)
                $('#workstation-and-furniture-reject-modal').modal('toggle')
                $('#workstation-and-furniture-detail-modal').modal('toggle')
                request_table.ajax.reload()
            }
        });
    })

    $("body").delegate("#workstation-and-furniture-approve-button", "click", function(e) {
        startLoadingButton('#workstation-and-furniture-approve-button')
    
        $.ajax({
            async: true,
            url: `${REQUEST_API_URL}/confirm/${REQUEST_ID}`,
            type: 'PUT',
            beforeSend: function (xhr) {
                xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
            },
            error: function(res) {
                const response = JSON.parse(res.responseText)
                let isRetry = retryRequest(response)
                if(isRetry) $.ajax(this)
                else endLoadingButton('#workstation-and-furniture-approve-button', 'Iya')
            },
            success: function(res) {
                endLoadingButton('#workstation-and-furniture-approve-button', 'Iya')
                showSuccess(res.message)
                $('#workstation-and-furniture-approve-modal').modal('toggle')
                $('#workstation-and-furniture-detail-modal').modal('toggle')
                request_table.ajax.reload()
            }
        });
    })

    $("body").delegate("#workstation-and-furniture-complete-button", "click", function(e) {
        startLoadingButton('#workstation-and-furniture-complete-button')
    
        $.ajax({
            async: true,
            url: `${REQUEST_API_URL}/complete/${REQUEST_ID}`,
            type: 'PUT',
            beforeSend: function (xhr) {
                xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
            },
            error: function(res) {
                const response = JSON.parse(res.responseText)
                let isRetry = retryRequest(response)
                if(isRetry) $.ajax(this)
                else endLoadingButton('#workstation-and-furniture-complete-button', 'Iya')
            },
            success: function(res) {
                endLoadingButton('#workstation-and-furniture-complete-button', 'Iya')
                showSuccess(res.message)
                $('#workstation-and-furniture-complete-modal').modal('toggle')
                $('#workstation-and-furniture-detail-modal').modal('toggle')
                request_table.ajax.reload()
            }
        });
    })

    $("body").delegate("#workstation-and-furniture-accomplish-button", "click", function(e) {
        startLoadingButton('#workstation-and-furniture-accomplish-button')
    
        $.ajax({
            async: true,
            url: `${REQUEST_API_URL}/accomplished/${REQUEST_ID}`,
            type: 'PUT',
            beforeSend: function (xhr) {
                xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
            },
            error: function(res) {
                const response = JSON.parse(res.responseText)
                let isRetry = retryRequest(response)
                if(isRetry) $.ajax(this)
                else endLoadingButton('#workstation-and-furniture-accomplish-button', 'Iya')
            },
            success: function(res) {
                endLoadingButton('#workstation-and-furniture-accomplish-button', 'Iya')
                showSuccess(res.message)
                $('#workstation-and-furniture-accomplish-modal').modal('toggle')
                $('#workstation-and-furniture-detail-modal').modal('toggle')
                request_table.ajax.reload()
            }
        });
    })

    $("body").delegate(".workstation-and-furniture-delete-button", "click", function(e) {
        let id = $(this).data("id")
        WORKSTATION_AND_FURNITURE.splice(id, 1)
        renderRequestWorkstationAndFurnitureOption()
    })

    $("body").delegate(".workstation-and-furniture-category", "change", function(e) {
        let id = $(this).data("id")
        WORKSTATION_AND_FURNITURE[id].category_workstation_and_furniture_id = $(this).val()
    })

    $("body").delegate(".workstation-and-furniture-qty", "change", function(e) {
        let id = $(this).data("id")
        WORKSTATION_AND_FURNITURE[id].qty = $(this).val()
    })

    $('#workstation-and-furniture-create-form').submit(function(e){
        e.preventDefault()

        startLoadingButton('#workstation-and-furniture-create-button')
    
        let $form = $(this)
        let data = {
            category_function_id: $("#workstation-and-furniture-function-create-field").find(":selected").val(), 
            items: WORKSTATION_AND_FURNITURE,
            notes: $form.find( "textarea[name='notes']" ).val(),
        }
        $.ajax({
            async: true,
            url: `${REQUEST_API_URL}/workstation-and-furniture`,
            type: 'POST',
            beforeSend: function (xhr) {
                xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
            },
            data: JSON.stringify(data),
            error: function(res) {
                const response  = JSON.parse(res.responseText)
                let isRetry     = retryRequest(response)
                if(isRetry) $.ajax(this)
                else endLoadingButton('#workstation-and-furniture-create-button', 'Submit')
            },
            success: function(res) {
                endLoadingButton('#workstation-and-furniture-create-button', 'Submit')
                showSuccess(res.message)
                $('#workstation-and-furniture-create-modal').modal('hide')
            }
        });
    })

    $("body").delegate(".request-detail-workstation-and-furniture-toggle", "click", function(e) {
        REQUEST_ID = $(this).data("id")
        console.log(REQUEST_ID)

        $.ajax({
            async: true,
            url: `${REQUEST_API_URL}/id/${REQUEST_ID}`,
            type: 'GET',
            beforeSend: function (xhr) {
              xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
            },
            error: function(res) {
              const response = JSON.parse(res.responseText)
              let isRetry = retryRequest(response)
              if(isRetry) $.ajax(this)
            },
            success: function(res) {
              const response = res.data
              renderRequestWorkstationAndFurnitureForm(response)
            }
        });
    })
})

function getCategoryWorkstationAndFurniture(){
    $.ajax({
        async: true,
        url: `${CATEGORY_API_URL}?type=workstation_and_furniture`,
        type: 'GET',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
        },
        success: function(res) {
          const response = res.data
          CATEGORY_WORKSTATION_AND_FURNITURE = response
        }
    });
}

function renderRequestWorkstationAndFurnitureOption(type = "create", isReadOnly = false){
    let readonly = isReadOnly ? "readonly" : ''
    let workstation_and_furniture_html = ''
    WORKSTATION_AND_FURNITURE.forEach((item, id) => {
        let option_html = `<option>--Pilih Perabotan Kantor--</option>`
        CATEGORY_WORKSTATION_AND_FURNITURE.forEach(category => {
            option_html += `<option value="${category.id}" ${category.id == item.category_workstation_and_furniture_id ? 'selected' : ''}>${category.name}</option>`
        })
        workstation_and_furniture_html += `<div class="col-6 mt-2">
            <select class="form-control workstation-and-furniture-category" data-id="${id}" ${readonly}>
                ${option_html}
            </select>
        </div>
        <div class="col-5 mt-2">
            <input type="number" class="form-control workstation-and-furniture-qty" data-id="${id}" placeholder="Jumlah.." value="${item.qty}" ${readonly}>
        </div>`
        if(!isReadOnly){
            workstation_and_furniture_html += `<div class="col-1 mt-2">
                <button type="button" class="btn btn-danger workstation-and-furniture-delete-button" data-id="${id}">
                    <i class="fas fa-trash"></i>
                </button>
            </div>`
        }
    })

    $(`#workstation-and-furniture-${type}-section`).html(workstation_and_furniture_html)
}

function renderRequestWorkstationAndFurnitureForm(request){
    let data = request.data
    console.log("Rendering..", data)
    let $form = $('#workstation-and-furniture-detail-form')
    $form.find( "textarea[name='notes']" ).val(request.notes)
    $(`#workstation-and-furniture-function-detail-field`).val(request.category_function_id).change()
    WORKSTATION_AND_FURNITURE = data
    renderRequestWorkstationAndFurnitureOption("detail", true)
  
    let color = getColorBadge(request.status)
    let badge = `<span class="badge badge-pill badge-${color}">${request.status}</span>`
    $(`#workstation-and-furniture-status-detail-field`).html(badge)

    renderFormButton(request, "workstation-and-furniture")
}