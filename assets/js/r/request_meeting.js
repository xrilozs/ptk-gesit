let ADDITIONAL_REQUEST = [] 
let CATEGORY_ADDITIONAL_REQUEST = []
let BUILDING = []
let LIST_MEETING_ROOM = []
let DATA_MEETING_ID
let FILE_URL

$(document).ready(function(){
    getAdditionalRequest()
    getBuilding()

    $('#meeting-create-toggle').click(function(){
        $(`#meeting-create-form`)[0].reset();
        $(".meeting-room-option").prop("disabled", true)
        ADDITIONAL_REQUEST = [] 
        renderAdditionalRequestOption()
    })

    $(".building-option").change(function(){
        $(".meeting-room-option").prop("disabled", false)
        getMeetingRoom( $(this).val() )
    })

    $('#additional-request-add-button').click(function(){
        ADDITIONAL_REQUEST.push({category_additional_request_id: null, qty: null})
        renderAdditionalRequestOption()
    })

    $("body").delegate("#meeting-reject-form", "submit", function(e) {
        startLoadingButton('#meeting-reject-button')
        let $form = $(this)
        let data = {
            reject_reason: $form.find( "textarea[name='reject_reason']" ).val(),
        }

        $.ajax({
            async: true,
            url: `${REQUEST_API_URL}/reject/${REQUEST_ID}`,
            type: 'PUT',
            data: JSON.stringify(data),
            beforeSend: function (xhr) {
                xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
            },
            error: function(res) {
                const response = JSON.parse(res.responseText)
                let isRetry = retryRequest(response)
                if(isRetry) $.ajax(this)
                else endLoadingButton('#meeting-reject-button', 'Iya')
            },
            success: function(res) {
                endLoadingButton('#meeting-reject-button', 'Iya')
                showSuccess(res.message)
                $('#meeting-reject-modal').modal('toggle')
                $('#meeting-room-detail-modal').modal('toggle')
                request_table.ajax.reload()
            }
        });
    })

    $("body").delegate("#meeting-approve-button", "click", function(e) {
        startLoadingButton('#meeting-approve-button')
    
        $.ajax({
            async: true,
            url: `${REQUEST_API_URL}/confirm/${REQUEST_ID}`,
            type: 'PUT',
            beforeSend: function (xhr) {
                xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
            },
            error: function(res) {
                const response = JSON.parse(res.responseText)
                let isRetry = retryRequest(response)
                if(isRetry) $.ajax(this)
                else endLoadingButton('#meeting-approve-button', 'Iya')
            },
            success: function(res) {
                endLoadingButton('#meeting-approve-button', 'Iya')
                showSuccess(res.message)
                $('#meeting-approve-modal').modal('toggle')
                $('#meeting-room-detail-modal').modal('toggle')
                request_table.ajax.reload()
            }
        });
    })

    $("body").delegate("#meeting-complete-button", "click", function(e) {
        startLoadingButton('#meeting-complete-button')
    
        $.ajax({
            async: true,
            url: `${REQUEST_API_URL}/complete/${REQUEST_ID}`,
            type: 'PUT',
            beforeSend: function (xhr) {
                xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
            },
            error: function(res) {
                const response = JSON.parse(res.responseText)
                let isRetry = retryRequest(response)
                if(isRetry) $.ajax(this)
                else endLoadingButton('#meeting-complete-button', 'Iya')
            },
            success: function(res) {
                endLoadingButton('#meeting-complete-button', 'Iya')
                showSuccess(res.message)
                $('#meeting-complete-modal').modal('toggle')
                $('#meeting-room-detail-modal').modal('toggle')
                request_table.ajax.reload()
            }
        });
    })

    $("body").delegate("#meeting-accomplish-form", "submit", function(e) {
        startLoadingButton('#meeting-accomplish-button')
    
        $.ajax({
            async: true,
            url: `${REQUEST_API_URL}/accomplished/${REQUEST_ID}`,
            type: 'PUT',
            data: JSON.stringify({
                data_meeting_id: DATA_MEETING_ID,
                attachment: FILE_URL
            }),
            beforeSend: function (xhr) {
                xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
            },
            error: function(res) {
                const response = JSON.parse(res.responseText)
                let isRetry = retryRequest(response)
                if(isRetry) $.ajax(this)
                else endLoadingButton('#meeting-accomplish-button', 'Iya')
            },
            success: function(res) {
                endLoadingButton('#meeting-accomplish-button', 'Iya')
                showSuccess(res.message)
                $('#meeting-accomplish-modal').modal('toggle')
                $('#meeting-room-detail-modal').modal('toggle')
                request_table.ajax.reload()
            }
        });
    })

    $("body").delegate(".additional-request-delete-button", "click", function(e) {
        let id = $(this).data("id")
        ADDITIONAL_REQUEST.splice(id, 1)
        renderAdditionalRequestOption()
    })

    $("body").delegate(".additional-request-category", "change", function(e) {
        let id = $(this).data("id")
        ADDITIONAL_REQUEST[id].category_additional_request_id = $(this).val()
    })

    $("body").delegate(".additional-request-qty", "keyup", function(e) {
        let id = $(this).data("id")
        ADDITIONAL_REQUEST[id].qty = $(this).val()
    })

    $('#meeting-room-create-field').change(function(){
        let val = $(this).val()
        console.log("Selected Meeting Room: ", val)
        if(val){
            let meeting_room = LIST_MEETING_ROOM.find(item => item.id == val)
            console.log("Meeting Room: ", meeting_room)
            $('#meeting-room-capacity-create').css("display", "block")
            $('#meeting-room-capacity-create-field').val(meeting_room.capacity > 1 ? meeting_room.capacity : 1)
            $('#meeting-total-participant-create-field').attr("max", meeting_room.capacity > 1 ? meeting_room.capacity : 1)
        }else{
            $('#meeting-room-capacity-create').css("display", "none")
            $('#meeting-total-participant-create-field').attr("max", 100)
        }

    })

    $('#meeting-create-form').submit(function(e){
        e.preventDefault()

        startLoadingButton('#meeting-create-button')
    
        let $form = $(this)
        let data = {
            category_function_id: $("#meeting-function-create-field").find(":selected").val(),
            building_id: $("#meeting-building-create-field").find(":selected").val(),
            meeting_room_id: $("#meeting-room-create-field").find(":selected").val(),
            meeting_type: $("#meeting-type-create-field").find(":selected").val(),
            meeting_title: $form.find( "input[name='meeting_title']" ).val(),
            total_participant: $form.find( "input[name='total_participant']" ).val(), 
            meeting_date: $form.find( "input[name='meeting_date']" ).val(),
            start_time: $form.find( "input[name='start_time']" ).val(),
            end_time: $form.find( "input[name='end_time']" ).val(),
            meeting_invitation_url: FILE_URL,
            additional_request: ADDITIONAL_REQUEST,
            notes: $form.find( "textarea[name='notes']" ).val(),
        }
        $.ajax({
            async: true,
            url: `${REQUEST_API_URL}/meeting-room`,
            type: 'POST',
            beforeSend: function (xhr) {
                xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
            },
            data: JSON.stringify(data),
            error: function(res) {
                const response  = JSON.parse(res.responseText)
                let isRetry     = retryRequest(response)
                if(isRetry) $.ajax(this)
                else endLoadingButton('#meeting-create-button', 'Submit')
            },
            success: function(res) {
                endLoadingButton('#meeting-create-button', 'Submit')
                showSuccess(res.message)
                $('#meeting-create-modal').modal('hide')
            }
        });
    })

    $("body").delegate(".request-detail-meeting-room-toggle", "click", function(e) {
        REQUEST_ID = $(this).data("id")
        console.log(REQUEST_ID)
        let drEvent = $('#meeting-attachment-accomplish-field').dropify();
        drEvent = drEvent.data('dropify');
        drEvent.resetPreview();
        drEvent.clearElement();

        $.ajax({
            async: true,
            url: `${REQUEST_API_URL}/id/${REQUEST_ID}`,
            type: 'GET',
            beforeSend: function (xhr) {
              xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
            },
            error: function(res) {
              const response = JSON.parse(res.responseText)
              let isRetry = retryRequest(response)
              if(isRetry) $.ajax(this)
            },
            success: function(res) {
              const response = res.data
              renderRequestMeetingRoomForm(response)
            }
        });
    })

    $('#meeting-invitation-create-field').change(function(e) {
        let file = e.target.files[0];
        upload_attachment(file)
    });

    $('#meeting-attachment-accomplish-field').change(function(e) {
        let file = e.target.files[0];
        upload_attachment(file)
    });
})

function getMeetingRoom(buildingId){
    $.ajax({
        async: true,
        url: `${MEETING_ROOM_API_URL}?building_id=${buildingId}`,
        type: 'GET',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
        },
        success: function(res) {
          const response = res.data
          LIST_MEETING_ROOM   = response
          renderMeetingRoomOption(response)
        }
    });
}

function getBuilding(){
    $.ajax({
        async: true,
        url: `${BUILDING_API_URL}`,
        type: 'GET',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
        },
        success: function(res) {
          const response = res.data
          BUILDING   = response
          renderBuildingOption(response)
        }
    });
}

function getAdditionalRequest(){
    $.ajax({
        async: true,
        url: `${CATEGORY_API_URL}?type=additional_request`,
        type: 'GET',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
        },
        success: function(res) {
          const response = res.data
          CATEGORY_ADDITIONAL_REQUEST = response
        }
    });
}

function renderMeetingRoomOption(meeting_room){
    let meeting_room_html = '<option>--Pilih Ruangan--</option>'
    if(meeting_room.length > 0){
        meeting_room.forEach(item => {
            meeting_room_html += `<option value="${item.id}">${item.name} (Lantai ${item.floor_number})</option>`
        });
    }else{
        meeting_room_html = `<option disabled>-Ruangan tidak tersedia-</option>`
    }

    $('.meeting-room-option').html(meeting_room_html)
}

function renderBuildingOption(building){
    let building_html = '<option>--Pilih Gedung--</option>'
    if(building.length > 0){
        building.forEach(item => {
            building_html += `<option value="${item.id}">${item.name}</option>`
        });
    }else{
        building_html = `<option disabled>-Gedung tidak tersedia-</option>`
    }

    $('.building-option').html(building_html)
}

function renderAdditionalRequestOption(type = "create", isReadOnly = false){
    let readonly = isReadOnly ? "readonly" : ''
    let additional_request_html = ''
    ADDITIONAL_REQUEST.forEach((item, id) => {
        let option_html = `<option>--Pilih Jenis Tambahan--</option>`
        CATEGORY_ADDITIONAL_REQUEST.forEach(category => {
            option_html += `<option value="${category.id}" ${category.id == item.category_additional_request_id ? 'selected' : ''}>${category.name}</option>`
        })
        additional_request_html += `<div class="col-6 mt-2">
            <select class="form-control additional-request-category" data-id="${id}" ${readonly}>
                ${option_html}
            </select>
        </div>
        <div class="col-5 mt-2">
            <input type="number" class="form-control additional-request-qty" data-id="${id}" placeholder="Jumlah.." value="${item.qty}" ${readonly}>
        </div>`
        if(!isReadOnly){
            additional_request_html += `<div class="col-1 mt-2">
                <button type="button" class="btn btn-danger additional-request-delete-button" data-id="${id}">
                    <i class="fas fa-trash"></i>
                </button>
            </div>`
        }
    })

    $(`#additional-request-${type}-section`).html(additional_request_html)
}

function renderRequestMeetingRoomForm(request){
    let data            = request.data[0]
    DATA_MEETING_ID     = data.id
    let $form           = $('#meeting-detail-form')
    console.log("Rendering..", data)
    $(`#meeting-function-detail-field`).val(request.category_function_id).change()
    // $(`.meeting-room-option`).val(data.meeting_room_id).change()
    $(`#meeting-type-detail-field`).val(data.meeting_type).change()
    $form.find( "input[name='meeting_room']" ).val(data.meeting_room)
    $form.find( "input[name='building']" ).val(data.building)
    $form.find( "input[name='meeting_title']" ).val(data.meeting_title)
    $form.find( "input[name='total_participant']" ).val(data.total_participant) 
    $form.find( "input[name='meeting_date']" ).val(data.meeting_date)
    $form.find( "input[name='start_time']" ).val(data.start_time)
    $form.find( "input[name='end_time']" ).val(data.end_time)
    $form.find( "textarea[name='notes']" ).val(request.notes)
    ADDITIONAL_REQUEST = data.additional_request
    renderAdditionalRequestOption("detail", true)

    if(data.attachment){    
        $('#meeting-attachment-detail').html(`<a href="${WEB_URL}/${data.attachment}" target="_blank">Download</a>`);
    }else{
        $('#meeting-attachment-detail').html(`-`);
    }

    if(data.meeting_invitation_url){    
        $('#meeting-invitation-detail').html(`<a href="${WEB_URL}/${data.meeting_invitation_url}" target="_blank">Download</a>`);
    }else{
        $('#meeting-invitation-detail').html(`-`);
    }
  
    let color = getColorBadge(request.status)
    let badge = `<span class="badge badge-pill badge-${color}">${request.status}</span>`
    $(`#meeting-status-detail-field`).html(badge)

    renderFormButton(request, "meeting")
}

//upload image
function upload_attachment(file){
    $(`#meeting-accomplish-button`).attr("disabled", true)
    $(`#meeting-create-button`).attr("disabled", true)
  
    let formData = new FormData();
    formData.append('file', file);
    
    $.ajax({
        async: true,
        url: `${REQUEST_API_URL}/upload`,
        type: 'POST',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: formData,
        processData: false, 
        contentType: false, 
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else{
            showError("Gagal melakukan upload lampiran!")
            $(`#meeting-create-button`).attr("disabled", false)
            $(`#meeting-accomplish-button`).attr("disabled", false)
          }
        },
        success: function(res) {
          const response = res.data
          FILE_URL       = response.file_url
  
          $(`#meeting-create-button`).attr("disabled", false)
          $(`#meeting-accomplish-button`).attr("disabled", false)
          showSuccess(res.message)
        }
    });
}