let MEETING_ROOM = []
let SELECTED_MEETING_ROOM = null
let MEETING_CONSUMPTION = [] 
let CATEGORY_MEETING_CONSUMPTION = []

$(document).ready(function(){
    getRequestMeetingRoom()
    getCategoryMeetingConsumption()

    $('#meeting-consumption-create-toggle').click(function(){
        $(`#meeting-consumption-create-form`)[0].reset();
        MEETING_CONSUMPTION = []
        renderRequestMeetingOption()
    })

    $('#meeting-consumption-add-button').click(function(){
        MEETING_CONSUMPTION.push({category_meeting_consumption_id: null, qty: SELECTED_MEETING_ROOM.total_participant})
        console.log("TEST")
        renderRequestMeetingOption()
    })

    $("body").delegate("#meeting-consumption-reject-form", "submit", function(e) {
        startLoadingButton('#meeting-consumption-reject-button')
        let $form = $(this)
        let data = {
            reject_reason: $form.find( "textarea[name='reject_reason']" ).val(),
        }

        $.ajax({
            async: true,
            url: `${REQUEST_API_URL}/reject/${REQUEST_ID}`,
            type: 'PUT',
            data: JSON.stringify(data),
            beforeSend: function (xhr) {
                xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
            },
            error: function(res) {
                const response = JSON.parse(res.responseText)
                let isRetry = retryRequest(response)
                if(isRetry) $.ajax(this)
                else endLoadingButton('#meeting-consumption-reject-button', 'Iya')
            },
            success: function(res) {
                endLoadingButton('#meeting-consumption-reject-button', 'Iya')
                showSuccess(res.message)
                $('#meeting-consumption-reject-modal').modal('toggle')
                $('#meeting-consumption-detail-modal').modal('toggle')
                request_table.ajax.reload()
            }
        });
    })

    $("body").delegate("#meeting-consumption-approve-button", "click", function(e) {
        startLoadingButton('#meeting-consumption-approve-button')
    
        $.ajax({
            async: true,
            url: `${REQUEST_API_URL}/confirm/${REQUEST_ID}`,
            type: 'PUT',
            beforeSend: function (xhr) {
                xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
            },
            error: function(res) {
                const response = JSON.parse(res.responseText)
                let isRetry = retryRequest(response)
                if(isRetry) $.ajax(this)
                else endLoadingButton('#meeting-consumption-approve-button', 'Iya')
            },
            success: function(res) {
                endLoadingButton('#meeting-consumption-approve-button', 'Iya')
                showSuccess(res.message)
                $('#meeting-consumption-approve-modal').modal('toggle')
                $('#meeting-consumption-detail-modal').modal('toggle')
                request_table.ajax.reload()
            }
        });
    })

    $("body").delegate("#meeting-consumption-complete-button", "click", function(e) {
        startLoadingButton('#meeting-consumption-complete-button')
    
        $.ajax({
            async: true,
            url: `${REQUEST_API_URL}/complete/${REQUEST_ID}`,
            type: 'PUT',
            beforeSend: function (xhr) {
                xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
            },
            error: function(res) {
                const response = JSON.parse(res.responseText)
                let isRetry = retryRequest(response)
                if(isRetry) $.ajax(this)
                else endLoadingButton('#meeting-consumption-complete-button', 'Iya')
            },
            success: function(res) {
                endLoadingButton('#meeting-consumption-complete-button', 'Iya')
                showSuccess(res.message)
                $('#meeting-consumption-complete-modal').modal('toggle')
                $('#meeting-consumption-detail-modal').modal('toggle')
                request_table.ajax.reload()
            }
        });
    })

    $("body").delegate("#meeting-consumption-accomplish-button", "click", function(e) {
        startLoadingButton('#meeting-consumption-accomplish-button')
    
        $.ajax({
            async: true,
            url: `${REQUEST_API_URL}/accomplished/${REQUEST_ID}`,
            type: 'PUT',
            beforeSend: function (xhr) {
                xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
            },
            error: function(res) {
                const response = JSON.parse(res.responseText)
                let isRetry = retryRequest(response)
                if(isRetry) $.ajax(this)
                else endLoadingButton('#meeting-consumption-accomplish-button', 'Iya')
            },
            success: function(res) {
                endLoadingButton('#meeting-consumption-accomplish-button', 'Iya')
                showSuccess(res.message)
                $('#meeting-consumption-accomplish-modal').modal('toggle')
                $('#meeting-consumption-detail-modal').modal('toggle')
                request_table.ajax.reload()
            }
        });
    })

    $("body").delegate(".meeting-consumption-delete-button", "click", function(e) {
        let id = $(this).data("id")
        MEETING_CONSUMPTION.splice(id, 1)
        renderRequestMeetingOption()
    })

    $("body").delegate(".meeting-consumption-category", "change", function(e) {
        let id = $(this).data("id")
        MEETING_CONSUMPTION[id].category_meeting_consumption_id = $(this).val()
    })

    // $("body").delegate(".meeting-consumption-qty", "keyup", function(e) {
    //     let id = $(this).data("id")
    //     MEETING_CONSUMPTION[id].qty = $(this).val()
    // })

    $(".meeting-option").change(function(){
        let id = $(this).val()
        getDetailRequestMeetingRoom(id)
        MEETING_CONSUMPTION = []
        renderRequestMeetingOption()
    })

    $('#meeting-consumption-create-form').submit(function(e){
        e.preventDefault()

        startLoadingButton('#meeting-consumption-create-button')
    
        let $form = $(this)
        let data = {
            category_function_id: $("#meeting-consumption-function-create-field").find(":selected").val(), 
            data_meeting_id: $("#meeting-consumption-meeting-create-field").find(":selected").val(),
            items: MEETING_CONSUMPTION,
            notes: $form.find( "textarea[name='notes']" ).val(),
        }
        $.ajax({
            async: true,
            url: `${REQUEST_API_URL}/meeting-consumption`,
            type: 'POST',
            beforeSend: function (xhr) {
                xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
            },
            data: JSON.stringify(data),
            error: function(res) {
                const response  = JSON.parse(res.responseText)
                let isRetry     = retryRequest(response)
                if(isRetry) $.ajax(this)
                else endLoadingButton('#meeting-consumption-create-button', 'Submit')
            },
            success: function(res) {
                endLoadingButton('#meeting-consumption-create-button', 'Submit')
                showSuccess(res.message)
                $('#meeting-consumption-create-modal').modal('hide')
            }
        });
    })

    $("body").delegate(".request-detail-meeting-consumption-toggle", "click", function(e) {
        REQUEST_ID = $(this).data("id")
        console.log(REQUEST_ID)

        $.ajax({
            async: true,
            url: `${REQUEST_API_URL}/id/${REQUEST_ID}`,
            type: 'GET',
            beforeSend: function (xhr) {
              xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
            },
            error: function(res) {
              const response = JSON.parse(res.responseText)
              let isRetry = retryRequest(response)
              if(isRetry) $.ajax(this)
            },
            success: function(res) {
              const response = res.data
              renderRequestMeetingConsumptionForm(response)
            }
        });
    })
})

function getRequestMeetingRoom(){
    const status = "ON PROGRESS"
    const category = "MEETING ROOM"
    $.ajax({
        async: true,
        url: `${REQUEST_API_URL}?status=${status}&request_category=${category}`,
        type: 'GET',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
        },
        success: function(res) {
          const response = res.data
          MEETING_ROOM = response
          renderRequestMeetingRoomOption(response)
        }
    });
}

function getDetailRequestMeetingRoom(id){
    $.ajax({
        async: true,
        url: `${REQUEST_API_URL}/meeting-room/id/${id}`,
        type: 'GET',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
        },
        success: function(res) {
          const response = res.data
          SELECTED_MEETING_ROOM = response
        }
    });
}

function getCategoryMeetingConsumption(){
    $.ajax({
        async: true,
        url: `${CATEGORY_API_URL}?type=meeting_consumption`,
        type: 'GET',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
        },
        success: function(res) {
          const response = res.data
          CATEGORY_MEETING_CONSUMPTION = response
        }
    });
}

function renderRequestMeetingRoomOption(request_meeting_room){
    let request_meeting_room_html = '<option>--Pilih Meeting--</option>'
    if(request_meeting_room.length > 0){
        request_meeting_room.forEach(item => {
            request_meeting_room_html += `<option value="${item.meeting_id}">${item.meeting_title}</option>`
        });
    }else{
        request_meeting_room_html = `<option disabled>-Meeting tidak tersedia-</option>`
    }

    $('.meeting-option').html(request_meeting_room_html)
}

function renderRequestMeetingOption(type = "create", isReadOnly = false){
    let readonly = isReadOnly ? "readonly" : ''
    let meeting_consumption_html = ''
    MEETING_CONSUMPTION.forEach((item, id) => {
        let option_html = `<option>--Pilih Jenis Konsumsi--</option>`
        CATEGORY_MEETING_CONSUMPTION.forEach(category => {
            option_html += `<option value="${category.id}" ${category.id == item.category_meeting_consumption_id ? 'selected' : ''}>${category.name}</option>`
        })
        meeting_consumption_html += `<div class="col-6 mt-2">
            <select class="form-control meeting-consumption-category" data-id="${id}" ${readonly}>
                ${option_html}
            </select>
        </div>
        <div class="col-5 mt-2">
            <input type="number" class="form-control meeting-consumption-qty" data-id="${id}" placeholder="Jumlah.." value="${SELECTED_MEETING_ROOM ? SELECTED_MEETING_ROOM.total_participant : item.qty}" readonly>
        </div>`
        if(!isReadOnly){
            meeting_consumption_html += `<div class="col-1 mt-2">
                <button type="button" class="btn btn-danger meeting-consumption-delete-button" data-id="${id}">
                    <i class="fas fa-trash"></i>
                </button>
            </div>`
        }
    })

    $(`#meeting-consumption-${type}-section`).html(meeting_consumption_html)
}

function renderRequestMeetingConsumptionForm(request){
    let data = request.data
    console.log("Rendering..", data)
    let $form = $('#meeting-consumption-detail-form')
    $form.find( "input[name='meeting']" ).val(data[0].meeting_title)
    $form.find( "textarea[name='notes']" ).val(request.notes)
    $(`#meeting-consumption-function-detail-field`).val(request.category_function_id).change()
    MEETING_CONSUMPTION = data
    renderRequestMeetingOption("detail", true)
  
    let color = getColorBadge(request.status)
    let badge = `<span class="badge badge-pill badge-${color}">${request.status}</span>`
    $(`#meeting-consumption-status-detail-field`).html(badge)

    renderFormButton(request, "meeting-consumption")
}