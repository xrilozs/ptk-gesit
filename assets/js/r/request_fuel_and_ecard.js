let FUEL_AND_ECARD = [] 
let CATEGORY_FUEL_AND_ECARD = []

$(document).ready(function(){
    getCategoryFuelAndECard()
    $('#fuel-and-ecard-create-toggle').click(function(){
        $(`#fuel-and-ecard-create-form`)[0].reset();
        FUEL_AND_ECARD = []
        renderRequestFuelAndECardOption()
    })

    $('#fuel-and-ecard-add-button').click(function(){
        FUEL_AND_ECARD.push({category_fuel_and_ecard_id: null})
        console.log("TEST")
        renderRequestFuelAndECardOption()
    })

    $("body").delegate("#fuel-and-ecard-reject-form", "submit", function(e) {
        startLoadingButton('#fuel-and-ecard-reject-button')
        let $form = $(this)
        let data = {
            reject_reason: $form.find( "textarea[name='reject_reason']" ).val(),
        }
    
        $.ajax({
            async: true,
            url: `${REQUEST_API_URL}/reject/${REQUEST_ID}`,
            type: 'PUT',
            data: JSON.stringify(data),
            beforeSend: function (xhr) {
                xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
            },
            error: function(res) {
                const response = JSON.parse(res.responseText)
                let isRetry = retryRequest(response)
                if(isRetry) $.ajax(this)
                else endLoadingButton('#fuel-and-ecard-reject-button', 'Iya')
            },
            success: function(res) {
                endLoadingButton('#fuel-and-ecard-reject-button', 'Iya')
                showSuccess(res.message)
                $('#fuel-and-ecard-reject-modal').modal('toggle')
                $('#fuel-and-ecard-detail-modal').modal('toggle')
                request_table.ajax.reload()
            }
        });
    })

    $("body").delegate("#fuel-and-ecard-approve-button", "click", function(e) {
        startLoadingButton('#fuel-and-ecard-approve-button')
    
        $.ajax({
            async: true,
            url: `${REQUEST_API_URL}/confirm/${REQUEST_ID}`,
            type: 'PUT',
            beforeSend: function (xhr) {
                xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
            },
            error: function(res) {
                const response = JSON.parse(res.responseText)
                let isRetry = retryRequest(response)
                if(isRetry) $.ajax(this)
                else endLoadingButton('#fuel-and-ecard-approve-button', 'Iya')
            },
            success: function(res) {
                endLoadingButton('#fuel-and-ecard-approve-button', 'Iya')
                showSuccess(res.message)
                $('#fuel-and-ecard-approve-modal').modal('toggle')
                $('#fuel-and-ecard-detail-modal').modal('toggle')
                request_table.ajax.reload()
            }
        });
    })

    $("body").delegate("#fuel-and-ecard-complete-button", "click", function(e) {
        startLoadingButton('#fuel-and-ecard-complete-button')
    
        $.ajax({
            async: true,
            url: `${REQUEST_API_URL}/complete/${REQUEST_ID}`,
            type: 'PUT',
            beforeSend: function (xhr) {
                xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
            },
            error: function(res) {
                const response = JSON.parse(res.responseText)
                let isRetry = retryRequest(response)
                if(isRetry) $.ajax(this)
                else endLoadingButton('#fuel-and-ecard-complete-button', 'Iya')
            },
            success: function(res) {
                endLoadingButton('#fuel-and-ecard-complete-button', 'Iya')
                showSuccess(res.message)
                $('#fuel-and-ecard-complete-modal').modal('toggle')
                $('#fuel-and-ecard-detail-modal').modal('toggle')
                request_table.ajax.reload()
            }
        });
    })

    $("body").delegate("#fuel-and-ecard-accomplish-button", "click", function(e) {
        startLoadingButton('#fuel-and-ecard-accomplish-button')
    
        $.ajax({
            async: true,
            url: `${REQUEST_API_URL}/accomplished/${REQUEST_ID}`,
            type: 'PUT',
            beforeSend: function (xhr) {
                xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
            },
            error: function(res) {
                const response = JSON.parse(res.responseText)
                let isRetry = retryRequest(response)
                if(isRetry) $.ajax(this)
                else endLoadingButton('#fuel-and-ecard-accomplish-button', 'Iya')
            },
            success: function(res) {
                endLoadingButton('#fuel-and-ecard-accomplish-button', 'Iya')
                showSuccess(res.message)
                $('#fuel-and-ecard-accomplish-modal').modal('toggle')
                $('#fuel-and-ecard-detail-modal').modal('toggle')
                request_table.ajax.reload()
            }
        });
    })

    $("body").delegate(".fuel-and-ecard-delete-button", "click", function(e) {
        let id = $(this).data("id")
        FUEL_AND_ECARD.splice(id, 1)
        renderRequestFuelAndECardOption()
    })

    $("body").delegate(".fuel-and-ecard-category", "change", function(e) {
        let id = $(this).data("id")
        FUEL_AND_ECARD[id].category_fuel_and_ecard_id = $(this).val()
    })

    // $("body").delegate(".fuel-and-ecard-qty", "change", function(e) {
    //     let id = $(this).data("id")
    //     FUEL_AND_ECARD[id].qty = $(this).val()
    // })

    $('#fuel-and-ecard-create-form').submit(function(e){
        e.preventDefault()

        startLoadingButton('#fuel-and-ecard-create-button')
    
        let $form = $(this)
        let data = {
            category_function_id: $("#fuel-and-ecard-function-create-field").find(":selected").val(), 
            items: FUEL_AND_ECARD,
            notes: $form.find( "textarea[name='notes']" ).val(),
        }
        $.ajax({
            async: true,
            url: `${REQUEST_API_URL}/fuel-and-ecard`,
            type: 'POST',
            beforeSend: function (xhr) {
                xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
            },
            data: JSON.stringify(data),
            error: function(res) {
                const response  = JSON.parse(res.responseText)
                let isRetry     = retryRequest(response)
                if(isRetry) $.ajax(this)
                else endLoadingButton('#fuel-and-ecard-create-button', 'Submit')
            },
            success: function(res) {
                endLoadingButton('#fuel-and-ecard-create-button', 'Submit')
                showSuccess(res.message)
                $('#fuel-and-ecard-create-modal').modal('hide')
            }
        });
    })

    $("body").delegate(".request-detail-fuel-and-ecard-toggle", "click", function(e) {
        REQUEST_ID = $(this).data("id")
        console.log(REQUEST_ID)

        $.ajax({
            async: true,
            url: `${REQUEST_API_URL}/id/${REQUEST_ID}`,
            type: 'GET',
            beforeSend: function (xhr) {
              xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
            },
            error: function(res) {
              const response = JSON.parse(res.responseText)
              let isRetry = retryRequest(response)
              if(isRetry) $.ajax(this)
            },
            success: function(res) {
              const response = res.data
              renderRequestFuelAndECardForm(response)
            }
        });
    })
})

function getCategoryFuelAndECard(){
    $.ajax({
        async: true,
        url: `${CATEGORY_API_URL}?type=fuel_and_ecard`,
        type: 'GET',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
        },
        success: function(res) {
          const response = res.data
          CATEGORY_FUEL_AND_ECARD = response
        }
    });
}

function renderRequestFuelAndECardOption(type = "create", isReadOnly = false){
    let readonly = isReadOnly ? "readonly" : ''
    let fuel_and_ecard_html = ''
    FUEL_AND_ECARD.forEach((item, id) => {
        let option_html = `<option>--Pilih Bahan Bakar dan Kartu Elektronik--</option>`
        CATEGORY_FUEL_AND_ECARD.forEach(category => {
            option_html += `<option value="${category.id}" ${category.id == item.category_fuel_and_ecard_id ? 'selected' : ''}>${category.name}</option>`
        })
        fuel_and_ecard_html += `<div class="col-11 mt-2">
            <select class="form-control fuel-and-ecard-category" data-id="${id}" ${readonly}>
                ${option_html}
            </select>
        </div>`
        if(!isReadOnly){
            fuel_and_ecard_html += `<div class="col-1 mt-2">
                <button type="button" class="btn btn-danger fuel-and-ecard-delete-button" data-id="${id}">
                    <i class="fas fa-trash"></i>
                </button>
            </div>`
        }
    })

    $(`#fuel-and-ecard-${type}-section`).html(fuel_and_ecard_html)
}

function renderRequestFuelAndECardForm(request){
    console.log("Rendering..", request.data)
    FUEL_AND_ECARD  = request.data
    let $form       = $('#fuel-and-ecard-detail-form')
    $form.find( "textarea[name='notes']" ).val(request.notes)
    $(`#fuel-and-ecard-function-detail-field`).val(request.category_function_id).change()
    renderRequestFuelAndECardOption("detail", true)
  
    let color = getColorBadge(request.status)
    let badge = `<span class="badge badge-pill badge-${color}">${request.status}</span>`
    $(`#fuel-and-ecard-status-detail-field`).html(badge)

    renderFormButton(request, "fuel-and-ecard")
}