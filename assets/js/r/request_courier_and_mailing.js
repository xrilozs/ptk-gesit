let COURIER_AND_MAILING = [] 
let CATEGORY_COURIER_AND_MAILING = []

$(document).ready(function(){
    // getCategoryCourierAndMailing()
    // $('#courier-and-mailing-create-toggle').click(function(){
    //     $(`#courier-and-mailing-create-form`)[0].reset();
    //     COURIER_AND_MAILING = []
    //     renderRequestCourierAndMailingOption()
    // })

    // $('#courier-and-mailing-add-button').click(function(){
    //     COURIER_AND_MAILING.push({category_courier_and_mailing_id: null, qty: null})
    //     console.log("TEST")
    //     renderRequestCourierAndMailingOption()
    // })

    $("body").delegate("#courier-and-mailing-reject-form", "submit", function(e) {
        startLoadingButton('#courier-and-mailing-reject-button')
        let $form = $(this)
        let data = {
            reject_reason: $form.find( "textarea[name='reject_reason']" ).val(),
        }

        $.ajax({
            async: true,
            url: `${REQUEST_API_URL}/reject/${REQUEST_ID}`,
            type: 'PUT',
            data: JSON.stringify(data),
            beforeSend: function (xhr) {
                xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
            },
            error: function(res) {
                const response = JSON.parse(res.responseText)
                let isRetry = retryRequest(response)
                if(isRetry) $.ajax(this)
                else endLoadingButton('#courier-and-mailing-reject-button', 'Iya')
            },
            success: function(res) {
                endLoadingButton('#courier-and-mailing-reject-button', 'Iya')
                showSuccess(res.message)
                $('#courier-and-mailing-reject-modal').modal('toggle')
                $('#courier-and-mailing-detail-modal').modal('toggle')
                request_table.ajax.reload()
            }
        });
    })

    $("body").delegate("#courier-and-mailing-approve-button", "click", function(e) {
        startLoadingButton('#courier-and-mailing-approve-button')
    
        $.ajax({
            async: true,
            url: `${REQUEST_API_URL}/confirm/${REQUEST_ID}`,
            type: 'PUT',
            beforeSend: function (xhr) {
                xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
            },
            error: function(res) {
                const response = JSON.parse(res.responseText)
                let isRetry = retryRequest(response)
                if(isRetry) $.ajax(this)
                else endLoadingButton('#courier-and-mailing-approve-button', 'Iya')
            },
            success: function(res) {
                endLoadingButton('#courier-and-mailing-approve-button', 'Iya')
                showSuccess(res.message)
                $('#courier-and-mailing-approve-modal').modal('toggle')
                $('#courier-and-mailing-detail-modal').modal('toggle')
                request_table.ajax.reload()
            }
        });
    })

    $("body").delegate("#courier-and-mailing-complete-button", "click", function(e) {
        startLoadingButton('#courier-and-mailing-complete-button')
    
        $.ajax({
            async: true,
            url: `${REQUEST_API_URL}/complete/${REQUEST_ID}`,
            type: 'PUT',
            beforeSend: function (xhr) {
                xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
            },
            error: function(res) {
                const response = JSON.parse(res.responseText)
                let isRetry = retryRequest(response)
                if(isRetry) $.ajax(this)
                else endLoadingButton('#courier-and-mailing-complete-button', 'Iya')
            },
            success: function(res) {
                endLoadingButton('#courier-and-mailing-complete-button', 'Iya')
                showSuccess(res.message)
                $('#courier-and-mailing-complete-modal').modal('toggle')
                $('#courier-and-mailing-detail-modal').modal('toggle')
                request_table.ajax.reload()
            }
        });
    })

    $("body").delegate("#courier-and-mailing-accomplish-button", "click", function(e) {
        startLoadingButton('#courier-and-mailing-accomplish-button')
    
        $.ajax({
            async: true,
            url: `${REQUEST_API_URL}/accomplished/${REQUEST_ID}`,
            type: 'PUT',
            beforeSend: function (xhr) {
                xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
            },
            error: function(res) {
                const response = JSON.parse(res.responseText)
                let isRetry = retryRequest(response)
                if(isRetry) $.ajax(this)
                else endLoadingButton('#courier-and-mailing-accomplish-button', 'Iya')
            },
            success: function(res) {
                endLoadingButton('#courier-and-mailing-accomplish-button', 'Iya')
                showSuccess(res.message)
                $('#courier-and-mailing-accomplish-modal').modal('toggle')
                $('#courier-and-mailing-detail-modal').modal('toggle')
                request_table.ajax.reload()
            }
        });
    })

    $("body").delegate(".courier-and-mailing-delete-button", "click", function(e) {
        let id = $(this).data("id")
        COURIER_AND_MAILING.splice(id, 1)
        renderRequestCourierAndMailingOption()
    })

    // $("body").delegate(".courier-and-mailing-category", "change", function(e) {
    //     let id = $(this).data("id")
    //     COURIER_AND_MAILING[id].category_courier_and_mailing_id = $(this).val()
    // })

    // $("body").delegate(".courier-and-mailing-qty", "change", function(e) {
    //     let id = $(this).data("id")
    //     COURIER_AND_MAILING[id].qty = $(this).val()
    // })

    $('#courier-and-mailing-create-form').submit(function(e){
        e.preventDefault()

        startLoadingButton('#courier-and-mailing-create-button')
    
        let $form = $(this)
        let data = {
            category_function_id: $("#courier-and-mailing-function-create-field").find(":selected").val(), 
            delivery_date: $form.find( "input[name='delivery_date']" ).val(),
            destination: $form.find( "input[name='destination']" ).val(),
            receiver_name: $form.find( "input[name='receiver_name']" ).val(),
            receiver_phone: $form.find( "input[name='receiver_phone']" ).val(),
            notes: $form.find( "textarea[name='notes']" ).val(),
        }
        $.ajax({
            async: true,
            url: `${REQUEST_API_URL}/courier-and-mailing`,
            type: 'POST',
            beforeSend: function (xhr) {
                xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
            },
            data: JSON.stringify(data),
            error: function(res) {
                const response  = JSON.parse(res.responseText)
                let isRetry     = retryRequest(response)
                if(isRetry) $.ajax(this)
                else endLoadingButton('#courier-and-mailing-create-button', 'Submit')
            },
            success: function(res) {
                endLoadingButton('#courier-and-mailing-create-button', 'Submit')
                showSuccess(res.message)
                $('#courier-and-mailing-create-modal').modal('hide')
            }
        });
    })

    $("body").delegate(".request-detail-courier-and-mailing-toggle", "click", function(e) {
        REQUEST_ID = $(this).data("id")
        console.log(REQUEST_ID)

        $.ajax({
            async: true,
            url: `${REQUEST_API_URL}/id/${REQUEST_ID}`,
            type: 'GET',
            beforeSend: function (xhr) {
              xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
            },
            error: function(res) {
              const response = JSON.parse(res.responseText)
              let isRetry = retryRequest(response)
              if(isRetry) $.ajax(this)
            },
            success: function(res) {
              const response = res.data
              renderRequestCourierAndMailingForm(response)
            }
        });
    })
})

// function getCategoryCourierAndMailing(){
//     $.ajax({
//         async: true,
//         url: `${CATEGORY_API_URL}?type=courier_and_mailing`,
//         type: 'GET',
//         beforeSend: function (xhr) {
//           xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
//         },
//         error: function(res) {
//           const response = JSON.parse(res.responseText)
//           let isRetry = retryRequest(response)
//           if(isRetry) $.ajax(this)
//         },
//         success: function(res) {
//           const response = res.data
//           CATEGORY_COURIER_AND_MAILING = response
//         }
//     });
// }

// function renderRequestCourierAndMailingOption(type = "create", isReadOnly = false){
//     let readonly = isReadOnly ? "readonly" : ''
//     let courier_and_mailing_html = ''
//     COURIER_AND_MAILING.forEach((item, id) => {
//         let option_html = `<option>--Pilih Kurir dan Pesan--</option>`
//         CATEGORY_COURIER_AND_MAILING.forEach(category => {
//             option_html += `<option value="${category.id}" ${category.id == item.category_courier_and_mailing_id ? 'selected' : ''}>${category.name}</option>`
//         })
//         courier_and_mailing_html += `<div class="col-6 mt-2">
//             <select class="form-control courier-and-mailing-category" data-id="${id}" ${readonly}>
//                 ${option_html}
//             </select>
//         </div>
//         <div class="col-5 mt-2">
//             <input type="number" class="form-control courier-and-mailing-qty" data-id="${id}" placeholder="Jumlah.." value="${item.qty}" ${readonly}>
//         </div>`
//         if(!isReadOnly){
//             courier_and_mailing_html += `<div class="col-1 mt-2">
//                 <button type="button" class="btn btn-danger courier-and-mailing-delete-button" data-id="${id}">
//                     <i class="fas fa-trash"></i>
//                 </button>
//             </div>`
//         }
//     })

//     $(`#courier-and-mailing-${type}-section`).html(courier_and_mailing_html)
// }

function renderRequestCourierAndMailingForm(request){
    console.log("Rendering..", request.data)
    let data    = request.data[0]
    let $form   = $('#courier-and-mailing-detail-form')
    $form.find( "input[name='delivery_date']" ).val(data.delivery_date)
    $form.find( "input[name='destination']" ).val(data.destination)
    $form.find( "input[name='receiver_name']" ).val(data.receiver_name)
    $form.find( "input[name='receiver_phone']" ).val(data.receiver_phone)
    $form.find( "textarea[name='notes']" ).val(request.notes)
    $(`#courier-and-mailing-function-detail-field`).val(request.category_function_id).change()
  
    let color = getColorBadge(request.status)
    let badge = `<span class="badge badge-pill badge-${color}">${request.status}</span>`
    $(`#courier-and-mailing-status-detail-field`).html(badge)

    renderFormButton(request, "courier-and-mailing")
}