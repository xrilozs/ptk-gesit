let ASSET = []
let CATEGORY_ASSET = []

$(document).ready(function(){
    getCategoryAsset()

    $('#asset-create-toggle').click(function(){
        $(`#asset-create-form`)[0].reset();
        ASSET = []
        renderRequestAssetOption()
    })

    $('#asset-add-button').click(function(){
        ASSET.push({category_asset_id: null})
        renderRequestAssetOption()
    })

    $("body").delegate("#asset-reject-form", "submit", function(e) {
        startLoadingButton('#asset-reject-button')
        let $form = $(this)
        let data = {
            reject_reason: $form.find( "textarea[name='reject_reason']" ).val(),
        }
    
        $.ajax({
            async: true,
            url: `${REQUEST_API_URL}/reject/${REQUEST_ID}`,
            type: 'PUT',
            data: JSON.stringify(data),
            beforeSend: function (xhr) {
                xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
            },
            error: function(res) {
                const response = JSON.parse(res.responseText)
                let isRetry = retryRequest(response)
                if(isRetry) $.ajax(this)
                else endLoadingButton('#asset-reject-button', 'Iya')
            },
            success: function(res) {
                endLoadingButton('#asset-reject-button', 'Iya')
                showSuccess(res.message)
                $('#asset-reject-modal').modal('toggle')
                $('#asset-detail-modal').modal('toggle')
                request_table.ajax.reload()
            }
        });
    })

    $("body").delegate("#asset-approve-button", "click", function(e) {
        startLoadingButton('#asset-approve-button')
    
        $.ajax({
            async: true,
            url: `${REQUEST_API_URL}/confirm/${REQUEST_ID}`,
            type: 'PUT',
            beforeSend: function (xhr) {
                xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
            },
            error: function(res) {
                const response = JSON.parse(res.responseText)
                let isRetry = retryRequest(response)
                if(isRetry) $.ajax(this)
                else endLoadingButton('#asset-approve-button', 'Iya')
            },
            success: function(res) {
                endLoadingButton('#asset-approve-button', 'Iya')
                showSuccess(res.message)
                $('#asset-approve-modal').modal('toggle')
                $('#asset-detail-modal').modal('toggle')
                request_table.ajax.reload()
            }
        });
    })

    $("body").delegate("#asset-complete-button", "click", function(e) {
        startLoadingButton('#asset-complete-button')
    
        $.ajax({
            async: true,
            url: `${REQUEST_API_URL}/complete/${REQUEST_ID}`,
            type: 'PUT',
            beforeSend: function (xhr) {
                xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
            },
            error: function(res) {
                const response = JSON.parse(res.responseText)
                let isRetry = retryRequest(response)
                if(isRetry) $.ajax(this)
                else endLoadingButton('#asset-complete-button', 'Iya')
            },
            success: function(res) {
                endLoadingButton('#asset-complete-button', 'Iya')
                showSuccess(res.message)
                $('#asset-complete-modal').modal('toggle')
                $('#asset-detail-modal').modal('toggle')
                request_table.ajax.reload()
            }
        });
    })

    $("body").delegate("#asset-accomplish-button", "click", function(e) {
        startLoadingButton('#asset-accomplish-button')
    
        $.ajax({
            async: true,
            url: `${REQUEST_API_URL}/accomplished/${REQUEST_ID}`,
            type: 'PUT',
            beforeSend: function (xhr) {
                xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
            },
            error: function(res) {
                const response = JSON.parse(res.responseText)
                let isRetry = retryRequest(response)
                if(isRetry) $.ajax(this)
                else endLoadingButton('#asset-accomplish-button', 'Iya')
            },
            success: function(res) {
                endLoadingButton('#asset-accomplish-button', 'Iya')
                showSuccess(res.message)
                $('#asset-accomplish-modal').modal('toggle')
                $('#asset-detail-modal').modal('toggle')
                request_table.ajax.reload()
            }
        });
    })

    $("body").delegate(".asset-delete-button", "click", function(e) {
        let id = $(this).data("id")
        ASSET.splice(id, 1)
        renderRequestAssetOption()
    })

    $("body").delegate(".asset-category", "change", function(e) {
        let id = $(this).data("id")
        ASSET[id].category_asset_id = $(this).val()
    })

    $('#asset-create-form').submit(function(e){
        e.preventDefault()

        startLoadingButton('#asset-create-button')
    
        let $form = $(this)
        let data = {
            category_function_id: $("#asset-function-create-field").find(":selected").val(), 
            items: ASSET,
            notes: $form.find( "textarea[name='notes']" ).val(),
        }
        $.ajax({
            async: true,
            url: `${REQUEST_API_URL}/asset`,
            type: 'POST',
            beforeSend: function (xhr) {
                xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
            },
            data: JSON.stringify(data),
            error: function(res) {
                const response  = JSON.parse(res.responseText)
                let isRetry     = retryRequest(response)
                if(isRetry) $.ajax(this)
                else endLoadingButton('#asset-create-button', 'Submit')
            },
            success: function(res) {
                endLoadingButton('#asset-create-button', 'Submit')
                showSuccess(res.message)
                $('#asset-create-modal').modal('hide')
            }
        });
    })

    $("body").delegate(".request-detail-asset-toggle", "click", function(e) {
        REQUEST_ID = $(this).data("id")
        console.log(REQUEST_ID)

        $.ajax({
            async: true,
            url: `${REQUEST_API_URL}/id/${REQUEST_ID}`,
            type: 'GET',
            beforeSend: function (xhr) {
              xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
            },
            error: function(res) {
              const response = JSON.parse(res.responseText)
              let isRetry = retryRequest(response)
              if(isRetry) $.ajax(this)
            },
            success: function(res) {
              const response = res.data
              renderRequestAssetForm(response)
            }
        });
    })
})

function getCategoryAsset(){
    $.ajax({
        async: true,
        url: `${CATEGORY_API_URL}?type=asset`,
        type: 'GET',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
        },
        success: function(res) {
          const response = res.data
          CATEGORY_ASSET = response
        //   rendergetCategoryAssetOption(response)
        }
    });
}

function rendergetCategoryAssetOption(category_asset){
    let category_asset_html = '<option>--Pilih Kategori--</option>'
    if(category_asset.length > 0){
        category_asset.forEach(item => {
            category_asset_html += `<option value="${item.id}">${item.name}</option>`
        });
    }else{
        category_asset_html = `<option disabled>-Data Aset tidak tersedia-</option>`
    }

    $('.category-option').html(category_asset_html)
}

function renderRequestAssetOption(type = "create", isReadOnly = false){
    let readonly = isReadOnly ? "readonly" : ''
    let asset_html = ''
    ASSET.forEach((item, id) => {
        let option_html = `<option>--Pilih Data Aset--</option>`
        CATEGORY_ASSET.forEach(category => {
            option_html += `<option value="${category.id}" ${category.id == item.category_asset_id ? 'selected' : ''}>${category.name}</option>`
        })
        asset_html += `<div class="col-11 mt-2">
            <select class="form-control asset-category" data-id="${id}" ${readonly}>
                ${option_html}
            </select>
        </div>`
        if(!isReadOnly){
            asset_html += `<div class="col-1 mt-2">
                <button type="button" class="btn btn-danger asset-delete-button" data-id="${id}">
                    <i class="fas fa-trash"></i>
                </button>
            </div>`
        }
    })

    $(`#asset-${type}-section`).html(asset_html)
}

function renderRequestAssetForm(request){
    console.log("Rendering..", request.data)
    ASSET       = request.data
    let $form   = $('#asset-detail-form')
    $form.find( "textarea[name='notes']" ).val(request.notes)
    $(`#asset-function-detail-field`).val(request.category_function_id).change()

    renderRequestAssetOption("detail", true)
  
    let color = getColorBadge(request.status)
    let badge = `<span class="badge badge-pill badge-${color}">${request.status}</span>`
    $(`#asset-status-detail-field`).html(badge)

    renderFormButton(request, "asset")
}