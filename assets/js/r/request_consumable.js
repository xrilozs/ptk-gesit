let CONSUMABLE = [] 
let CATEGORY_CONSUMABLE = []

$(document).ready(function(){
    getCategoryConsumable()
    $('#consumable-create-toggle').click(function(){
        $(`#consumable-create-form`)[0].reset();
        CONSUMABLE = []
        renderRequestConsumableOption()
    })

    $('#consumable-add-button').click(function(){
        CONSUMABLE.push({category_consumable_id: null, qty: null})
        console.log("TEST")
        renderRequestConsumableOption()
    })

    $("body").delegate("#consumable-reject-form", "submit", function(e) {
        startLoadingButton('#consumable-reject-button')
        let $form = $(this)
        let data = {
            reject_reason: $form.find( "textarea[name='reject_reason']" ).val(),
        }

        $.ajax({
            async: true,
            url: `${REQUEST_API_URL}/reject/${REQUEST_ID}`,
            type: 'PUT',
            data: JSON.stringify(data),
            beforeSend: function (xhr) {
                xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
            },
            error: function(res) {
                const response = JSON.parse(res.responseText)
                let isRetry = retryRequest(response)
                if(isRetry) $.ajax(this)
                else endLoadingButton('#consumable-reject-button', 'Iya')
            },
            success: function(res) {
                endLoadingButton('#consumable-reject-button', 'Iya')
                showSuccess(res.message)
                $('#consumable-reject-modal').modal('toggle')
                $('#consumable-detail-modal').modal('toggle')
                request_table.ajax.reload()
            }
        });
    })

    $("body").delegate("#consumable-approve-button", "click", function(e) {
        startLoadingButton('#consumable-approve-button')
    
        $.ajax({
            async: true,
            url: `${REQUEST_API_URL}/confirm/${REQUEST_ID}`,
            type: 'PUT',
            beforeSend: function (xhr) {
                xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
            },
            error: function(res) {
                const response = JSON.parse(res.responseText)
                let isRetry = retryRequest(response)
                if(isRetry) $.ajax(this)
                else endLoadingButton('#consumable-approve-button', 'Iya')
            },
            success: function(res) {
                endLoadingButton('#consumable-approve-button', 'Iya')
                showSuccess(res.message)
                $('#consumable-approve-modal').modal('toggle')
                $('#consumable-detail-modal').modal('toggle')
                request_table.ajax.reload()
            }
        });
    })

    $("body").delegate("#consumable-complete-button", "click", function(e) {
        startLoadingButton('#consumable-complete-button')
    
        $.ajax({
            async: true,
            url: `${REQUEST_API_URL}/complete/${REQUEST_ID}`,
            type: 'PUT',
            beforeSend: function (xhr) {
                xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
            },
            error: function(res) {
                const response = JSON.parse(res.responseText)
                let isRetry = retryRequest(response)
                if(isRetry) $.ajax(this)
                else endLoadingButton('#consumable-complete-button', 'Iya')
            },
            success: function(res) {
                endLoadingButton('#consumable-complete-button', 'Iya')
                showSuccess(res.message)
                $('#consumable-complete-modal').modal('toggle')
                $('#consumable-detail-modal').modal('toggle')
                request_table.ajax.reload()
            }
        });
    })

    $("body").delegate("#consumable-accomplish-button", "click", function(e) {
        startLoadingButton('#consumable-accomplish-button')
    
        $.ajax({
            async: true,
            url: `${REQUEST_API_URL}/accomplished/${REQUEST_ID}`,
            type: 'PUT',
            beforeSend: function (xhr) {
                xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
            },
            error: function(res) {
                const response = JSON.parse(res.responseText)
                let isRetry = retryRequest(response)
                if(isRetry) $.ajax(this)
                else endLoadingButton('#consumable-accomplish-button', 'Iya')
            },
            success: function(res) {
                endLoadingButton('#consumable-accomplish-button', 'Iya')
                showSuccess(res.message)
                $('#consumable-accomplish-modal').modal('toggle')
                $('#consumable-detail-modal').modal('toggle')
                request_table.ajax.reload()
            }
        });
    })

    $("body").delegate(".consumable-delete-button", "click", function(e) {
        let id = $(this).data("id")
        CONSUMABLE.splice(id, 1)
        renderRequestConsumableOption()
    })

    $("body").delegate(".consumable-category", "change", function(e) {
        let id = $(this).data("id")
        CONSUMABLE[id].category_consumable_id = $(this).val()
    })

    $("body").delegate(".consumable-qty", "change", function(e) {
        let id = $(this).data("id")
        CONSUMABLE[id].qty = $(this).val()
    })

    $('#consumable-create-form').submit(function(e){
        e.preventDefault()

        startLoadingButton('#consumable-create-button')
    
        let $form = $(this)
        let data = {
            category_function_id: $("#consumable-function-create-field").find(":selected").val(), 
            items: CONSUMABLE,
            notes: $form.find( "textarea[name='notes']" ).val(),
        }
        $.ajax({
            async: true,
            url: `${REQUEST_API_URL}/consumable`,
            type: 'POST',
            beforeSend: function (xhr) {
                xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
            },
            data: JSON.stringify(data),
            error: function(res) {
                const response  = JSON.parse(res.responseText)
                let isRetry     = retryRequest(response)
                if(isRetry) $.ajax(this)
                else endLoadingButton('#consumable-create-button', 'Submit')
            },
            success: function(res) {
                endLoadingButton('#consumable-create-button', 'Submit')
                showSuccess(res.message)
                $('#consumable-create-modal').modal('hide')
            }
        });
    })

    $("body").delegate(".request-detail-consumable-toggle", "click", function(e) {
        REQUEST_ID = $(this).data("id")
        console.log(REQUEST_ID)

        $.ajax({
            async: true,
            url: `${REQUEST_API_URL}/id/${REQUEST_ID}`,
            type: 'GET',
            beforeSend: function (xhr) {
              xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
            },
            error: function(res) {
              const response = JSON.parse(res.responseText)
              let isRetry = retryRequest(response)
              if(isRetry) $.ajax(this)
            },
            success: function(res) {
              const response = res.data
              renderRequestConsumableForm(response)
            }
        });
    })
})

function getCategoryConsumable(){
    $.ajax({
        async: true,
        url: `${CATEGORY_API_URL}?type=consumable`,
        type: 'GET',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
        },
        success: function(res) {
          const response = res.data
          CATEGORY_CONSUMABLE = response
        }
    });
}

function renderRequestConsumableOption(type = "create", isReadOnly = false){
    let readonly = isReadOnly ? "readonly" : ''
    let consumable_html = ''
    CONSUMABLE.forEach((item, id) => {
        let option_html = `<option>--Pilih Barang Habis Pakai--</option>`
        CATEGORY_CONSUMABLE.forEach(category => {
            option_html += `<option value="${category.id}" ${category.id == item.category_consumable_id ? 'selected' : ''}>${category.name}</option>`
        })
        consumable_html += `<div class="col-6 mt-2">
            <select class="form-control consumable-category" data-id="${id}" ${readonly}>
                ${option_html}
            </select>
        </div>
        <div class="col-5 mt-2">
            <input type="number" class="form-control consumable-qty" data-id="${id}" placeholder="Jumlah.." value="${item.qty}" ${readonly}>
        </div>`
        if(!isReadOnly){
            consumable_html += `<div class="col-1 mt-2">
                <button type="button" class="btn btn-danger consumable-delete-button" data-id="${id}">
                    <i class="fas fa-trash"></i>
                </button>
            </div>`
        }
    })

    $(`#consumable-${type}-section`).html(consumable_html)
}

function renderRequestConsumableForm(request){
    console.log("Rendering..", request.data)
    CONSUMABLE  = request.data
    let $form   = $('#consumable-detail-form')
    $form.find( "textarea[name='notes']" ).val(request.notes)
    $(`#consumable-function-detail-field`).val(request.category_function_id).change()
    renderRequestConsumableOption("detail", true)
  
    let color = getColorBadge(request.status)
    let badge = `<span class="badge badge-pill badge-${color}">${request.status}</span>`
    $(`#consumable-status-detail-field`).html(badge)

    renderFormButton(request, "consumable")
}