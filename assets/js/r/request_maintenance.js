let BEFORE_URL
let AFTER_URL
let DATA_MAINTENANCE_ID

$(document).ready(function(){
    $('#maintenance-attachment-create-field').dropify()
    getCategoryMaintenance()
    
    $('#maintenance-create-toggle').click(function(){
        $(`#maintenance-create-form`)[0].reset();
        
        let drEvent = $('#maintenance-attachment-create-field').dropify();
        drEvent = drEvent.data('dropify');
        drEvent.resetPreview();
        drEvent.clearElement();
    })

    $('#maintenance-attachment-create-field').change(function(e) {
        let file = e.target.files[0];
        upload_attachment_maintenance(file)
    });

    $('#maintenance-attachment-complete-field').change(function(e) {
        let file = e.target.files[0];
        upload_attachment_maintenance(file, false, true)
    });

    $('#maintenance-create-form').submit(function(e){
        e.preventDefault()

        startLoadingButton('#maintenance-create-button')
    
        let $form = $(this)
        let data = {
            category_function_id: $("#maintenance-function-create-field").find(":selected").val(), 
            category_maintenance_id	: $("#maintenance-category-create-field").find(":selected").val(),
            before_url: BEFORE_URL,
            notes: $form.find( "textarea[name='notes']" ).val(),
        }
        $.ajax({
            async: true,
            url: `${REQUEST_API_URL}/maintenance`,
            type: 'POST',
            beforeSend: function (xhr) {
                xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
            },
            data: JSON.stringify(data),
            error: function(res) {
                const response  = JSON.parse(res.responseText)
                let isRetry     = retryRequest(response)
                if(isRetry) $.ajax(this)
                else endLoadingButton('#maintenance-create-button', 'Submit')
            },
            success: function(res) {
                endLoadingButton('#maintenance-create-button', 'Submit')
                showSuccess(res.message)
                $('#maintenance-create-modal').modal('hide')
            }
        });
    })

    $("body").delegate("#maintenance-reject-form", "submit", function(e) {
        startLoadingButton('#maintenance-reject-button')
        let $form = $(this)
        let data = {
            reject_reason: $form.find( "textarea[name='reject_reason']" ).val(),
        }

        $.ajax({
            async: true,
            url: `${REQUEST_API_URL}/reject/${REQUEST_ID}`,
            type: 'PUT',
            data: JSON.stringify(data),
            beforeSend: function (xhr) {
                xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
            },
            error: function(res) {
                const response = JSON.parse(res.responseText)
                let isRetry = retryRequest(response)
                if(isRetry) $.ajax(this)
                else endLoadingButton('#maintenance-reject-button', 'Iya')
            },
            success: function(res) {
                endLoadingButton('#maintenance-reject-button', 'Iya')
                showSuccess(res.message)
                $('#maintenance-reject-modal').modal('toggle')
                $('#maintenance-detail-modal').modal('toggle')
                request_table.ajax.reload()
            }
        });
    })

    $("body").delegate("#maintenance-approve-button", "click", function(e) {
        startLoadingButton('#maintenance-approve-button')
    
        $.ajax({
            async: true,
            url: `${REQUEST_API_URL}/confirm/${REQUEST_ID}`,
            type: 'PUT',
            beforeSend: function (xhr) {
                xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
            },
            error: function(res) {
                const response = JSON.parse(res.responseText)
                let isRetry = retryRequest(response)
                if(isRetry) $.ajax(this)
                else endLoadingButton('#maintenance-approve-button', 'Iya')
            },
            success: function(res) {
                endLoadingButton('#maintenance-approve-button', 'Iya')
                showSuccess(res.message)
                $('#maintenance-approve-modal').modal('toggle')
                $('#maintenance-detail-modal').modal('toggle')
                request_table.ajax.reload()
            }
        });
    })

    $("body").delegate("#maintenance-accomplish-button", "click", function(e) {
        startLoadingButton('#maintenance-accomplish-button')
    
        $.ajax({
            async: true,
            url: `${REQUEST_API_URL}/accomplished/${REQUEST_ID}`,
            type: 'PUT',
            beforeSend: function (xhr) {
                xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
            },
            error: function(res) {
                const response = JSON.parse(res.responseText)
                let isRetry = retryRequest(response)
                if(isRetry) $.ajax(this)
                else endLoadingButton('#maintenance-accomplish-button', 'Iya')
            },
            success: function(res) {
                endLoadingButton('#maintenance-accomplish-button', 'Iya')
                showSuccess(res.message)
                $('#maintenance-accomplish-modal').modal('toggle')
                $('#maintenance-detail-modal').modal('toggle')
                request_table.ajax.reload()
            }
        });
    })

    $("body").delegate("#maintenance-complete-form", "submit", function(e) {
        startLoadingButton('#maintenance-complete-button')
    
        $.ajax({
            async: true,
            url: `${REQUEST_API_URL}/complete/${REQUEST_ID}`,
            type: 'PUT',
            data: JSON.stringify({
                data_maintenance_id: DATA_MAINTENANCE_ID,
                after_url: AFTER_URL
            }),
            beforeSend: function (xhr) {
                xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
            },
            error: function(res) {
                const response = JSON.parse(res.responseText)
                let isRetry = retryRequest(response)
                if(isRetry) $.ajax(this)
                else endLoadingButton('#maintenance-complete-button', 'Iya')
            },
            success: function(res) {
                endLoadingButton('#maintenance-complete-button', 'Iya')
                showSuccess(res.message)
                $('#maintenance-complete-modal').modal('toggle')
                $('#maintenance-detail-modal').modal('toggle')
                request_table.ajax.reload()
            }
        });
    })

    $("body").delegate(".request-detail-maintenance-toggle", "click", function(e) {
        REQUEST_ID = $(this).data("id")
        console.log(REQUEST_ID)
        let drEvent = $('#maintenance-attachment-complete-field').dropify();
        drEvent = drEvent.data('dropify');
        drEvent.resetPreview();
        drEvent.clearElement();

        $.ajax({
            async: true,
            url: `${REQUEST_API_URL}/id/${REQUEST_ID}`,
            type: 'GET',
            beforeSend: function (xhr) {
              xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
            },
            error: function(res) {
              const response = JSON.parse(res.responseText)
              let isRetry = retryRequest(response)
              if(isRetry) $.ajax(this)
            },
            success: function(res) {
              const response = res.data
              renderRequestMaintenanceForm(response)
            }
        });
    })
})

function getCategoryMaintenance(){
    $.ajax({
        async: true,
        url: `${CATEGORY_API_URL}?type=maintenance`,
        type: 'GET',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
        },
        success: function(res) {
          const response = res.data
          renderCategoryMaintenanceOption(response)
        }
    });
}

function renderCategoryMaintenanceOption(category_maintenance){
    let category_maintenance_html = ''
    if(category_maintenance.length > 0){
        category_maintenance.forEach(item => {
            category_maintenance_html += `<option value="${item.id}">${item.name}</option>`
        });
    }else{
        category_maintenance_html = `<option disabled>-Kategori tidak tersedia-</option>`
    }

    $('.category-option').html(category_maintenance_html)
}

function renderRequestMaintenanceForm(request){
    let data            = request.data[0]
    DATA_MAINTENANCE_ID = data.id
    console.log("Rendering..", data)
    let $form = $('#maintenance-detail-form')
    $form.find( "textarea[name='notes']" ).val(request.notes)
    $(`#maintenance-function-detail-field`).val(request.category_function_id).change()
    $(`#maintenance-category-detail-field`).val(data.category_maintenance_id).change()

    if(data.before_url){    
        $('#maintenance-before-detail').html(`<a href="${WEB_URL}/${data.before_url}" target="_blank">Download</a>`);
    }else{
        $('#maintenance-before-detail').html("-")
    }
    if(data.after_url){    
        $('#maintenance-after-detail').html(`<a href="${WEB_URL}/${data.after_url}" target="_blank">Download</a>`);
    }else{
        $('#maintenance-after-detail').html("-")
    }
    // $('#maintenance-before-detail-field').dropify()
    // $('#maintenance-after-detail-field').dropify()
  
    let color = getColorBadge(request.status)
    let badge = `<span class="badge badge-pill badge-${color}">${request.status}</span>`
    $(`#maintenance-status-detail-field`).html(badge)

    renderFormButton(request, "maintenance")
}

//upload image
function upload_attachment_maintenance(file, isBefore=true, isAfter=false){
    $(`#maintenance-create-button`).attr("disabled", true)
  
    let formData = new FormData();
    formData.append('file', file);
    
    $.ajax({
        async: true,
        url: `${REQUEST_API_URL}/upload`,
        type: 'POST',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: formData,
        processData: false, 
        contentType: false, 
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else{
            showError("Gagal melakukan upload lampiran!")
            $(`#maintenance-create-button`).attr("disabled", false)
          }
        },
        success: function(res) {
          const response = res.data
          if(isBefore) BEFORE_URL   = response.file_url
          if(isAfter) AFTER_URL     = response.file_url
          console.log("AFTER URL:", AFTER_URL)
          $(`#maintenance-create-button`).attr("disabled", false)
          showSuccess(res.message)
        }
    });
}