let EVENT_SUPPORT = [] 
let CATEGORY_EVENT_SUPPORT = []

$(document).ready(function(){
    getCategoryEventSupport()
    $('#event-support-create-toggle').click(function(){
        $(`#event-support-create-form`)[0].reset();
        EVENT_SUPPORT = []
        renderRequestEventSupportOption()
    })

    $('#event-support-add-button').click(function(){
        EVENT_SUPPORT.push({category_event_support_id: null, qty: null})
        console.log("TEST")
        renderRequestEventSupportOption()
    })

    $('input[type="radio"][name="is_outside_office"]').change(function() {
        const selectedValue = $(this).val(); // Get the value of the selected radio button
        if(!parseInt(selectedValue)){
            $('#location-option-create').show()
            $('#location-text-create').hide()
        }else{
            $('#location-option-create').hide()
            $('#location-text-create').show()
        }
    });

    $("body").delegate("#event-support-reject-form", "submit", function(e) {
        startLoadingButton('#event-support-reject-button')
        let $form = $(this)
        let data = {
            reject_reason: $form.find( "textarea[name='reject_reason']" ).val(),
        }

        $.ajax({
            async: true,
            url: `${REQUEST_API_URL}/reject/${REQUEST_ID}`,
            type: 'PUT',
            data: JSON.stringify(data),
            beforeSend: function (xhr) {
                xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
            },
            error: function(res) {
                const response = JSON.parse(res.responseText)
                let isRetry = retryRequest(response)
                if(isRetry) $.ajax(this)
                else endLoadingButton('#event-support-reject-button', 'Iya')
            },
            success: function(res) {
                endLoadingButton('#event-support-reject-button', 'Iya')
                showSuccess(res.message)
                $('#event-support-reject-modal').modal('toggle')
                $('#event-support-detail-modal').modal('toggle')
                request_table.ajax.reload()
            }
        });
    })

    $("body").delegate("#event-support-approve-button", "click", function(e) {
        startLoadingButton('#event-support-approve-button')
    
        $.ajax({
            async: true,
            url: `${REQUEST_API_URL}/confirm/${REQUEST_ID}`,
            type: 'PUT',
            beforeSend: function (xhr) {
                xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
            },
            error: function(res) {
                const response = JSON.parse(res.responseText)
                let isRetry = retryRequest(response)
                if(isRetry) $.ajax(this)
                else endLoadingButton('#event-support-approve-button', 'Iya')
            },
            success: function(res) {
                endLoadingButton('#event-support-approve-button', 'Iya')
                showSuccess(res.message)
                $('#event-support-approve-modal').modal('toggle')
                $('#event-support-detail-modal').modal('toggle')
                request_table.ajax.reload()
            }
        });
    })

    $("body").delegate("#event-support-complete-button", "click", function(e) {
        startLoadingButton('#event-support-complete-button')
    
        $.ajax({
            async: true,
            url: `${REQUEST_API_URL}/complete/${REQUEST_ID}`,
            type: 'PUT',
            beforeSend: function (xhr) {
                xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
            },
            error: function(res) {
                const response = JSON.parse(res.responseText)
                let isRetry = retryRequest(response)
                if(isRetry) $.ajax(this)
                else endLoadingButton('#event-support-complete-button', 'Iya')
            },
            success: function(res) {
                endLoadingButton('#event-support-complete-button', 'Iya')
                showSuccess(res.message)
                $('#event-support-complete-modal').modal('toggle')
                $('#event-support-detail-modal').modal('toggle')
                request_table.ajax.reload()
            }
        });
    })

    $("body").delegate("#event-support-accomplish-button", "click", function(e) {
        startLoadingButton('#event-support-accomplish-button')
    
        $.ajax({
            async: true,
            url: `${REQUEST_API_URL}/accomplished/${REQUEST_ID}`,
            type: 'PUT',
            beforeSend: function (xhr) {
                xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
            },
            error: function(res) {
                const response = JSON.parse(res.responseText)
                let isRetry = retryRequest(response)
                if(isRetry) $.ajax(this)
                else endLoadingButton('#event-support-accomplish-button', 'Iya')
            },
            success: function(res) {
                endLoadingButton('#event-support-accomplish-button', 'Iya')
                showSuccess(res.message)
                $('#event-support-accomplish-modal').modal('toggle')
                $('#event-support-detail-modal').modal('toggle')
                request_table.ajax.reload()
            }
        });
    })

    $("body").delegate(".event-support-delete-button", "click", function(e) {
        let id = $(this).data("id")
        EVENT_SUPPORT.splice(id, 1)
        renderRequestEventSupportOption()
    })

    $("body").delegate(".event-support-category", "change", function(e) {
        let id = $(this).data("id")
        EVENT_SUPPORT[id].category_event_support_id = $(this).val()
    })

    $("body").delegate(".event-support-qty", "change", function(e) {
        let id = $(this).data("id")
        EVENT_SUPPORT[id].qty = $(this).val()
    })

    $('#event-support-create-form').submit(function(e){
        e.preventDefault()

        startLoadingButton('#event-support-create-button')
    
        let $form = $(this)
        let data = {
            category_function_id: $("#event-support-function-create-field").find(":selected").val(), 
            items: EVENT_SUPPORT,
            notes: $form.find( "textarea[name='notes']" ).val(),
            event_name: $form.find( "input[name='event_name']" ).val(),
            event_date: $form.find( "input[name='event_date']" ).val(),
            start_time: $form.find( "input[name='start_time']" ).val(),
            end_time: $form.find( "input[name='end_time']" ).val(),
            is_outside_office: $form.find( "input[name='is_outside_office']:checked" ).val(),
            use_eo: $form.find( "input[name='use_eo']:checked" ).val(),
        }
        if(!parseInt($form.find( "input[name='is_outside_office']:checked" ).val())){
            data.location = $("#location-create").find(":selected").val()
        }else{
            data.location = $form.find( "input[name='location']" ).val()
        }

        $.ajax({
            async: true,
            url: `${REQUEST_API_URL}/event-support`,
            type: 'POST',
            beforeSend: function (xhr) {
                xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
            },
            data: JSON.stringify(data),
            error: function(res) {
                const response  = JSON.parse(res.responseText)
                let isRetry     = retryRequest(response)
                if(isRetry) $.ajax(this)
                else endLoadingButton('#event-support-create-button', 'Submit')
            },
            success: function(res) {
                endLoadingButton('#event-support-create-button', 'Submit')
                showSuccess(res.message)
                $('#event-support-create-modal').modal('hide')
            }
        });
    })

    $("body").delegate(".request-detail-event-support-toggle", "click", function(e) {
        REQUEST_ID = $(this).data("id")
        console.log(REQUEST_ID)

        $.ajax({
            async: true,
            url: `${REQUEST_API_URL}/id/${REQUEST_ID}`,
            type: 'GET',
            beforeSend: function (xhr) {
              xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
            },
            error: function(res) {
              const response = JSON.parse(res.responseText)
              let isRetry = retryRequest(response)
              if(isRetry) $.ajax(this)
            },
            success: function(res) {
              const response = res.data
              renderRequestEventSupportForm(response)
            }
        });
    })
})

function getCategoryEventSupport(){
    $.ajax({
        async: true,
        url: `${CATEGORY_API_URL}?type=event_support`,
        type: 'GET',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
        },
        success: function(res) {
          const response = res.data
          CATEGORY_EVENT_SUPPORT = response
        }
    });
}

function renderRequestEventSupportOption(type = "create", isReadOnly = false){
    let readonly = isReadOnly ? "readonly" : ''
    let event_support_html = ''
    EVENT_SUPPORT.forEach((item, id) => {
        let option_html = `<option>--Pilih Event Support--</option>`
        CATEGORY_EVENT_SUPPORT.forEach(category => {
            option_html += `<option value="${category.id}" ${category.id == item.category_event_support_id ? 'selected' : ''}>${category.name}</option>`
        })
        event_support_html += `<div class="col-6 mt-2">
            <select class="form-control event-support-category" data-id="${id}" ${readonly}>
                ${option_html}
            </select>
        </div>
        <div class="col-5 mt-2">
            <input type="number" class="form-control event-support-qty" data-id="${id}" placeholder="Jumlah.." value="${item.qty}" ${readonly}>
        </div>`
        if(!isReadOnly){
            event_support_html += `<div class="col-1 mt-2">
                <button type="button" class="btn btn-danger event-support-delete-button" data-id="${id}">
                    <i class="fas fa-trash"></i>
                </button>
            </div>`
        }
    })

    $(`#event-support-${type}-section`).html(event_support_html)
}

function renderRequestEventSupportForm(request){
    console.log("Rendering..", request.data)
    EVENT_SUPPORT = request.data[0].items
    let $form     = $('#event-support-detail-form')
    $(`#event-support-function-detail-field`).val(request.category_function_id).change()
    $form.find( "textarea[name='notes']" ).val(request.notes)
    $form.find( "input[name='event_name']" ).val(request.data[0].event_name)
    $form.find( "input[name='event_date']" ).val(request.data[0].event_date)
    $form.find( "input[name='start_time']" ).val(request.data[0].start_time)
    $form.find( "input[name='end_time']" ).val(request.data[0].end_time)
    if(!parseInt(request.data[0].is_outside_office)){
        $('#location-option-detail').show()
        $('#location-text-detail').hide()
        $(`#location-detail`).val(request.data[0].location).change()
    }else{
        $('#location-option-detail').hide()
        $('#location-text-detail').show()
        $form.find( "input[name='location']" ).val(request.data[0].location)
    }
    $form.find(`input[name="is_outside_office"][value="${request.data[0].is_outside_office}"]`).prop('checked', true);
    $form.find(`input[name="use_eo"][value="${request.data[0].use_eo}"]`).prop('checked', true);
    renderRequestEventSupportOption("detail", true)
  
    let color = getColorBadge(request.status)
    let badge = `<span class="badge badge-pill badge-${color}">${request.status}</span>`
    $(`#event-support-status-detail-field`).html(badge)

    renderFormButton(request, "event-support")
}