$(document).ready(function(){
    $('#other-create-toggle').click(function(){
        $(`#other-create-form`)[0].reset();
    })
    // getCategoryFunction()

    $('#other-create-form').submit(function(e){
        e.preventDefault()

        startLoadingButton('#other-create-button')
    
        let $form = $(this)
        let data = {
            category_function_id: $("#other-function-create-field").find(":selected").val(), 
            notes: $form.find( "textarea[name='notes']" ).val(),
        }
        $.ajax({
            async: true,
            url: `${REQUEST_API_URL}/other`,
            type: 'POST',
            beforeSend: function (xhr) {
                xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
            },
            data: JSON.stringify(data),
            error: function(res) {
                const response  = JSON.parse(res.responseText)
                let isRetry     = retryRequest(response)
                if(isRetry) $.ajax(this)
                else endLoadingButton('#other-create-button', 'Submit')
            },
            success: function(res) {
                endLoadingButton('#other-create-button', 'Submit')
                showSuccess(res.message)
                $('#other-create-modal').modal('hide')
            }
        });
    })

    $("body").delegate("#other-reject-form", "submit", function(e) {
        startLoadingButton('#other-reject-button')
        let $form = $(this)
        let data = {
            reject_reason: $form.find( "textarea[name='reject_reason']" ).val(),
        }

        $.ajax({
            async: true,
            url: `${REQUEST_API_URL}/reject/${REQUEST_ID}`,
            type: 'PUT',
            data: JSON.stringify(data),
            beforeSend: function (xhr) {
                xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
            },
            error: function(res) {
                const response = JSON.parse(res.responseText)
                let isRetry = retryRequest(response)
                if(isRetry) $.ajax(this)
                else endLoadingButton('#other-reject-button', 'Iya')
            },
            success: function(res) {
                endLoadingButton('#other-reject-button', 'Iya')
                showSuccess(res.message)
                $('#other-reject-modal').modal('toggle')
                $('#other-detail-modal').modal('toggle')
                request_table.ajax.reload()
            }
        });
    })

    $("body").delegate("#other-approve-button", "click", function(e) {
        startLoadingButton('#other-approve-button')
    
        $.ajax({
            async: true,
            url: `${REQUEST_API_URL}/confirm/${REQUEST_ID}`,
            type: 'PUT',
            beforeSend: function (xhr) {
                xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
            },
            error: function(res) {
                const response = JSON.parse(res.responseText)
                let isRetry = retryRequest(response)
                if(isRetry) $.ajax(this)
                else endLoadingButton('#other-approve-button', 'Iya')
            },
            success: function(res) {
                endLoadingButton('#other-approve-button', 'Iya')
                showSuccess(res.message)
                $('#other-approve-modal').modal('toggle')
                $('#other-detail-modal').modal('toggle')
                request_table.ajax.reload()
            }
        });
    })

    $("body").delegate("#other-complete-button", "click", function(e) {
        startLoadingButton('#other-complete-button')
    
        $.ajax({
            async: true,
            url: `${REQUEST_API_URL}/complete/${REQUEST_ID}`,
            type: 'PUT',
            beforeSend: function (xhr) {
                xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
            },
            error: function(res) {
                const response = JSON.parse(res.responseText)
                let isRetry = retryRequest(response)
                if(isRetry) $.ajax(this)
                else endLoadingButton('#other-complete-button', 'Iya')
            },
            success: function(res) {
                endLoadingButton('#other-complete-button', 'Iya')
                showSuccess(res.message)
                $('#other-complete-modal').modal('toggle')
                $('#other-detail-modal').modal('toggle')
                request_table.ajax.reload()
            }
        });
    })

    $("body").delegate("#other-accomplish-button", "click", function(e) {
        startLoadingButton('#other-accomplish-button')
    
        $.ajax({
            async: true,
            url: `${REQUEST_API_URL}/accomplished/${REQUEST_ID}`,
            type: 'PUT',
            beforeSend: function (xhr) {
                xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
            },
            error: function(res) {
                const response = JSON.parse(res.responseText)
                let isRetry = retryRequest(response)
                if(isRetry) $.ajax(this)
                else endLoadingButton('#other-accomplish-button', 'Iya')
            },
            success: function(res) {
                endLoadingButton('#other-accomplish-button', 'Iya')
                showSuccess(res.message)
                $('#other-accomplish-modal').modal('toggle')
                $('#other-detail-modal').modal('toggle')
                request_table.ajax.reload()
            }
        });
    })

    $("body").delegate(".request-detail-other-toggle", "click", function(e) {
        REQUEST_ID = $(this).data("id")
        console.log(REQUEST_ID)

        $.ajax({
            async: true,
            url: `${REQUEST_API_URL}/id/${REQUEST_ID}`,
            type: 'GET',
            beforeSend: function (xhr) {
              xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
            },
            error: function(res) {
              const response = JSON.parse(res.responseText)
              let isRetry = retryRequest(response)
              if(isRetry) $.ajax(this)
            },
            success: function(res) {
              const response = res.data
              renderRequestOtherForm(response)
            }
        });
    })
})

function renderRequestOtherForm(request){
    let data = request.data[0]
    console.log("Rendering..", data)
    let $form = $('#other-detail-form')
    $form.find( "textarea[name='notes']" ).val(request.notes)
    $(`#other-function-detail-field`).val(request.category_function_id).change()
  
    let color = getColorBadge(request.status)
    let badge = `<span class="badge badge-pill badge-${color}">${request.status}</span>`
    $(`#other-status-detail-field`).html(badge)

    renderFormButton(request, "other")
}