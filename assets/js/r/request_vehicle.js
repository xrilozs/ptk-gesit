let DRIVER_ID

$(document).ready(function(){
    getDriver()

    $('#vehicle-create-toggle').click(function(){
        $(`#vehicle-create-form`)[0].reset();
    })

    $('#vehicle-driver-create-field').change(function(){
        DRIVER_ID = $(this).val()
    })

    $('#vehicle-create-form').submit(function(e){
        e.preventDefault()

        startLoadingButton('#vehicle-create-button')
    
        let $form = $(this)
        let data = {
            category_function_id: $("#vehicle-function-create-field").find(":selected").val(), 
            total_passenger: $form.find( "input[name='total_passenger']" ).val(),
            request_date: $form.find( "input[name='request_date']" ).val(), 
            request_time: $form.find( "input[name='request_time']" ).val(), 
            dest_1_mob: $form.find( "input[name='dest_1_mob']" ).val(), 
            dest_2_mob: $form.find( "input[name='dest_2_mob']" ).val(),
            driver_id: DRIVER_ID,
            acceptance_non_preferred_driver: $form.find('input[name="acceptance_non_preferred_driver"]:checked').val(),
            notes: $form.find( "textarea[name='notes']" ).val(),
        }
        $.ajax({
            async: true,
            url: `${REQUEST_API_URL}/operation-vehicle`,
            type: 'POST',
            beforeSend: function (xhr) {
                xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
            },
            data: JSON.stringify(data),
            error: function(res) {
                const response  = JSON.parse(res.responseText)
                let isRetry     = retryRequest(response)
                if(isRetry) $.ajax(this)
                else endLoadingButton('#vehicle-create-button', 'Submit')
            },
            success: function(res) {
                endLoadingButton('#vehicle-create-button', 'Submit')
                showSuccess(res.message)
                $('#vehicle-create-modal').modal('hide')
            }
        });
    })

    $("body").delegate("#vehicle-reject-form", "submit", function(e) {
        startLoadingButton('#vehicle-reject-button')
        let $form = $(this)
        let data = {
            reject_reason: $form.find( "textarea[name='reject_reason']" ).val(),
        }

        $.ajax({
            async: true,
            url: `${REQUEST_API_URL}/reject/${REQUEST_ID}`,
            type: 'PUT',
            data: JSON.stringify(data),
            beforeSend: function (xhr) {
                xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
            },
            error: function(res) {
                const response = JSON.parse(res.responseText)
                let isRetry = retryRequest(response)
                if(isRetry) $.ajax(this)
                else endLoadingButton('#vehicle-reject-button', 'Iya')
            },
            success: function(res) {
                endLoadingButton('#vehicle-reject-button', 'Iya')
                showSuccess(res.message)
                $('#vehicle-reject-modal').modal('toggle')
                $('#operational-vehicle-detail-modal').modal('toggle')
                request_table.ajax.reload()
            }
        });
    })

    $("body").delegate("#vehicle-approve-button", "click", function(e) {
        startLoadingButton('#vehicle-approve-button')
    
        $.ajax({
            async: true,
            url: `${REQUEST_API_URL}/confirm/${REQUEST_ID}`,
            type: 'PUT',
            beforeSend: function (xhr) {
                xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
            },
            error: function(res) {
                const response = JSON.parse(res.responseText)
                let isRetry = retryRequest(response)
                if(isRetry) $.ajax(this)
                else endLoadingButton('#vehicle-approve-button', 'Iya')
            },
            success: function(res) {
                endLoadingButton('#vehicle-approve-button', 'Iya')
                showSuccess(res.message)
                $('#vehicle-approve-modal').modal('toggle')
                $('#operational-vehicle-detail-modal').modal('toggle')
                request_table.ajax.reload()
            }
        });
    })

    $("body").delegate("#vehicle-complete-button", "click", function(e) {
        startLoadingButton('#vehicle-complete-button')
    
        $.ajax({
            async: true,
            url: `${REQUEST_API_URL}/complete/${REQUEST_ID}`,
            type: 'PUT',
            beforeSend: function (xhr) {
                xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
            },
            error: function(res) {
                const response = JSON.parse(res.responseText)
                let isRetry = retryRequest(response)
                if(isRetry) $.ajax(this)
                else endLoadingButton('#vehicle-complete-button', 'Iya')
            },
            success: function(res) {
                endLoadingButton('#vehicle-complete-button', 'Iya')
                showSuccess(res.message)
                $('#vehicle-complete-modal').modal('toggle')
                $('#operational-vehicle-detail-modal').modal('toggle')
                request_table.ajax.reload()
            }
        });
    })

    $("body").delegate("#vehicle-accomplish-button", "click", function(e) {
        startLoadingButton('#vehicle-accomplish-button')
    
        $.ajax({
            async: true,
            url: `${REQUEST_API_URL}/accomplished/${REQUEST_ID}`,
            type: 'PUT',
            beforeSend: function (xhr) {
                xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
            },
            error: function(res) {
                const response = JSON.parse(res.responseText)
                let isRetry = retryRequest(response)
                if(isRetry) $.ajax(this)
                else endLoadingButton('#vehicle-accomplish-button', 'Iya')
            },
            success: function(res) {
                endLoadingButton('#vehicle-accomplish-button', 'Iya')
                showSuccess(res.message)
                $('#vehicle-accomplish-modal').modal('toggle')
                $('#operational-vehicle-detail-modal').modal('toggle')
                request_table.ajax.reload()
            }
        });
    })

    $("body").delegate(".request-detail-operational-vehicle-toggle", "click", function(e) {
        REQUEST_ID = $(this).data("id")
        console.log(REQUEST_ID)

        $.ajax({
            async: true,
            url: `${REQUEST_API_URL}/id/${REQUEST_ID}`,
            type: 'GET',
            beforeSend: function (xhr) {
              xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
            },
            error: function(res) {
              const response = JSON.parse(res.responseText)
              let isRetry = retryRequest(response)
              if(isRetry) $.ajax(this)
            },
            success: function(res) {
              const response = res.data
              renderRequestVehicleForm(response)
            }
        });
    })
})

function getDriver(){
    $.ajax({
        async: true,
        url: `${DRIVER_API_URL}?status=AVAILABLE`,
        type: 'GET',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
        },
        success: function(res) {
          const response = res.data
          renderDriverOption(response)
        }
    });
}

function renderDriverOption(driver){
    let driver_html = '<option>--Pilih Pengemudi--</option>'
    if(driver.length > 0){
        driver.forEach(item => {
            driver_html += `<option value="${item.id}">${item.name}</option>`
        });
    }else{
        driver_html = `<option disabled>-Pengemudi tidak tersedia-</option>`
    }

    $('.driver-option').html(driver_html)
}

function renderRequestVehicleForm(request){
    let data = request.data[0]
    console.log("Rendering..", data)
    let $form = $('#vehicle-detail-form')
    $(`#vehicle-function-detail-field`).val(request.category_function_id).change()
    $form.find( "input[name='total_passenger']" ).val(data.total_passenger)
    $form.find( "input[name='request_date']" ).val(data.request_date)
    $form.find( "input[name='request_time']" ).val(data.request_time)
    $form.find( "input[name='dest_1_mob']" ).val(data.dest_1_mob)
    $form.find( "input[name='dest_2_mob']" ).val(data.dest_2_mob)
    $form.find( "textarea[name='notes']" ).val(request.notes)
    if(data.driver_id){
        $(`#vehicle-driver-detail-field`).val(data.driver_name)
        $form.find(`input[name="acceptance_non_preferred_driver"][value="${data.acceptance_non_preferred_driver}"]`).prop('checked', true);
    }
  
    let color = getColorBadge(request.status)
    let badge = `<span class="badge badge-pill badge-${color}">${request.status}</span>`
    $(`#vehicle-status-detail-field`).html(badge)

    renderFormButton(request, "vehicle")
}
