let OFFICE_STATIONERY = [] 
let CATEGORY_OFFICE_STATIONERY = []

$(document).ready(function(){
    getCategoryOfficeStationery()
    $('#office-stationery-create-toggle').click(function(){
        $(`#office-stationery-create-form`)[0].reset();
        OFFICE_STATIONERY = []
        renderRequestOfficeStationeryOption()
    })

    $('#office-stationery-add-button').click(function(){
        OFFICE_STATIONERY.push({category_office_stationery_id: null, qty: null})
        console.log("TEST")
        renderRequestOfficeStationeryOption()
    })

    $("body").delegate("#office-stationery-reject-form", "form", function(e) {
        startLoadingButton('#office-stationery-reject-button')
        let $form = $(this)
        let data = {
            reject_reason: $form.find( "textarea[name='reject_reason']" ).val(),
        }

        $.ajax({
            async: true,
            url: `${REQUEST_API_URL}/reject/${REQUEST_ID}`,
            type: 'PUT',
            data: JSON.stringify(data),
            beforeSend: function (xhr) {
                xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
            },
            error: function(res) {
                const response = JSON.parse(res.responseText)
                let isRetry = retryRequest(response)
                if(isRetry) $.ajax(this)
                else endLoadingButton('#office-stationery-reject-button', 'Iya')
            },
            success: function(res) {
                endLoadingButton('#office-stationery-reject-button', 'Iya')
                showSuccess(res.message)
                $('#office-stationery-reject-modal').modal('toggle')
                $('#office-stationery-detail-modal').modal('toggle')
                request_table.ajax.reload()
            }
        });
    })

    $("body").delegate("#office-stationery-approve-button", "click", function(e) {
        startLoadingButton('#office-stationery-approve-button')
    
        $.ajax({
            async: true,
            url: `${REQUEST_API_URL}/confirm/${REQUEST_ID}`,
            type: 'PUT',
            beforeSend: function (xhr) {
                xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
            },
            error: function(res) {
                const response = JSON.parse(res.responseText)
                let isRetry = retryRequest(response)
                if(isRetry) $.ajax(this)
                else endLoadingButton('#office-stationery-approve-button', 'Iya')
            },
            success: function(res) {
                endLoadingButton('#office-stationery-approve-button', 'Iya')
                showSuccess(res.message)
                $('#office-stationery-approve-modal').modal('toggle')
                $('#office-stationery-detail-modal').modal('toggle')
                request_table.ajax.reload()
            }
        });
    })

    $("body").delegate("#office-stationery-complete-button", "click", function(e) {
        startLoadingButton('#office-stationery-complete-button')
    
        $.ajax({
            async: true,
            url: `${REQUEST_API_URL}/complete/${REQUEST_ID}`,
            type: 'PUT',
            beforeSend: function (xhr) {
                xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
            },
            error: function(res) {
                const response = JSON.parse(res.responseText)
                let isRetry = retryRequest(response)
                if(isRetry) $.ajax(this)
                else endLoadingButton('#office-stationery-complete-button', 'Iya')
            },
            success: function(res) {
                endLoadingButton('#office-stationery-complete-button', 'Iya')
                showSuccess(res.message)
                $('#office-stationery-complete-modal').modal('toggle')
                $('#office-stationery-detail-modal').modal('toggle')
                request_table.ajax.reload()
            }
        });
    })

    $("body").delegate("#office-stationery-accomplish-button", "click", function(e) {
        startLoadingButton('#office-stationery-accomplish-button')
    
        $.ajax({
            async: true,
            url: `${REQUEST_API_URL}/accomplished/${REQUEST_ID}`,
            type: 'PUT',
            beforeSend: function (xhr) {
                xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
            },
            error: function(res) {
                const response = JSON.parse(res.responseText)
                let isRetry = retryRequest(response)
                if(isRetry) $.ajax(this)
                else endLoadingButton('#office-stationery-accomplish-button', 'Iya')
            },
            success: function(res) {
                endLoadingButton('#office-stationery-accomplish-button', 'Iya')
                showSuccess(res.message)
                $('#office-stationery-accomplish-modal').modal('toggle')
                $('#office-stationery-detail-modal').modal('toggle')
                request_table.ajax.reload()
            }
        });
    })

    $("body").delegate(".office-stationery-delete-button", "click", function(e) {
        let id = $(this).data("id")
        OFFICE_STATIONERY.splice(id, 1)
        renderRequestOfficeStationeryOption()
    })

    $("body").delegate(".office-stationery-category", "change", function(e) {
        let id = $(this).data("id")
        OFFICE_STATIONERY[id].category_office_stationery_id = $(this).val()
    })

    $("body").delegate(".office-stationery-qty", "change", function(e) {
        let id = $(this).data("id")
        OFFICE_STATIONERY[id].qty = $(this).val()
    })

    $('#office-stationery-create-form').submit(function(e){
        e.preventDefault()

        startLoadingButton('#office-stationery-create-button')
    
        let $form = $(this)
        let data = {
            category_function_id: $("#office-stationery-function-create-field").find(":selected").val(), 
            items: OFFICE_STATIONERY,
            notes: $form.find( "textarea[name='notes']" ).val(),
        }
        $.ajax({
            async: true,
            url: `${REQUEST_API_URL}/office-stationery`,
            type: 'POST',
            beforeSend: function (xhr) {
                xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
            },
            data: JSON.stringify(data),
            error: function(res) {
                const response  = JSON.parse(res.responseText)
                let isRetry     = retryRequest(response)
                if(isRetry) $.ajax(this)
                else endLoadingButton('#office-stationery-create-button', 'Submit')
            },
            success: function(res) {
                endLoadingButton('#office-stationery-create-button', 'Submit')
                showSuccess(res.message)
                $('#office-stationery-create-modal').modal('hide')
            }
        });
    })

    $("body").delegate(".request-detail-office-stationery-toggle", "click", function(e) {
        REQUEST_ID = $(this).data("id")
        console.log(REQUEST_ID)

        $.ajax({
            async: true,
            url: `${REQUEST_API_URL}/id/${REQUEST_ID}`,
            type: 'GET',
            beforeSend: function (xhr) {
              xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
            },
            error: function(res) {
              const response = JSON.parse(res.responseText)
              let isRetry = retryRequest(response)
              if(isRetry) $.ajax(this)
            },
            success: function(res) {
              const response = res.data
              renderRequestOfficeStationeryForm(response)
            }
        });
    })
})

function getCategoryOfficeStationery(){
    $.ajax({
        async: true,
        url: `${CATEGORY_API_URL}?type=office_stationery`,
        type: 'GET',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
        },
        success: function(res) {
          const response = res.data
          CATEGORY_OFFICE_STATIONERY = response
        }
    });
}

function renderRequestOfficeStationeryOption(type = "create", isReadOnly = false){
    let readonly = isReadOnly ? "readonly" : ''
    let office_stationery_html = ''
    OFFICE_STATIONERY.forEach((item, id) => {
        let option_html = `<option>--Pilih ATK--</option>`
        CATEGORY_OFFICE_STATIONERY.forEach(category => {
            option_html += `<option value="${category.id}" ${category.id == item.category_office_stationery_id ? 'selected' : ''}>${category.name}</option>`
        })
        office_stationery_html += `<div class="col-6 mt-2">
            <select class="form-control office-stationery-category" data-id="${id}" ${readonly}>
                ${option_html}
            </select>
        </div>
        <div class="col-5 mt-2">
            <input type="number" class="form-control office-stationery-qty" data-id="${id}" placeholder="Jumlah.." value="${item.qty}" ${readonly}>
        </div>`
        if(!isReadOnly){
            office_stationery_html += `<div class="col-1 mt-2">
                <button type="button" class="btn btn-danger office-stationery-delete-button" data-id="${id}">
                    <i class="fas fa-trash"></i>
                </button>
            </div>`
        }
    })

    $(`#office-stationery-${type}-section`).html(office_stationery_html)
}

function renderRequestOfficeStationeryForm(request){
    let data = request.data
    console.log("Rendering..", data)
    let $form = $('#office-stationery-detail-form')
    $form.find( "textarea[name='notes']" ).val(request.notes)
    $(`#office-stationery-function-detail-field`).val(request.category_function_id).change()
    OFFICE_STATIONERY = data
    renderRequestOfficeStationeryOption("detail", true)
  
    let color = getColorBadge(request.status)
    let badge = `<span class="badge badge-pill badge-${color}">${request.status}</span>`
    $(`#office-stationery-status-detail-field`).html(badge)

    renderFormButton(request, "office-stationery")
}