let OFFICE_HOUSEHOLD = [] 
let CATEGORY_OFFICE_HOUSEHOLD = []

$(document).ready(function(){
    getCategoryOfficeHousehold()
    $('#office-household-create-toggle').click(function(){
        $(`#office-household-create-form`)[0].reset();
        OFFICE_HOUSEHOLD = []
        renderRequestOfficeHouseholdOption()
    })

    $('#office-household-add-button').click(function(){
        OFFICE_HOUSEHOLD.push({category_office_household_id: null, qty: null})
        console.log("TEST")
        renderRequestOfficeHouseholdOption()
    })

    $("body").delegate("#office-household-reject-form", "submit", function(e) {
        startLoadingButton('#office-household-reject-button')
        let $form = $(this)
        let data = {
            reject_reason: $form.find( "textarea[name='reject_reason']" ).val(),
        }

        $.ajax({
            async: true,
            url: `${REQUEST_API_URL}/reject/${REQUEST_ID}`,
            type: 'PUT',
            data: JSON.stringify(data),
            beforeSend: function (xhr) {
                xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
            },
            error: function(res) {
                const response = JSON.parse(res.responseText)
                let isRetry = retryRequest(response)
                if(isRetry) $.ajax(this)
                else endLoadingButton('#office-household-reject-button', 'Iya')
            },
            success: function(res) {
                endLoadingButton('#office-household-reject-button', 'Iya')
                showSuccess(res.message)
                $('#office-household-reject-modal').modal('toggle')
                $('#office-household-detail-modal').modal('toggle')
                request_table.ajax.reload()
            }
        });
    })

    $("body").delegate("#office-household-approve-button", "click", function(e) {
        startLoadingButton('#office-household-approve-button')
    
        $.ajax({
            async: true,
            url: `${REQUEST_API_URL}/confirm/${REQUEST_ID}`,
            type: 'PUT',
            beforeSend: function (xhr) {
                xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
            },
            error: function(res) {
                const response = JSON.parse(res.responseText)
                let isRetry = retryRequest(response)
                if(isRetry) $.ajax(this)
                else endLoadingButton('#office-household-approve-button', 'Iya')
            },
            success: function(res) {
                endLoadingButton('#office-household-approve-button', 'Iya')
                showSuccess(res.message)
                $('#office-household-approve-modal').modal('toggle')
                $('#office-household-detail-modal').modal('toggle')
                request_table.ajax.reload()
            }
        });
    })

    $("body").delegate("#office-household-complete-button", "click", function(e) {
        startLoadingButton('#office-household-complete-button')
    
        $.ajax({
            async: true,
            url: `${REQUEST_API_URL}/complete/${REQUEST_ID}`,
            type: 'PUT',
            beforeSend: function (xhr) {
                xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
            },
            error: function(res) {
                const response = JSON.parse(res.responseText)
                let isRetry = retryRequest(response)
                if(isRetry) $.ajax(this)
                else endLoadingButton('#office-household-complete-button', 'Iya')
            },
            success: function(res) {
                endLoadingButton('#office-household-complete-button', 'Iya')
                showSuccess(res.message)
                $('#office-household-complete-modal').modal('toggle')
                $('#office-household-detail-modal').modal('toggle')
                request_table.ajax.reload()
            }
        });
    })

    $("body").delegate("#office-household-accomplish-button", "click", function(e) {
        startLoadingButton('#office-household-accomplish-button')
    
        $.ajax({
            async: true,
            url: `${REQUEST_API_URL}/accomplished/${REQUEST_ID}`,
            type: 'PUT',
            beforeSend: function (xhr) {
                xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
            },
            error: function(res) {
                const response = JSON.parse(res.responseText)
                let isRetry = retryRequest(response)
                if(isRetry) $.ajax(this)
                else endLoadingButton('#office-household-accomplish-button', 'Iya')
            },
            success: function(res) {
                endLoadingButton('#office-household-accomplish-button', 'Iya')
                showSuccess(res.message)
                $('#office-household-accomplish-modal').modal('toggle')
                $('#office-household-detail-modal').modal('toggle')
                request_table.ajax.reload()
            }
        });
    })

    $("body").delegate(".office-household-delete-button", "click", function(e) {
        let id = $(this).data("id")
        OFFICE_HOUSEHOLD.splice(id, 1)
        renderRequestOfficeHouseholdOption()
    })

    $("body").delegate(".office-household-category", "change", function(e) {
        let id = $(this).data("id")
        OFFICE_HOUSEHOLD[id].category_office_household_id = $(this).val()
    })

    $("body").delegate(".office-household-qty", "change", function(e) {
        let id = $(this).data("id")
        OFFICE_HOUSEHOLD[id].qty = $(this).val()
    })

    $('#office-household-create-form').submit(function(e){
        e.preventDefault()

        startLoadingButton('#office-household-create-button')
    
        let $form = $(this)
        let data = {
            category_function_id: $("#office-household-function-create-field").find(":selected").val(), 
            items: OFFICE_HOUSEHOLD,
            notes: $form.find( "textarea[name='notes']" ).val(),
        }
        $.ajax({
            async: true,
            url: `${REQUEST_API_URL}/office-household`,
            type: 'POST',
            beforeSend: function (xhr) {
                xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
            },
            data: JSON.stringify(data),
            error: function(res) {
                const response  = JSON.parse(res.responseText)
                let isRetry     = retryRequest(response)
                if(isRetry) $.ajax(this)
                else endLoadingButton('#office-household-create-button', 'Submit')
            },
            success: function(res) {
                endLoadingButton('#office-household-create-button', 'Submit')
                showSuccess(res.message)
                $('#office-household-create-modal').modal('hide')
            }
        });
    })

    $("body").delegate(".request-detail-office-household-toggle", "click", function(e) {
        REQUEST_ID = $(this).data("id")
        console.log(REQUEST_ID)

        $.ajax({
            async: true,
            url: `${REQUEST_API_URL}/id/${REQUEST_ID}`,
            type: 'GET',
            beforeSend: function (xhr) {
              xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
            },
            error: function(res) {
              const response = JSON.parse(res.responseText)
              let isRetry = retryRequest(response)
              if(isRetry) $.ajax(this)
            },
            success: function(res) {
              const response = res.data
              renderRequestOfficeHouseholdForm(response)
            }
        });
    })
})

function getCategoryOfficeHousehold(){
    $.ajax({
        async: true,
        url: `${CATEGORY_API_URL}?type=office_household`,
        type: 'GET',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
        },
        success: function(res) {
          const response = res.data
          CATEGORY_OFFICE_HOUSEHOLD = response
        }
    });
}

function renderRequestOfficeHouseholdOption(type = "create", isReadOnly = false){
    let readonly = isReadOnly ? "readonly" : ''
    let office_household_html = ''
    OFFICE_HOUSEHOLD.forEach((item, id) => {
        let option_html = `<option>--Pilih Rumah Tangga Kantor--</option>`
        CATEGORY_OFFICE_HOUSEHOLD.forEach(category => {
            option_html += `<option value="${category.id}" ${category.id == item.category_office_household_id ? 'selected' : ''}>${category.name}</option>`
        })
        office_household_html += `<div class="col-6 mt-2">
            <select class="form-control office-household-category" data-id="${id}" ${readonly}>
                ${option_html}
            </select>
        </div>
        <div class="col-5 mt-2">
            <input type="number" class="form-control office-household-qty" data-id="${id}" placeholder="Jumlah.." value="${item.qty}" ${readonly}>
        </div>`
        if(!isReadOnly){
            office_household_html += `<div class="col-1 mt-2">
                <button type="button" class="btn btn-danger office-household-delete-button" data-id="${id}">
                    <i class="fas fa-trash"></i>
                </button>
            </div>`
        }
    })

    $(`#office-household-${type}-section`).html(office_household_html)
}

function renderRequestOfficeHouseholdForm(request){
    let data = request.data
    console.log("Rendering..", data)
    let $form = $('#office-household-detail-form')
    $form.find( "textarea[name='notes']" ).val(request.notes)
    $(`#office-household-function-detail-field`).val(request.category_function_id).change()
    OFFICE_HOUSEHOLD = data
    renderRequestOfficeHouseholdOption("detail", true)
  
    let color = getColorBadge(request.status)
    let badge = `<span class="badge badge-pill badge-${color}">${request.status}</span>`
    $(`#office-household-status-detail-field`).html(badge)

    renderFormButton(request, "office-household")
}