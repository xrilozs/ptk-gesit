$(document).ready(function(){
  getDriverTotal()
  getBuildingTotal()
  getRoomTotal()
  getRequestTotal()
});

function getDriverTotal(){
  $.ajax({
    async: true,
    url: `${DRIVER_API_URL}/total`,
    type: 'GET',
    beforeSend: function (xhr) {
      xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
    },
    error: function(res) {
      const response = JSON.parse(res.responseText)
      showError(response.message)
      let isRetry = retryRequest(response)
      if(isRetry) $.ajax(this)
    },
    success: function(res) {
      const response = res.data
      $('#dashboard-driver-total').html(response)
    }
  });
}

function getBuildingTotal(){
  $.ajax({
    async: true,
    url: `${BUILDING_API_URL}/total`,
    type: 'GET',
    beforeSend: function (xhr) {
      xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
    },
    error: function(res) {
      const response = JSON.parse(res.responseText)
      showError(response.message)
      let isRetry = retryRequest(response)
      if(isRetry) $.ajax(this)
    },
    success: function(res) {
      const response = res.data
      $('#dashboard-building-total').html(response)
    }
  });
}

function getRoomTotal(){
  $.ajax({
    async: true,
    url: `${MEETING_ROOM_API_URL}/total`,
    type: 'GET',
    beforeSend: function (xhr) {
      xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
    },
    error: function(res) {
      const response = JSON.parse(res.responseText)
      showError(response.message)
      let isRetry = retryRequest(response)
      if(isRetry) $.ajax(this)
    },
    success: function(res) {
      const response = res.data
      $('#dashboard-room-total').html(response)
    }
  });
}

function getRequestTotal(){
  $.ajax({
      async: true,
      url: `${REQUEST_API_URL}/total`,
      type: 'GET',
      beforeSend: function (xhr) {
        xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
      },
      error: function(res) {
        const response = JSON.parse(res.responseText)
        let isRetry = retryRequest(response)
        if(isRetry) $.ajax(this)
      },
      success: function(res) {
        const response = res.data
        $('#dashboard-request-total').html(response)
      }
  });
}