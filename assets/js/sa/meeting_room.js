let MEETING_ROOM_ID

$(document).ready(function(){
  getBuilding()

  //render datatable
  let meeting_room_table = $('#meeting-room-datatable').DataTable( {
      processing: true,
      serverSide: true,
      searching: true,
      ajax: {
        async: true,
        url: MEETING_ROOM_API_URL,
        type: "GET",
        dataType: "json",
        crossDomain: true,
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: function ( d ) {
          let newObj          = {}
          let start           = d.start
          let size            = d.length
          newObj.page_number  = d.start > 0 ? (start/size) : 0;
          newObj.page_size    = size
          newObj.search       = d.search.value
          newObj.draw         = d.draw
          d                   = newObj
          console.log("D itu:", d)
          return d
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
        }
      },
      columns: [
          { 
            data: "building_name",
            orderable: false
          },
          { 
            data: "name",
            orderable: false
          },
          { 
            data: "description",
            orderable: false
          },
          { 
            data: "floor_number",
            orderable: false
          },
          { 
            data: "capacity",
            orderable: false
          },
          {
            data: "status",
            className: "dt-body-right",
            render: function (data, type, row, meta) {
              let color = data == 'AVAILABLE' ? 'success' : 'danger'
              let text = data == 'AVAILABLE' ? 'TERSEDIA' : 'DIPESAN'
              let badge = `<span class="badge badge-pill badge-${color}">${text}</span>`

              return badge
            }
          },
          {
            data: "created_at",
            orderable: false
          },
          {
            data: "id",
            className: "dt-body-right",
            render: function (data, type, row, meta) {
                let button = `<button class="btn btn-sm btn-success meeting-room-update-toggle" data-id="${data}" data-toggle="modal" data-target="#meeting-room-update-modal" title="update">
                  <i class="fas fa-edit"></i>
                </button> <button class="btn btn-sm btn-danger meeting-room-delete-toggle" data-id="${data}" data-toggle="modal" data-target="#meeting-room-delete-modal" title="hapus">
                  <i class="fas fa-trash"></i>
                </button>`
                if(row.status == 'AVAILABLE'){
                  button += ` <button class="btn btn-sm btn-warning meeting-room-book-toggle" data-id="${data}" data-toggle="modal" data-target="#meeting-room-book-modal" title="set bekerja">
                    <i class="fas fa-car-alt"></i>
                  </button>`
                }else{
                  button += ` <button class="btn btn-sm btn-primary meeting-room-free-toggle" data-id="${data}" data-toggle="modal" data-target="#meeting-room-free-modal" title="set tersedia">
                    <i class="fas fa-check"></i>
                  </button>`
                }
                return button
            },
            orderable: false
          }
      ]
  });

  //button action click
  $("#meeting-room-create-toggle").click(function(e) {
    clearForm('create')
  })

  $("body").delegate(".meeting-room-delete-toggle", "click", function(e) {
    MEETING_ROOM_ID = $(this).data('id')
  })

  $("body").delegate(".meeting-room-book-toggle", "click", function(e) {
    MEETING_ROOM_ID = $(this).data('id')
  })

  $("body").delegate(".meeting-room-free-toggle", "click", function(e) {
    MEETING_ROOM_ID = $(this).data('id')
  })
  
  $("body").delegate(".meeting-room-update-toggle", "click", function(e) {
    MEETING_ROOM_ID = $(this).data('id')
    clearForm('update')

    $('#meeting-room-update-overlay').show()
    $.ajax({
        async: true,
        url: `${MEETING_ROOM_API_URL}/id/${MEETING_ROOM_ID}`,
        type: 'GET',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else $('#meeting-room-update-modal').modal('toggle')
        },
        success: function(res) {
          const response = res.data
          $('#meeting-room-update-overlay').hide()
          renderForm(response, 'update')
        }
    });
  })

  //submit form
  $('#meeting-room-create-form').submit(function (e){
    e.preventDefault();
    startLoadingButton('#meeting-room-create-button')
    
    let $form = $(this)
    let data = {
      name: $form.find( "input[name='name']" ).val(),
      description: $form.find( "textarea[name='description']" ).val(),
      capacity: $form.find( "input[name='capacity']" ).val(),
      floor_number: $form.find( "input[name='floor_number']" ).val(),
      building_id: $("#meeting-room-building-update-field").find(":selected").val()
    }

    $.ajax({
        async: true,
        url: `${MEETING_ROOM_API_URL}`,
        type: 'POST',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: JSON.stringify(data),
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#meeting-room-create-button', 'Submit')
        },
        success: function(res) {
          endLoadingButton('#meeting-room-create-button', 'Submit')
          showSuccess(res.message)
          $('#meeting-room-create-modal').modal('hide')
          meeting_room_table.ajax.reload()
        }
    });
  })
  
  $('#meeting-room-update-form').submit(function (e){
    e.preventDefault()
    startLoadingButton('#meeting-room-update-button')

    let $form = $(this)
    let data = {
      id: MEETING_ROOM_ID,
      name: $form.find( "input[name='name']" ).val(),
      description: $form.find( "textarea[name='description']" ).val(),
      capacity: $form.find( "input[name='capacity']" ).val(),
      floor_number: $form.find( "input[name='floor_number']" ).val(),
      building_id: $("#meeting-room-building-update-field").find(":selected").val()
    }

    $.ajax({
        async: true,
        url: `${MEETING_ROOM_API_URL}`,
        type: 'PUT',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: JSON.stringify(data),
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#meeting-room-update-button', 'Submit')
        },
        success: function(res) {
          endLoadingButton('#meeting-room-update-button', 'Submit')
          showSuccess(res.message)
          $('#meeting-room-update-modal').modal('toggle')
          meeting_room_table.ajax.reload()
        }
    });
  })
  
  $('#meeting-room-delete-button').click(function (){
    startLoadingButton('#meeting-room-delete-button')
    
    $.ajax({
        async: true,
        url: `${MEETING_ROOM_API_URL}/delete/${MEETING_ROOM_ID}`,
        type: 'DELETE',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#meeting-room-delete-button', 'Iya')
        },
        success: function(res) {
          endLoadingButton('#meeting-room-delete-button', 'Iya')
          showSuccess(res.message)
          $('#meeting-room-delete-modal').modal('toggle')
          meeting_room_table.ajax.reload()
        }
    });
  })

  $('#meeting-room-free-button').click(function (){
    startLoadingButton('#meeting-room-free-button')
    
    $.ajax({
        async: true,
        url: `${MEETING_ROOM_API_URL}/free/${MEETING_ROOM_ID}`,
        type: 'PUT',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#meeting-room-free-button', 'Iya')
        },
        success: function(res) {
          endLoadingButton('#meeting-room-free-button', 'Iya')
          showSuccess(res.message)
          $('#meeting-room-free-modal').modal('toggle')
          meeting_room_table.ajax.reload()
        }
    });
  })

  $('#meeting-room-book-button').click(function (){
    startLoadingButton('#meeting-room-book-button')
    
    $.ajax({
        async: true,
        url: `${MEETING_ROOM_API_URL}/book/${MEETING_ROOM_ID}`,
        type: 'PUT',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#meeting-room-book-button', 'Iya')
        },
        success: function(res) {
          endLoadingButton('#meeting-room-book-button', 'Iya')
          showSuccess(res.message)
          $('#meeting-room-book-modal').modal('toggle')
          meeting_room_table.ajax.reload()
        }
    });
  })
})

function renderForm(data, type){
  let $form = $(`#meeting-room-${type}-form`)
  $form.find( "input[name='name']" ).val(data.name)
  $form.find( "textarea[name='description']" ).val(data.description),
  $form.find( "input[name='capacity']" ).val(data.capacity),
  $form.find( "input[name='floor_number']" ).val(data.floor_number),
  $(`#meeting-room-building-${type}-field`).val(data.building_id).change()

  let color = data.status == 'AVAILABLE' ? 'success' : 'danger'
  let text  = data.status == 'AVAILABLE' ? 'TERSEDIA' : 'BEKERJA'
  let badge = `<span class="badge badge-pill badge-${color}">${text}</span>`
  $(`#meeting-room-status-${type}-field`).html(badge)
}

function clearForm(type){
  let $form = $(`#meeting-room-${type}-form`)
  $form.find( "input[name='name']" ).val("")
  $form.find( "textarea[name='description']" ).val("")
  $form.find( "input[name='capacity']" ).val("")
  $form.find( "input[name='floor_number']" ).val("")
}

function getBuilding(){
  $.ajax({
      async: true,
      url: `${BUILDING_API_URL}?status=AVAILABLE`,
      type: 'GET',
      beforeSend: function (xhr) {
        xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
      },
      error: function(res) {
        const response = JSON.parse(res.responseText)
      },
      success: function(res) {
        let building = res.data
        console.log(building)
        renderBuildingOption(building)
      }
  });
}

function renderBuildingOption(data){
  let building_html = ""
  data.forEach(item => {
    let option_html = `<option value="${item.id}">${item.name}</option>`
    building_html += option_html
  });
  $(`.building-option`).html(building_html)
}