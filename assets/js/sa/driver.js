let DRIVER_ID

$(document).ready(function(){
  //render datatable
  let driver_table = $('#driver-datatable').DataTable( {
      processing: true,
      serverSide: true,
      searching: true,
      ajax: {
        async: true,
        url: DRIVER_API_URL,
        type: "GET",
        dataType: "json",
        crossDomain: true,
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: function ( d ) {
          let newObj          = {}
          let start           = d.start
          let size            = d.length
          newObj.page_number  = d.start > 0 ? (start/size) : 0;
          newObj.page_size    = size
          newObj.search       = d.search.value
          newObj.draw         = d.draw
          d                   = newObj
          console.log("D itu:", d)
          return d
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
        }
      },
      columns: [
          { 
            data: "name",
            orderable: false
          },
          {
            data: "status",
            className: "dt-body-right",
            render: function (data, type, row, meta) {
              let color = data == 'AVAILABLE' ? 'success' : 'danger'
              let text = data == 'AVAILABLE' ? 'TERSEDIA' : 'BEKERJA'
              let badge = `<span class="badge badge-pill badge-${color}">${text}</span>`

              return badge
            }
          },
          {
            data: "created_at",
            orderable: false
          },
          {
            data: "id",
            className: "dt-body-right",
            render: function (data, type, row, meta) {
                let button = `<button class="btn btn-sm btn-success driver-update-toggle" data-id="${data}" data-toggle="modal" data-target="#driver-update-modal" title="update">
                  <i class="fas fa-edit"></i>
                </button> <button class="btn btn-sm btn-danger driver-delete-toggle" data-id="${data}" data-toggle="modal" data-target="#driver-delete-modal" title="hapus">
                  <i class="fas fa-trash"></i>
                </button>`
                if(row.status == 'AVAILABLE'){
                  button += ` <button class="btn btn-sm btn-warning driver-book-toggle" data-id="${data}" data-toggle="modal" data-target="#driver-book-modal" title="set bekerja">
                    <i class="fas fa-car-alt"></i>
                  </button>`
                }else{
                  button += ` <button class="btn btn-sm btn-primary drive-free-toggle" data-id="${data}" data-toggle="modal" data-target="#driver-free-modal" title="set tersedia">
                    <i class="fas fa-check"></i>
                  </button>`
                }
                return button
            },
            orderable: false
          }
      ]
  });

  //button action click
  $("#driver-create-toggle").click(function(e) {
    clearForm('create')
  })

  $("body").delegate(".driver-delete-toggle", "click", function(e) {
    DRIVER_ID = $(this).data('id')
  })

  $("body").delegate(".driver-book-toggle", "click", function(e) {
    DRIVER_ID = $(this).data('id')
  })

  $("body").delegate(".driver-free-toggle", "click", function(e) {
    DRIVER_ID = $(this).data('id')
  })
  
  $("body").delegate(".driver-update-toggle", "click", function(e) {
    DRIVER_ID = $(this).data('id')
    clearForm('update')

    $('#driver-update-overlay').show()
    $.ajax({
        async: true,
        url: `${DRIVER_API_URL}/id/${DRIVER_ID}`,
        type: 'GET',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else $('#driver-update-modal').modal('toggle')
        },
        success: function(res) {
          const response = res.data
          $('#driver-update-overlay').hide()
          renderForm(response, 'update')
        }
    });
  })

  //submit form
  $('#driver-create-form').submit(function (e){
    e.preventDefault();
    startLoadingButton('#driver-create-button')
    
    let $form = $(this)
    let data = {
      name:  $form.find( "input[name='name']" ).val()
    }
    $.ajax({
        async: true,
        url: `${DRIVER_API_URL}`,
        type: 'POST',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: JSON.stringify(data),
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#driver-create-button', 'Submit')
        },
        success: function(res) {
          endLoadingButton('#driver-create-button', 'Submit')
          showSuccess(res.message)
          $('#driver-create-modal').modal('hide')
          driver_table.ajax.reload()
        }
    });
  })
  
  $('#driver-update-form').submit(function (e){
    e.preventDefault()
    startLoadingButton('#driver-update-button')

    let $form = $(this)
    let data = {
      id: DRIVER_ID,
      name:  $form.find( "input[name='name']" ).val()
    }

    $.ajax({
        async: true,
        url: `${DRIVER_API_URL}`,
        type: 'PUT',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: JSON.stringify(data),
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#driver-update-button', 'Submit')
        },
        success: function(res) {
          endLoadingButton('#driver-update-button', 'Submit')
          showSuccess(res.message)
          $('#driver-update-modal').modal('toggle')
          driver_table.ajax.reload()
        }
    });
  })
  
  $('#driver-delete-button').click(function (){
    startLoadingButton('#driver-delete-button')
    
    $.ajax({
        async: true,
        url: `${DRIVER_API_URL}/delete/${DRIVER_ID}`,
        type: 'DELETE',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#driver-delete-button', 'Iya')
        },
        success: function(res) {
          endLoadingButton('#driver-delete-button', 'Iya')
          showSuccess(res.message)
          $('#driver-delete-modal').modal('toggle')
          driver_table.ajax.reload()
        }
    });
  })

  $('#driver-free-button').click(function (){
    startLoadingButton('#driver-free-button')
    
    $.ajax({
        async: true,
        url: `${DRIVER_API_URL}/free/${DRIVER_ID}`,
        type: 'PUT',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#driver-free-button', 'Iya')
        },
        success: function(res) {
          endLoadingButton('#driver-free-button', 'Iya')
          showSuccess(res.message)
          $('#driver-free-modal').modal('toggle')
          driver_table.ajax.reload()
        }
    });
  })

  $('#driver-book-button').click(function (){
    startLoadingButton('#driver-book-button')
    
    $.ajax({
        async: true,
        url: `${DRIVER_API_URL}/book/${DRIVER_ID}`,
        type: 'PUT',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#driver-book-button', 'Iya')
        },
        success: function(res) {
          endLoadingButton('#driver-book-button', 'Iya')
          showSuccess(res.message)
          $('#driver-book-modal').modal('toggle')
          driver_table.ajax.reload()
        }
    });
  })
})

function renderForm(data, type){
  let $form = $(`#driver-${type}-form`)
  $form.find( "input[name='name']" ).val(data.name)

  let color = data.status == 'AVAILABLE' ? 'success' : 'danger'
  let text  = data.status == 'AVAILABLE' ? 'TERSEDIA' : 'BEKERJA'
  let badge = `<span class="badge badge-pill badge-${color}">${text}</span>`
  $(`#driver-status-${type}-field`).html(badge)
}

function clearForm(type){
  let $form = $(`#driver-${type}-form`)
  $form.find( "input[name='name']" ).val("")
}
