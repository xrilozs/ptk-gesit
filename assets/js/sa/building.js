let BUILDING_ID

$(document).ready(function(){
  //render datatable
  let building_table = $('#building-datatable').DataTable( {
      processing: true,
      serverSide: true,
      searching: true,
      ajax: {
        async: true,
        url: BUILDING_API_URL,
        type: "GET",
        dataType: "json",
        crossDomain: true,
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: function ( d ) {
          let newObj          = {}
          let start           = d.start
          let size            = d.length
          newObj.page_number  = d.start > 0 ? (start/size) : 0;
          newObj.page_size    = size
          newObj.search       = d.search.value
          newObj.draw         = d.draw
          d                   = newObj
          console.log("D itu:", d)
          return d
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
        }
      },
      columns: [
          { 
            data: "name",
            orderable: false
          },
          {
            data: "created_at",
            orderable: false
          },
          {
            data: "id",
            className: "dt-body-right",
            render: function (data, type, row, meta) {
                let button = `<button class="btn btn-sm btn-success building-update-toggle" data-id="${data}" data-toggle="modal" data-target="#building-update-modal" title="update">
                  <i class="fas fa-edit"></i>
                </button> <button class="btn btn-sm btn-danger building-delete-toggle" data-id="${data}" data-toggle="modal" data-target="#building-delete-modal" title="hapus">
                  <i class="fas fa-trash"></i>
                </button>`
                return button
            },
            orderable: false
          }
      ]
  });

  //button action click
  $("#building-create-toggle").click(function(e) {
    clearForm('create')
  })

  $("body").delegate(".building-delete-toggle", "click", function(e) {
    BUILDING_ID = $(this).data('id')
  })
  
  $("body").delegate(".building-update-toggle", "click", function(e) {
    BUILDING_ID = $(this).data('id')
    clearForm('update')

    $('#building-update-overlay').show()
    $.ajax({
        async: true,
        url: `${BUILDING_API_URL}/id/${BUILDING_ID}`,
        type: 'GET',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else $('#building-update-modal').modal('toggle')
        },
        success: function(res) {
          const response = res.data
          $('#building-update-overlay').hide()
          renderForm(response, 'update')
        }
    });
  })

  //submit form
  $('#building-create-form').submit(function (e){
    e.preventDefault();
    startLoadingButton('#building-create-button')
    
    let $form = $(this)
    let data = {
      name:  $form.find( "input[name='name']" ).val()
    }
    $.ajax({
        async: true,
        url: `${BUILDING_API_URL}`,
        type: 'POST',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: JSON.stringify(data),
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#building-create-button', 'Submit')
        },
        success: function(res) {
          endLoadingButton('#building-create-button', 'Submit')
          showSuccess(res.message)
          $('#building-create-modal').modal('hide')
          building_table.ajax.reload()
        }
    });
  })
  
  $('#building-update-form').submit(function (e){
    e.preventDefault()
    startLoadingButton('#building-update-button')

    let $form = $(this)
    let data = {
      id: BUILDING_ID,
      name:  $form.find( "input[name='name']" ).val()
    }

    $.ajax({
        async: true,
        url: `${BUILDING_API_URL}`,
        type: 'PUT',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: JSON.stringify(data),
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#building-update-button', 'Submit')
        },
        success: function(res) {
          endLoadingButton('#building-update-button', 'Submit')
          showSuccess(res.message)
          $('#building-update-modal').modal('toggle')
          building_table.ajax.reload()
        }
    });
  })
  
  $('#building-delete-button').click(function (){
    startLoadingButton('#building-delete-button')
    
    $.ajax({
        async: true,
        url: `${BUILDING_API_URL}/delete/${BUILDING_ID}`,
        type: 'DELETE',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#building-delete-button', 'Yes')
        },
        success: function(res) {
          endLoadingButton('#building-delete-button', 'Yes')
          showSuccess(res.message)
          $('#building-delete-modal').modal('toggle')
          building_table.ajax.reload()
        }
    });
  })
})

function renderForm(data, type){
  let $form = $(`#building-${type}-form`)
  $form.find( "input[name='name']" ).val(data.name)
}

function clearForm(type){
  let $form = $(`#building-${type}-form`)
  $form.find( "input[name='name']" ).val("")
}
