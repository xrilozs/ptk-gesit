let CATEGORY_ID

$(document).ready(function(){
  //render datatable
  let category_table = $('#category-datatable').DataTable( {
      processing: true,
      serverSide: true,
      searching: true,
      ajax: {
        async: true,
        url: CATEGORY_API_URL,
        type: "GET",
        dataType: "json",
        crossDomain: true,
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: function ( d ) {
          let newObj          = {}
          let start           = d.start
          let size            = d.length
          newObj.page_number  = d.start > 0 ? (start/size) : 0;
          newObj.page_size    = size
          newObj.search       = d.search.value
          newObj.draw         = d.draw
          newObj.type         = TYPE
          d                   = newObj
          console.log("D itu:", d)
          return d
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
        }
      },
      columns: [
          { 
            data: "name",
            orderable: false
          },
          {
            data: "created_at",
            orderable: false
          },
          {
            data: "id",
            className: "dt-body-right",
            render: function (data, type, row, meta) {
                return `<button class="btn btn-sm btn-success category-update-toggle" data-id="${data}" data-toggle="modal" data-target="#category-update-modal" title="update">
                  <i class="fas fa-edit"></i>
                </button>
                <button class="btn btn-sm btn-danger category-delete-toggle" data-id="${data}" data-toggle="modal" data-target="#category-delete-modal" title="hapus">
                  <i class="fas fa-trash"></i>
                </button>`
            },
            orderable: false
          }
      ]
  });

  //button action click
  $("#category-create-toggle").click(function(e) {
    clearForm('create')
  })

  $("body").delegate(".category-delete-toggle", "click", function(e) {
    CATEGORY_ID = $(this).data('id')
  })
  
  $("body").delegate(".category-update-toggle", "click", function(e) {
    CATEGORY_ID = $(this).data('id')
    clearForm('update')

    $('#category-update-overlay').show()
    $.ajax({
        async: true,
        url: `${CATEGORY_API_URL}/id/${CATEGORY_ID}?type=${TYPE}`,
        type: 'GET',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else $('#category-update-modal').modal('toggle')
        },
        success: function(res) {
          const response = res.data
          $('#category-update-overlay').hide()
          renderForm(response, 'update')
        }
    });
  })

  //submit form
  $('#category-create-form').submit(function (e){
    e.preventDefault();
    startLoadingButton('#category-create-button')
    
    let $form = $(this)
    let data = {
      name:  $form.find( "input[name='name']" ).val()
    }
    $.ajax({
        async: true,
        url: `${CATEGORY_API_URL}?type=${TYPE}`,
        type: 'POST',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: JSON.stringify(data),
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#category-create-button', 'Submit')
        },
        success: function(res) {
          endLoadingButton('#category-create-button', 'Submit')
          showSuccess(res.message)
          $('#category-create-modal').modal('hide')
          category_table.ajax.reload()
        }
    });
  })
  
  $('#category-update-form').submit(function (e){
    e.preventDefault()
    startLoadingButton('#category-update-button')

    let $form = $(this)
    let data = {
      id: CATEGORY_ID,
      name:  $form.find( "input[name='name']" ).val()
    }

    $.ajax({
        async: true,
        url: `${CATEGORY_API_URL}?type=${TYPE}`,
        type: 'PUT',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: JSON.stringify(data),
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#category-update-button', 'Submit')
        },
        success: function(res) {
          endLoadingButton('#category-update-button', 'Submit')
          showSuccess(res.message)
          $('#category-update-modal').modal('toggle')
          category_table.ajax.reload()
        }
    });
  })
  
  $('#category-delete-button').click(function (){
    startLoadingButton('#category-delete-button')
    
    $.ajax({
        async: true,
        url: `${CATEGORY_API_URL}/delete/${CATEGORY_ID}?type=${TYPE}`,
        type: 'DELETE',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#category-delete-button', 'Iya')
        },
        success: function(res) {
          endLoadingButton('#category-delete-button', 'Iya')
          showSuccess(res.message)
          $('#category-delete-modal').modal('toggle')
          category_table.ajax.reload()
        }
    });
  })
})

function renderForm(data, type){
  let $form = $(`#category-${type}-form`)
  $form.find( "input[name='name']" ).val(data.name)
}

function clearForm(type){
  let $form = $(`#category-${type}-form`)
  $form.find( "input[name='name']" ).val("")
}
