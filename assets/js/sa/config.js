let CONFIG_ID;
let APPS_LOGO_URL;
let APPS_ICON_URL;

$(document).ready(function(){
  $('#config-apps-logo').change(function(e) {
    let file = e.target.files[0];
    upload_image(file)
  });

  $('#config-apps-icon').change(function(e) {
    let file = e.target.files[0];
    upload_image(file, true)
  });
  
  $('#config-form').submit(function (e){
    e.preventDefault()
    startLoadingButton("#config-button")

    let request = {
          id: CONFIG_ID,
          apps_logo: APPS_LOGO_URL,
          apps_icon: APPS_ICON_URL,
        }

    $.ajax({
        async: true,
        url: `${CONFIG_API_URL}`,
        type: 'POST',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: JSON.stringify(request),
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#config-button', 'Submit')
        },
        success: function(res) {
          endLoadingButton('#config-button', 'Submit')
          showSuccess(res.message)
          getConfig()
        }
    });
  })
})

function renderConfigForm(data){
  CONFIG_ID     = data.id
  APPS_LOGO_URL = data.apps_logo
  APPS_ICON_URL = data.apps_icon

  if(APPS_LOGO_URL){
    $('#config-apps-logo').attr("data-default-file", `${WEB_URL}/${APPS_LOGO_URL}`);
  }
  $('#config-apps-logo').dropify()

  if(APPS_ICON_URL){
    $('#config-apps-icon').attr("data-default-file", `${WEB_URL}/${APPS_ICON_URL}`);
  }
  $('#config-apps-icon').dropify()
}

//upload image
function upload_image(file, is_icon=false){
  $(`#config-button`).attr("disabled", true)

  let formData = new FormData();
  formData.append('image', file);
  
  $.ajax({
      async: true,
      url: `${CONFIG_API_URL}/upload`,
      type: 'POST',
      beforeSend: function (xhr) {
        xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
      },
      data: formData,
      processData: false, 
      contentType: false, 
      error: function(res) {
        const response = JSON.parse(res.responseText)
        let isRetry = retryRequest(response)
        if(isRetry) $.ajax(this)
        else{
          showError("Upload gambar", "Gagal melakukan upload gambar!")
          $(`#config-button`).attr("disabled", false)
        }
      },
      success: function(res) {
        const response = res.data
        if(is_icon){
          APPS_ICON_URL = response.file_url
        }else{
          APPS_LOGO_URL = response.file_url
        }

        $(`#config-button`).attr("disabled", false)
        showSuccess(res.message)
      }
  });
}

function renderConfigFormBlank(){
  $('#config-apps-logo').dropify()
  $('#config-apps-icon').dropify()
}