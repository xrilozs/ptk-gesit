$(document).ready(function(){
    getRequestTotal("OPERATIONAL VEHICLE")
    getRequestTotal("MEETING ROOM")
    getRequestTotal("MEETING CONSUMPTION")
    getRequestTotal("OFFICE STATIONERY")
    getRequestTotal("MAINTENANCE")
    getRequestTotal("ASSET")
    getRequestTotal("OTHER")
    getRequestTotal("CONSUMABLE")
    getRequestTotal("COURIER AND MAILING")
    getRequestTotal("EVENT SUPPORT")
    getRequestTotal("FUEL AND ECARD")
    getRequestTotal("OFFICE HOUSEHOLD")
    getRequestTotal("WORKSTATION AND FURNITURE")
})

function getRequestTotal(category){
    $.ajax({
        async: true,
        url: `${REQUEST_API_URL}/total?request_category=${category}`,
        type: 'GET',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
        },
        success: function(res) {
          const response = res.data
          renderTotalRequest(response, category)
        }
    });
}

function renderTotalRequest(total, category){
    let id = category.toLowerCase().replace(/ /g, "-")
    console.log(`#request-total-${id}`)
    $(`#request-total-${id}`).html(total)
}