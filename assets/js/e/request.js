let STATUS
let REQUEST_DATE
let REQUEST_ID
let request_table

$(document).ready(function(){
    getRequestTotal()
    getRequestTotal("REQUESTED")
    getRequestTotal("REJECTED")
    getRequestTotal("ON PROGRESS")
    getRequestTotal("COMPLETED")
    getRequestTotal("REQUEST ACCOMPLISHED")
    
    //render datatable
    request_table = $('#request-datatable').DataTable( {
        processing: true,
        serverSide: true,
        searching: true,
        ajax: {
        async: true,
        url: REQUEST_API_URL,
        type: "GET",
        dataType: "json",
        crossDomain: true,
        beforeSend: function (xhr) {
            xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: function ( d ) {
            let newObj          = {}
            let start           = d.start
            let size            = d.length
            newObj.page_number  = d.start > 0 ? (start/size) : 0;
            newObj.page_size    = size
            newObj.search       = d.search.value
            newObj.draw         = d.draw
            newObj.status       = STATUS
            newObj.request_date = REQUEST_DATE
            d                   = newObj
            console.log("D itu:", d)
            return d
        },
        error: function(res) {
            const response = JSON.parse(res.responseText)
            let isRetry = retryRequest(response)
            if(isRetry) $.ajax(this)
        }
        },
        columns: [
            { 
                data: "request_category",
                orderable: false
            },
            { 
                data: "requestor_name",
                orderable: false
            },
            { 
                data: "request_number",
                orderable: false
            },
            { 
                data: "status",
                orderable: false,
                render: function (data, type, row, meta) {
                    let color = 'secondary'
                    if(data == 'REJECTED'){
                      color = 'danger'
                    }else if(data == 'ON PROGRESS'){
                      color = 'warning'
                    }else if(data == 'COMPLETED'){
                      color = 'success'
                    }else if(data == 'REQUEST ACCOMPLISHED'){
                      color = 'primary'
                    }
                    return `<span class="badge badge-pill badge-${color}">${data}</span>`
                },
            },
            { 
                data: "reject_reason",
                orderable: false,
                render: function (data, type, row, meta) {
                    return data ? data : "-"
                }
            },
            {
                data: "created_at",
                orderable: false
            },
            {
                data: "id",
                className: "dt-body-right",
                render: function (data, type, row, meta) {
                    let alias = row.request_category.toLowerCase().replace(/ /g, "-")
                    console.log(`#${alias}-detail-modal`)

                    let icon     = "fas fa-search"
                    let btnColor = "light"
                    let btnTitle = "detail"
                    if(row.status == "REQUESTED"){
                        icon     = "fas fa-clock"
                        btnColor = "warning"
                        btnTitle = "Kerjakan"
                    }else if(row.status == "ON PROGRESS"){
                        icon     = "fas fa-check"
                        btnColor = "success"
                        btnTitle = "Selesai"
                    }

                    let button = `<button class="btn btn-sm btn-${btnColor} request-detail-${alias}-toggle" data-id="${data}" data-toggle="modal" data-target="#${alias}-detail-modal" title="${btnTitle}">
                        <i class="${icon}"></i></button>`
                    if(row.status == "REQUEST ACCOMPLISHED"){
                        button += `<a href="${WEB_URL}/${row.requisition_url}" class="btn btn-sm btn-dark" target="_blank" title="Download Requisition List">
                            <i class="fas fa-download"></i>
                        </a>`
                    }
                    return button
                },
                orderable: false
            }
        ]
    });

    
    $('#request-status-option').change(function(){
        STATUS = $(this).val()
        request_table.ajax.reload()
    })

    $('#request-date').change(function(){
        REQUEST_DATE = $(this).val()
        request_table.ajax.reload()
    })   
})

function getRequestTotal(status=""){
    $.ajax({
        async: true,
        url: `${REQUEST_API_URL}/total?status=${status}`,
        type: 'GET',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
        },
        success: function(res) {
          const response = res.data
          renderTotalRequest(response, status)
        }
    });
}

function renderTotalRequest(total, status){
    if(status == "REQUESTED"){
        $('#request-total-requested').html(total)
    }else if(status == "ON PROGRESS"){
        $('#request-total-onprogress').html(total)
    }else if(status == "REJECTED"){
        $('#request-total-rejected').html(total)
    }else if(status == "COMPLETED"){
        $('#request-total-completed').html(total)
    }else if(status == "REQUEST ACCOMPLISHED"){
        $('#request-total-requestaccomplished').html(total)
    }else{
        $('#request-total').html(total)
    }
}

function renderFormButton(request, modal){
    if(request.status == 'REQUESTED'){
        $(`.request-detail-footer-modal`).html(`<button class="btn btn-info request-on-progress-button" data-toggle="modal" data-target="#${modal}-approve-modal">Execute</button>
        <button class="btn btn-danger request-reject-button" data-toggle="modal" data-target="#${modal}-reject-modal">Reject</button>`)
    }else if(request.status == 'ON PROGRESS'){
        $(`.request-detail-footer-modal`).html(`<button class="btn btn-success request-completed-button" data-toggle="modal" data-target="#${modal}-complete-modal">Done</button>`)
    }else{
        $(`.request-detail-footer-modal`).html(``)
    }
}

function getColorBadge(data){
    let color = 'secondary'
    if(data == 'REJECTED'){
        color = 'danger'
    }else if(data == 'ON PROGRESS'){
        color = 'warning'
    }else if(data == 'COMPLETED'){
        color = 'success'
    }else if(data == 'REQUEST ACCOMPLISHED'){
        color = 'primary'
    }
    return color
}